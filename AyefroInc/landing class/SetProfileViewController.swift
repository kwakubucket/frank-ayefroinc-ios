//
//  SetProfileViewController.swift
//  AyefroInc
//
//  Created by Vijay Darkonde on 13/11/17.
//  Copyright © 2017 Inceptive Consulting Pvt. Ltd. All rights reserved.
//

import UIKit
import DLRadioButton
import Alamofire
import Branch

class SetProfileViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var checkbox: VKCheckbox!
    @IBOutlet var FirstNameTF: DTTextField!
    @IBOutlet var LastNameTF: DTTextField!
    @IBOutlet var EmailTF: DTTextField!
    @IBOutlet var phoneNumberTF: DTTextField!
    @IBOutlet var ProfilePicImgView: UIImageView!
    @IBOutlet var termLbl: UILabel!
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var scrollHeight: NSLayoutConstraint!
    
    var checkBoxValue: Bool! = false
    var roleValue: String! = ""
    
    var phoneNumber : String!
    var country : String!
    var base64Str : String! = ""
    
    let imagePicker = UIImagePickerController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        checkbox.checkboxValueChangedBlock = {
            isOn in
            print("Basic checkbox is \(isOn ? "ON" : "OFF")")
            self.checkBoxValue = isOn
        }
        
        phoneNumberTF.text = phoneNumber
        print(phoneNumber)
        imagePicker.delegate = self
        ProfilePicImgView.layer.cornerRadius = ProfilePicImgView.frame.size.height/2
        ProfilePicImgView.layer.masksToBounds = true
        self.navigationController?.isNavigationBarHidden = true
        
        termLbl.text = "By clicking on Register, you agree to the Terms and conditions,  and  Privacy Policy of Ayefro Inc."
        let text = (termLbl.text)!
        let underlineAttriString = NSMutableAttributedString(string: text)
        let range1 = (text as NSString).range(of: "Terms and conditions")
        underlineAttriString.addAttribute(NSAttributedStringKey.foregroundColor, value:  UIColor(red: 0.0/255.0, green: 122.0/255.0, blue: 255.0/255.0, alpha: 1.0), range: range1)
        let range2 = (text as NSString).range(of: "Privacy Policy")
        underlineAttriString.addAttribute(NSAttributedStringKey.foregroundColor, value:  UIColor(red: 0.0/255.0, green: 122.0/255.0, blue: 255.0/255.0, alpha: 1.0), range: range2)
        termLbl.attributedText = underlineAttriString
        termLbl.isUserInteractionEnabled = true
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapLabel))
        termLbl.addGestureRecognizer(tapGesture)
        
        if UIScreen.main.bounds.size.width == 414
        {
            
        }
        else if UIScreen.main.bounds.size.width == 375 && UIScreen.main.bounds.size.height == 667
        {
            
        }
        else if UIScreen.main.bounds.size.width == 320 && UIScreen.main.bounds.size.height == 568
        {
            scrollHeight.constant = 60
        }
        else if UIScreen.main.bounds.size.width == 320 && UIScreen.main.bounds.size.height == 480
        {
            scrollHeight.constant = 100
        }
        
        // Do any additional setup after loading the view.
    }
    
    @objc func tapLabel(gesture: UITapGestureRecognizer) {
        let text = (termLbl.text)!
        let termsRange = (text as NSString).range(of: "Terms and conditions")
        let privacyRange = (text as NSString).range(of: "Privacy Policy")
        
        let suffixRange = NSRange(location: termsRange.location, length: termsRange.length + 20)
        
        let preffixRange = NSRange(location: privacyRange.location + 10, length: privacyRange.length + 20)
        
        if gesture.didTapAttributedTextInLabel(label: termLbl, inRange: suffixRange) {
            let storyboard =  UIStoryboard(name: "Main", bundle: nil)
            let promo = storyboard.instantiateViewController(withIdentifier: "TermsAndConditionViewController") as! TermsAndConditionViewController
            self.navigationController?.pushViewController(promo, animated: true)
        } else if gesture.didTapAttributedTextInLabel(label: termLbl, inRange: preffixRange) {
            let storyboard =  UIStoryboard(name: "Main", bundle: nil)
            let promo = storyboard.instantiateViewController(withIdentifier: "PrivacyPolicyViewController") as! PrivacyPolicyViewController
            self.navigationController?.pushViewController(promo, animated: true)
        } else {
            print("Tapped none")
        }
    }
    
    
    
    @IBAction func logSelectedButton(radioButton : DLRadioButton) {
        if (radioButton.isMultipleSelectionEnabled) {
            for button in radioButton.selectedButtons() {
                print(String(format: "%@ is selected.\n", button.titleLabel!.text!));
                roleValue = button.titleLabel!.text!
            }
        } else {
            print(String(format: "%@ is selected.\n", radioButton.selected()!.titleLabel!.text!));
            roleValue = radioButton.selected()!.titleLabel!.text!
        }
    }
    
    @IBAction func setProfileBtnClk(_ sender: UIButton) {
        if (FirstNameTF.text?.isEmptyStr)! {
            let alert: UIAlertView = UIAlertView(title: "", message: "Please enter First Name", delegate: nil, cancelButtonTitle: "OK");
            alert.show()
            FirstNameTF.errorMessage = "Please enter First Name"
        }
        else if (LastNameTF.text?.isEmptyStr)!{
            let alert: UIAlertView = UIAlertView(title: "", message: "Please enter Last Name", delegate: nil, cancelButtonTitle: "OK");
            alert.show()
            LastNameTF.errorMessage = "Please enter Last Name"
        }
        else if (EmailTF.text?.isEmptyStr)!{
            let alert: UIAlertView = UIAlertView(title: "", message: "Please enter Email-Id", delegate: nil, cancelButtonTitle: "OK");
            alert.show()
            EmailTF.errorMessage = "Please enter Email-Id"
        }
        else if checkBoxValue == false{
            let alert: UIAlertView = UIAlertView(title: "", message: "Please accept Terms & Condition and Privacy Policy", delegate: nil, cancelButtonTitle: "OK");
            alert.show()
        }
        else if roleValue == ""{
            let alert: UIAlertView = UIAlertView(title: "", message: "Please select role", delegate: nil, cancelButtonTitle: "OK");
            alert.show()
        }
        else{
            self.view.showActivityView(withLabel: "Loading")
            DispatchQueue.main.async {
                let deviceID = UIDevice.current.identifierForVendor!.uuidString
                print(deviceID)
                var role : String!
                if self.roleValue == "I need a Service"{
                    role = "2"
                }
                else{
                    role = "3"
                }
                
                let parameters: [String: String] = [
                    "Email": self.EmailTF.text!,
                    "Password": "12345678",
                    "FirstName": self.FirstNameTF.text!,
                    "LastName": self.LastNameTF.text!,
                    "Image":self.base64Str,
                    "Role":role,
                    "Platform":"ios",
                    "DeviceUUID":deviceID,
                    "DeviceToken":deviceTokenStr,
                    "PhoneNumber":self.phoneNumber,
                    "Country":self.country,
                    ]
                
                let manager = Alamofire.SessionManager.default
                manager.session.configuration.timeoutIntervalForRequest = 120
                
                manager.request(baseURL + "api/AccountAPI/RegisterExternal", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
                    
                    switch(response.result) {
                    case .success(_):
                        if response.result.value != nil{
                            //print(response.result.value as Any)
                        }
                        
                        let jsonResults : NSDictionary
                        jsonResults = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! NSDictionary
                        print(jsonResults)
                        if (jsonResults["status"] as? String) != nil
                        {
                            let status = jsonResults["status"] as! String
                            let Message = jsonResults["Message"] as! String
                            let alert: UIAlertView = UIAlertView(title: status, message: Message, delegate: nil, cancelButtonTitle: "OK");
                            alert.show()
                            
                        }
                        else{
                            let Country = jsonResults["Country"] as! String
                            let Email = jsonResults["Email"] as! String
                            let FirstName = jsonResults["FirstName"] as! String
                            let userID = jsonResults["Id"] as! String
                            let LastName = jsonResults["LastName"] as! String
                            let Role = jsonResults["Role"] as! Int
                            var companyAddress = ""
                            if (jsonResults["companyAddress"] as? String) != nil
                            {
                                companyAddress = jsonResults["companyAddress"] as! String
                            }
                            var companyName = ""
                            if (jsonResults["companyAddress"] as? String) != nil
                            {
                                companyName = jsonResults["companyName"] as! String
                            }
                            var imageName = ""
                            if (jsonResults["image"] as? String) != nil
                            {
                                imageName = jsonResults["image"] as! String
                            }
                            
                            UserDefaults.standard.setValue(self.phoneNumber, forKey: "PhoneNumber")

                            let action = "registration"
                            Branch.getInstance().userCompletedAction(action)
                            
                            let productInfo = ProductInfo()
                            productInfo.userFirstName = FirstName
                            productInfo.userLastName = LastName
                            productInfo.userEmail = Email
                            productInfo.Country = Country
                            productInfo.userID = userID
                            productInfo.Role = Role
                            productInfo.companyName = companyName
                            productInfo.companyAddress = companyAddress
                            productInfo.image = imageName
                            let isResult = ModelManager.instance.addUserDetails(productInfo)
                            if isResult == true
                            {
                                if Role == 2{
                                    let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                    let viewController = mainStoryboard.instantiateViewController(withIdentifier: "UserTabbarController") as! UserTabbarController
                                    UIApplication.shared.keyWindow?.rootViewController = viewController
                                }
                                else{
                                    
                                    let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                    let viewController = mainStoryboard.instantiateViewController(withIdentifier: "ServiceProviderTabbarController") as! ServiceProviderTabbarController
                                    UIApplication.shared.keyWindow?.rootViewController = viewController
                                }
                            }
                        }
                        
                        self.view.hideActivityView()
                        break
                        
                    case .failure(_):
                        print(response.error!.localizedDescription)
                        let alert: UIAlertView = UIAlertView(title: "", message: response.error!.localizedDescription, delegate: nil, cancelButtonTitle: "OK");
                        alert.show()
                        self.view.hideActivityView()
                        print(response.result.error as Any)
                        break
                        
                    }
                }
            }
        }
    }
    
    @IBAction func buttonOnClick(_ sender: UIButton)
    {
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallary()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera))
        {
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openGallary()
    {
        imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        imagePicker.allowsEditing = false
    
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    internal func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            ProfilePicImgView.image = pickedImage
            let imageData: NSData = UIImageJPEGRepresentation(pickedImage, 0.4)! as NSData
            let imageStr = imageData.base64EncodedString(options: .lineLength64Characters)
            base64Str = imageStr
        }
        
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    
    private func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}

extension UITapGestureRecognizer {
    
    func didTapAttributedTextInLabel(label: UILabel, inRange targetRange: NSRange) -> Bool {
        // Create instances of NSLayoutManager, NSTextContainer and NSTextStorage
        let layoutManager = NSLayoutManager()
        let textContainer = NSTextContainer(size: CGSize.zero)
        let textStorage = NSTextStorage(attributedString: label.attributedText!)
        
        // Configure layoutManager and textStorage
        layoutManager.addTextContainer(textContainer)
        textStorage.addLayoutManager(layoutManager)
        
        // Configure textContainer
        textContainer.lineFragmentPadding = 0.0
        textContainer.lineBreakMode = label.lineBreakMode
        textContainer.maximumNumberOfLines = label.numberOfLines
        let labelSize = label.bounds.size
        textContainer.size = labelSize
        
        // Find the tapped character location and compare it to the specified range
        let locationOfTouchInLabel = self.location(in: label)
        let textBoundingBox = layoutManager.usedRect(for: textContainer)
        let textContainerOffset = CGPoint(x:(labelSize.width - textBoundingBox.size.width) * 0.5 - textBoundingBox.origin.x,
                                          y:(labelSize.height - textBoundingBox.size.height) * 0.5 - textBoundingBox.origin.y);
        let locationOfTouchInTextContainer = CGPoint(x:locationOfTouchInLabel.x - textContainerOffset.x,
                                                     y:locationOfTouchInLabel.y - textContainerOffset.y);
        let indexOfCharacter = layoutManager.characterIndex(for: locationOfTouchInTextContainer, in: textContainer, fractionOfDistanceBetweenInsertionPoints: nil)
        
        return NSLocationInRange(indexOfCharacter, targetRange)
    }
    
}
