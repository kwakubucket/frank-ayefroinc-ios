//
//  OnBoardingViewController.swift
//  AyefroInc
//
//  Created by a on 11/10/17.
//  Copyright © 2017 Inceptive Consulting Pvt. Ltd. All rights reserved.
//

import UIKit

class OnBoardingViewController: UIViewController, CPSliderDelegate, UIScrollViewDelegate {
    
    var imageArray = [String]()
    
    @IBOutlet weak var slider : CPImageSlider!
    
    var index: Int = 0
    
    static var leftArrowImage : UIImage?
    static var rightArrowImage : UIImage?
    
    
    var lastIndex : Int = 0
    
    @IBOutlet weak fileprivate var myScrollView: UIScrollView!
    @IBOutlet weak fileprivate var myPageControl: UIPageControl!
    
    @IBOutlet weak var pageIndicatorBottomConstraint : NSLayoutConstraint!
    
    @IBOutlet weak fileprivate var prevArrowButton : UIButton!
    @IBOutlet weak fileprivate var nextArrowButton : UIButton!
    @IBOutlet weak fileprivate var arrowButtonsView : UIView!
    
    private var currentIndex : Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imageArray = ["b1.png","b2.png","b3.png","b4.png","b5.png"]
        self.navigationController?.isNavigationBarHidden = true
        resetValues()
        addImagesOnScrollView()
        myPageControl.numberOfPages = imageArray.count
        myScrollView.delegate = self as? UIScrollViewDelegate
    }
    
    
    func sliderImageTapped(slider: CPImageSlider, index: Int) {
        print("\(index)")
        self.index = index
        let count = imageArray.count-1
        if(index == count){
            
        }
    }
    
    
    
    var allowCircular : Bool = true{
        didSet{
            addImagesOnScrollView()
        }
    }
    
    var durationTime : TimeInterval = 3.0
    
    var images = [String](){
        didSet{
            
            addImagesOnScrollView()
        }
    }
    
    var enableSwipe : Bool = false{
        didSet{
            myScrollView.isUserInteractionEnabled = enableSwipe
        }
    }
    
    var enableArrowIndicator : Bool = false{
        didSet{
            arrowButtonsView.isHidden = !enableArrowIndicator
        }
    }
    
    var enablePageIndicator : Bool = false{
        didSet{
            myPageControl.isHidden = !enablePageIndicator
        }
    }
    
    private var imageViewArray : [UIImageView] = []
    
    var autoSrcollEnabled : Bool = false{
        didSet{
            checkForAutoScrolled()
        }
    }
    
    var activeTimer:Timer?
    
    
    override func viewDidLayoutSubviews()
    {
         self.automaticallyAdjustsScrollViewInsets = false
        for index in 0..<myScrollView.subviews.count
        {
            let sub = myScrollView.subviews[index]
            sub.frame = CGRect(x: CGFloat(index)*self.view.bounds.width, y: 0, width: self.view.bounds.width, height: self.view.frame.size.height)
        }
        var count = imageArray.count
        if allowCircular
        {
            count += 2
        }
        myScrollView.contentSize = CGSize(width: self.view.bounds.width*CGFloat(count), height: self.view.frame.size.height)
        adjustContentOffsetFor(index: currentIndex, offsetIndex: convertIndex(), animated: false)
    }
    
    func convertIndex()->Int
    {
        if allowCircular
        {
            return currentIndex + 1
        }
        else
        {
            return currentIndex
        }
    }
    
    func resetValues()
    {
        allowCircular = true
        enableSwipe = true
        enablePageIndicator = true
        enableArrowIndicator = true
    }
    

    
    private func adjustContentOffsetFor(index : Int, offsetIndex offset : Int, animated : Bool)
    {
        myScrollView.setContentOffset(CGPoint(x: CGFloat(offset)*self.view.bounds.width, y: 0), animated: animated)
        myPageControl.currentPage = index
        checkButtonsIfNeedsDisable()
        checkForAutoScrolled()
    }
    
    func addImagesOnScrollView()
    {
        for sub in myScrollView.subviews
        {
            sub.removeFromSuperview()
        }
        if imageArray.count == 0
        {
            return
        }
        var count = imageArray.count
        if allowCircular && imageArray.count != 0
        {
            count += 2
        }
        for index in 0..<count
        {
            let imageV = getImageView(index: index)
            imageV.frame = CGRect(x: CGFloat(index)*self.view.bounds.width, y: 0, width: self.view.bounds.width, height: self.view.frame.size.height)
            if allowCircular && imageArray.count != 0
            {
                if index == 0 {
                    imageV.image = UIImage(named:imageArray.last!)
                }
                else if index > imageArray.count
                {
                    imageV.image = UIImage(named:imageArray.first!)
                }
                else
                {
                    imageV.image = UIImage(named:imageArray[index - 1])
                }
            }
            else
            {
                imageV.image = UIImage(named:imageArray[index])
            }
            myScrollView.addSubview(imageV)
        }
        
        if count < imageViewArray.count {
            imageViewArray.removeSubrange(count..<imageViewArray.count)
        }
        myScrollView.contentSize = CGSize(width: self.view.bounds.width*CGFloat(count), height: self.view.frame.size.height)
        adjustContentOffsetFor(index: currentIndex, offsetIndex: convertIndex(), animated: false)
    }
    
    func getImageView(index : Int)-> UIImageView
    {
        if index < imageViewArray.count
        {
            return imageViewArray[index]
        }
        
        let imageV = UIImageView()
        imageV.contentMode = .scaleAspectFill
        imageV.clipsToBounds = true
        imageV.isUserInteractionEnabled = true
        imageViewArray.append(imageV)
        
        return imageV
    }
    
    func createSlider(withImages images: [String], withAutoScroll isAutoScrollEnabled: Bool, in parentView: UIView)
    {
        self.view.frame = UIScreen.main.bounds
        self.images = images
        autoSrcollEnabled = isAutoScrollEnabled
    }
    
    
    func getCurrentIndex()->Int
    {
        let width: CGFloat = myScrollView.frame.size.width
        return Int((myScrollView.contentOffset.x + (0.5 * width)) / width)
    }
    
    func getCurrentIndex(x : CGFloat)->Int
    {
        let width: CGFloat = myScrollView.frame.size.width
        return Int((x + (0.5 * width)) / width)
    }
    
    //#pragma mark - UIScrollView delegate
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        print(#function)
        lastIndex = getCurrentIndex()
        self.invalidateTimer()
    }
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        let index = getCurrentIndex(x: targetContentOffset.pointee.x)
        if index != lastIndex
        {
            print("\(#function)")
            currentIndex = index
            if allowCircular
            {
                currentIndex = index - 1
                if currentIndex < 0 {
                    currentIndex = imageArray.count - 1
                }else if currentIndex > imageArray.count - 1
                {
                    currentIndex = 0
                }
            }
            adjustContentOffsetFor(index: currentIndex, offsetIndex: index, animated: true)
        }
    }
    
    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        print(#function)
        if allowCircular && imageArray.count != 0
        {
            if (currentIndex == 0 && myScrollView.contentOffset.x != getOffsetFor(index: 0)) || ((currentIndex == (imageArray.count - 1)) && myScrollView.contentOffset.x != getOffsetFor(index: (imageArray.count - 1)))
            {
                adjustContentOffsetFor(index: currentIndex, offsetIndex: convertIndex(), animated: false)
            }
        }
    }
    
    private func getOffsetFor(index : Int)->CGFloat
    {
        var tempIndex = index
        if allowCircular
        {
            tempIndex += 1
        }
        return CGFloat(tempIndex)*self.view.bounds.width
    }
    
    private func getActualOffsetFor(index : Int)->CGFloat
    {
        return CGFloat(index)*self.view.bounds.width
    }
    
    //pragma mark end
    @objc func slideImage()
    {
        let previous = currentIndex
        currentIndex = currentIndex + 1
        var convertedIndex = convertIndex()
        if currentIndex > imageArray.count - 1 {
            if allowCircular {
                currentIndex = 0
            }
            else
            {
                currentIndex = previous
                convertedIndex = convertIndex()
            }
            
        }
        adjustContentOffsetFor(index: currentIndex, offsetIndex: convertedIndex, animated: true)
    }
    
    func checkForAutoScrolled()
    {
        if(imageArray.count > 1 && autoSrcollEnabled){
            self .startTimerThread()
        }
        else
        {
            invalidateTimer()
        }
    }
    
    func startTimerThread()
    {
        invalidateTimer()
        activeTimer = Timer.scheduledTimer(timeInterval: durationTime, target: self, selector: #selector(self.slideImage), userInfo: nil, repeats: true)
    }
    
    func startAutoPlay() {
        autoSrcollEnabled = true
    }
    
    func stopAutoPlay() {
        autoSrcollEnabled =  false
        invalidateTimer()
    }
    
    func invalidateTimer()
    {
        if activeTimer != nil
        {
            activeTimer!.invalidate()
            activeTimer = nil
        }
    }
    
    private func checkButtonsIfNeedsDisable()
    {
        checkIfPrevNeedsDisable()
        checkIfNextNeedsDisable()
    }
    
    private func checkIfNextNeedsDisable()
    {
        if !allowCircular && currentIndex == imageArray.count-1  {
            nextArrowButton.isEnabled = true
        }
        else
        {
            nextArrowButton.isEnabled = true
        }
    }
    
    private func checkIfPrevNeedsDisable()
    {
        if !allowCircular && currentIndex == 0
        {
            prevArrowButton.isEnabled = true
        }
        else
        {
            prevArrowButton.isEnabled = true
        }
    }
    
    @IBAction func nextButtonPressed()
    {
        invalidateTimer()
        currentIndex = currentIndex + 1
        let convertedIndex = convertIndex()
        if currentIndex > imageArray.count - 1 {
            currentIndex = 0
            let productInfo : ProductInfo = ProductInfo()
            productInfo.alertValue = true
            let isData = ModelManager.instance.updateAlerTable(productInfo)
            let storyboard =  UIStoryboard(name: "Main", bundle: nil)
            let home = storyboard.instantiateViewController(withIdentifier: "LandingNavViewController") as! LandingNavViewController
            self.navigationController?.present(home, animated: true, completion: nil)
        }
        else{
            adjustContentOffsetFor(index: currentIndex, offsetIndex: convertedIndex, animated: true)
        }
        
//        delegate?.sliderImageTapped(slider: self, index: currentIndex)
    }
    
    @IBAction func previousButtonPressed()
    {
        /*invalidateTimer()
        currentIndex = currentIndex - 1
        let convertedIndex = convertIndex()
        if currentIndex < 0 {
            currentIndex = imageArray.count - 1
        }
        adjustContentOffsetFor(index: currentIndex, offsetIndex: convertedIndex, animated: true)
        delegate?.sliderImageTapped(slider: self, index: currentIndex)*/
        let productInfo : ProductInfo = ProductInfo()
        productInfo.alertValue = true
        let isData = ModelManager.instance.updateAlerTable(productInfo)
        let storyboard =  UIStoryboard(name: "Main", bundle: nil)
        let home = storyboard.instantiateViewController(withIdentifier: "LandingNavViewController") as! LandingNavViewController
        self.navigationController?.present(home, animated: true, completion: nil)
        
    }
}

