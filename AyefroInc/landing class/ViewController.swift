//
//  ViewController.swift
//  AyefroInc
//
//  Created by a on 11/7/17.
//  Copyright © 2017 Inceptive Consulting Pvt. Ltd. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    
    @IBOutlet var nextBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Ayefro Inc"
        
        nextBtn.layer.cornerRadius = 5
        
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

