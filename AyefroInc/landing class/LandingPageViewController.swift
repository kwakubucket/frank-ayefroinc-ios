//
//  LandingPageViewController.swift
//  AyefroInc
//
//  Created by a on 11/9/17.
//  Copyright © 2017 Inceptive Consulting Pvt. Ltd. All rights reserved.
//

import UIKit
import DropDown
import FirebaseAuth
import Alamofire
import Branch

class LandingPageViewController: UIViewController {
    
    @IBOutlet weak var chooseArticleButton: UIButton!
    @IBOutlet var phoneNumberTF: NiceTextField!
    @IBOutlet var sendbtn: UIButton!
    @IBOutlet var confirmationView: UIView!
    @IBOutlet var mobileNoLabel: UILabel!
    @IBOutlet var otpTf: NiceTextField!
    @IBOutlet var continueBtn: UIButton!
    @IBOutlet var scrollHeight: NSLayoutConstraint!
    @IBOutlet var scrollView: UIScrollView!
    
    
    let chooseArticleDropDown = DropDown()
    var countryCodeArr : [String] = []
    var countryArr : [String] = []
    var currencySymbolArr : [String] = []
    var selectedIndex: Int = 0
    var codeArr : [String] = []
    var verificationID : String = ""
    
    lazy var dropDowns: [DropDown] = {
        return [
            self.chooseArticleDropDown
        ]
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Ayefro Inc"
        self.navigationItem.setHidesBackButton(true, animated:true)
        
        UserDefaults.standard.setValue("true", forKey: "MoreOverlay")
        UserDefaults.standard.setValue("true", forKey: "DashboardOverlay")
        UserDefaults.standard.setValue("true", forKey: "UploadOver")
        UserDefaults.standard.setValue("false", forKey: "Followed")
        
        dropDowns.forEach { $0.dismissMode = .onTap }
        dropDowns.forEach { $0.direction = .any }
        
        let path = Bundle.main.path(forResource: "countryCode", ofType: "json")
        do {
            let data = try Data(contentsOf: URL(fileURLWithPath: path!), options: .mappedIfSafe)
            let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves) as! NSDictionary
            let countries = jsonResult.object(forKey: "countries") as! NSDictionary
            let country = countries.object(forKey: "country") as! NSArray
            let descriptor: NSSortDescriptor = NSSortDescriptor(key: "countryName", ascending: true, selector: #selector(NSString.localizedCaseInsensitiveCompare(_:)))
            let sortedResults: NSArray = country.sortedArray(using: [descriptor]) as NSArray
            for i in 0 ..< sortedResults.count
            {
                let countryObj = sortedResults[i] as! NSDictionary
                let countryName = countryObj.object(forKey: "countryName") as! String
                let countryCode = countryObj.object(forKey: "countryCode") as! String
                let currencySymbol = countryObj.object(forKey: "currencySymbol") as! String
                
                self.countryCodeArr.append(countryName + " +" + countryCode)
                self.codeArr.append("+" + countryCode)
                self.countryArr.append(countryName)
                self.currencySymbolArr.append(currencySymbol)
            }
            if address != "" {
                let countryName = self.countryName(from: address)
                self.selectedIndex = self.countryArr.index(of: countryName)!
                setupChooseArticleDropDown()
            }
            else{
                setupChooseArticleDropDown()
            }
           
        } catch {
            
        }
        
        
        sendbtn.layer.cornerRadius = 5
        continueBtn.layer.cornerRadius = 5
        confirmationView.isHidden = true
        
        if UIScreen.main.bounds.size.width == 414
        {

        }
        else if UIScreen.main.bounds.size.width == 375 && UIScreen.main.bounds.size.height == 667
        {

        }
        else if UIScreen.main.bounds.size.width == 320 && UIScreen.main.bounds.size.height == 568
        {
            scrollHeight.constant = 100
        }
        else if UIScreen.main.bounds.size.width == 320 && UIScreen.main.bounds.size.height == 480
        {
            scrollHeight.constant = 160
        }
        
    }
    
    func countryName(from countryCode: String) -> String {
        if let name = (Locale.current as NSLocale).displayName(forKey: .countryCode, value: countryCode) {
            // Country name was found
            return name
        } else {
            // Country name cannot be found
            return countryCode
        }
    }
    
    
    func setupChooseArticleDropDown() {
        chooseArticleDropDown.anchorView = chooseArticleButton
        chooseArticleDropDown.bottomOffset = CGPoint(x: 0, y: chooseArticleButton.bounds.height)
        chooseArticleDropDown.dataSource = countryCodeArr
        chooseArticleDropDown.selectionAction = { [unowned self] (index, item) in
            self.chooseArticleButton.setTitle(item, for: .normal)
            self.selectedIndex = index
        }
        
        self.chooseArticleButton.setTitle(chooseArticleDropDown.dataSource[self.selectedIndex], for: .normal)
    }
    
    func validate(value: String) -> Bool {
        let PHONE_REGEX = "^((\\+)|(00))[0-9]{6,14}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        let result =  phoneTest.evaluate(with: value)
        return result
    }
    
    @IBAction func chooseArticle(_ sender: AnyObject) {
        chooseArticleDropDown.show()
    }
    
    @IBAction func sendCodeBtnClk(_ sender: UIButton) {
        phoneNumberTF.resignFirstResponder()
        otpTf.resignFirstResponder()
        if phoneNumberTF.text == ""{
            let alert: UIAlertView = UIAlertView(title: "", message: "Please enter phone number", delegate: nil, cancelButtonTitle: "OK");
            alert.show()
        }
        else if (self.validate(value: self.codeArr[self.selectedIndex] + phoneNumberTF.text!) == false){
            let alert: UIAlertView = UIAlertView(title: "", message: "Please enter valid phone number", delegate: nil, cancelButtonTitle: "OK");
            alert.show()
        }
        else{
//            validatedPhoneNumber(number: self.codeArr[self.selectedIndex] + phoneNumberTF.text!)

            self.view.showActivityView(withLabel: "Loading")
            print(self.codeArr[self.selectedIndex] + phoneNumberTF.text!)
            
            currencySymbolString = self.currencySymbolArr[self.selectedIndex]
            UserDefaults.standard.set(currencySymbolString, forKey: "currencySymbol")

            PhoneAuthProvider.provider().verifyPhoneNumber(self.codeArr[self.selectedIndex] + phoneNumberTF.text!, uiDelegate: nil) { (verificationID, error) in
                if let error = error {
                    print(error.localizedDescription.description)
                    if error.localizedDescription.description == "Network error (such as timeout, interrupted connection or unreachable host) has occurred."{
                        let alert: UIAlertView = UIAlertView(title: "", message: "Please check your internet connection!", delegate: nil, cancelButtonTitle: "OK");
                        alert.show()
                    }
                    else{
                        let alert: UIAlertView = UIAlertView(title: "", message: error.localizedDescription.description, delegate: nil, cancelButtonTitle: "OK");
                        alert.show()
                    }

                    self.view.hideActivityView()
                    return
                }
                self.view.hideActivityView()
                print(verificationID as Any)
                self.verificationID = verificationID as! String
                self.confirmationView.isHidden = false
                self.sendbtn.setTitle("RESEND CODE", for: .normal)
                self.mobileNoLabel.text = self.codeArr[self.selectedIndex] + self.phoneNumberTF.text!
            }
            
        }
    }
    
    
    
    @IBAction func continueBtnClk(_ sender: UIButton) {
        
        phoneNumberTF.resignFirstResponder()
        otpTf.resignFirstResponder()
        if otpTf.text == ""{
            let alert: UIAlertView = UIAlertView(title: "", message: "Please enter OTP", delegate: nil, cancelButtonTitle: "OK");
            alert.show()
        }
        else{
            self.view.showActivityView(withLabel: "Loading")
            var number = self.phoneNumberTF.text!
            let index = number.index(number.startIndex, offsetBy: 0)
            if number[index] == "0"{
                number.remove(at: number.startIndex)
            }
            
            
            let credential = PhoneAuthProvider.provider().credential(
                withVerificationID: self.verificationID,
                verificationCode: self.otpTf.text!)
            Auth.auth().signIn(with: credential) { (user, error) in
                if let error = error {
                    print(error.localizedDescription)
                    let alert: UIAlertView = UIAlertView(title: "", message: "Invalid OTP", delegate: nil, cancelButtonTitle: "OK");
                    alert.show()
                    self.view.hideActivityView()
                    return
                }
                
                self.validatedPhoneNumber(number: self.codeArr[self.selectedIndex] + number)
            }
        }
    }
    
    func validatedPhoneNumber(number: String)  {
        DispatchQueue.main.async {
            let manager = Alamofire.SessionManager.default
            manager.session.configuration.timeoutIntervalForRequest = 120
            
            manager.request(baseURL + "api/AccountAPI/GetUserExist/?PhoneNumber=" + number, method: .get, parameters: ["":""], encoding: URLEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
                
                switch(response.result) {
                case .success(_):
                    if response.result.value != nil{
                        let jsonResults : NSDictionary
                        jsonResults = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! NSDictionary
                        print(jsonResults)
                        let status = jsonResults["status"] as! String
                        if status == "Success"{
                            let dataArr = jsonResults["userExists"] as! NSArray
                            let data = dataArr[0] as! NSDictionary
                            let Country = data["Country"] as! String
                            let Email = data["Email"] as! String
                            let FirstName = data["FirstName"] as! String
                            let userID = data["Id"] as! String
                            let LastName = data["LastName"] as! String
                            let Role = data["Role"] as! Int
                            var companyAddress = ""
                            if (data["companyAddress"] as? String) != nil
                            {
                                companyAddress = data["companyAddress"] as! String
                            }
                            var companyName = ""
                            if  (data["companyAddress"] as? String) != nil
                            {
                                companyName = data["companyName"] as! String
                            }
                            var imageName = ""
                            if (data["image"] as? String) != nil
                            {
                                imageName = data["image"] as! String
                            }
                            
                            var ReferalCode = ""
                            if (data["ReferalCode"] as? String) != nil
                            {
                                ReferalCode = data["ReferalCode"] as! String
                            }
                            
                            var points = ""
                            if (data["points"] as? String) != nil
                            {
                                points = data["points"] as! String
                            }
                            
                            UserDefaults.standard.setValue(number, forKey: "PhoneNumber")
                            UserDefaults.standard.setValue(ReferalCode, forKey: "ReferalCode")
                            UserDefaults.standard.setValue(points, forKey: "points")
                            
                            let productInfo = ProductInfo()
                            productInfo.userFirstName = FirstName
                            productInfo.userLastName = LastName
                            productInfo.userEmail = Email
                            productInfo.Country = Country
                            productInfo.userID = userID
                            productInfo.Role = Role
                            productInfo.companyName = companyName
                            productInfo.companyAddress = companyAddress
                            productInfo.image = imageName
                            let isResult = ModelManager.instance.addUserDetails(productInfo)
                            if isResult == true
                            {
                                self.sendDeviceToken(userID: userID)
                            }
                           

                        }
                        else{
                            self.view.hideActivityView()
                            let storyboard =  UIStoryboard(name: "Main", bundle: nil)
                            let home = storyboard.instantiateViewController(withIdentifier: "SetProfileViewController") as! SetProfileViewController
                            home.phoneNumber = number
                            home.country = self.countryArr[self.selectedIndex]
                            self.navigationController?.pushViewController(home, animated: true)
                        }
                    }
                    break
                    
                case .failure(_):
                    print(response.error!.localizedDescription)
                    let alert: UIAlertView = UIAlertView(title: "", message: response.error!.localizedDescription, delegate: nil, cancelButtonTitle: "OK");
                    alert.show()
                    self.view.hideActivityView()
                    print(response.result.error as Any)
                    break
                    
                }
            }
        }
    }
    
    func sendDeviceToken(userID: String) {
        DispatchQueue.main.async {
            let parameters : [String: String] = ["DeviceToken":deviceTokenStr, "Platform":"ios", "UserId":userID]
            let manager = Alamofire.SessionManager.default
            manager.session.configuration.timeoutIntervalForRequest = 120
            
            manager.request(baseURL + "api/AccountAPI/UpdateDeviceInfo", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
                
                switch(response.result) {
                case .success(_):
                    if response.result.value != nil{
                        //print(response.result.value as Any)
                    }
                    _ = ModelManager.instance.getUserLoginStatus()
                    if Role == 2{
                        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                        let viewController = mainStoryboard.instantiateViewController(withIdentifier: "UserTabbarController") as! UserTabbarController
                        UIApplication.shared.keyWindow?.rootViewController = viewController
                    }
                    else{

                        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                        let viewController = mainStoryboard.instantiateViewController(withIdentifier: "ServiceProviderTabbarController") as! ServiceProviderTabbarController
                        UIApplication.shared.keyWindow?.rootViewController = viewController
                    }
                    self.view.hideActivityView()
                    break
                    
                case .failure(_):
                    print(response.error!.localizedDescription)
                    let alert: UIAlertView = UIAlertView(title: "", message: response.error!.localizedDescription, delegate: nil, cancelButtonTitle: "OK");
                    alert.show()
                    self.view.hideActivityView()
                    print(response.result.error as Any)
                    break
                    
                }
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}
