//
//  AppDelegate.swift
//  AyefroInc
//
//  Created by a on 11/7/17.
//  Copyright © 2017 Inceptive Consulting Pvt. Ltd. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import Firebase
import Fabric
import Crashlytics
import CoreLocation
import UserNotifications
import Siren
import Branch


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, CLLocationManagerDelegate, SFSwiftNotificationProtocol, UNUserNotificationCenterDelegate {

    var window: UIWindow?
    let locationManager = CLLocationManager()
    
    var notifyView:SFSwiftNotification?
    var notifyFrame:CGRect?
    var isnotification : Bool! = false
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
//        UserDefaults.standard.setValue("true", forKey: "MoreOverlay")
//        UserDefaults.standard.setValue("true", forKey: "DashboardOverlay")
//        UserDefaults.standard.setValue("false", forKey: "Followed")
        
        let branch: Branch = Branch.getInstance()
        Branch.getInstance().setDebug()
        branch.initSession(launchOptions: launchOptions, andRegisterDeepLinkHandler: {params, error in
            if error == nil {
                // params are the deep linked params associated with the link that the user clicked -> was re-directed to this app
                // params will be empty if no data found
                // ... insert custom logic here ...
                print("params: %@", params as? [String: AnyObject] ?? {})
            }
        })
        let sessionParams = Branch.getInstance().getLatestReferringParams()
        
        let installParams = Branch.getInstance().getFirstReferringParams()

        
        IQKeyboardManager.sharedManager().enable = true
        FirebaseApp.configure()
        
        let siren = Siren.shared
        siren.alertType = .force
        siren.delegate = self
        siren.debugEnabled = true
        
        UIApplication.shared.statusBarView?.backgroundColor = UIColor(red: 247.0/255.0, green: 247.0/255.0, blue: 247.0/255.0, alpha: 1.0)
        Fabric.with([Crashlytics.self])
        Util.copyFile(fileName: "Ayefro.sqlite")
        self.locationManager.requestAlwaysAuthorization()
        self.locationManager.requestWhenInUseAuthorization()
        self.locationManager.requestAlwaysAuthorization()
        
        let settings = UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
        application.registerUserNotificationSettings(settings)
        application.registerForRemoteNotifications()
        
        
        if CLLocationManager.locationServicesEnabled() {
            
            locationManager.delegate = self
            
            locationManager.requestWhenInUseAuthorization()
            
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            
            locationManager.startUpdatingLocation()
            
            locationManager.startMonitoringSignificantLocationChanges()
            switch(CLLocationManager.authorizationStatus())
            {
                
            case .authorizedAlways, .authorizedWhenInUse:
                
                print("Authorize.")
                
                break
                
            case .notDetermined:
                
                print("Not determined.")
                
                break
                
            case .restricted:
                
                print("Restricted.")
                
                break
                
            case .denied:
                
                print("Denied.")
            }
        }
        let isData = ModelManager.instance.getAlertValue()
        if isData == true
        {
            let isLogin = ModelManager.instance.getUserLoginStatus()
            if isLogin == true{
                // only canonicalIdentifier is required
                let storyboard =  UIStoryboard(name: "Main", bundle: nil)
                let promo = storyboard.instantiateViewController(withIdentifier: "UnderProgressViewController") as! UnderProgressViewController
                self.window?.rootViewController = promo
            }
            else{
                let storyboard =  UIStoryboard(name: "Main", bundle: nil)
                let promo = storyboard.instantiateViewController(withIdentifier: "LandingNavViewController") as! LandingNavViewController
                self.window?.rootViewController = promo
            }
        }
        else{
            let storyboard =  UIStoryboard(name: "Main", bundle: nil)
            let promo = storyboard.instantiateViewController(withIdentifier: "OnBoardViewController") as! OnBoardViewController
            self.window?.rootViewController = promo
        }

        return true
    }
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
//        print("locations = \(locValue.latitude) \(locValue.longitude)")
        getAddress(selectedLat: locValue.latitude, selectedLon: locValue.longitude)
    }
    
    func getAddress(selectedLat: Double, selectedLon: Double) {
        let geoCoder = CLGeocoder()
        let location = CLLocation(latitude: selectedLat, longitude: selectedLon)
        //selectedLat and selectedLon are double values set by the app in a previous process
        
        geoCoder.reverseGeocodeLocation(location, completionHandler: { (placemarks, error) -> Void in
            
            // Place details
            var placeMark: CLPlacemark!
            placeMark = placemarks?[0]
            
            if (placeMark?.locality) != nil {
                if (placeMark.addressDictionary!["Country"] as? String) != nil {
                    //print(country)
                    let country = placeMark.addressDictionary!["Country"] as! String
                    address = country
                }
            }
        })
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        // handler for URI Schemes (depreciated in iOS 9.2+, but still used by some apps)
        Branch.getInstance().application(app, open: url, options: options)
        return true
    }
    
    func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([Any]?) -> Void) -> Bool {
        // handler for Universal Links
        Branch.getInstance().continue(userActivity)
        return true
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        var token = ""
        for i in 0..<deviceToken.count {
            token = token + String(format: "%02.2hhx", arguments: [deviceToken[i]])
        }
        print(token)
        deviceTokenStr = token
        
        let firebaseAuth = Auth.auth()
        firebaseAuth.setAPNSToken(deviceToken, type: AuthAPNSTokenType.prod)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        let firebaseAuth = Auth.auth()
        _ = ModelManager.instance.getUserLoginStatus()
        Branch.getInstance().handlePushNotification(userInfo)
        completionHandler(.newData)
        print(userInfo)
        isnotification = true
        if application.applicationState == UIApplicationState.background
        {
            if let info = userInfo[("aps" as AnyObject) as! NSObject] as? Dictionary<String, AnyObject>
            {
                DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                    self.navigate(info: info)
                }
            }
        }
        else
        {
            if let info = userInfo[("aps" as AnyObject) as! NSObject] as? Dictionary<String, AnyObject>
            {
                let alertMsg = info["alert"] as! String
                let key = info["key"] as! String
                if key == "PostAnswer" || key == "ReplyAnswer"{
                    forumNotification = true
                    q_id = info["Id"] as! String
                }
                if key == "DiaryImageComment" || key == "DiaryImageReply"{
                    dairy_i_id = info["Id"] as! String
                    dairyNotification = true
                }
                if key == "ImageReply" || key == "ImageComment"{
                    imageNotification = true
                    v_id = info["ServiceProviderId"] as! String
                    i_id = info["Img_Id"] as! String
                }
                if key == "1"{
                    eventNotification = true
                    e_id = info["eventId"] as! String
                }
                if key == "ImageUpload"{
                    imageUploadSelf = true
                }
                if key == "6"{
                    imageUpload = true
                    ven_id = info["providerId"] as! String
                }
                if key == "2" || key == "3"{
                    submitBitNotification = true
                    eve_id = info["eventId"] as! String
                    eve_Name = info["eventName"] as! String
                }
                if key == "4"{
                    awardNotification = true
                    ord_id = info["orderId"] as! String
                }
                if key == "5" {
                    
                }
                
                notifyFrame = CGRect(x:0, y:0, width:self.window!.frame.size.width, height:80)
                notifyView = SFSwiftNotification(frame: notifyFrame!,
                                                 title: nil,
                                                 animationType: AnimationType.AnimationTypeBounce,
                                                 direction: Direction.TopToBottom,
                                                 delegate: self)
                notifyView!.backgroundColor = UIColor.darkGray
                notifyView!.label.textColor = UIColor.white
                notifyView!.label.text = "Ayefro Inc"
                notifyView!.description_Label.text = alertMsg
                notifyView!.description_Label.textColor = UIColor.white
                notifyView!.image.image = UIImage(named: "40.png")
                self.window!.addSubview(notifyView!)
                self.notifyView!.animate(toFrame: notifyFrame!, delay: 1)
                
            }
            
        }

        if (firebaseAuth.canHandleNotification(userInfo)){
            print(userInfo)
            return
        }
        
        
    }

    func applicationWillResignActive(_ application: UIApplication) {
        
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        isnotification = false
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        Siren.shared.checkVersion(checkType: .immediately)

    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        Siren.shared.checkVersion(checkType: .immediately)

    }

    func applicationWillTerminate(_ application: UIApplication) {
    }
    
    func navigate(info: Dictionary<String, AnyObject>){
        let key = info["key"] as! String
        if isnotification == true {
            isnotification = false
            if key == "PostAnswer" || key == "ReplyAnswer"{
                q_id = info["Id"] as! String
                forumNotification = true
                if Role == 2{
                    let storyboard =  UIStoryboard(name: "Main", bundle: nil)
                    let promo = storyboard.instantiateViewController(withIdentifier: "UserTabbarController") as! UserTabbarController
                    self.window?.rootViewController = promo
                    promo.selectedIndex = 2
                }
                else{
                    let storyboard =  UIStoryboard(name: "Main", bundle: nil)
                    let promo = storyboard.instantiateViewController(withIdentifier: "ServiceProviderTabbarController") as! ServiceProviderTabbarController
                    self.window?.rootViewController = promo
                    promo.selectedIndex = 2
                }
            }
            if key == "DiaryImageComment" || key == "DiaryImageReply"{
                dairy_i_id = info["Id"] as! String
                dairyNotification = true
                if Role == 2{
                    let storyboard =  UIStoryboard(name: "Main", bundle: nil)
                    let promo = storyboard.instantiateViewController(withIdentifier: "UserTabbarController") as! UserTabbarController
                    self.window?.rootViewController = promo
                    promo.selectedIndex = 0
                }
                else{
                    let storyboard =  UIStoryboard(name: "Main", bundle: nil)
                    let promo = storyboard.instantiateViewController(withIdentifier: "ServiceProviderTabbarController") as! ServiceProviderTabbarController
                    self.window?.rootViewController = promo
                    promo.selectedIndex = 0
                }
            }
            if key == "ImageReply" || key == "ImageComment"{
                imageNotification = true
                v_id = info["ServiceProviderId"] as! String
                i_id = info["Img_Id"] as! String
                if Role == 2{
                    let storyboard =  UIStoryboard(name: "Main", bundle: nil)
                    let promo = storyboard.instantiateViewController(withIdentifier: "UserTabbarController") as! UserTabbarController
                    self.window?.rootViewController = promo
                    promo.selectedIndex = 1
                }
                else{
                    let storyboard =  UIStoryboard(name: "Main", bundle: nil)
                    let promo = storyboard.instantiateViewController(withIdentifier: "ServiceProviderTabbarController") as! ServiceProviderTabbarController
                    self.window?.rootViewController = promo
                    promo.selectedIndex = 1
                }
            }
            if key == "1"{
                eventNotification = true
                e_id = info["eventId"] as! String
                if Role == 2{
                    let storyboard =  UIStoryboard(name: "Main", bundle: nil)
                    let promo = storyboard.instantiateViewController(withIdentifier: "UserTabbarController") as! UserTabbarController
                    self.window?.rootViewController = promo
                    promo.selectedIndex = 3
                }
                else{
                    let storyboard =  UIStoryboard(name: "Main", bundle: nil)
                    let promo = storyboard.instantiateViewController(withIdentifier: "ServiceProviderTabbarController") as! ServiceProviderTabbarController
                    self.window?.rootViewController = promo
                    promo.selectedIndex = 3
                }
            }
            if key == "ImageUpload"{
                imageUploadSelf = true
                if Role == 2{
                    let storyboard =  UIStoryboard(name: "Main", bundle: nil)
                    let promo = storyboard.instantiateViewController(withIdentifier: "UserTabbarController") as! UserTabbarController
                    self.window?.rootViewController = promo
                    promo.selectedIndex = 3
                }
                else{
                    let storyboard =  UIStoryboard(name: "Main", bundle: nil)
                    let promo = storyboard.instantiateViewController(withIdentifier: "ServiceProviderTabbarController") as! ServiceProviderTabbarController
                    self.window?.rootViewController = promo
                    promo.selectedIndex = 3
                }
            }
            if key == "6"{
                imageUpload = true
                ven_id = info["providerId"] as! String
                if Role == 2{
                    let storyboard =  UIStoryboard(name: "Main", bundle: nil)
                    let promo = storyboard.instantiateViewController(withIdentifier: "UserTabbarController") as! UserTabbarController
                    self.window?.rootViewController = promo
                    promo.selectedIndex = 1
                }
                else{
                    let storyboard =  UIStoryboard(name: "Main", bundle: nil)
                    let promo = storyboard.instantiateViewController(withIdentifier: "ServiceProviderTabbarController") as! ServiceProviderTabbarController
                    self.window?.rootViewController = promo
                    promo.selectedIndex = 1
                }
            }
            if key == "2" || key == "3"{
                submitBitNotification = true
                eve_id = info["eventId"] as! String
                eve_Name = info["eventName"] as! String
                
                if Role == 2{
                    let storyboard =  UIStoryboard(name: "Main", bundle: nil)
                    let promo = storyboard.instantiateViewController(withIdentifier: "UserTabbarController") as! UserTabbarController
                    
                    self.window?.rootViewController = promo
                    promo.selectedIndex = 3
                }
                else{
                    let storyboard =  UIStoryboard(name: "Main", bundle: nil)
                    let promo = storyboard.instantiateViewController(withIdentifier: "ServiceProviderTabbarController") as! ServiceProviderTabbarController
                    self.window?.rootViewController = promo
                    promo.selectedIndex = 3
                }
            }
            if key == "4"{
                awardNotification = true
                ord_id = info["orderId"] as! String
                if Role == 2{
                    let storyboard =  UIStoryboard(name: "Main", bundle: nil)
                    let promo = storyboard.instantiateViewController(withIdentifier: "UserTabbarController") as! UserTabbarController
                    self.window?.rootViewController = promo
                    promo.selectedIndex = 3
                }
                else{
                    let storyboard =  UIStoryboard(name: "Main", bundle: nil)
                    let promo = storyboard.instantiateViewController(withIdentifier: "ServiceProviderTabbarController") as! ServiceProviderTabbarController
                    self.window?.rootViewController = promo
                    promo.selectedIndex = 3
                }
            }
            if key == "5" {
                if Role == 3{
                    let storyboard =  UIStoryboard(name: "Main", bundle: nil)
                    let promo = storyboard.instantiateViewController(withIdentifier: "ServiceProviderTabbarController") as! ServiceProviderTabbarController
                    self.window?.rootViewController = promo
                    promo.selectedIndex = 1
                }
            }
        }
        
    }

    

    func didNotifyFinishedAnimation(results: Bool) {
        
    }
    
    func didTapNotification() {
        if forumNotification == true{
            if Role == 2{
                let storyboard =  UIStoryboard(name: "Main", bundle: nil)
                let promo = storyboard.instantiateViewController(withIdentifier: "UserTabbarController") as! UserTabbarController
                self.window?.rootViewController = promo
                promo.selectedIndex = 2
            }
            else{
                let storyboard =  UIStoryboard(name: "Main", bundle: nil)
                let promo = storyboard.instantiateViewController(withIdentifier: "ServiceProviderTabbarController") as! ServiceProviderTabbarController
                self.window?.rootViewController = promo
                promo.selectedIndex = 2
            }
        }
        if imageNotification == true {
            if Role == 2{
                let storyboard =  UIStoryboard(name: "Main", bundle: nil)
                let promo = storyboard.instantiateViewController(withIdentifier: "UserTabbarController") as! UserTabbarController
                self.window?.rootViewController = promo
                promo.selectedIndex = 1
            }
            else{
                let storyboard =  UIStoryboard(name: "Main", bundle: nil)
                let promo = storyboard.instantiateViewController(withIdentifier: "ServiceProviderTabbarController") as! ServiceProviderTabbarController
                self.window?.rootViewController = promo
                promo.selectedIndex = 1
            }
        }
        if eventNotification == true{
            if Role == 2{
                let storyboard =  UIStoryboard(name: "Main", bundle: nil)
                let promo = storyboard.instantiateViewController(withIdentifier: "UserTabbarController") as! UserTabbarController
                self.window?.rootViewController = promo
                promo.selectedIndex = 3
            }
            else{
                let storyboard =  UIStoryboard(name: "Main", bundle: nil)
                let promo = storyboard.instantiateViewController(withIdentifier: "ServiceProviderTabbarController") as! ServiceProviderTabbarController
                self.window?.rootViewController = promo
                promo.selectedIndex = 3
            }
        }
        if imageUploadSelf == true{
            if Role == 2{
                let storyboard =  UIStoryboard(name: "Main", bundle: nil)
                let promo = storyboard.instantiateViewController(withIdentifier: "UserTabbarController") as! UserTabbarController
                self.window?.rootViewController = promo
                promo.selectedIndex = 3
            }
            else{
                let storyboard =  UIStoryboard(name: "Main", bundle: nil)
                let promo = storyboard.instantiateViewController(withIdentifier: "ServiceProviderTabbarController") as! ServiceProviderTabbarController
                self.window?.rootViewController = promo
                promo.selectedIndex = 3
            }
        }
        if imageUpload == true{
            if Role == 2{
                let storyboard =  UIStoryboard(name: "Main", bundle: nil)
                let promo = storyboard.instantiateViewController(withIdentifier: "UserTabbarController") as! UserTabbarController
                self.window?.rootViewController = promo
                promo.selectedIndex = 3
            }
            else{
                let storyboard =  UIStoryboard(name: "Main", bundle: nil)
                let promo = storyboard.instantiateViewController(withIdentifier: "ServiceProviderTabbarController") as! ServiceProviderTabbarController
                self.window?.rootViewController = promo
                promo.selectedIndex = 3
            }
        }
        if submitBitNotification == true{
            if Role == 2{
                let storyboard =  UIStoryboard(name: "Main", bundle: nil)
                let promo = storyboard.instantiateViewController(withIdentifier: "UserTabbarController") as! UserTabbarController
                self.window?.rootViewController = promo
                promo.selectedIndex = 3
            }
            else{
                let storyboard =  UIStoryboard(name: "Main", bundle: nil)
                let promo = storyboard.instantiateViewController(withIdentifier: "ServiceProviderTabbarController") as! ServiceProviderTabbarController
                self.window?.rootViewController = promo
                promo.selectedIndex = 3
            }
        }
        if awardNotification == true{
            if Role == 2{
                let storyboard =  UIStoryboard(name: "Main", bundle: nil)
                let promo = storyboard.instantiateViewController(withIdentifier: "UserTabbarController") as! UserTabbarController
                self.window?.rootViewController = promo
                promo.selectedIndex = 3
            }
            else{
                let storyboard =  UIStoryboard(name: "Main", bundle: nil)
                let promo = storyboard.instantiateViewController(withIdentifier: "ServiceProviderTabbarController") as! ServiceProviderTabbarController
                self.window?.rootViewController = promo
                promo.selectedIndex = 3
            }
        }
        if dairyNotification == true{
            if Role == 2{
                let storyboard =  UIStoryboard(name: "Main", bundle: nil)
                let promo = storyboard.instantiateViewController(withIdentifier: "UserTabbarController") as! UserTabbarController
                self.window?.rootViewController = promo
                promo.selectedIndex = 0
            }
            else{
                let storyboard =  UIStoryboard(name: "Main", bundle: nil)
                let promo = storyboard.instantiateViewController(withIdentifier: "ServiceProviderTabbarController") as! ServiceProviderTabbarController
                self.window?.rootViewController = promo
                promo.selectedIndex = 0
            }
        }
    }

}

extension UIApplication {
    var statusBarView: UIView? {
        return value(forKey: "statusBar") as? UIView
    }
}

extension AppDelegate: SirenDelegate
{
    func sirenDidShowUpdateDialog(alertType: Siren.AlertType) {
        
    }
    
    func sirenUserDidCancel() {
        print(#function)
    }
    
    func sirenUserDidSkipVersion() {
        print(#function)
    }
    
    func sirenUserDidLaunchAppStore() {
        print(#function)
    }
    
    func sirenDidFailVersionCheck(error: Error) {
        print(#function, error)
    }
    
    func sirenLatestVersionInstalled() {
        print(#function, "Latest version of app is installed")
    }
    
    // This delegate method is only hit when alertType is initialized to .none
    func sirenDidDetectNewVersionWithoutAlert(message: String, updateType: UpdateType) {
        print(#function, "\(message).\nRelease type: \(updateType.rawValue.capitalized)")
    }
}


