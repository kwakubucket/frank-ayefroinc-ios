//
//  UnderProgressViewController.swift
//  AyefroInc
//
//  Created by Vijay Darkonde on 08/01/18.
//  Copyright © 2018 Inceptive Consulting Pvt. Ltd. All rights reserved.
//

import UIKit
import Alamofire
import Branch

class UnderProgressViewController: UIViewController {

    @IBOutlet weak var bgImg: UIImageView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        
        getWorkingStatus()
        // Do any additional setup after loading the view.
    }
    
    func getWorkingStatus()  {
        DispatchQueue.main.async {
            let manager = Alamofire.SessionManager.default
            manager.session.configuration.timeoutIntervalForRequest = 120
            
            manager.request( baseURL + "api/AccountAPI/UnderMaintainence", method: .get, parameters: ["":""], encoding: URLEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
                
                switch(response.result) {
                case .success(_):
                    if response.result.value != nil{
//                        print(response.result.value as Any)
                    }
                    let jsonResults : NSDictionary
                    jsonResults = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! NSDictionary
                    let Success = jsonResults["Success"] as! String
                    if Success == "false"{
                        _ = ModelManager.instance.getUserLoginStatus()
                        if Role == 2{
                            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                            let viewController = mainStoryboard.instantiateViewController(withIdentifier: "UserTabbarController") as! UserTabbarController
                            UIApplication.shared.keyWindow?.rootViewController = viewController
                        }
                        else{
                            
                            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                            let viewController = mainStoryboard.instantiateViewController(withIdentifier: "ServiceProviderTabbarController") as! ServiceProviderTabbarController
                            UIApplication.shared.keyWindow?.rootViewController = viewController
                        }
                    }
                    else{
//                        if Role == 2{
//                            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//                            let viewController = mainStoryboard.instantiateViewController(withIdentifier: "UserTabbarController") as! UserTabbarController
//                            UIApplication.shared.keyWindow?.rootViewController = viewController
//                        }
//                        else{
//
//                            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//                            let viewController = mainStoryboard.instantiateViewController(withIdentifier: "ServiceProviderTabbarController") as! ServiceProviderTabbarController
//                            UIApplication.shared.keyWindow?.rootViewController = viewController
//                        }
                        self.bgImg.image = UIImage(named: "maintainance.png")
                    }
                    break
                    
                case .failure(_):
                    print(response.error!.localizedDescription)
                    let alert: UIAlertView = UIAlertView(title: "", message: response.error!.localizedDescription, delegate: nil, cancelButtonTitle: "OK");
                    alert.show()
                    self.view.hideActivityView()
                    print(response.result.error as Any)
                    break
                    
                }
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}
