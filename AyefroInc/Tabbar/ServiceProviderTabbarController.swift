//
//  ServiceProviderTabbarController.swift
//  AyefroInc
//
//  Created by Vijay Darkonde on 14/11/17.
//  Copyright © 2017 Inceptive Consulting Pvt. Ltd. All rights reserved.
//

import UIKit

class ServiceProviderTabbarController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let fvc = UIView()
        fvc.frame = CGRect(x: self.view.frame.origin.x, y: self.view.frame.origin.y, width: self.view.frame.size.width, height: self.view.frame.size.height)
        fvc.backgroundColor = UIColor(red:0.0/255.0, green:0.0/255.0, blue:0.0/255.0, alpha:0.6)
        self.tabBarController?.view.addSubview(fvc)
        
        _ = ModelManager.instance.getUserLoginStatus()
        let path = Bundle.main.path(forResource: "countryCode", ofType: "json")
        do{
            let data = try Data(contentsOf: URL(fileURLWithPath: path!), options: .mappedIfSafe)
            let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves) as! NSDictionary
            let countries = jsonResult.object(forKey: "countries") as! NSDictionary
            let country = countries.object(forKey: "country") as! NSArray
            for i in 0 ..< country.count
            {
                let countryObj = country[i] as! NSDictionary
                let countryName = countryObj.object(forKey: "countryName") as! String
                if Country == countryName{
                    let currencySymbol = countryObj.object(forKey: "currencySymbol") as! String
                    UserDefaults.standard.setValue(currencySymbol, forKey: "currencySymbol")
                    print("\(UserDefaults.standard.value(forKey: "currencySymbol")!)")
                    break
                }
            }
            
            
        }
        catch{
            
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
