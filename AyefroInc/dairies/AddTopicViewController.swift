//
//  AddTopicViewController.swift
//  AyefroInc
//
//  Created by Vijay Darkonde on 08/01/18.
//  Copyright © 2018 Inceptive Consulting Pvt. Ltd. All rights reserved.
//

import UIKit
import Alamofire

class AddTopicViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    
    var topicName : [String] = []
    var topicAction : [String] = []
    var topicID : [String] = []

    var apicall: Bool = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tabBarController?.tabBar.isHidden = true
        tableView.tableFooterView = UIView()
        self.title = "Add Topics"
        self.tableView.register(UINib.init(nibName: "TopicCell", bundle: nil), forCellReuseIdentifier: "TopicCell")
        // Do any additional setup after loading the view.
        self.navigationController?.navigationBar.tintColor = UIColor.black
        getTopic()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.setBackgroundImage(nil, for: .default)
        self.navigationController?.navigationBar.shadowImage = nil
        UIApplication.shared.statusBarView?.backgroundColor = UIColor(red: 247.0/255.0, green: 247.0/255.0, blue: 247.0/255.0, alpha: 1.0)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if topicAction.contains("true"){
            UserDefaults.standard.setValue("true", forKey: "Followed")
            followedTopic = true
        }
        else{
            UserDefaults.standard.setValue("false", forKey: "Followed")
            followedTopic = false
        }
    }
    
    func getTopic()  {
        topicName = []
        topicAction = []
        topicID = []
//        tableView.reloadData()
        DispatchQueue.main.async {
            let manager = Alamofire.SessionManager.default
            manager.session.configuration.timeoutIntervalForRequest = 120
            
            manager.request( baseURL + "api/Diary/DairyTopicsList/?userid=" + userID, method: .get, parameters: ["":""], encoding: URLEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
                
                switch(response.result) {
                case .success(_):
                    if response.result.value != nil{
//                        //print(response.result.value as Any)
                    }
                    let jsonResults : NSDictionary
                    jsonResults = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! NSDictionary
                    let Success = jsonResults["status"] as! String
                    if Success == "Success"{
                        let DiaryTopicList = jsonResults["DiaryTopicList"] as! NSArray
                        for i in 0 ..< DiaryTopicList.count{
                            let data = DiaryTopicList[i] as! NSDictionary
                            let TopicId = data["TopicId"] as! Int
                            let TopicName = data["TopicName"] as! String
                            let Status = data["Status"] as! Bool
                            self.topicName.append(TopicName)
                            self.topicID.append(String(TopicId))
                            if Status == true{
                                self.topicAction.append("true")
                            }
                            else{
                                self.topicAction.append("false")
                            }
                            
                        }
                        self.tableView.reloadData()
                    }
                    
                    
                    break
                    
                case .failure(_):
                    print(response.error!.localizedDescription)
                    let alert: UIAlertView = UIAlertView(title: "", message: response.error!.localizedDescription, delegate: nil, cancelButtonTitle: "OK");
                    alert.show()
                    self.view.hideActivityView()
                    print(response.result.error as Any)
                    break
                    
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return topicName.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "TopicCell", for: indexPath) as! TopicCell
        cell.topicTitle.text = topicName[indexPath.row]
        if topicAction[indexPath.row] == "true"{
            cell.actionImg.image = UIImage(named: "Following")
        }
        else{
            cell.actionImg.image = UIImage(named: "Follow")
        }
        let tap1 = UITapGestureRecognizer(target: self, action: #selector(self.tapCellTap))
        cell.actionImg.addGestureRecognizer(tap1)
        cell.actionImg.isUserInteractionEnabled = true
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    @objc func tapCellTap(_ sender: UITapGestureRecognizer)  {
        reloadValue = true
        var cell: TopicCell?
        tableView.isUserInteractionEnabled = false
        var view = sender.view
        while view != nil {
            if view is TopicCell {
                cell = view as? TopicCell
            }
            if view is UITableView {
                tableView = view as? UITableView
            }
            view = view?.superview
        }
        
        if let indexPath = (cell != nil) ? tableView?.indexPath(for: cell!) : nil {
            if apicall == true{
                if topicAction[indexPath.row] == "true"{
                    topicAction[indexPath.row] = "false"
                }
                else{
                    topicAction[indexPath.row] = "true"
                }
                self.tableView.reloadData()
                DispatchQueue.main.async {
                    cell?.actionImg.isUserInteractionEnabled = false
                    
                    let manager = Alamofire.SessionManager.default
                    manager.session.configuration.timeoutIntervalForRequest = 120
                    
                    manager.request( baseURL + "api/Diary/AddFollow/?topicid=" + self.topicID[indexPath.row] + "&userid=" + userID, method: .get, parameters: ["":""], encoding: URLEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
                        
                        switch(response.result) {
                        case .success(_):
                            self.apicall = true
                            if response.result.value != nil{
//                                //print(response.result.value as Any)
                            }
//                            self.getTopic()
                            cell?.actionImg.isUserInteractionEnabled = true
                            self.tableView.isUserInteractionEnabled = true
                            break
                            
                        case .failure(_):
                            print(response.error!.localizedDescription)
                            let alert: UIAlertView = UIAlertView(title: "", message: response.error!.localizedDescription, delegate: nil, cancelButtonTitle: "OK");
                            alert.show()
                            self.view.hideActivityView()
                            print(response.result.error as Any)
                            break
                            
                        }
                    }
                }
            }
            else{
                print("api false")
            }
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
