//
//  MyDairiesViewController.swift
//  AyefroInc
//
//  Created by Vijay Darkonde on 06/01/18.
//  Copyright © 2018 Inceptive Consulting Pvt. Ltd. All rights reserved.
//

import UIKit
import Alamofire
import SDWebImage
import MessageUI

class MyDairiesViewController: UIViewController, UITableViewDelegate,UITableViewDataSource , MFMailComposeViewControllerDelegate, emailSend{
    
    var feedEntitySections: [[DairyEntity]] = []
    
    var prototypeEntitiesFromJSON: [DairyEntity] = []
    
    
    @IBOutlet var headerView: UIView!
    @IBOutlet var tableView: UITableView!
    @IBOutlet var bgView: UIView!
    @IBOutlet weak var readMore: UIImageView!
    
    @IBOutlet weak var firstTitleView: UIView!
    @IBOutlet weak var secondTitleView: UIView!
    @IBOutlet weak var blogImageView: UIImageView!
    
    @IBOutlet weak var firstViewHieght: NSLayoutConstraint!
    @IBOutlet weak var secondViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var twitterBtn: UIButton!
    @IBOutlet weak var instaBtn: UIButton!
    @IBOutlet weak var fbBtn: UIButton!
    @IBOutlet weak var linksView: UIView!
    
    @IBOutlet var shortcutView: UIView!
    @IBOutlet var shortcut1Img: UIImageView!
    @IBOutlet var shortcut2Img: UIImageView!
    @IBOutlet var shortcut3Img: UIImageView!
    @IBOutlet var shortcut4Img: UIImageView!
    @IBOutlet var shortcut1Lbl: UILabel!
    @IBOutlet var shortcut2Lbl: UILabel!
    @IBOutlet var shortcut3Lbl: UILabel!
    @IBOutlet var shortcut4Lbl: UILabel!
    
    var fbURL : String! = ""
    var instaURL: String! = ""
    var twitter: String! = ""
    
    var category: [String] = ["All Topics"]
    var categoryID : [String] = ["0"]
    var activeState : [Bool] = []
    var QuestionId: String = ""
    var selectedIndex : Int = 0
    
    let button = UIButton(type: .system)
    
    let statusHeightDefault : CGFloat = 20
    var statusHeight : CGFloat!
    let screenHeight : CGFloat = UIScreen.main.bounds.height
    var labelBottomY : CGFloat!

    
    var refreshControl: UIRefreshControl!
    var statusBarHidden = false
    var popViewController : ContactPopUpViewController!
    var delegate: isDataAvailable!
    var blogURL : String = ""
   
    override func viewDidLoad() {
        super.viewDidLoad()
        _ = ModelManager.instance.getUserLoginStatus()
        
        if Role == 2 {
            //user
            shortcut1Img.image = #imageLiteral(resourceName: "eventSC")
            shortcut2Img.image = #imageLiteral(resourceName: "inspirationSC")
            shortcut3Img.image = #imageLiteral(resourceName: "winPrizesSC")
            shortcut4Img.image = #imageLiteral(resourceName: "reviewSC")
            shortcut1Lbl.text = "Add Event"
            shortcut2Lbl.text = "Inspiration"
            shortcut3Lbl.text = "Win Prizes"
            shortcut4Lbl.text = "Review"
        }
        if Role == 3 {
            //service provider
            shortcut1Img.image = #imageLiteral(resourceName: "eventSC")
            shortcut2Img.image = #imageLiteral(resourceName: "gallerySC")
            shortcut3Img.image = #imageLiteral(resourceName: "winPrizesSC")
            shortcut4Img.image = #imageLiteral(resourceName: "profileSC")
            shortcut1Lbl.text = "Events"
            shortcut2Lbl.text = "Gallery"
            shortcut3Lbl.text = "Win Prizes"
            shortcut4Lbl.text = "Profile"
        }
        
        self.blogImageView.image = nil
        self.readMore.isHidden = true
        self.tableView.register(DairyCell.self, forCellReuseIdentifier: "cell")
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.allowsSelection = true
        self.tableView.fd_debugLogEnabled = true
        bgView.layer.cornerRadius = 3
        
        self.tableView.tableFooterView = UIView()
        
        self.tabBarController?.tabBar.isHidden = false
        
        refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: "Reloading")
        refreshControl.addTarget(self, action: #selector(refresh), for: UIControlEvents.valueChanged)
        tableView.addSubview(refreshControl)
        self.linksView.isHidden = true
        self.tableView.backgroundColor = UIColor.white
        
        _ = ModelManager.instance.getUserLoginStatus()
        let path = Bundle.main.path(forResource: "countryCode", ofType: "json")
        do{
            let data = try Data(contentsOf: URL(fileURLWithPath: path!), options: .mappedIfSafe)
            let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves) as! NSDictionary
            let countries = jsonResult.object(forKey: "countries") as! NSDictionary
            let country = countries.object(forKey: "country") as! NSArray
            for i in 0 ..< country.count
            {
                let countryObj = country[i] as! NSDictionary
                let countryName = countryObj.object(forKey: "countryName") as! String
                if Country == countryName{
                    let currencySymbol = countryObj.object(forKey: "currencySymbol") as! String
                    UserDefaults.standard.setValue(currencySymbol, forKey: "currencySymbol")
                    print("\(UserDefaults.standard.value(forKey: "currencySymbol")!)")
                    break
                }
            }
        }
        catch{
            
        }
        
        let leadingConstraint = NSLayoutConstraint(item: headerView, attribute: NSLayoutAttribute.leading, relatedBy: NSLayoutRelation.equal, toItem: headerView, attribute: NSLayoutAttribute.leading, multiplier: 1.0, constant: 8.0)
        
        let trailingConstraint = NSLayoutConstraint(item: headerView, attribute: NSLayoutAttribute.trailing, relatedBy: NSLayoutRelation.equal, toItem: headerView, attribute: NSLayoutAttribute.trailing, multiplier: 1.0, constant: -8.0)
        
        headerView.addConstraint(leadingConstraint)
        headerView.addConstraint(trailingConstraint)
        
        let topConstraint = NSLayoutConstraint(item: headerView, attribute: NSLayoutAttribute.top, relatedBy: NSLayoutRelation.equal, toItem: headerView, attribute: NSLayoutAttribute.top, multiplier: 1.0, constant: 0)
        
        let bottomConstraint = NSLayoutConstraint(item: headerView, attribute: NSLayoutAttribute.bottom, relatedBy: NSLayoutRelation.equal, toItem: headerView, attribute: NSLayoutAttribute.bottom, multiplier: 1.0, constant: 0)
        
        headerView.addConstraint(topConstraint)
        headerView.addConstraint(bottomConstraint)
        
        
        
        getBlog {
            
        }
        
        buildTestData {
        }
        
        let tapGesture1: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UserHomeViewController.removeView))
        self.view.addGestureRecognizer(tapGesture1)
        
        firstViewHieght.constant = 0.0
        secondViewHeight.constant = 0.0
        self.firstTitleView.isHidden = true
        self.secondTitleView.isHidden = true
        
    }
    
    
    
    @objc func removeView()  {
        linksView.isHidden = true
    }
    
    @objc func backButtonAction() {
        
    }
    
    @objc func tapLabel(gesture: UITapGestureRecognizer) {
        let backItem = UIBarButtonItem()
        backItem.title = "Back"
        backItem.tintColor = UIColor.black
        navigationItem.backBarButtonItem = backItem
        let storyboard =  UIStoryboard(name: "Main", bundle: nil)
        let promo = storyboard.instantiateViewController(withIdentifier: "ForumTermsViewController") as! ForumTermsViewController
        self.navigationController?.pushViewController(promo, animated: true)
    }
    
    
    @objc func showCategories() {
    }
    
   
    override func viewWillAppear(_ animated: Bool) {
        getAllTopic()
        self.tabBarController?.tabBar.isHidden = false
        self.navigationItem.rightBarButtonItem = nil
        
        if reloadValue == true{
            reloadValue = false
            buildTestData {
            }
            getBlog {
                
            }
        }
    }
    
    
    
    override func viewDidLayoutSubviews() {
        self.automaticallyAdjustsScrollViewInsets = false
        
    }
    

    
    func getBlog(then: @escaping () -> Void) {
        // Simulate an async request
        self.tableView.backgroundColor = UIColor.white
        DispatchQueue.main.async {
            
            // Data from `data.json`
            self.prototypeEntitiesFromJSON = []
            self.tableView.reloadData()
            let manager = Alamofire.SessionManager.default
            manager.session.configuration.timeoutIntervalForRequest = 120
            
            manager.request(baseURL + "api/Diary/GetLatestBlog", method: .get, parameters: ["":""], encoding: URLEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
                
                switch(response.result) {
                case .success(_):
                    if response.result.value != nil{
                        self.tableView.backgroundColor = UIColor(red: 230.0/255.0, green: 230.0/255.0, blue: 230.0/255.0, alpha: 1.0)
                        let jsonResults : NSDictionary
                        jsonResults = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! NSDictionary
                        let status = jsonResults["status"] as! String
                        if status == "Success"{
                            let blogList = jsonResults["blogList"] as! NSArray
                            if blogList.count != 0{
                                let dict = blogList[0] as! NSDictionary
                                var BlogImage = dict["BlogImage"] as! String
                                self.blogURL = dict["BlogUrl"] as! String
                                
                                BlogImage = BlogImage.replacingOccurrences(of: " ", with: "%20")
//                                let url1 = NSURL(fileURL:BlogImage)
                                let url = URL(string: BlogImage, relativeTo: nil) //NSURL(string: BlogImage)//NSURL(fileURLWithPath: BlogImage) //
                                let str: String? = Bundle.main.path(forResource: "loading_spinner", ofType: "gif")
                                let fileData = NSData(contentsOfFile: str!)
                                if url != nil{
                                    self.blogImageView.sd_setImage(with: (url! as URL), placeholderImage: UIImage.sd_animatedGIF(with: fileData! as Data!), options: SDWebImageOptions.allowInvalidSSLCertificates, progress: nil, completed: nil)
                                }
                                let tap1 = UITapGestureRecognizer(target: self, action: #selector(self.blogTap))
                                self.blogImageView.addGestureRecognizer(tap1)
                                self.blogImageView.isUserInteractionEnabled = true
                                self.readMore.isHidden = false
                            }
                        }
                    }
                    break
                    
                case .failure(_):
                    print(response.error!.localizedDescription)
                    let alert: UIAlertView = UIAlertView(title: "", message: response.error!.localizedDescription, delegate: nil, cancelButtonTitle: "OK");
                    alert.show()
                    self.view.hideActivityView()
                    print(response.result.error as Any)
                    break
                    
                }
            }
        }
    }
        
    @objc func blogTap(){
        if self.blogURL != ""{
            let backItem = UIBarButtonItem()
            backItem.title = "Back"
            backItem.tintColor = UIColor.black
            navigationItem.backBarButtonItem = backItem
            let storyboard =  UIStoryboard(name: "Main", bundle: nil)
            let promo = storyboard.instantiateViewController(withIdentifier: "BlogViewController") as! BlogViewController
            promo.blogURL = self.blogURL
            self.navigationController?.pushViewController(promo, animated: true)
        }
        
    }
    
    @objc func refresh() {
        // Code to refresh table view
        getBlog {
            
        }
        self.refreshControl.endRefreshing()
        self.prototypeEntitiesFromJSON = []
        self.tableView.reloadData()
        DispatchQueue.global().async {
            // Data from `data.json`
            let manager = Alamofire.SessionManager.default
            manager.session.configuration.timeoutIntervalForRequest = 120
            
            manager.request(baseURL + "api/Diary/GetDImgList/?userid=" + userID, method: .get, parameters: ["":""], encoding: URLEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
                
                switch(response.result) {
                case .success(_):
                    if response.result.value != nil{
                        let jsonResults : NSDictionary
                        jsonResults = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! NSDictionary
                        let diaryForumList = jsonResults["diaryForumList"] as! NSArray
                        let followed = UserDefaults.standard.string(forKey: "Followed")
                        if diaryForumList.count == 0 && followed == "false"{
                            self.firstViewHieght.constant = 0.0
                            self.secondViewHeight.constant = 211.0
                            self.firstTitleView.isHidden = true
                            self.secondTitleView.isHidden = false
                            self.headerView.frame.size.height = 211 + 40 + 217
                            self.delegate.dairiesData(count:  diaryForumList.count)
                        }
                        else{
                            self.firstViewHieght.constant = 93.0
                            self.secondViewHeight.constant = 0.0
                            self.firstTitleView.isHidden = false
                            self.secondTitleView.isHidden = true
                            self.headerView.frame.size.height = 93  + 40 + 217
                            self.delegate.dairiesData(count:  diaryForumList.count)
                        }
                        let jsonArr = NSMutableArray()
                        for i in 0 ..< diaryForumList.count{
                            let dicWithoutNulls = diaryForumList[i] as! NSDictionary
                            
                            let outputDict = self.removeNSNull(from: dicWithoutNulls.mutableCopy() as! NSMutableDictionary)
                            
                            jsonArr.add(outputDict)
                        }
                        
                        let arr = NSArray(array: jsonArr)
                        for i in 0 ..< arr.count{
                            self.prototypeEntitiesFromJSON.append(DairyEntity(dictionary: arr[i] as! [AnyHashable : Any] ))
                        }
                        
                    }
                    self.tableView.reloadData()
                    
                    break
                    
                case .failure(_):
                    print(response.error!.localizedDescription)
                    let alert: UIAlertView = UIAlertView(title: "", message: response.error!.localizedDescription, delegate: nil, cancelButtonTitle: "OK");
                    alert.show()
                    self.view.hideActivityView()
                    print(response.result.error as Any)
                    break
                    
                }
            }
        }
    }
    
    
    func buildTestData(then: @escaping () -> Void) {
        // Simulate an async request
        self.tableView.backgroundColor = UIColor.white
        DispatchQueue.main.async {
            
            // Data from `data.json`
            self.prototypeEntitiesFromJSON = []
//            self.tableView.reloadData()
            let manager = Alamofire.SessionManager.default
            manager.session.configuration.timeoutIntervalForRequest = 120
            
            manager.request(baseURL + "api/Diary/GetDImgList/?userid=" + userID, method: .get, parameters: ["":""], encoding: URLEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
                
                switch(response.result) {
                case .success(_):
                    if response.result.value != nil{
                        self.tableView.backgroundColor = UIColor(red: 230.0/255.0, green: 230.0/255.0, blue: 230.0/255.0, alpha: 1.0)
                        let jsonResults : NSDictionary
                        jsonResults = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! NSDictionary
                        let status = jsonResults["status"] as! String
                        if status == "Success"
                        {
                            let diaryForumList = jsonResults["diaryForumList"] as! NSArray
                            let followed = UserDefaults.standard.string(forKey: "Followed")
                            if diaryForumList.count == 0 && followed == "false"{
                                self.firstViewHieght.constant = 0.0
                                self.secondViewHeight.constant = 211.0
                                self.firstTitleView.isHidden = true
                                self.secondTitleView.isHidden = false
                                self.headerView.frame.size.height = 211 + 40 + 217
                                self.delegate.dairiesData(count: diaryForumList.count)
                            }
                            else{
                                self.firstViewHieght.constant = 93.0
                                self.secondViewHeight.constant = 0.0
                                self.firstTitleView.isHidden = false
                                self.secondTitleView.isHidden = true
                                self.headerView.frame.size.height = 93  + 40 + 217
                                self.delegate.dairiesData(count: diaryForumList.count)
                            }
                            let jsonArr = NSMutableArray()
                            for i in 0 ..< diaryForumList.count{
                                let dicWithoutNulls = diaryForumList[i] as! NSDictionary
                                
                                let outputDict = self.removeNSNull(from: dicWithoutNulls.mutableCopy() as! NSMutableDictionary)
                                
                                jsonArr.add(outputDict)
                            }
                            
                            let arr = NSArray(array: jsonArr)
                            for i in 0 ..< arr.count{
                                self.prototypeEntitiesFromJSON.append(DairyEntity(dictionary: arr[i] as! [AnyHashable : Any] ))
                            }
                        }
                        else{
                            self.firstViewHieght.constant = 0.0
                            self.secondViewHeight.constant = 211.0
                            self.firstTitleView.isHidden = true
                            self.secondTitleView.isHidden = false
                            self.headerView.frame.size.height = 211 + 40 + 217
                            self.delegate.dairiesData(count: 0)
                        }
                    }
                    self.tableView.reloadData()
                    break
                    
                case .failure(_):
                    print(response.error!.localizedDescription)
                    let alert: UIAlertView = UIAlertView(title: "", message: response.error!.localizedDescription, delegate: nil, cancelButtonTitle: "OK");
                    alert.show()
                    self.view.hideActivityView()
                    print(response.result.error as Any)
                    break
                    
                }
            }
        }
    }
    
    func updateList(topicID: String)  {
        DispatchQueue.main.async {
            self.prototypeEntitiesFromJSON = []
            let manager = Alamofire.SessionManager.default
            manager.session.configuration.timeoutIntervalForRequest = 120
            manager.request(baseURL + "api/Diary/GetDImgList/?userid=" + userID , method: .get, parameters: ["":""], encoding: URLEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
                
                switch(response.result) {
                case .success(_):
                    if response.result.value != nil{
                        let jsonResults : NSDictionary
                        jsonResults = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! NSDictionary
                        let status = jsonResults["status"] as! String
                        if status == "Success"
                        {
                            let diaryForumList = jsonResults["diaryForumList"] as! NSArray
                            let followed = UserDefaults.standard.string(forKey: "Followed")
                            if diaryForumList.count == 0 && followed == "false"{
                                self.firstViewHieght.constant = 0.0
                                self.secondViewHeight.constant = 211.0
                                self.firstTitleView.isHidden = true
                                self.secondTitleView.isHidden = false
                                self.headerView.frame.size.height = 211 + 40 + 217
                                self.delegate.dairiesData(count: diaryForumList.count)
                            }
                            else{
                                self.firstViewHieght.constant = 93.0
                                self.secondViewHeight.constant = 0.0
                                self.firstTitleView.isHidden = false
                                self.secondTitleView.isHidden = true
                                self.headerView.frame.size.height = 93  + 40 + 217
                                self.delegate.dairiesData(count: diaryForumList.count)
                            }
                            let jsonArr = NSMutableArray()
                            for i in 0 ..< diaryForumList.count{
                                let dicWithoutNulls = diaryForumList[i] as! NSDictionary
                                
                                let outputDict = self.removeNSNull(from: dicWithoutNulls.mutableCopy() as! NSMutableDictionary)
                                
                                jsonArr.add(outputDict)
                            }
                            
                            let arr = NSArray(array: jsonArr)
                            for i in 0 ..< arr.count{
                                self.prototypeEntitiesFromJSON.append(DairyEntity(dictionary: arr[i] as! [AnyHashable : Any] ))
                            }
                        }
                        else{
                            self.firstViewHieght.constant = 0.0
                            self.secondViewHeight.constant = 211.0
                            self.firstTitleView.isHidden = true
                            self.secondTitleView.isHidden = false
                            self.headerView.frame.size.height = 211 + 40 + 217
                            self.delegate.dairiesData(count: 0)
                        }
                        
                        
                    }
                    self.tableView.reloadData()
                    self.view.hideActivityView()
                    break
                    
                case .failure(_):
                    print(response.error!.localizedDescription)
                    let alert: UIAlertView = UIAlertView(title: "", message: response.error!.localizedDescription, delegate: nil, cancelButtonTitle: "OK");
                    alert.show()
                    self.view.hideActivityView()
                    print(response.result.error as Any)
                    break
                    
                }
            }
        }
    }
    
    func removeNSNull(from dict: NSMutableDictionary) -> NSDictionary {
        let mutableDict = dict
        let keysWithEmptString = dict.filter { $0.1 is NSNull }.map { $0.0 }
        for key in keysWithEmptString {
            mutableDict[key] = ""
        }
        
        return mutableDict
    }
    
    // MARK: - UITableViewDataSource
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return prototypeEntitiesFromJSON.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DairyCell") as? DairyCell
        if prototypeEntitiesFromJSON.count != 0{
            configureCell(cell!, at: indexPath)
        }
        
        return cell!
    }
    
    // MARK: - UITableViewDelegate
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return tableView.fd_heightForCell(withIdentifier: "DairyCell") { cell in
            self.configureCell(cell as! DairyCell, at: indexPath)
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        
        
    }
    
    
    func configureCell(_ cell: DairyCell, at indexPath: IndexPath) {
        if prototypeEntitiesFromJSON.count != 0{
            cell.entity = prototypeEntitiesFromJSON[indexPath.row]
            cell.contentImageView.tag = indexPath.row
            let tap = UITapGestureRecognizer(target: self, action: #selector(self.onTapImage))
            cell.contentImageView.addGestureRecognizer(tap)
            cell.contentImageView.isUserInteractionEnabled = true
            
            let tap1 = UITapGestureRecognizer(target: self, action: #selector(self.tapCellTap))
            cell.addGestureRecognizer(tap1)
            cell.isUserInteractionEnabled = true
            
            let tap3 = UITapGestureRecognizer(target: self, action: #selector(self.tapCellTap))
            cell.commentbtn.addGestureRecognizer(tap3)
            cell.commentbtn.isUserInteractionEnabled = true
            
            let tap2 = UITapGestureRecognizer(target: self, action: #selector(self.tapOnlike))
            cell.likeBtn.isUserInteractionEnabled = true
            cell.likeBtn.addGestureRecognizer(tap2)
        }
    }
    
    @objc func tapOnlike(_ sender: UITapGestureRecognizer) {
        let backItem = UIBarButtonItem()
        backItem.title = "Back"
        backItem.tintColor = UIColor.black
        navigationItem.backBarButtonItem = backItem
        var cell: DairyCell?
        var view = sender.view
        while view != nil {
            if view is DairyCell {
                cell = view as? DairyCell
            }
            if view is UITableView {
                tableView = view as? UITableView
            }
            view = view?.superview
        }
        
        if let indexPath = (cell != nil) ? tableView?.indexPath(for: cell!) : nil {
            if prototypeEntitiesFromJSON.count != 0{
                let type = prototypeEntitiesFromJSON[indexPath.row].type as String
                if type == "Diary"{
                    self.QuestionId = String(self.prototypeEntitiesFromJSON[indexPath.row].imageId)
                    let likeValue = self.prototypeEntitiesFromJSON[indexPath.row].dImgliked
                    if likeValue == true{
                        self.prototypeEntitiesFromJSON[indexPath.row].dImgliked = false
                        self.prototypeEntitiesFromJSON[indexPath.row].dImglikeCount = self.prototypeEntitiesFromJSON[indexPath.row].dImglikeCount - 1
                    }
                    else{
                        self.prototypeEntitiesFromJSON[indexPath.row].dImgliked = true
                        self.prototypeEntitiesFromJSON[indexPath.row].dImglikeCount = self.prototypeEntitiesFromJSON[indexPath.row].dImglikeCount + 1
                    }
                    self.tableView.reloadData()
                    DispatchQueue.main.async {
                        
                        
                        let parameters : [String: String] = [
                            "User_Id": userID,
                            "DImg_Id":  self.QuestionId,
                            "DImgCmt_Id":"0",
                            "DImgRly_Id":"0"
                        ]
                        
                        let manager = Alamofire.SessionManager.default
                        manager.session.configuration.timeoutIntervalForRequest = 120
                        
                        manager.request(baseURL + "api/Diary/DImgLike", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
                            
                            switch(response.result) {
                            case .success(_):
                                if response.result.value != nil{
                                    //print(response.result.value as Any)
                                }
//                                self.buildTestData {
//
//                                }
                                break
                                
                            case .failure(_):
                                print(response.error!.localizedDescription)
                                let alert: UIAlertView = UIAlertView(title: "", message: response.error!.localizedDescription, delegate: nil, cancelButtonTitle: "OK");
                                alert.show()
                                self.view.hideActivityView()
                                print(response.result.error as Any)
                                break
                                
                            }
                        }
                    }
                }
                else{
                    self.handleAdvertise(index: indexPath.row)
                }
            }
        }
    }
    
    
    @objc func tapCellTap(_ sender: UITapGestureRecognizer) {
        let backItem = UIBarButtonItem()
        backItem.title = "Back"
        backItem.tintColor = UIColor.black
        navigationItem.backBarButtonItem = backItem
        var cell: DairyCell?
        var view = sender.view
        while view != nil {
            if view is DairyCell {
                cell = view as? DairyCell
            }
            if view is UITableView {
                tableView = view as? UITableView
            }
            view = view?.superview
        }
        
        if let indexPath = (cell != nil) ? tableView?.indexPath(for: cell!) : nil {
            let backItem = UIBarButtonItem()
            backItem.title = "Back"
            backItem.tintColor = UIColor.black
            navigationItem.backBarButtonItem = backItem
            if prototypeEntitiesFromJSON.count != 0{
                let type = prototypeEntitiesFromJSON[indexPath.row].type as String
                if type == "Diary"{
                    self.QuestionId = String(prototypeEntitiesFromJSON[indexPath.row].imageId)
                    let vendorID = String(prototypeEntitiesFromJSON[indexPath.row].dImageUser)
                    let viewTitle = prototypeEntitiesFromJSON[indexPath.row].dTopics
                    let destinationVC = storyboard?.instantiateViewController(withIdentifier: "DairyDetailViewController") as! DairyDetailViewController
                    destinationVC.QuestionId = self.QuestionId
//                    destinationVC.viewtitle = viewTitle!
//                    destinationVC.vendorID = vendorID
                    self.navigationController?.pushViewController(destinationVC, animated: true)
                }
                else{
                    self.handleAdvertise(index: indexPath.row)
                }
            }
        }
        
        
    }
    
    @objc func tapOnProfilePic(_ sender: UITapGestureRecognizer) {
        let backItem = UIBarButtonItem()
        backItem.title = "Back"
        backItem.tintColor = UIColor.black
        navigationItem.backBarButtonItem = backItem
        var cell: DairyCell?
        var view = sender.view
        while view != nil {
            if view is DairyCell {
                cell = view as? DairyCell
            }
            if view is UITableView {
                tableView = view as? UITableView
            }
            view = view?.superview
        }
        
        if let indexPath = (cell != nil) ? tableView?.indexPath(for: cell!) : nil {
            let backItem = UIBarButtonItem()
            backItem.title = "Back"
            backItem.tintColor = UIColor.black
            navigationItem.backBarButtonItem = backItem
            let storyboard =  UIStoryboard(name: "Main", bundle: nil)
            let promo = storyboard.instantiateViewController(withIdentifier: "VendorDetailsViewController") as! VendorDetailsViewController
            promo.vendorID = self.prototypeEntitiesFromJSON[indexPath.row].value(forKey: "AnsSendUser") as! String
            promo.poptoRoot = true
            self.navigationController?.pushViewController(promo, animated: true)
        }
    }
    
    
    
    
    @objc func onTapImage(_ sender: UITapGestureRecognizer) {
        let backItem = UIBarButtonItem()
        backItem.title = "Back"
        backItem.tintColor = UIColor.black
        navigationItem.backBarButtonItem = backItem
        var cell: DairyCell?
        var view = sender.view
        while view != nil {
            if view is DairyCell {
                cell = view as? DairyCell
            }
            if view is UITableView {
                tableView = view as? UITableView
            }
            view = view?.superview
        }
        
        if let indexPath = (cell != nil) ? tableView?.indexPath(for: cell!) : nil {
            let type = prototypeEntitiesFromJSON[indexPath.row].type as String
            if type == "Diary"{
                let imageURL = prototypeEntitiesFromJSON[indexPath.row].image as String
                let storyboard =  UIStoryboard(name: "Main", bundle: nil)
                let promo = storyboard.instantiateViewController(withIdentifier: "ImageDisplayViewController") as! ImageDisplayViewController
                print(imageURL)
                promo.imageURL = imageURL
                promo.deleteShow = false
                self.navigationController?.pushViewController(promo, animated: true)
            }
            else{
                self.handleAdvertise(index: indexPath.row)
            }
        }
    }
    
    
    
    @IBAction func createNewTopicBtnClk(_ sender: UIButton) {
        let backItem = UIBarButtonItem()
        backItem.title = "Back"
        backItem.tintColor = UIColor.black
        navigationItem.backBarButtonItem = backItem
        let storyboard =  UIStoryboard(name: "Main", bundle: nil)
        let promo = storyboard.instantiateViewController(withIdentifier: "AddDairyPostViewController") as! AddDairyPostViewController
        self.navigationController?.pushViewController(promo, animated: true)
    }
    
    @IBAction func addTopicBtnClk(_ sender: UIButton) {
        let backItem = UIBarButtonItem()
        backItem.title = "Back"
        backItem.tintColor = UIColor.black
        navigationItem.backBarButtonItem = backItem
        let storyboard =  UIStoryboard(name: "Main", bundle: nil)
        let promo = storyboard.instantiateViewController(withIdentifier: "AddTopicViewController") as! AddTopicViewController
        self.navigationController?.pushViewController(promo, animated: true)
    }
    
    
    func getAllTopic()  {
        self.category = ["All Topics"]
        self.categoryID = ["0"]
        DispatchQueue.main.async {
            let manager = Alamofire.SessionManager.default
            manager.session.configuration.timeoutIntervalForRequest = 120
            
            manager.request( baseURL + "api/TopicCategories/GetAllTopic", method: .get, parameters: ["":""], encoding: URLEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
                
                switch(response.result) {
                case .success(_):
                    if response.result.value != nil{
                        //print(response.result.value as Any)
                    }
                    let jsonResults : NSArray
                    jsonResults = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! NSArray
                    for i in 0 ..< jsonResults.count{
                        let data = jsonResults[i] as! NSDictionary
                        let categoryID = data["Id"] as! Int
                        let CategoryTitle = data["CategoryTitle"] as! String
                        let Active = data["Active"] as! Bool
                        if Active == true{
                            self.category.append(CategoryTitle)
                            self.categoryID.append(String(categoryID))
                        }
                    }
                    
                    break
                    
                case .failure(_):
                    print(response.error!.localizedDescription)
                    let alert: UIAlertView = UIAlertView(title: "", message: response.error!.localizedDescription, delegate: nil, cancelButtonTitle: "OK");
                    alert.show()
                    self.view.hideActivityView()
                    print(response.result.error as Any)
                    break
                    
                }
            }
        }
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let backItem = UIBarButtonItem()
        backItem.title = "Back"
        backItem.tintColor = UIColor.black
        navigationItem.backBarButtonItem = backItem
        if (segue.identifier == "CreateTopic") {
            let destinationVC = segue.destination as! CreateTopicViewController
            print(self.categoryID)
            print(self.category)
            destinationVC.categoryID = self.categoryID
            destinationVC.category = self.category
        }
        if (segue.identifier == "ForumDetail") {
            if let indexPath = tableView.indexPathForSelectedRow{
                let selectedRow = indexPath.row
                self.QuestionId = String(prototypeEntitiesFromJSON[selectedRow].imageId)
                let viewTitle = prototypeEntitiesFromJSON[selectedRow].dTopics
                let destinationVC = segue.destination as! DairyDetailViewController
                destinationVC.QuestionId = self.QuestionId
//                destinationVC.viewtitle = viewTitle!
            }
            
        }
        
        // This will show in the next view controller being pushed
    }
    
    func handleAdvertise(index: Int)  {
        let promotionAim = prototypeEntitiesFromJSON[index].promotionAim as String
        if promotionAim == "Increase brand Awareness"{
            let backItem = UIBarButtonItem()
            backItem.title = "Back"
            backItem.tintColor = UIColor.black
            navigationItem.backBarButtonItem = backItem
            let storyboard =  UIStoryboard(name: "Main", bundle: nil)
            let promo = storyboard.instantiateViewController(withIdentifier: "VendorDetailsViewController") as! VendorDetailsViewController
            promo.vendorID = prototypeEntitiesFromJSON[index].providerId as String
            promo.poptoRoot = true
            self.navigationController?.pushViewController(promo, animated: true)
        }
        if promotionAim == "Get more website visits"{
            var websiteList = prototypeEntitiesFromJSON[index].websiteLink as String
            if websiteList != ""{
                let str = websiteList.contains(find: "http://")
                if str == false{
                    websiteList = "http://" + websiteList
                }
                guard let url = URL(string: websiteList) else {
                    return //be safe
                }
                
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
            else{
                let alert: UIAlertView = UIAlertView(title: "", message: "Website link not updated by service provider", delegate: nil, cancelButtonTitle: "OK");
                alert.show()
            }
        }
        if promotionAim == "Get more calls for your business"{
            self.tableView.isUserInteractionEnabled = false
            DispatchQueue.main.async {
                let manager = Alamofire.SessionManager.default
                manager.session.configuration.timeoutIntervalForRequest = 120
                
                let id = self.prototypeEntitiesFromJSON[index].providerId as String
                manager.request( baseURL + "api/Albums/GetContactDetails/?providerId=" + id, method: .get, parameters: ["":""], encoding: URLEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
                    
                    switch(response.result) {
                    case .success(_):
                        if response.result.value != nil{
                            //print(response.result.value as Any)
                        }
                        
                        let jsonResults : NSArray
                        jsonResults = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! NSArray
                        let dict = jsonResults[0] as! NSDictionary
                        let phoneNumber = dict["phoneNumber"] as! String
                        let email = dict["email"] as! String
                        
                        self.popViewController = self.storyboard?.instantiateViewController(withIdentifier: "ContactPopUpViewController")as! ContactPopUpViewController
                        self.popViewController.phoneNum = phoneNumber
                        self.popViewController.emailID = email
                        self.popViewController.delegate = self
                        self.popViewController.showInView(self.view, animated: true)
                        self.tableView.isUserInteractionEnabled = true
                        break
                        
                    case .failure(_):
                        if response.error?.localizedDescription == "The Internet connection appears to be offline."
                        {
                            let alert: UIAlertView = UIAlertView(title: "", message: response.error!.localizedDescription, delegate: nil, cancelButtonTitle: "OK");
                            alert.show()
                        }
                        else{
                            let alert: UIAlertView = UIAlertView(title: "", message: response.error!.localizedDescription, delegate: nil, cancelButtonTitle: "OK");
                            alert.show()
                            print(response.result.error as Any)
                        }
                        break
                        
                    }
                }
            }
        }
        if promotionAim == "Promote On Social Media"{
            self.linksView.isHidden = false
            self.fbURL = self.prototypeEntitiesFromJSON[index].socialMedia_facebook as String
            self.instaURL = self.prototypeEntitiesFromJSON[index].socialMedia_Instagram as String
            self.twitter = self.prototypeEntitiesFromJSON[index].socialMedia_twitter as String
            if self.fbURL == ""{
                self.fbBtn.setImage(UIImage(named:"fbgray"), for: UIControlState.normal)
            }
            else{
                self.fbBtn.setImage(UIImage(named:"facebook"), for: UIControlState.normal)
            }
            if self.instaURL == ""{
                self.instaBtn.setImage(UIImage(named:"instagray"), for: UIControlState.normal)
            }
            else{
                self.instaBtn.setImage(UIImage(named:"instagram-1"), for: UIControlState.normal)
            }
            if self.twitter == ""{
                self.twitterBtn.setImage(UIImage(named:"twittergray"), for: UIControlState.normal)
            }
            else{
                self.twitterBtn.setImage(UIImage(named:"twitter"), for: UIControlState.normal)
            }
            
            
        }
    }
    
    func emailBox(email: String){
        if MFMailComposeViewController.canSendMail() {
            let toRecipients : [String] = [email]
            let mc: MFMailComposeViewController = MFMailComposeViewController()
            mc.mailComposeDelegate = self
            mc.setToRecipients(toRecipients)
            mc.isNavigationBarHidden = true
            self.present(mc, animated: false, completion: nil)
        }
        else
        {
            let alert: UIAlertView = UIAlertView(title: "", message: "Mail app not install on your device. Please send an email to info@ayefroinc.com.com", delegate: nil, cancelButtonTitle: "OK");
            alert.show()
        }
    }
    
    @IBAction func fbBtnClk(_ sender: UIButton) {
        self.linksView.isHidden = true
        if fbURL != ""{
            let str = fbURL.contains(find: "http://")
            if str == false{
                fbURL = "http://" + fbURL
            }
            guard let url = URL(string:  fbURL) else {
                return //be safe
            }
            
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
        else{
            let alert: UIAlertView = UIAlertView(title: "", message: "No social media channel yet for Facebook", delegate: nil, cancelButtonTitle: "OK");
            alert.show()
        }
    }
    
    @IBAction func instaBtnClk(_ sender: UIButton) {
        self.linksView.isHidden = true
        if instaURL != ""{
            let str = instaURL.contains(find: "http://")
            if str == false{
                instaURL = "http://" + instaURL
            }
            
            guard let url = URL(string: instaURL) else {
                return //be safe
            }
            
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
        else{
            let alert: UIAlertView = UIAlertView(title: "", message: "No social media channel yet for Instagram", delegate: nil, cancelButtonTitle: "OK");
            alert.show()
        }
    }
    
    @IBAction func twitterBtnClk(_ sender: UIButton) {
        self.linksView.isHidden = true
        if twitter != ""{
            let str = twitter.contains(find: "http://")
            if str == false{
                twitter = "http://" + twitter
            }
            guard let url = URL(string: twitter) else {
                return //be safe
            }
            
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
        else{
            let alert: UIAlertView = UIAlertView(title: "", message: "No social media channel yet for Twitter", delegate: nil, cancelButtonTitle: "OK");
            alert.show()
        }
    }
    
    @IBAction func likeBtnClk(_ sender: UIButton) {
    }
    
    // MARK: - Shortcuts
    @IBAction func shortcut1BtnClk(_ sender: Any) {
        if Role == 2 {
            //user - Add event screen
            print("user - Add event screen")
            let storyboard =  UIStoryboard(name: "Main", bundle: nil)
            let promo = storyboard.instantiateViewController(withIdentifier: "UserAddEventViewController") as! UserAddEventViewController
            self.navigationController?.pushViewController(promo, animated: true)
        }
        if Role == 3 {
            //service provider - Event dashboard
            print("service provider - Event dashboard")
            let storyboard =  UIStoryboard(name: "Main", bundle: nil)
            let promo = storyboard.instantiateViewController(withIdentifier: "EventsViewController") as! EventsViewController
            self.navigationController?.pushViewController(promo, animated: true)
        }
    }
    
    @IBAction func shortcut2BtnClk(_ sender: Any) {
        if Role == 2 {
            //user - Gallery screen
            print("user - Gallery screen")
            let storyboard =  UIStoryboard(name: "Main", bundle: nil)
            let promo = storyboard.instantiateViewController(withIdentifier: "BrowesGalleryViewController") as! BrowesGalleryViewController
            self.navigationController?.pushViewController(promo, animated: true)
        }
        if Role == 3 {
            //service provider - Vendor gallery
            print("service provider - Vendor gallery")
            let storyboard =  UIStoryboard(name: "Main", bundle: nil)
            let promo = storyboard.instantiateViewController(withIdentifier: "SPGalleryViewController") as! SPGalleryViewController
            self.navigationController?.pushViewController(promo, animated: true)
        }
    }
    
    @IBAction func shortcut3BtnClk(_ sender: Any) {
        let storyboard =  UIStoryboard(name: "Main", bundle: nil)
        let promo = storyboard.instantiateViewController(withIdentifier: "ReferEarnViewController") as! ReferEarnViewController
        self.navigationController?.pushViewController(promo, animated: true)
    }
    
    @IBAction func shortcut4BtnClk(_ sender: Any) {
        if Role == 2 {
            //user - vendor screen
            print("user - vendor screen")
            self.tabBarController?.selectedIndex = 1
        }
        if Role == 3 {
            //service provider - Vendor profile
            print("service provider - Vendor profile")
            let storyboard =  UIStoryboard(name: "Main", bundle: nil)
            let promo = storyboard.instantiateViewController(withIdentifier: "SPProfileViewController") as! SPProfileViewController
            self.navigationController?.pushViewController(promo, animated: true)
        }
    }
}




