//
//  DairyDetailViewController.swift
//  AyefroInc
//
//  Created by Vijay Darkonde on 09/01/18.
//  Copyright © 2018 Inceptive Consulting Pvt. Ltd. All rights reserved.
//

import UIKit
import Alamofire
import PHFComposeBarView
import IQKeyboardManagerSwift
import SystemConfiguration

class DairyDetailViewController: UIViewController, UITableViewDelegate,UITableViewDataSource, UIImagePickerControllerDelegate, UINavigationControllerDelegate, PHFComposeBarViewDelegate, editComment {
    
    
    @IBOutlet weak var bottomSpace: NSLayoutConstraint!
    @IBOutlet var tableView: UITableView!
    var QuestionId : String = ""
    var viewtitle : String = ""
    
    var feedEntitySections: [[ImageCommentEntity]] = []
    
    var prototypeEntitiesFromJSON: [ImageCommentEntity] = []
    
    var popViewController : DairyReportViewController!
    var popViewController1 : EditDairyCommentController!
    
    @IBOutlet weak var tagsCountLbl: UILabel!
    @IBOutlet var profilePic: UIImageView!
    @IBOutlet var titleLbl: UILabel!
    @IBOutlet var activityLbl: UILabel!
    @IBOutlet var desciptionLbl: UILabel!
    @IBOutlet var topicImage: UIImageView!
    @IBOutlet var likeImage: UIImageView!
    @IBOutlet var reportImage: UIImageView!
    @IBOutlet var notificationBtn: UIButton!
    @IBOutlet var likeLbl: UILabel!
    @IBOutlet var reportLbl: UILabel!
    @IBOutlet var bgView: UIView!
    @IBOutlet var viewCell: UIView!
    @IBOutlet var likeBtn: UIButton!
    @IBOutlet var reportBtn: UIButton!
    @IBOutlet weak var commentCountLbl: UILabel!
    
    
    
    var composeBarView = PHFComposeBarView()
    var NotificationOn : Bool!
    var vendorID : String! = ""
    var imageView: UIImageView!
    var discussionImage : String! = ""
    
    var base64Str : String! = ""
    var category: [String] = ["Reply","Edit","Delete","Hide"]
    var menuOptionImageNameArray : [String] = []
    let imagePicker = UIImagePickerController()
    var answer_id : Int = 0
    
    
    var screenRect = UIScreen.main.bounds
    var coverView = UIView()
    var footerValue : String = ""
    var Anstext : String = ""
    var refreshControl: UIRefreshControl!
    var action : String! = ""
    
    var isImageLiked: Bool = false
    var likeCount: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        coverView = UIView(frame: screenRect)
        self.title = viewtitle
        self.tabBarController?.tabBar.isHidden = true
        imagePicker.delegate = self
        self.navigationController?.navigationBar.titleTextAttributes = [ NSAttributedStringKey.font: UIFont.boldSystemFont(ofSize: 14.0)]
        self.tableView.register(ImageCommentCell.self, forCellReuseIdentifier: "cell")
        
        self.tableView.fd_debugLogEnabled = true
        if UIScreen.main.bounds.size.width == 414
        {
            self.shyNavBarManager.scrollView = self.tableView;
        }
        else if UIScreen.main.bounds.size.width == 375 && UIScreen.main.bounds.size.height == 667
        {
            self.shyNavBarManager.scrollView = self.tableView;
        }
        else if UIScreen.main.bounds.size.width == 320 && UIScreen.main.bounds.size.height == 568
        {
            self.shyNavBarManager.scrollView = self.tableView;
        }
        else if UIScreen.main.bounds.size.width == 320 && UIScreen.main.bounds.size.height == 480
        {
            
        }
        self.tableView.tableFooterView = UIView()
        
        let viewBounds = self.view.bounds
        let frame = CGRect(x:0.0, y:viewBounds.size.height - PHFComposeBarViewInitialHeight, width:viewBounds.size.width, height:PHFComposeBarViewInitialHeight)
        composeBarView = PHFComposeBarView(frame: frame)
        if UIScreen.main.bounds.size.width == 375 && UIScreen.main.bounds.size.height == 812
        {
            let frame = CGRect(x:0.0, y:viewBounds.size.height - PHFComposeBarViewInitialHeight-40, width:viewBounds.size.width, height:PHFComposeBarViewInitialHeight)
            composeBarView = PHFComposeBarView(frame: frame)
            bottomSpace.constant = 84
        }
        composeBarView.textView.autocorrectionType = UITextAutocorrectionType.no
        composeBarView.maxCharCount = 500
        composeBarView.maxLinesCount = 5
        composeBarView.placeholder = "Enter Comment Here"
        composeBarView.utilityButtonImage = nil
        composeBarView.delegate = self
        composeBarView.placeholderLabel.accessibilityIdentifier = "Placeholder"
        composeBarView.textView.accessibilityIdentifier = "Input"
        composeBarView.utilityButtonImage = nil
        composeBarView.button.accessibilityIdentifier = "Submit"
        composeBarView.buttonTitle = "SUBMIT"
        composeBarView.textView.autocorrectionType = UITextAutocorrectionType.default
        composeBarView.utilityButton.accessibilityIdentifier = "Utility"
        composeBarView.textView.font = UIFont(name: "Helvetica Neue", size: 15)!
        self.view.addSubview(composeBarView)
        composeBarView.textView.addDoneOnKeyboardWithTarget(self, action: #selector(self.onDoneBtnClk))
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.onTap))
        self.topicImage.addGestureRecognizer(tap)
        self.topicImage.isUserInteractionEnabled = true
        profilePic.layer.cornerRadius = profilePic.frame.height/2
        profilePic.layer.masksToBounds = true
        self.topicImage.contentMode = .scaleAspectFit
        
        let config = FTConfiguration.shared
        config.textColor = UIColor.black
        config.backgoundTintColor = UIColor.white
        config.borderColor = UIColor.lightGray
        config.menuWidth = self.view.frame.width - 10
        config.menuSeparatorColor = UIColor.lightGray
        config.textAlignment = .left
        config.textFont = UIFont(name: "Helvetica Neue", size: 17)!
        config.menuRowHeight = 50
        config.cornerRadius = 6
        
        let tapGesture1: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(ForumDetailViewController.removeView))
        self.view.addGestureRecognizer(tapGesture1)
        
        let button = UIButton(type: .system)
        button.frame.size.height = 50
        button.setTitle(" Back      ", for: .normal)
        button.setImage(UIImage(named: "backbtn"), for: .normal)
        button.addTarget(self, action: #selector(reset), for: .touchUpInside)
        button.tintColor = UIColor.black
        button.contentHorizontalAlignment = .left
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: button)
        
        refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: "Reloading")
        refreshControl.addTarget(self, action: #selector(reloadView), for: UIControlEvents.valueChanged)
        tableView.addSubview(refreshControl)
        
        let leadingConstraint = NSLayoutConstraint(item: viewCell, attribute: NSLayoutAttribute.leading, relatedBy: NSLayoutRelation.equal, toItem: viewCell, attribute: NSLayoutAttribute.leading, multiplier: 1.0, constant: 8.0)
        
        let trailingConstraint = NSLayoutConstraint(item: viewCell, attribute: NSLayoutAttribute.trailing, relatedBy: NSLayoutRelation.equal, toItem: viewCell, attribute: NSLayoutAttribute.trailing, multiplier: 1.0, constant: -8.0)
        
        viewCell.addConstraint(leadingConstraint)
        viewCell.addConstraint(trailingConstraint)
        
        let topConstraint = NSLayoutConstraint(item: viewCell, attribute: NSLayoutAttribute.top, relatedBy: NSLayoutRelation.equal, toItem: viewCell, attribute: NSLayoutAttribute.top, multiplier: 1.0, constant: 0)
        
        let bottomConstraint = NSLayoutConstraint(item: viewCell, attribute: NSLayoutAttribute.bottom, relatedBy: NSLayoutRelation.equal, toItem: viewCell, attribute: NSLayoutAttribute.bottom, multiplier: 1.0, constant: 0)
        
        viewCell.addConstraint(topConstraint)
        viewCell.addConstraint(bottomConstraint)
        
        self.notificationBtn.isHidden = true
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        buildTestData {
            
        }
    }
    
    override func viewDidLayoutSubviews() {
        self.automaticallyAdjustsScrollViewInsets = false
    }
    
    // MARK: - On Tap Methods
    @objc func onTap(_ sender: UITapGestureRecognizer) {
        composeBarView.textView.resignFirstResponder()
        if self.discussionImage != ""{
            let backItem = UIBarButtonItem()
            backItem.title = "Back"
            backItem.tintColor = UIColor.black
            navigationItem.backBarButtonItem = backItem
            let storyboard =  UIStoryboard(name: "Main", bundle: nil)
            let promo = storyboard.instantiateViewController(withIdentifier: "ImageDisplayViewController") as! ImageDisplayViewController
            promo.imageURL = self.discussionImage
            promo.deleteShow = false
            self.navigationController?.pushViewController(promo, animated: true)
        }
        
    }
    
    @objc func reset()  {
        if composeBarView.textView.text == "" {
            self.navigationController?.popViewController(animated: true)
        }
        else{
            let refreshAlert = UIAlertController(title: "", message: "You have not finished commenting. People want to hear what you have to say!", preferredStyle: UIAlertControllerStyle.alert)
            
            refreshAlert.addAction(UIAlertAction(title: "Discard", style: .cancel , handler: { (action: UIAlertAction!) in
                self.navigationController?.popViewController(animated: true)
            }))
            
            
            refreshAlert.addAction(UIAlertAction(title: "Continue", style: .default, handler: { (action: UIAlertAction!) in
            }))
            
            self.present(refreshAlert, animated: true, completion: nil)
        }
    }
    
    @objc func onDoneBtnClk(){
        if self.footerValue == "Comment"{
            composeBarView.textView.resignFirstResponder()
        }
        else{
            if composeBarView.textView.text == ""{
                self.footerValue = "Comment"
                composeBarView.setText("", animated: true)
                composeBarView.placeholder = "Enter Comment Here"
                composeBarView.utilityButtonImage = nil
                composeBarView.textView.resignFirstResponder()
            }
            else{
                composeBarView.textView.resignFirstResponder()
            }
            
        }
        
    }
    
    @objc func removeView()  {
        composeBarView.textView.resignFirstResponder()
        coverView.removeFromSuperview()
    }
    
    @objc func onTapImage(_ sender: UITapGestureRecognizer) {
        composeBarView.textView.resignFirstResponder()
        let backItem = UIBarButtonItem()
        backItem.title = "Back"
        backItem.tintColor = UIColor.black
        navigationItem.backBarButtonItem = backItem
        var cell: ImageCommentCell?
        var view = sender.view
        while view != nil {
            if view is ImageCommentCell {
                cell = view as? ImageCommentCell
            }
            if view is UITableView {
                tableView = view as? UITableView
            }
            view = view?.superview
        }
        
        if let indexPath = (cell != nil) ? tableView?.indexPath(for: cell!) : nil {
//            let imageURL = prototypeEntitiesFromJSON[indexPath.row].image as String
//            if imageURL != ""{
//                let storyboard =  UIStoryboard(name: "Main", bundle: nil)
//                let promo = storyboard.instantiateViewController(withIdentifier: "ImageDisplayViewController") as! ImageDisplayViewController
//                promo.imageURL = imageURL
//                promo.deleteShow = false
//                self.navigationController?.pushViewController(promo, animated: true)
//            }
        }
    }
    

    
    func removeNSNull(from dict: NSMutableDictionary) -> NSDictionary {
        let mutableDict = dict
        let keysWithEmptString = dict.filter { $0.1 is NSNull }.map { $0.0 }
        for key in keysWithEmptString {
            mutableDict[key] = ""
        }
        
        return mutableDict
    }
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera))
        {
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openGallary()
    {
        imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        imagePicker.allowsEditing = false
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    func isConnectedToNetwork() -> Bool {
        
        var zeroAddress = sockaddr_in(sin_len: 0, sin_family: 0, sin_port: 0, sin_addr: in_addr(s_addr: 0), sin_zero: (0, 0, 0, 0, 0, 0, 0, 0))
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        
        return (isReachable && !needsConnection)
        
    }
    
    // MARK: - API integration
    func buildTestData(then: @escaping () -> Void) {
        prototypeEntitiesFromJSON = []
        self.tableView.reloadData()
        DispatchQueue.main.async {
            self.prototypeEntitiesFromJSON = []
            let manager = Alamofire.SessionManager.default
            manager.session.configuration.timeoutIntervalForRequest = 120
            
            manager.request( baseURL + "api/Diary/GetDiaryImgDetails/?DImgid=" + self.QuestionId + "&userid=" + userID , method: .get, parameters: ["":""], encoding: URLEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
                
                switch(response.result) {
                case .success(_):
                    if response.result.value != nil{
                        //print(response.result.value as Any)
                    }
                    let jsonResults : NSDictionary
                    jsonResults = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! NSDictionary
                    let QuestionDetail = jsonResults["ImageDetails"] as! NSDictionary
                    let Isliked = QuestionDetail["DImgliked"] as! Bool
                    self.isImageLiked = Isliked
                    let QId = QuestionDetail["ImageId"] as! Int
                    let Qdate = QuestionDetail["DImgdate"] as! String
                    let Qdesc = QuestionDetail["DImageDesc"] as! String
                    let QlikeCount = QuestionDetail["DImglikeCount"] as! Int
                    self.likeCount = QlikeCount
                    let TimeDiffsec = QuestionDetail["TimeDiffsec"] as! Int
                    let TimeDiffmin = QuestionDetail["TimeDiffmin"] as! Int
                    let TimeDiffhr = QuestionDetail["TimeDiffhr"] as! Int
                    self.NotificationOn = QuestionDetail["NotificationOn"] as! Bool
                    let IsShowNotificaion = QuestionDetail["IsShowNotificaion"] as! Bool
                    let DImglikeCount = QuestionDetail["DImgCmtCount"] as! Int
//                    let Role = QuestionDetail["Role"] as! Int
                    let QuserId = QuestionDetail["DImageUser"] as! String
                    let DTopicsCount = QuestionDetail["DTopicsCount"] as! String
                    let DTopics = QuestionDetail["DTopics"] as! String
                    if DTopicsCount == ""{
                        self.tagsCountLbl.text = "Tags: " +  DTopics
                    }
                    else{
                        self.tagsCountLbl.text = "Tags: " +  DTopicsCount
                    }
                    
                    self.vendorID = QuserId
                    if DImglikeCount == 0 || DImglikeCount == 1{
                        self.commentCountLbl.text = String(DImglikeCount) + " Comment"
                    }
                    else{
                        self.commentCountLbl.text = String(DImglikeCount) + " Comments"
                    }
                    if userID == QuserId{
                        self.reportBtn.isHidden = true
                        self.reportLbl.isHidden = true
                        self.reportImage.isHidden = true
                    }
                    else{
                        self.reportBtn.isHidden = false
                        self.reportLbl.isHidden = false
                        self.reportImage.isHidden = false
                    }
                    
                    if IsShowNotificaion == false{
                        self.notificationBtn.isHidden = true
                    }
                    else{
                        self.notificationBtn.isHidden = false
                        if self.NotificationOn == true{
                            self.notificationBtn.setImage(UIImage(named: "notification_on"), for: UIControlState.normal)
                        }
                        else{
                            self.notificationBtn.setImage(UIImage(named: "notification_off"), for: UIControlState.normal)
                        }
                    }
                    
                    let Qtext = QuestionDetail["DTopics"] as! String
                    var Quserimage = ""
                    if QuestionDetail["DImgUserImage"] is NSNull{
                        Quserimage = ""
                    }
                    else{
                        Quserimage = QuestionDetail["DImgUserImage"] as! String
                    }
                    
                    let Qusername = QuestionDetail["DImgUserName"] as! String
                    
                    var image = ""
                    if QuestionDetail["image"] is NSNull{
                        image = ""
                    }
                    else{
                        image = QuestionDetail["image"] as! String
                    }
                    
                    self.discussionImage = image
                    
                    if self.likeCount == 0 || self.likeCount == 1{
                        self.likeLbl.text = String(self.likeCount) + " Like"
                    }
                    else{
                        self.likeLbl.text = String(self.likeCount) + " Likes"
                    }
                    
                    if Isliked == false{
                        self.likeImage.image = UIImage(named: "dlike")
                    }
                    else{
                        self.likeImage.image = UIImage(named: "like")
                    }
                    self.titleLbl.text = Qusername
                    self.title = Qusername + "'s Diary"
                    self.desciptionLbl.text = Qdesc
                    if Quserimage != ""{
                        self.profilePic.sd_setImage(with: URL(string: Quserimage), placeholderImage: UIImage(named: "user.png"))
                    }
                    else{
                        self.profilePic.image = UIImage(named: "user.png")
                    }
                    if image != ""
                    {
                        let str: String? = Bundle.main.path(forResource: "loading_spinner", ofType: "gif")
                        let fileData = NSData(contentsOfFile: str!)
                        self.topicImage.sd_setImage(with: URL(string: image), placeholderImage: UIImage.sd_animatedGIF(with: fileData! as Data))
                    }
                    else{
                        self.topicImage.image = nil
                        self.topicImage.isHidden = true
                    }
                    
                    var time = ""
                    if (TimeDiffhr > 0){
                        if ((TimeDiffhr / 24) <= 0){
                            time = "\(TimeDiffhr) hour(s)"
                        }
                        else if ((TimeDiffhr) / (24 * 30) <= 0){
                            let days = (TimeDiffhr / 24)
                            time = "\(days) day(s)"
                        }
                        else{
                            let month = TimeDiffhr / (24 * 30)
                            time = "\(month) month(s)"
                        }
                    }
                    else if (TimeDiffmin > 0){
                        time = "\(TimeDiffmin) minute(s)"
                    }
                    else{
                        time = "\(TimeDiffsec) second(s)"
                    }
                    
                    self.activityLbl.text = Qusername + " - Last activity " + time + " ago."
                    
                    
                    
                    var viewHeight : CGFloat = 0
                    let size = CGSize(width: 359, height: 0)
                    viewHeight += self.titleLbl.sizeThatFits(size).height
                    viewHeight += self.activityLbl.sizeThatFits(size).height
                    viewHeight += self.desciptionLbl.sizeThatFits(size).height
                    viewHeight += self.tagsCountLbl.sizeThatFits(size).height
                    if (image == ""){
                        viewHeight += 0;
                    }
                    else{
                        viewHeight += 250;
                    }
                    
                    viewHeight += self.likeImage.sizeThatFits(size).height
                    viewHeight += 40 + 60
                    
                    self.viewCell.frame.size.height = viewHeight
                    let jsonArr = NSMutableArray()
                    let AnsList = jsonResults["CommentList"] as! NSArray
                    for i in 0 ..< AnsList.count{
                        let dicWithoutNulls = AnsList[i] as! NSDictionary
                        
                        let outputDict = self.removeNSNull(from: dicWithoutNulls.mutableCopy() as! NSMutableDictionary)
                        
                        jsonArr.add(outputDict)
                    }
                    
                    let arr = NSArray(array: jsonArr)
                    for i in 0 ..< arr.count{
                        self.prototypeEntitiesFromJSON.append(ImageCommentEntity(dictionary: arr[i] as! [AnyHashable : Any] ))
                    }
                    
                    self.tableView.reloadData()
                    break
                    
                case .failure(_):
                    print(response.error!.localizedDescription)
                    let alert: UIAlertView = UIAlertView(title: "", message: response.error!.localizedDescription, delegate: nil, cancelButtonTitle: "OK");
                    alert.show()
                    self.view.hideActivityView()
                    print(response.result.error as Any)
                    break
                    
                }
            }
        }
    }
    
    @objc func reloadView()  {
        self.refreshControl.endRefreshing()
        prototypeEntitiesFromJSON = []
        self.tableView.reloadData()
        DispatchQueue.main.async {
            let manager = Alamofire.SessionManager.default
            manager.session.configuration.timeoutIntervalForRequest = 120
            
            manager.request(baseURL + "api/Diary/GetDiaryImgDetails/?DImgid=" + self.QuestionId + "&userid=" + userID , method: .get, parameters: ["":""], encoding: URLEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
                
                switch(response.result) {
                case .success(_):
                    if response.result.value != nil{
                        //print(response.result.value as Any)
                    }
                    let jsonResults : NSDictionary
                    jsonResults = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! NSDictionary
                    let QuestionDetail = jsonResults["ImageDetails"] as! NSDictionary
                    let Isliked = QuestionDetail["DImgliked"] as! Bool
                    let QId = QuestionDetail["ImageId"] as! Int
                    let Qdate = QuestionDetail["DImgdate"] as! String
                    let Qdesc = QuestionDetail["DImageDesc"] as! String
                    let QlikeCount = QuestionDetail["DImglikeCount"] as! Int
                    let TimeDiffsec = QuestionDetail["TimeDiffsec"] as! Int
                    let TimeDiffmin = QuestionDetail["TimeDiffmin"] as! Int
                    let TimeDiffhr = QuestionDetail["TimeDiffhr"] as! Int
                    let DImglikeCount = QuestionDetail["DImgCmtCount"] as! Int
                    self.NotificationOn = QuestionDetail["NotificationOn"] as! Bool
                    let IsShowNotificaion = QuestionDetail["IsShowNotificaion"] as! Bool
                    let QuserId = QuestionDetail["DImageUser"] as! String
                    self.isImageLiked = Isliked
                    self.likeCount = QlikeCount
                    let DTopicsCount = QuestionDetail["DTopicsCount"] as! String
                    let DTopics = QuestionDetail["DTopics"] as! String
                    if DTopicsCount == ""{
                        self.tagsCountLbl.text = "Tags: " +  DTopics
                    }
                    else{
                        self.tagsCountLbl.text = "Tags: " +  DTopicsCount
                    }
                    
                    
                    self.vendorID = QuserId
                    if userID == QuserId{
                        
                    }
                    else{
                        
                    }
                    
                    if DImglikeCount == 0 || DImglikeCount == 1{
                        self.commentCountLbl.text = String(DImglikeCount) + " Comment"
                    }
                    else{
                        self.commentCountLbl.text = String(DImglikeCount) + " Comments"
                    }
                    
                    
                    if IsShowNotificaion == false{
                        self.notificationBtn.isHidden = true
                    }
                    else{
                        self.notificationBtn.isHidden = false
                        if self.NotificationOn == true{
                            self.notificationBtn.setImage(UIImage(named: "notification_on"), for: UIControlState.normal)
                        }
                        else{
                            self.notificationBtn.setImage(UIImage(named: "notification_off"), for: UIControlState.normal)
                        }
                    }
                    
                    let Qtext = QuestionDetail["DTopics"] as! String
                    var Quserimage = ""
                    if QuestionDetail["DImgUserImage"] is NSNull{
                        Quserimage = ""
                    }
                    else{
                        Quserimage = QuestionDetail["DImgUserImage"] as! String
                    }
                    
                    let Qusername = QuestionDetail["DImgUserName"] as! String
                    
                    var image = ""
                    if QuestionDetail["image"] is NSNull{
                        image = ""
                    }
                    else{
                        image = QuestionDetail["image"] as! String
                    }
                    
                    self.discussionImage = image
                    
                    if self.likeCount == 0 || self.likeCount == 1{
                        self.likeLbl.text = String(self.likeCount) + " Like"
                    }
                    else{
                        self.likeLbl.text = String(self.likeCount) + " Likes"
                    }
                    
                    if Isliked == false{
                        self.likeImage.image = UIImage(named: "dlike")
                    }
                    else{
                        self.likeImage.image = UIImage(named: "like")
                    }
                    self.titleLbl.text = Qusername
                    self.title = Qusername
                    self.desciptionLbl.text = Qdesc
                    if Quserimage != ""{
                        self.profilePic.sd_setImage(with: URL(string: Quserimage), placeholderImage: UIImage(named: "user.png"))
                    }
                    else{
                        self.profilePic.image = UIImage(named: "user.png")
                    }
                    if image != ""
                    {
                        let str: String? = Bundle.main.path(forResource: "loading_spinner", ofType: "gif")
                        let fileData = NSData(contentsOfFile: str!)
                        self.topicImage.sd_setImage(with: URL(string: image), placeholderImage: UIImage.sd_animatedGIF(with: fileData! as Data))
                    }
                    else{
                        self.topicImage.image = nil
                        self.topicImage.isHidden = true
                    }
                    
                    var time = ""
                    if (TimeDiffhr > 0){
                        if ((TimeDiffhr / 24) <= 0){
                            time = "\(TimeDiffhr) hour(s)"
                        }
                        else if ((TimeDiffhr) / (24 * 30) <= 0){
                            let days = (TimeDiffhr / 24)
                            time = "\(days) day(s)"
                        }
                        else{
                            let month = TimeDiffhr / (24 * 30)
                            time = "\(month) month(s)"
                        }
                    }
                    else if (TimeDiffmin > 0){
                        time = "\(TimeDiffmin) minute(s)"
                    }
                    else{
                        time = "\(TimeDiffsec) second(s)"
                    }
                    
                    self.activityLbl.text = Qusername + " - Last activity " + time + " ago."
                    
                    
                    
                    var viewHeight : CGFloat = 0
                    let size = CGSize(width: 359, height: 0)
                    viewHeight += self.titleLbl.sizeThatFits(size).height
                    viewHeight += self.activityLbl.sizeThatFits(size).height
                    viewHeight += self.desciptionLbl.sizeThatFits(size).height
                    viewHeight += self.tagsCountLbl.sizeThatFits(size).height
                    if (image == ""){
                        viewHeight += 0;
                    }
                    else{
                        viewHeight += 250;
                    }
                    
                    viewHeight += self.likeImage.sizeThatFits(size).height
                    viewHeight += 40  + 60
                    
                    self.viewCell.frame.size.height = viewHeight
                    let jsonArr = NSMutableArray()
                    let AnsList = jsonResults["CommentList"] as! NSArray
                    for i in 0 ..< AnsList.count{
                        let dicWithoutNulls = AnsList[i] as! NSDictionary
                        
                        let outputDict = self.removeNSNull(from: dicWithoutNulls.mutableCopy() as! NSMutableDictionary)
                        
                        jsonArr.add(outputDict)
                    }
                    
                    let arr = NSArray(array: jsonArr)
                    for i in 0 ..< arr.count{
                        self.prototypeEntitiesFromJSON.append(ImageCommentEntity(dictionary: arr[i] as! [AnyHashable : Any] ))
                    }
                    
                    self.tableView.reloadData()
                    break
                    
                case .failure(_):
                    print(response.error!.localizedDescription)
                    let alert: UIAlertView = UIAlertView(title: "", message: response.error!.localizedDescription, delegate: nil, cancelButtonTitle: "OK");
                    alert.show()
                    self.view.hideActivityView()
                    print(response.result.error as Any)
                    break
                    
                }
            }
        }
    }
    
    
    // MARK: - Comment Box Delegate
    func composeBarViewDidPressButton(_ composeBarView: PHFComposeBarView!) {
        if composeBarView.textView.text == "" {
            composeBarView.textView.resignFirstResponder()
            let alert: UIAlertView = UIAlertView(title: "", message: "Please write something!", delegate: nil, cancelButtonTitle: "OK");
            alert.show()
        }
        else if (composeBarView.textView.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            composeBarView.textView.resignFirstResponder()
            let alert: UIAlertView = UIAlertView(title: "", message: "Please write something!", delegate: nil, cancelButtonTitle: "OK");
            alert.show()
        }
        else{
            if isConnectedToNetwork() == true{
                composeBarView.button.isUserInteractionEnabled = false
                if footerValue == "Reply"{
                    DispatchQueue.main.async {
                        let parameters : [String: String] = [
                            "DImgCmt_Id": String(self.answer_id),
                            "User_Id": userID,
                            "ReplyText": composeBarView.textView.text as String
                        ]
                        
                        let manager = Alamofire.SessionManager.default
                        manager.session.configuration.timeoutIntervalForRequest = 120
                        
                        manager.request(baseURL + "api/Diary/DImgCmtReply", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
                            
                            switch(response.result) {
                            case .success(_):
                                if response.result.value != nil{
                                    //print(response.result.value as Any)
                                }
                                self.footerValue = "Comment"
                                composeBarView.setText("", animated: true)
                                composeBarView.placeholder = "Enter Comment Here"
                                composeBarView.utilityButtonImage = nil
                                composeBarView.textView.resignFirstResponder()
                                self.reloadView()
                                composeBarView.button.isUserInteractionEnabled = true
                                break
                                
                            case .failure(_):
                                print(response.error!.localizedDescription)
                                let alert: UIAlertView = UIAlertView(title: "", message: response.error!.localizedDescription, delegate: nil, cancelButtonTitle: "OK");
                                alert.show()
                                self.view.hideActivityView()
                                print(response.result.error as Any)
                                composeBarView.button.isUserInteractionEnabled = true
                                break
                                
                            }
                        }
                    }
                }
                else{
                    DispatchQueue.main.async {
                        composeBarView.textView.resignFirstResponder()
                        let str = composeBarView.textView.text
                        
                        let trimStr = str?.replacingOccurrences(of: "\u{ef}", with: "")
                        let parameters : [String: String] = [
                            "User_Id": userID,
                            "DImg_Id": self.QuestionId,
                            "CommentsText": str!
                        ]
                        
                        let manager = Alamofire.SessionManager.default
                        manager.session.configuration.timeoutIntervalForRequest = 120
                        
                        manager.request(baseURL + "api/Diary/DiaryImgComment", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
                            
                            switch(response.result) {
                            case .success(_):
                                if response.result.value != nil{
                                    //print(response.result.value as Any)
                                }
                                self.footerValue = "Comment"
                                composeBarView.setText("", animated: true)
                                composeBarView.placeholder = "Enter Comment Here"
                                composeBarView.utilityButtonImage = nil
                                composeBarView.textView.resignFirstResponder()
                                self.reloadView()
                                self.base64Str = ""
                                composeBarView.button.isUserInteractionEnabled = true
                                break
                                
                            case .failure(_):
                                print(response.error!.localizedDescription)
                                let alert: UIAlertView = UIAlertView(title: "", message: response.error!.localizedDescription, delegate: nil, cancelButtonTitle: "OK");
                                alert.show()
                                self.view.hideActivityView()
                                print(response.result.error as Any)
                                composeBarView.button.isUserInteractionEnabled = true
                                break
                                
                            }
                        }
                    }
                }
            }
            
            
        }
    }
    
    func composeBarViewDidPressUtilityButton(_ composeBarView: PHFComposeBarView!) {
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallary()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func composeBarView(_ composeBarView: PHFComposeBarView!, didChangeFromFrame startFrame: CGRect, toFrame endFrame: CGRect) {
        
    }
    
    func composeBarView(_ composeBarView: PHFComposeBarView!, willChangeFromFrame startFrame: CGRect, toFrame endFrame: CGRect, duration: TimeInterval, animationCurve: UIViewAnimationCurve) {
        
    }
    
    
    
    // MARK: - IBActions
    @IBAction func notificationBtnClk(_ sender: UIButton) {
        
        var notification = ""
        if self.NotificationOn == true{
            notification = "false"
        }
        else{
            notification = "true"
        }
        
        DispatchQueue.main.async {
            let manager = Alamofire.SessionManager.default
            manager.session.configuration.timeoutIntervalForRequest = 120
            
            manager.request( baseURL + "api/Diary/DiaryNotificationOn/?userid=" + userID + "&DImgid=" + self.QuestionId + "&notificationOn=" + notification, method: .get, parameters: ["":""], encoding: URLEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
                
                
                switch(response.result) {
                case .success(_):
                    if response.result.value != nil{
                        //print(response.result.value as Any)
                    }
                    self.likeBtn.isUserInteractionEnabled = true
                    reloadValue = true
                    if notification == "true"
                    {
                        let alert: UIAlertView = UIAlertView(title: "Notifications are on for this dairy.", message: "", delegate: nil, cancelButtonTitle: "OK");
                        alert.show()
                    }
                    else{
                        let alert: UIAlertView = UIAlertView(title: "Notifications are off for this dairy.", message: "", delegate: nil, cancelButtonTitle: "OK");
                        alert.show()
                    }
                    self.buildTestData {
                        
                    }
                    
                    break
                    
                case .failure(_):
                    print(response.error!.localizedDescription)
                    let alert: UIAlertView = UIAlertView(title: "", message: response.error!.localizedDescription, delegate: nil, cancelButtonTitle: "OK");
                    alert.show()
                    self.view.hideActivityView()
                    print(response.result.error as Any)
                    break
                    
                }
            }
        }
    }
    
    @IBAction func headerLikeBtnClk(_ sender: UIButton) {
        self.likeBtn.isUserInteractionEnabled = false
        if isConnectedToNetwork() == true{
            DispatchQueue.main.async {
                
                
                if self.isImageLiked == true{
                    self.likeCount = self.likeCount - 1
                    if self.likeCount == 0 || self.likeCount == 1{
                        self.likeLbl.text = String(self.likeCount) + " Like"
                    }
                    else{
                        self.likeLbl.text = String(self.likeCount) + " Likes"
                    }
                    self.likeImage.image = UIImage(named: "dlike")
                    self.isImageLiked = false
                }
                else{
                    self.likeCount = self.likeCount + 1
                    if self.likeCount == 0 || self.likeCount == 1{
                        self.likeLbl.text = String(self.likeCount) + " Like"
                    }
                    else{
                        self.likeLbl.text = String(self.likeCount) + " Likes"
                    }
                    self.likeImage.image = UIImage(named: "like")
                    self.isImageLiked = true
                }
                
                let parameters : [String: String] = [
                    "User_Id": userID,
                    "DImg_Id":  self.QuestionId,
                    "DImgCmt_Id":"0",
                    "DImgRly_Id":"0"
                ]
                
                let manager = Alamofire.SessionManager.default
                manager.session.configuration.timeoutIntervalForRequest = 120
                
                manager.request(baseURL + "api/Diary/DImgLike", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
                    
                    switch(response.result) {
                    case .success(_):
                        if response.result.value != nil{
                            //print(response.result.value as Any)
                        }
                        self.likeBtn.isUserInteractionEnabled = true
                        reloadValue = true
                        //                    self.buildTestData {
                        //
                        //                    }
                        break
                        
                    case .failure(_):
                        print(response.error!.localizedDescription)
                        let alert: UIAlertView = UIAlertView(title: "", message: response.error!.localizedDescription, delegate: nil, cancelButtonTitle: "OK");
                        alert.show()
                        self.view.hideActivityView()
                        print(response.result.error as Any)
                        break
                        
                    }
                }
            }
        }
        
    }
    
    @IBAction func headerReportBtnClk(_ sender: Any) {
        self.popViewController = self.storyboard?.instantiateViewController(withIdentifier: "DairyReportViewController")as! DairyReportViewController
        self.popViewController.a_id = QuestionId
        self.popViewController.action = self.action
        self.popViewController.showInView(self.view, animated: true)
//        composeBarView.textView.becomeFirstResponder()
    }
    
    // MARK: - Delagates
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return prototypeEntitiesFromJSON.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ImageCommentCell") as! ImageCommentCell
        if prototypeEntitiesFromJSON.count != 0{
            configureCell(cell, at: indexPath)
        }
        return cell
    }
    
    
    func configureCell(_ cell: ImageCommentCell, at indexPath: IndexPath) {
        if prototypeEntitiesFromJSON.count != 0{
            cell.fd_enforceFrameLayout = true
            cell.entity = prototypeEntitiesFromJSON[indexPath.row]
//            cell.commentImgView.tag = indexPath.row
            let tap = UITapGestureRecognizer(target: self, action: #selector(self.onTapImage))
//            cell.commentImgView.addGestureRecognizer(tap)
//            cell.commentImgView.isUserInteractionEnabled = true
            let tap1 = UITapGestureRecognizer(target: self, action: #selector(self.tapFunction))
            cell.replyLbl.isUserInteractionEnabled = true
            cell.replyLbl.addGestureRecognizer(tap1)
            
            let tap2 = UITapGestureRecognizer(target: self, action: #selector(self.tapOnProfilePic))
            cell.profileImageView.isUserInteractionEnabled = true
            cell.profileImageView.addGestureRecognizer(tap2)
            
            let tap3 = UITapGestureRecognizer(target: self, action: #selector(self.tapOnProfilePic))
            cell.userNameLbl.isUserInteractionEnabled = true
            cell.userNameLbl.addGestureRecognizer(tap3)
            
            let tap4 = UITapGestureRecognizer(target: self, action: #selector(self.tapOnProfilePic))
            cell.activityLbl.isUserInteractionEnabled = true
            cell.activityLbl.addGestureRecognizer(tap4)
        }
    }
    
    @objc func tapOnProfilePic(_ sender: UITapGestureRecognizer) {
        let backItem = UIBarButtonItem()
        backItem.title = "Back"
        backItem.tintColor = UIColor.black
        navigationItem.backBarButtonItem = backItem
        var cell: ImageCommentCell?
        var view = sender.view
        while view != nil {
            if view is ImageCommentCell {
                cell = view as? ImageCommentCell
            }
            if view is UITableView {
                tableView = view as? UITableView
            }
            view = view?.superview
        }
        
        if let indexPath = (cell != nil) ? tableView?.indexPath(for: cell!) : nil {
            let role = prototypeEntitiesFromJSON[indexPath.row].role
            if role == 3{
                let backItem = UIBarButtonItem()
                backItem.title = "Back"
                backItem.tintColor = UIColor.black
                navigationItem.backBarButtonItem = backItem
                let storyboard =  UIStoryboard(name: "Main", bundle: nil)
                let promo = storyboard.instantiateViewController(withIdentifier: "VendorDetailsViewController") as! VendorDetailsViewController
                promo.vendorID = self.prototypeEntitiesFromJSON[indexPath.row].value(forKey: "CommentSendUser") as! String
                promo.poptoRoot = true
                self.navigationController?.pushViewController(promo, animated: true)
            }
            
        }
    }
    
    @objc func tapFunction(_ sender: UITapGestureRecognizer) {
        let backItem = UIBarButtonItem()
        backItem.title = "Back"
        backItem.tintColor = UIColor.black
        navigationItem.backBarButtonItem = backItem
        var cell: ImageCommentCell?
        var view = sender.view
        while view != nil {
            if view is ImageCommentCell {
                cell = view as? ImageCommentCell
            }
            if view is UITableView {
                tableView = view as? UITableView
            }
            view = view?.superview
        }
        
        if let indexPath = (cell != nil) ? tableView?.indexPath(for: cell!) : nil {
            var ReplyList = self.prototypeEntitiesFromJSON[indexPath.row].value(forKey: "ReplyList") as! NSArray
            let storyboard =  UIStoryboard(name: "Main", bundle: nil)
            let promo = storyboard.instantiateViewController(withIdentifier: "DairyCommentReplyController") as! DairyCommentReplyController
            promo.jsonResults = ReplyList
            promo.QuestionId = self.QuestionId
            promo.selectedIndex = indexPath
            self.navigationController?.pushViewController(promo, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tableView.fd_heightForCell(withIdentifier: "ImageCommentCell") { cell in
            
            self.configureCell(cell as! ImageCommentCell, at: indexPath)
        }
    }
    
    internal func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            let imageData: NSData = UIImageJPEGRepresentation(pickedImage, 0.4)! as NSData
            let imageStr = imageData.base64EncodedString(options: .lineLength64Characters)
            base64Str = imageStr
            let attributedString = NSMutableAttributedString(string: self.composeBarView.textView.text + "");
            let textAttachment = NSTextAttachment();
            textAttachment.image = pickedImage;
            
            let oldWidth = pickedImage.size.width;
            
            let scaleFactor = oldWidth / (self.composeBarView.frame.size.width / 4);
            
            textAttachment.image = UIImage(cgImage: textAttachment.image!.cgImage!, scale: scaleFactor, orientation: .up)
            let hr = textAttachment.image?.size.height
            self.composeBarView.frame = CGRect(x: 0, y: (self.composeBarView.frame.origin.y + 40) - (textAttachment.image?.size.height)!, width: self.view.frame.size.width, height: (textAttachment.image?.size.height)!)
            let attrStringWithImage = NSAttributedString(attachment: textAttachment)
            attributedString.append(attrStringWithImage)
            self.composeBarView.textView.attributedText = attributedString;
            self.composeBarView.placeholder = " "
            self.composeBarView.textView.font = UIFont(name: "Helvetica Neue", size: 15)!
        }
        
        dismiss(animated: true, completion: nil)
    }
    
    func replyOnCommen(){
        self.popViewController = self.storyboard?.instantiateViewController(withIdentifier: "DairyReportViewController")as! DairyReportViewController
        self.popViewController.a_id = QuestionId
        self.popViewController.action = self.action
        self.popViewController.showInView(self.view, animated: true)
    }
    
    func deleteComment() {
        let refreshAlert = UIAlertController(title: "", message: "This comment will be permanently deleted.", preferredStyle: UIAlertControllerStyle.alert)
        
        refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel , handler: { (action: UIAlertAction!) in
            
        }))
        
        
        refreshAlert.addAction(UIAlertAction(title: "Confirm", style: .default, handler: { (action: UIAlertAction!) in
            self.postDelete()
        }))
        
        self.present(refreshAlert, animated: true, completion: nil)
    }
    
    func EditComment() {
        self.popViewController1 = self.storyboard?.instantiateViewController(withIdentifier: "EditDairyCommentController")as! EditDairyCommentController
        self.popViewController1.answerID = String(answer_id)
        self.popViewController1.commentTxt = self.Anstext
        self.popViewController1.delegate = self
        self.popViewController1.showInView(self.view, animated: true)
    }
    
    func hideComment() {
        let refreshAlert = UIAlertController(title: "", message: "Are you sure you want to HIDE this comment? You won’t see this comment again.", preferredStyle: UIAlertControllerStyle.alert)
        
        refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel , handler: { (action: UIAlertAction!) in
            
        }))
        
        
        refreshAlert.addAction(UIAlertAction(title: "Confirm", style: .default, handler: { (action: UIAlertAction!) in
            self.postHide()
        }))
        
        self.present(refreshAlert, animated: true, completion: nil)
    }
    
    func postHide()  {
        DispatchQueue.main.async {
            let manager = Alamofire.SessionManager.default
            manager.session.configuration.timeoutIntervalForRequest = 120
            
            manager.request( baseURL + "api/Answers/HideAnswer/?ansid=" + String(self.answer_id) + "&userid=" + userID, method: .get, parameters: ["":""], encoding: URLEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
                
                switch(response.result) {
                case .success(_):
                    if response.result.value != nil{
                        //print(response.result.value as Any)
                    }
                    reloadValue = true
                    self.reloadView()
                    break
                    
                case .failure(_):
                    print(response.error!.localizedDescription)
                    let alert: UIAlertView = UIAlertView(title: "", message: response.error!.localizedDescription, delegate: nil, cancelButtonTitle: "OK");
                    alert.show()
                    self.view.hideActivityView()
                    print(response.result.error as Any)
                    break
                    
                }
            }
        }
    }
    
    func postDelete()  {
        DispatchQueue.main.async {
            let manager = Alamofire.SessionManager.default
            manager.session.configuration.timeoutIntervalForRequest = 120
            
            manager.request( baseURL + "api/Diary/DeleteDImgComment/?DCmtId=" + String(self.answer_id) + "&userid=" + userID, method: .get, parameters: ["":""], encoding: URLEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
                
                switch(response.result) {
                case .success(_):
                    if response.result.value != nil{
                        //print(response.result.value as Any)
                    }
                    reloadValue = true
                    self.reloadView()
                    break
                    
                case .failure(_):
                    print(response.error!.localizedDescription)
                    let alert: UIAlertView = UIAlertView(title: "", message: response.error!.localizedDescription, delegate: nil, cancelButtonTitle: "OK");
                    alert.show()
                    self.view.hideActivityView()
                    print(response.result.error as Any)
                    break
                    
                }
            }
        }
    }
    
    @IBAction func menuBtnClk(_ sender: UIButton) {
        let point = self.tableView.convert(CGPoint.zero, from: sender)
        var user_id = ""
        if let indexPath = self.tableView.indexPathForRow(at: point) {
            answer_id = self.prototypeEntitiesFromJSON[indexPath.row].value(forKey: "CommentId") as! Int
            user_id = self.prototypeEntitiesFromJSON[indexPath.row].value(forKey: "CommentSendUser") as! String
            Anstext = self.prototypeEntitiesFromJSON[indexPath.row].value(forKey: "CommentText") as! String
        }
        
        
        if userID == user_id{
            category = ["Edit","Delete"]
            menuOptionImageNameArray = ["edit","del"]
        }
        else{
            category = ["Report"]
            menuOptionImageNameArray = ["reportflag"]
        }
        
        coverView.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        self.view.addSubview(coverView)
        FTPopOverMenu.showForSender(sender: sender, with: category, menuImageArray: menuOptionImageNameArray, done: { (selectedIndex) -> () in
            print(selectedIndex)
            if userID == user_id{
                if selectedIndex == 0
                {
                    self.EditComment()
                }
                if selectedIndex == 1
                {
                    self.deleteComment()
                }
                
            }
            else{
                
                if selectedIndex == 0
                {
                    self.replyOnCommen()
                }
            }
            self.coverView.removeFromSuperview()
        }) {
            self.coverView.removeFromSuperview()
        }
    }
    
    @IBAction func commentLikeBtnClk(_ sender: UIButton) {
        DispatchQueue.main.async {
            let point = self.tableView.convert(CGPoint.zero, from: sender)
            var a_id : Int = 0
            if let indexPath = self.tableView.indexPathForRow(at: point) {
                a_id = self.prototypeEntitiesFromJSON[indexPath.row].value(forKey: "CommentId") as! Int
                let likevalue = self.prototypeEntitiesFromJSON[indexPath.row].isliked
                if likevalue == true{
                    self.prototypeEntitiesFromJSON[indexPath.row].isliked = false
                    self.prototypeEntitiesFromJSON[indexPath.row].commentLikeCount = self.prototypeEntitiesFromJSON[indexPath.row].commentLikeCount - 1
                }
                else{
                    self.prototypeEntitiesFromJSON[indexPath.row].isliked = true
                    self.prototypeEntitiesFromJSON[indexPath.row].commentLikeCount = self.prototypeEntitiesFromJSON[indexPath.row].commentLikeCount + 1
                }
            }
            
            self.tableView.reloadData()
            
            let parameters : [String: Any] = [
                "User_Id": userID,
                "DImg_Id":  self.QuestionId,
                "DImgCmt_Id": a_id,
                "DImgRly_Id":"0"
                ]
            
            let manager = Alamofire.SessionManager.default
            manager.session.configuration.timeoutIntervalForRequest = 120
            
            manager.request(baseURL + "api/Diary/DImgLike", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
                
                switch(response.result) {
                case .success(_):
                    if response.result.value != nil{
                        //print(response.result.value as Any)
                    }
                    reloadValue = true
//                    self.reloadView()
                    break
                    
                case .failure(_):
                    print(response.error!.localizedDescription)
                    let alert: UIAlertView = UIAlertView(title: "", message: response.error!.localizedDescription, delegate: nil, cancelButtonTitle: "OK");
                    alert.show()
                    self.view.hideActivityView()
                    print(response.result.error as Any)
                    break
                    
                }
            }
        }
    }
    
    @IBAction func commentReportBtnClk(_ sender: UIButton) {
        let point = self.tableView.convert(CGPoint.zero, from: sender)
        var AnsUsername = ""
        if let indexPath = self.tableView.indexPathForRow(at: point) {
            answer_id = self.prototypeEntitiesFromJSON[indexPath.row].value(forKey: "CommentId") as! Int
            AnsUsername = self.prototypeEntitiesFromJSON[indexPath.row].value(forKey: "CommentUsername") as! String
        }
        self.footerValue = "Reply"
        composeBarView.placeholder = "Reply to " + AnsUsername
        composeBarView.utilityButtonImage = nil
        composeBarView.textView.becomeFirstResponder()
        
    }
    
    
    @IBAction func imagePickerBtnClk(_ sender: Any) {
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallary()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    
    @IBAction func replyBtnClk(_ sender: UIButton) {
        
        
        
    }
    
    @IBAction func headerMenuBtnClk(_ sender: UIButton) {
        if userID == self.vendorID{
            category = ["Edit","Delete"]
            menuOptionImageNameArray = ["edit","del"]
        }
        else{
            category = ["Report"]
            menuOptionImageNameArray = ["reportflag"]
        }
        
        coverView.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        self.view.addSubview(coverView)
        FTPopOverMenu.showForSender(sender: sender, with: category, menuImageArray: menuOptionImageNameArray, done: { (selectedIndex) -> () in
            print(selectedIndex)
            if userID == self.vendorID{
                if selectedIndex == 0
                {
//                    self.editImage()
                }
                if selectedIndex == 1
                {
//                    self.deleteImage()
                }
                
            }
            else{
                if selectedIndex == 0
                {
                    self.replyOnCommen()
                    self.answer_id = Int(self.QuestionId)!
                    self.action = "Image"
                    
                }
            }
            self.coverView.removeFromSuperview()
        }) {
            self.coverView.removeFromSuperview()
        }
    }
    
//    func deleteImage()  {
//        let refreshAlert = UIAlertController(title: "", message: "Are you sure you want to DELETE the picture? This cannot be UNDONE.", preferredStyle: UIAlertControllerStyle.alert)
//
//        refreshAlert.addAction(UIAlertAction(title: "No, I don’t", style: .cancel , handler: { (action: UIAlertAction!) in
//
//        }))
//
//
//        refreshAlert.addAction(UIAlertAction(title: "Yes, I want to delete", style: .default, handler: { (action: UIAlertAction!) in
//            DispatchQueue.main.async {
//                var imgarr : [Int] = []
//                imgarr.append(Int(self.QuestionId)!)
//
//                let parameters : [String: Any] = [
//                    "ImagesId": imgarr,
//                    "ServiceProviderId" : userID
//                ]
//
//                let manager = Alamofire.SessionManager.default
//                manager.session.configuration.timeoutIntervalForRequest = 120
//
//                manager.request(baseURL + "api/Albums/DeletePhotoGallery", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
//
//                    switch(response.result) {
//                    case .success(_):
//                        if response.result.value != nil{
//                            //print(response.result.value as Any)
//                        }
//                        self.navigationController?.popViewController(animated: true)
//                        break
//
//                    case .failure(_):
//                        print(response.error!.localizedDescription)
//                        let alert: UIAlertView = UIAlertView(title: "", message: response.error!.localizedDescription, delegate: nil, cancelButtonTitle: "OK");
//                        alert.show()
//                        self.view.hideActivityView()
//                        print(response.result.error as Any)
//                        break
//
//                    }
//                }
//            }
//        }))
//
//        self.present(refreshAlert, animated: true, completion: nil)
//    }
//
//    func editImage()  {
//        let backItem = UIBarButtonItem()
//        backItem.title = "Back"
//        backItem.tintColor = UIColor.black
//        navigationItem.backBarButtonItem = backItem
//        self.popViewController2 = self.storyboard?.instantiateViewController(withIdentifier: "EditImageViewController")as! EditImageViewController
//        self.popViewController2.commentTxt = self.imageDescription
//        self.popViewController2.servicesArr = self.servicesArr
//        self.popViewController2.delegate = self
//        self.popViewController2.imgID = self.QuestionId
//        self.popViewController2.promotionText = self.promotionText
//        self.popViewController2.startDate = self.startDate
//        self.popViewController2.endDate = self.endDate
//        self.popViewController2.timespanValue = self.timespan
//        self.popViewController2.budgetValue = self.budgetValue
//        self.navigationController?.pushViewController(popViewController2, animated: true)
//    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    
}



