//
//  EditDairyCommentController.swift
//  AyefroInc
//
//  Created by Vijay Darkonde on 10/01/18.
//  Copyright © 2018 Inceptive Consulting Pvt. Ltd. All rights reserved.
//

import UIKit
import KMPlaceholderTextView
import Alamofire

class EditDairyCommentController: UIViewController  {
    
    @IBOutlet var popUpView: UIView!
    @IBOutlet var commentTxtView: KMPlaceholderTextView!
    @IBOutlet var submitBtn: UIButton!
    
    var answerID : String = ""
    var delegate: editComment!
    var commentTxt : String = ""
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    override init(nibName nibNameOrNil: String!, bundle nibBundleOrNil: Bundle!) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        self.popUpView.layer.cornerRadius = 5
        self.popUpView.layer.shadowOpacity = 0.0
        self.popUpView.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        self.commentTxtView.text = commentTxt
        
    }
    
    func showInView(_ aView: UIView!, animated: Bool)
    {
        aView.addSubview(self.view)
        if animated
        {
            self.showAnimate()
        }
    }
    
    func showAnimate()
    {
        self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view.alpha = 0.0;
        UIView.animate(withDuration: 0.25, animations: {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        });
    }
    
    func removeAnimate()
    {
        UIView.animate(withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view.alpha = 0.0;
        }, completion:{(finished : Bool)  in
            if (finished)
            {
                self.view.removeFromSuperview()
            }
        });
    }
    
    @IBAction func backBtnClk(_ sender: AnyObject) {
        commentTxtView.resignFirstResponder()
        removeAnimate()
    }
    
    
    @IBAction func saveBtnClk(_ sender: AnyObject) {
        
        commentTxtView.resignFirstResponder()
        if commentTxtView.text.isEmpty
        {
            commentTxtView.layer.borderColor = UIColor.red.cgColor
            commentTxtView.layer.borderWidth = 1
        }
        else
        {
            self.submitBtn.isUserInteractionEnabled = false
            DispatchQueue.main.async {
                let parameters : [String: String] = [
                    "User_Id": userID,
                    "DImgCmt_Id": self.answerID,
                    "CommentsText": self.commentTxtView.text as String
                ]
                let manager = Alamofire.SessionManager.default
                manager.session.configuration.timeoutIntervalForRequest = 120
                
                manager.request(baseURL + "api/Diary/EditDImgComment", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
                    
                    switch(response.result) {
                    case .success(_):
                        if response.result.value != nil{
                            //print(response.result.value as Any)
                        }
                        self.submitBtn.isUserInteractionEnabled = true
                        self.delegate.reloadView()
                        self.removeAnimate()
                        break
                        
                    case .failure(_):
                        print(response.error!.localizedDescription)
                        let alert: UIAlertView = UIAlertView(title: "", message: response.error!.localizedDescription, delegate: nil, cancelButtonTitle: "OK");
                        alert.show()
                        print(response.result.error as Any)
                        break
                        
                    }
                }
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}



