//
//  DairiesViewController.swift
//  AyefroInc
//
//  Created by Vijay Darkonde on 05/01/18.
//  Copyright © 2018 Inceptive Consulting Pvt. Ltd. All rights reserved.
//

import UIKit

protocol isDataAvailable {
    func dairiesData(count: Int)
}


class DairiesViewController: UIViewController, UIScrollViewDelegate, isDataAvailable {
    
    
    @IBOutlet weak var menuScrollView: UIScrollView!
    @IBOutlet weak var containerScrollView: UIScrollView!
    var kDefaultEdgeInsets = UIEdgeInsetsMake(8, 16, 8, 16)
    var SCREEN_WIDTH = UIScreen.main.bounds.size.width
    var btnArray = NSArray()
    @IBOutlet var rightBtn: UIBarButtonItem!
    
    let fvc = UIView()
    let lblView = UIView()
    let textLbl = UILabel()
    let icon = UIImageView()
    let closeicon = UIImageView()
    
    var viewCount : Int! = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        self.title = ""
        reloadValue = true
        
        self.navigationController?.navigationBar.titleTextAttributes = [ NSAttributedStringKey.font: UIFont.boldSystemFont(ofSize: 14.0)]

        _ = ModelManager.instance.getUserLoginStatus()
        let path = Bundle.main.path(forResource: "countryCode", ofType: "json")
        do{
            let data = try Data(contentsOf: URL(fileURLWithPath: path!), options: .mappedIfSafe)
            let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves) as! NSDictionary
            let countries = jsonResult.object(forKey: "countries") as! NSDictionary
            let country = countries.object(forKey: "country") as! NSArray
            for i in 0 ..< country.count
            {
                let countryObj = country[i] as! NSDictionary
                let countryName = countryObj.object(forKey: "countryName") as! String
                if Country == countryName{
                    let currencySymbol = countryObj.object(forKey: "currencySymbol") as! String
                    UserDefaults.standard.setValue(currencySymbol, forKey: "currencySymbol")
                    print("\(UserDefaults.standard.value(forKey: "currencySymbol")!)")
                    break
                }
            }
            
        }
        catch{
            
        }
        
        if dairyNotification == true{
            let storyboard =  UIStoryboard(name: "Main", bundle: nil)
            let promo = storyboard.instantiateViewController(withIdentifier: "DairyDetailViewController") as! DairyDetailViewController
            promo.QuestionId = dairy_i_id
            self.navigationController?.pushViewController(promo, animated: true)
            reloadValue = true
            dairyNotification = false
        }
        
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if reloadValue == true {
            reloadValue = false
            let storyboard =  UIStoryboard(name: "Main", bundle: nil)
            let vc1 = storyboard.instantiateViewController(withIdentifier: "MyDairiesViewController") as! MyDairiesViewController
            let vc2 = storyboard.instantiateViewController(withIdentifier: "TrendingViewController") as! TrendingViewController
            vc1.delegate = self
            btnArray = ["My Diaries", "Trending"]
            self.addButtonsInScrollMenu(btnArray)
            
            let controllerArray : NSArray = [vc1,vc2]
            self.addChildViewControllersOntoContainer(controllerArray)
            
            let border = CALayer()
            let width = CGFloat(0.5)
            border.borderColor = UIColor.lightGray.cgColor
            border.frame = CGRect(x: 0, y: menuScrollView.frame.size.height - width, width:  menuScrollView.frame.size.width, height: menuScrollView.frame.size.height)
            
            border.borderWidth = width
            menuScrollView.layer.addSublayer(border)
            menuScrollView.layer.masksToBounds = true
        }
    }
    
    func overlays() {
        
        let showValue = UserDefaults.standard.value(forKey: "DashboardOverlay") as! String
        if showValue == "true" {
            viewCount = 1
            fvc.frame = CGRect(x: self.view.frame.origin.x, y: self.view.frame.origin.y, width: self.view.frame.size.width, height: self.view.frame.size.height)
            fvc.backgroundColor = UIColor(red:0.0/255.0, green:0.0/255.0, blue:0.0/255.0, alpha:0.2)
            
            
            lblView.frame = CGRect(x: self.view.frame.origin.x + 20, y: self.view.frame.size.height - 130, width: self.view.frame.size.width - 40, height: 60)
            lblView.backgroundColor = UIColor(red:98.0/255.0, green:151.0/255.0, blue:207.0/255.0, alpha: 1.0)
            lblView.layer.cornerRadius = 15
            
            
            
            textLbl.text = "Share your wedding and other event experiences with the world"
            textLbl.numberOfLines = 0
            textLbl.lineBreakMode = .byWordWrapping
            textLbl.font = UIFont(name:"HelveticaNeue-Bold", size: 17.0)!
            textLbl.layer.cornerRadius = 15
            textLbl.backgroundColor = UIColor(red:98.0/255.0, green:151.0/255.0, blue:207.0/255.0, alpha: 1.0)
            textLbl.textColor = UIColor.white
            textLbl.frame = CGRect(x: lblView.frame.origin.x , y: 0, width: lblView.frame.size.width - 60, height: 60)
            lblView.addSubview(textLbl)
            
            icon.image = UIImage(named: "drr")
            icon.frame = CGRect(x: self.view.frame.origin.x + 40, y: self.view.frame.size.height - 80, width: 30, height: 30)
            fvc.addSubview(icon)
            
            closeicon.image = UIImage(named: "closeb")
            closeicon.frame = CGRect(x: lblView.frame.size.width - 30, y: 5, width: 20, height: 20)
            lblView.addSubview(closeicon)
            
            fvc.addSubview(lblView)
            self.tabBarController?.view.addSubview(fvc)
            
            let tap1 = UITapGestureRecognizer(target: self, action: #selector(self.tapOnProfilePic))
            fvc.isUserInteractionEnabled = true
            fvc.addGestureRecognizer(tap1)
            
            let tap2 = UITapGestureRecognizer(target: self, action: #selector(self.tapOnProfilePic))
            lblView.isUserInteractionEnabled = true
            lblView.addGestureRecognizer(tap2)
            
            let tap3 = UITapGestureRecognizer(target: self, action: #selector(self.tapOnProfilePic))
            closeicon.isUserInteractionEnabled = true
            closeicon.addGestureRecognizer(tap3)
            
        }
        
    }
    
    @objc func tapOnProfilePic(){
        self.viewCount = self.viewCount + 1
        if viewCount < 5{
            print(viewCount)
            getHeight()
        }
        else{
            fvc.removeFromSuperview()
            UserDefaults.standard.setValue("false", forKey: "DashboardOverlay")
            self.viewCount = 0
        }
    }
    
    func getHeight() {
        let space = self.view.frame.size.width / 4
        if viewCount == 2{
            textLbl.text = "Find and Compare vendors"
            icon.frame = CGRect(x: space + 40, y: self.view.frame.size.height - 80, width: 30, height: 30)
            lblView.frame = CGRect(x: self.view.frame.origin.x + 20, y: self.view.frame.size.height - 130, width: self.view.frame.size.width - 40, height: 60)
        }
        if viewCount == 3{
            textLbl.text = "Join the discussion of event lovers"
            icon.frame = CGRect(x: (space * 2) + 40, y: self.view.frame.size.height - 80, width: 30, height: 30)
            lblView.frame = CGRect(x: self.view.frame.origin.x + 20, y: self.view.frame.size.height - 130, width: self.view.frame.size.width - 40, height: 60)
        }
        if viewCount == 4{
            textLbl.text = "You can find more good stuff here!"
            icon.frame = CGRect(x: lblView.frame.size.width - 20, y: self.view.frame.size.height - 80, width: 30, height: 30)
            lblView.frame = CGRect(x: self.view.frame.origin.x + 20, y: self.view.frame.size.height - 130, width: self.view.frame.size.width - 40, height: 60)
        }
    }
    
    func overlaysOnUpload() {
        
        let showValue = UserDefaults.standard.value(forKey: "UploadOver") as! String
        if showValue == "true" {
            viewCount = 1
            fvc.frame = CGRect(x: self.view.frame.origin.x, y: self.view.frame.origin.y, width: self.view.frame.size.width, height: self.view.frame.size.height)
            fvc.backgroundColor = UIColor(red:0.0/255.0, green:0.0/255.0, blue:0.0/255.0, alpha:0.2)
            
            
            lblView.frame = CGRect(x: self.view.frame.origin.x + 20, y: 220, width: self.view.frame.size.width - 40, height: 120)
            lblView.backgroundColor = UIColor(red:98.0/255.0, green:151.0/255.0, blue:207.0/255.0, alpha: 1.0)
            lblView.layer.cornerRadius = 15
            
            
            textLbl.text = "Share interesting, exciting or funny real wedding and other event pictures as they happen for the world to see"
            textLbl.numberOfLines = 0
            textLbl.lineBreakMode = .byWordWrapping
            textLbl.font = UIFont(name:"HelveticaNeue-Bold", size: 17.0)!
            textLbl.layer.cornerRadius = 15
            textLbl.backgroundColor = UIColor(red:98.0/255.0, green:151.0/255.0, blue:207.0/255.0, alpha: 1.0)
            textLbl.textColor = UIColor.white
            textLbl.frame = CGRect(x: lblView.frame.origin.x , y: 0, width: lblView.frame.size.width - 60, height: 120)
            lblView.addSubview(textLbl)
            
            icon.image = UIImage(named: "uarr")
            icon.frame = CGRect(x: (self.view.frame.size.width / 2) - 30, y: 200, width: 30, height: 30)
            fvc.addSubview(icon)
            
            closeicon.image = UIImage(named: "closeb")
            closeicon.frame = CGRect(x: lblView.frame.size.width - 30, y: 5, width: 20, height: 20)
            lblView.addSubview(closeicon)
            
            fvc.addSubview(lblView)
            self.tabBarController?.view.addSubview(fvc)
            
            let tap1 = UITapGestureRecognizer(target: self, action: #selector(self.tapOnProfilePic1))
            fvc.isUserInteractionEnabled = true
            fvc.addGestureRecognizer(tap1)
            
            let tap2 = UITapGestureRecognizer(target: self, action: #selector(self.tapOnProfilePic1))
            lblView.isUserInteractionEnabled = true
            lblView.addGestureRecognizer(tap2)
            
            let tap3 = UITapGestureRecognizer(target: self, action: #selector(self.tapOnProfilePic1))
            closeicon.isUserInteractionEnabled = true
            closeicon.addGestureRecognizer(tap3)
            
        }
        
    }
    
    @objc func tapOnProfilePic1(){
        self.viewCount = self.viewCount + 1
        if viewCount < 3{
            print(viewCount)
            getHeight1()
        }
        else{
            fvc.removeFromSuperview()
            UserDefaults.standard.setValue("false", forKey: "UploadOver")
        }
    }
    
    func getHeight1() {
        let space = self.view.frame.size.width / 4
        if viewCount == 2{
            textLbl.text = "choose topics of interest to follow"
            icon.frame = CGRect(x: lblView.frame.size.width - 20, y: 64, width: 30, height: 30)
            lblView.frame = CGRect(x: self.view.frame.origin.x + 20, y: 84, width: self.view.frame.size.width - 40, height: 60)
            textLbl.frame = CGRect(x: lblView.frame.origin.x , y: 0, width: lblView.frame.size.width - 60, height: 60)
        }
        
    }
    
    func dairiesData(count: Int){
        let followed = UserDefaults.standard.string(forKey: "Followed")
        if count != 0 || followed == "true"{
            rightBtn.image = UIImage(named: "edit1")
            overlaysOnUpload()
        }
        else{
            rightBtn.image = UIImage(named: "")
            overlays()
        }
    }
    
    
    
    func addButtonsInScrollMenu(_ buttonArray : NSArray)
    {
        let subViews = self.menuScrollView.subviews
        for subview in subViews{
            subview.removeFromSuperview()
        }
        
        let buttonHeight: CGFloat = self.menuScrollView.frame.size.height
        var cWidth: CGFloat = 0.0
        for i in 0..<buttonArray.count {
            let tagTitle: String = buttonArray[i] as! String
            let buttonWidth: CGFloat = self.view.frame.width / 2
            let button: UIButton = UIButton()
            button.frame = CGRect(x: cWidth, y: 0.0, width: buttonWidth, height: buttonHeight)
            button.setTitle(tagTitle, for: UIControlState())
            button.addTarget(self, action: #selector(EventsViewController.buttonPressed(_:)), for: UIControlEvents.touchUpInside)
            button.tag = i
            self.menuScrollView.addSubview(button)
            let bottomView: UIView = UIView(frame: CGRect(x: 0, y: button.frame.size.height-2, width: button.frame.size.width, height: 2))
            button.setTitleColor(UIColor.gray, for: UIControlState())
            button.setTitleColor(UIColor.black, for: UIControlState.selected)
            if UIScreen.main.bounds.size.width == 414
            {
                button.titleLabel!.font = UIFont.boldSystemFont(ofSize: 19.0)
            }
            if UIScreen.main.bounds.size.width == 375
            {
                button.titleLabel!.font = UIFont.boldSystemFont(ofSize: 16.0)
            }
            if UIScreen.main.bounds.size.width == 320 && UIScreen.main.bounds.size.height == 568
            {
                button.titleLabel!.font = UIFont.boldSystemFont(ofSize: 13.0)
                
            }
            if UIScreen.main.bounds.size.width == 320 && UIScreen.main.bounds.size.height == 480
            {
                button.titleLabel!.font = UIFont.boldSystemFont(ofSize: 11.0)
                
            }
            bottomView.backgroundColor = UIColor.black
            bottomView.tag = 1001
            
            button.addSubview(bottomView)
            if i == 0 {
                button.isSelected = true
                bottomView.isHidden = false
            } else {
                bottomView.isHidden = true
            }
            cWidth += buttonWidth
            
        }
        print("scroll menu width->\(cWidth)")
        self.menuScrollView.contentSize = CGSize(width: cWidth, height: self.menuScrollView.frame.size.height)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.backgroundColor = UIColor.white
        UIApplication.shared.statusBarView?.backgroundColor = UIColor.white
    }
    
    @IBAction func filterView(_ sender: UIBarButtonItem) {
        let backItem = UIBarButtonItem()
        backItem.title = "Back"
        backItem.tintColor = UIColor.black
        navigationItem.backBarButtonItem = backItem
       
        let storyboard =  UIStoryboard(name: "Main", bundle: nil)
        let promo = storyboard.instantiateViewController(withIdentifier: "AddTopicViewController") as! AddTopicViewController
        self.navigationController?.pushViewController(promo, animated: true)
    }
    
    
    
    @objc func buttonPressed(_ sender: UIButton)
    {
        let senderbtn: UIButton = (sender as UIButton)
        
        let buttonWidth: Float = 0.0
        for subView : AnyObject in self.menuScrollView.subviews {
            
            let btn: UIButton = (subView as? UIButton)!
            let bottomView: UIView = btn.viewWithTag(1001)!
            if btn.tag == senderbtn.tag {
                btn.isSelected = true
                bottomView.isHidden = false
            } else {
                btn.isSelected = false
                bottomView.isHidden = true
                
            }
        }
        
        let yy : CGFloat = 0.0
        self.containerScrollView.setContentOffset(CGPoint(x: SCREEN_WIDTH * CGFloat(senderbtn.tag), y: 0), animated: true)
        let xx : CGFloat = (SCREEN_WIDTH * CGFloat(senderbtn.tag)) * (CGFloat(buttonWidth) / SCREEN_WIDTH) - CGFloat(buttonWidth)
        self.menuScrollView.scrollRectToVisible(CGRect(x: xx, y: yy, width: SCREEN_WIDTH, height: self.menuScrollView.frame.size.height), animated: true)
    }
    
    func addChildViewControllersOntoContainer(_ controllersArr : NSArray)
    {
        for i in 0..<controllersArr.count {
            let vc: UIViewController = (controllersArr[i] as! UIViewController)
            var frame: CGRect = CGRect(x: 0, y: 0, width: self.containerScrollView.frame.size.width, height: self.containerScrollView.frame.size.height)
            frame.origin.x = SCREEN_WIDTH * CGFloat(i)
            vc.view.frame = frame
            self.addChildViewController(vc)
            self.containerScrollView.addSubview(vc.view)
            vc.didMove(toParentViewController: self)
        }
        self.containerScrollView.contentSize = CGSize(width: SCREEN_WIDTH * CGFloat(controllersArr.count) + 1.0, height: self.containerScrollView.frame.size.height)
        self.containerScrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
        self.containerScrollView.isPagingEnabled = true
        self.containerScrollView.delegate = self as UIScrollViewDelegate
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView)
    {
        let page: CGFloat = (scrollView.contentOffset.x / SCREEN_WIDTH)
       
        
        var btn: UIButton
        var buttonWidth: CGFloat = 0.0
        for subView in self.menuScrollView.subviews {
            btn = (subView as? UIButton)!
            let bottomView: UIView = btn.viewWithTag(1001)!
            if btn.tag == Int(page) {
                btn.isSelected = true
                buttonWidth = btn.frame.size.width
                bottomView.isHidden = false
            } else {
                btn.isSelected = false
                bottomView.isHidden = true
                
            }
        }
        let yy : CGFloat = 0.0
        let xx : CGFloat = scrollView.contentOffset.x * (CGFloat(buttonWidth) / SCREEN_WIDTH ) - CGFloat(buttonWidth)
        self.menuScrollView.scrollRectToVisible(CGRect(x: CGFloat(xx), y: yy, width: SCREEN_WIDTH, height: self.menuScrollView.frame.size.height), animated: true)
    }
    
    
}


