//
//  TagListViewController.swift
//  AyefroInc
//
//  Created by Vijay Darkonde on 08/01/18.
//  Copyright © 2018 Inceptive Consulting Pvt. Ltd. All rights reserved.
//

import UIKit
import Alamofire

class TagListViewController: UIViewController , UITableViewDelegate, UITableViewDataSource{
    
    @IBOutlet var tableView: UITableView!
    
    var delagate: addTagsProto!
    var categoryArr : [String] = []
    var categoryIDArr : [String] = []
    var selectedCategory : [String] = []
    
    var loaded : Bool = false
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    override init(nibName nibNameOrNil: String!, bundle nibBundleOrNil: Bundle!) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        self.title = "Select Tags"
        
        self.tableView.tableFooterView = UIView()
        self.tableView.setEditing(true, animated: true)
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "Cell")
        
        for i in 0 ..< self.selectedCategory.count{
            if let indexof = self.categoryIDArr.index(of: self.selectedCategory[i]) {
                let indexPath = IndexPath(row: indexof, section: 0)
                self.tableView.selectRow(at: indexPath, animated: true, scrollPosition: .top)
            }
        }
        
    }
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categoryArr.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as UITableViewCell
        cell.textLabel?.text = categoryArr[indexPath.row]
        cell.textLabel?.lineBreakMode = .byWordWrapping
        cell.textLabel?.numberOfLines = 0
        cell.textLabel?.backgroundColor = UIColor.clear
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedCategory.append(categoryIDArr[indexPath.row])
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        
        print("deselected  \(categoryArr[indexPath.row])")
        categoryIDArr = categoryIDArr.filter{$0 != categoryArr[indexPath.row]}
        let serviceid = categoryIDArr[indexPath.row]
        if let indexof = selectedCategory.index(of: serviceid) {
            selectedCategory.remove(at: indexof)
        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let cellText = categoryArr[indexPath.row]
        let cellFont: UIFont? = UIFont(name:"HelveticaNeue-Bold", size: 17.0)!
        let attributedText = NSAttributedString(string: cellText, attributes: [NSAttributedStringKey.font: cellFont as Any])
        let rect: CGRect = attributedText.boundingRect(with: CGSize(width: tableView.bounds.size.width, height: CGFloat.greatestFiniteMagnitude), options: .usesLineFragmentOrigin, context: nil)
        return rect.size.height + 20
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
        return unsafeBitCast(3, to: UITableViewCellEditingStyle.self)
    }
    
    @IBAction func saveBtnClk(_ sender: AnyObject) {
        print(selectedCategory)
        delagate.selectedTags(services: String(selectedCategory.count), idArr: selectedCategory)
        self.navigationController?.popViewController(animated: true)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}



