//
//  AddDairyPostViewController.swift
//  AyefroInc
//
//  Created by Vijay Darkonde on 08/01/18.
//  Copyright © 2018 Inceptive Consulting Pvt. Ltd. All rights reserved.
//

import UIKit
import KMPlaceholderTextView
import Alamofire

protocol addTagsProto {
    func selectedTags(services: String, idArr: [String])
}

class AddDairyPostViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, addTagsProto {
    
    @IBOutlet weak var descriptionTF: KMPlaceholderTextView!
    @IBOutlet weak var uploadBtn: UIButton!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var tagLbl: UILabel!
    
    var base64Str : String! = ""
    let imagePicker = UIImagePickerController()
    var selectedImage : UIImage!
    var selectedTag: [String] = []
    var popViewController : TagListViewController!
    
    var categoryArr : [String] = []
    var categoryIDArr : [String] = []

    override func viewDidLoad() {
        super.viewDidLoad()

        imagePicker.delegate = self
        self.tabBarController?.tabBar.isHidden = true
        descriptionTF.layer.borderColor = UIColor.gray.cgColor
        descriptionTF.layer.borderWidth = 0.5
        descriptionTF.layer.cornerRadius = 5
        
        
        imageView.layer.borderColor = UIColor.gray.cgColor
        imageView.layer.borderWidth = 0.5
        imageView.layer.cornerRadius = 5
        
        uploadBtn.layer.cornerRadius = 5
        
        //        view1.dropShadow()
        //        view2.dropShadow()
        //        view3.dropShadow()
        
        self.title = "Upload to a Dairy"
        self.uploadBtn.setTitle("Upload", for: UIControlState.normal)
        // Do any additional setup after loading the view.
        getTopic()
    }
    
    
    func getTopic()  {
        categoryArr = []
        categoryIDArr = []
        DispatchQueue.main.async {
            let manager = Alamofire.SessionManager.default
            manager.session.configuration.timeoutIntervalForRequest = 120
            
            manager.request( baseURL + "api/Diary/DairyTopicsList/?userid=" + userID, method: .get, parameters: ["":""], encoding: URLEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
                
                switch(response.result) {
                case .success(_):
                    if response.result.value != nil{
                        //print(response.result.value as Any)
                    }
                    let jsonResults : NSDictionary
                    jsonResults = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! NSDictionary
                    let Success = jsonResults["status"] as! String
                    if Success == "Success"{
                        let DiaryTopicList = jsonResults["DiaryTopicList"] as! NSArray
                        for i in 0 ..< DiaryTopicList.count{
                            let data = DiaryTopicList[i] as! NSDictionary
                            let TopicId = data["TopicId"] as! Int
                            let TopicName = data["TopicName"] as! String
                            self.categoryArr.append(TopicName)
                            self.categoryIDArr.append(String(TopicId))
                        }
                    }
                    
                    break
                    
                case .failure(_):
                    print(response.error!.localizedDescription)
                    let alert: UIAlertView = UIAlertView(title: "", message: response.error!.localizedDescription, delegate: nil, cancelButtonTitle: "OK");
                    alert.show()
                    self.view.hideActivityView()
                    print(response.result.error as Any)
                    break
                    
                }
            }
        }
    }
    
    @IBAction func addImgBtnClk(_ sender: Any) {
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallary()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func addtagsBtnClk(_ sender: UIButton) {
        let backItem = UIBarButtonItem()
        backItem.title = "Back"
        backItem.tintColor = UIColor.black
        navigationItem.backBarButtonItem = backItem
        self.popViewController = self.storyboard?.instantiateViewController(withIdentifier: "TagListViewController")as! TagListViewController
        self.popViewController.delagate = self
        self.popViewController.selectedCategory = self.selectedTag
        self.popViewController.categoryArr = self.categoryArr
        self.popViewController.categoryIDArr = self.categoryIDArr
        self.navigationController?.pushViewController(self.popViewController, animated: true)
    }
    
    
    @IBAction func uploadImgBtnClk(_ sender: UIButton) {
        if base64Str == ""{
            let alert: UIAlertView = UIAlertView(title: "", message: "Please Select Image.", delegate: nil, cancelButtonTitle: "OK");
            alert.show()
        }
        else if (descriptionTF.text.trimmingCharacters(in: .whitespaces).isEmpty){
            let alert: UIAlertView = UIAlertView(title: "", message: "Please Enter Description.", delegate: nil, cancelButtonTitle: "OK");
            alert.show()
        }
        else if selectedTag.count == 0{
            let alert: UIAlertView = UIAlertView(title: "", message: "Please Select at least One Tag.", delegate: nil, cancelButtonTitle: "OK");
            alert.show()
        }
        else{
            let services = self.selectedTag.joined(separator: ",")
            DispatchQueue.main.async {
//                self.view.showActivityView(withLabel: "Loading")
                let parameters : [String: Any] = [
                    "ServiceProviderId": userID as String,
                    "DTopics": services as String,
                    "DImageDesc":self.descriptionTF.text as String,
                ]
                
                Alamofire.upload(multipartFormData: { (multipartFormData) in
                    multipartFormData.append(UIImageJPEGRepresentation(self.selectedImage, 0.5)!, withName: "image", fileName: "swift_file.jpeg", mimeType: "image/jpeg")
                    for (key, value) in parameters {
                        multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
                    }
                    
                }, to: baseURL + "api/Diary/PostDiaryImage")
                { (result) in
                    switch result {
                    case .success(let upload, _, _):
                        
                        
                        upload.uploadProgress(closure: { (Progress) in
                            print("Upload Progress: \(Progress.fractionCompleted)")
                        })
                        
                        upload.responseJSON { response in
                            self.view.hideActivityView()
                            let jsonResults : NSDictionary
                            jsonResults = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! NSDictionary
                            let status = jsonResults["status"] as! String
                            if status == "Success"{
                                
                            }
                            else{
                                let refreshAlert = UIAlertController(title: "", message: "Failed to upload. Please try later", preferredStyle: UIAlertControllerStyle.alert)
                                
                                refreshAlert.addAction(UIAlertAction(title: "OK", style: .cancel , handler: { (action: UIAlertAction!) in
                                    self.navigationController?.popViewController(animated: true)
                                }))
                      
                                self.present(refreshAlert, animated: true, completion: nil)
                            }
                            
                        }
                        
                        let refreshAlert = UIAlertController(title: "", message: "Your dairy is uploading, please check after some time.", preferredStyle: UIAlertControllerStyle.alert)
                        
                        refreshAlert.addAction(UIAlertAction(title: "OK", style: .cancel , handler: { (action: UIAlertAction!) in
                            self.navigationController?.popViewController(animated: true)
                        }))
                        
                        self.present(refreshAlert, animated: true, completion: nil)
                        
                        
                        
                    case .failure(let encodingError):
                        //self.delegate?.showFailAlert()
                        print(encodingError)
                    }
                    
                }
                
            }
        }
    }

    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera))
        {
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openGallary()
    {
        imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        imagePicker.allowsEditing = false
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    internal func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            imageView.image = pickedImage
            selectedImage = pickedImage
            imageView.layer.cornerRadius = 5
            let imageData: NSData = UIImageJPEGRepresentation(pickedImage, 0.4)! as NSData
            let imageStr = imageData.base64EncodedString(options: .lineLength64Characters)
            base64Str = imageStr
            self.uploadBtn.setTitle("", for: UIControlState.normal)
        }
        
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func selectedTags(services: String, idArr: [String]){
        selectedTag = idArr
        tagLbl.text = "What is this picture about? (Add some tags) " + services + " Tag(s)"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
