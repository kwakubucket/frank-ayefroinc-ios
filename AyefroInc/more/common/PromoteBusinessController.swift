//
//  PromoteBusinessController.swift
//  AyefroInc
//
//  Created by Vijay Darkonde on 28/12/17.
//  Copyright © 2017 Inceptive Consulting Pvt. Ltd. All rights reserved.
//

import UIKit

class PromoteBusinessController: UIViewController {
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Promote Your Business"
        self.tabBarController?.tabBar.isHidden = true
    }
    
    @IBAction func gotoGalleryBtnCLk(_ sender: UIButton) {
        let backItem = UIBarButtonItem()
        backItem.title = "Back"
        backItem.tintColor = UIColor.black
        navigationItem.backBarButtonItem = backItem
        let storyboard =  UIStoryboard(name: "Main", bundle: nil)
        let promo = storyboard.instantiateViewController(withIdentifier: "SPGalleryViewController") as! SPGalleryViewController
        self.navigationController?.pushViewController(promo, animated: true)
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    

}
