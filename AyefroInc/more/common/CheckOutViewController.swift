//
//  CheckOutViewController.swift
//  AyefroInc
//
//  Created by Vijay Darkonde on 05/01/18.
//  Copyright © 2018 Inceptive Consulting Pvt. Ltd. All rights reserved.
//

import UIKit

class CheckOutViewController: UIViewController, UIWebViewDelegate {
    
    @IBOutlet var webView: UIWebView!
    var webViewurl : String! = ""
    
    var cancelDel: cancelProto!
    var successDel: successProto!
    var token: String! = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.title = "Checkout"
        
        let url = NSURL (string: webViewurl);
        let myURLRequest:URLRequest = URLRequest(url: url! as URL)
        webView.loadRequest(myURLRequest)
        webView.delegate = self
        
        if UIScreen.main.bounds.size.width == 414
        {
            self.shyNavBarManager.scrollView = self.webView.scrollView;
        }
        else if UIScreen.main.bounds.size.width == 375 && UIScreen.main.bounds.size.height == 667
        {
            self.shyNavBarManager.scrollView = self.webView.scrollView;
        }
        else if UIScreen.main.bounds.size.width == 320 && UIScreen.main.bounds.size.height == 568
        {
            self.shyNavBarManager.scrollView = self.webView.scrollView;
        }
        else if UIScreen.main.bounds.size.width == 320 && UIScreen.main.bounds.size.height == 480
        {
            
        }
       
    }
    
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        print(request.url?.absoluteString as! String)
        if (request.url?.absoluteString == "https://ayefroinc.com/?token=" + self.token) {
            self.successDel.successTrasaction()
            self.navigationController?.popViewController(animated: true)
        }
        if (request.url?.absoluteString == "https://ayefroinc.com/about/") {
            self.cancelDel.cancelTransaction()
            self.navigationController?.popViewController(animated: true)
        }
        
        return true // allow the form to in fact send the form
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}

