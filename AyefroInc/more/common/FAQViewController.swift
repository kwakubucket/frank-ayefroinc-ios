//
//  FAQViewController.swift
//  AyefroInc
//
//  Created by Vijay Darkonde on 24/11/17.
//  Copyright © 2017 Inceptive Consulting Pvt. Ltd. All rights reserved.
//

import UIKit

class FAQViewController: UIViewController {
    
    
    @IBOutlet var webView: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "FAQ"
        
        let url = NSURL (string: "http://www.ayefroinc.com/faq");
        let myURLRequest:URLRequest = URLRequest(url: url! as URL)
        webView.loadRequest(myURLRequest);
        self.tabBarController?.tabBar.isHidden = true
        if UIScreen.main.bounds.size.width == 414
        {
            self.shyNavBarManager.scrollView = self.webView.scrollView;
        }
        else if UIScreen.main.bounds.size.width == 375 && UIScreen.main.bounds.size.height == 667
        {
            self.shyNavBarManager.scrollView = self.webView.scrollView;
        }
        else if UIScreen.main.bounds.size.width == 320 && UIScreen.main.bounds.size.height == 568
        {
            self.shyNavBarManager.scrollView = self.webView.scrollView;
        }
        else if UIScreen.main.bounds.size.width == 320 && UIScreen.main.bounds.size.height == 480
        {
            
        }
        self.navigationController?.isNavigationBarHidden = false
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

