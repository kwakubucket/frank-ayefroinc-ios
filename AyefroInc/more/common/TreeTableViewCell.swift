//
//  TreeTableViewCell.swift
//  RATreeViewExamples
//
//  Created by Rafal Augustyniak on 22/11/15.
//  Copyright © 2015 com.Augustyniak. All rights reserved.
//

import UIKit

class TreeTableViewCell : UITableViewCell {

    @IBOutlet var iconImageView: UIImageView!
    @IBOutlet private weak var customTitleLabel: UILabel!

    override func awakeFromNib() {
        selectedBackgroundView? = UIView()
        selectedBackgroundView?.backgroundColor = .white
    }

    var additionButtonActionBlock : ((TreeTableViewCell) -> Void)?;

    func setup(withTitle title: String, level : Int,imageName: String,childCount: Int, additionalButtonHidden: Bool) {
        customTitleLabel.text = title
        
        let left = 40.0 + 20.0 * CGFloat(level+1)
        self.customTitleLabel.frame.origin.x = left
        self.customTitleLabel.frame.size.width = self.frame.size.width
        iconImageView.image = UIImage(named: imageName)
        let imagealign = 20 * CGFloat(level+1)
        self.iconImageView.frame.origin.x = imagealign
//        self.dropDownImage.frame.origin.x = self.frame.size.width
        
    }

    func additionButtonTapped(_ sender : AnyObject) -> Void {
        if let action = additionButtonActionBlock {
            action(self)
        }
    }

}
