//
//  PromotionController.swift
//  AyefroInc
//
//  Created by Vijay Darkonde on 08/01/18.
//  Copyright © 2018 Inceptive Consulting Pvt. Ltd. All rights reserved.
//

import UIKit

class PromotionController: UIViewController, subscribetionProto {

    @IBOutlet weak var scrollHeight: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Promote Your Business"
        self.tabBarController?.tabBar.isHidden = true
        // Do any additional setup after loading the view.
        if UIScreen.main.bounds.size.width == 414
        {
            scrollHeight.constant = 70
        }
        else if UIScreen.main.bounds.size.width == 375 && UIScreen.main.bounds.size.height == 667
        {
            scrollHeight.constant = 150
        }
        else if UIScreen.main.bounds.size.width == 320 && UIScreen.main.bounds.size.height == 568
        {
            scrollHeight.constant = 250
        }
        else if UIScreen.main.bounds.size.width == 320 && UIScreen.main.bounds.size.height == 480
        {
            scrollHeight.constant = 350
        }
    }
    
    func subscribtionSuccess(){
        
    }
    
    @IBAction func recommendedVendorBtnClk(_ sender: UIButton) {
        let backItem = UIBarButtonItem()
        backItem.title = "Back"
        backItem.tintColor = UIColor.black
        self.navigationItem.backBarButtonItem = backItem
        let storyboard =  UIStoryboard(name: "Main", bundle: nil)
        let promo = storyboard.instantiateViewController(withIdentifier: "SubscriptionViewController") as! SubscriptionViewController
        promo.delegate = self
        self.navigationController?.pushViewController(promo, animated: true)
    }
    
    @IBAction func advertiseBtnClk(_ sender: UIButton) {
        let backItem = UIBarButtonItem()
        backItem.title = "Back"
        backItem.tintColor = UIColor.black
        navigationItem.backBarButtonItem = backItem
        let storyboard =  UIStoryboard(name: "Main", bundle: nil)
        let promo = storyboard.instantiateViewController(withIdentifier: "PromoteBusinessController") as! PromoteBusinessController
        self.navigationController?.pushViewController(promo, animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
