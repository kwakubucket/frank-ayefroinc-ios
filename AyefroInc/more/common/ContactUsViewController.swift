//
//  ContactUsViewController.swift
//  AyefroInc
//
//  Created by Vijay Darkonde on 24/11/17.
//  Copyright © 2017 Inceptive Consulting Pvt. Ltd. All rights reserved.
//

import UIKit
import MessageUI

class ContactUsViewController: UIViewController, MFMailComposeViewControllerDelegate, MFMessageComposeViewControllerDelegate, UITableViewDataSource, UITableViewDelegate {

    
    @IBOutlet var tableView: UITableView!
    
    var titlelbl: [String] = ["+233243817622","+233243817622","info@ayefroinc.com","Watch Video Tutorials", "Frequently Asked Questions"]
    var img: [String] = ["telephone.png","chaticon.png","emailicon.png","youtube-1","question-1"]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Contact Us"
        self.tabBarController?.tabBar.isHidden = true
        
        tableView.tableFooterView = UIView()
        // Do any additional setup after loading the view.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ContactTableViewCell", for: indexPath) as! ContactTableViewCell
        cell.menulbl.text = titlelbl[indexPath.row]
        cell.menuimg.image = UIImage(named: img[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0{
            if let url = URL(string: "tel://+233243817622"), UIApplication.shared.canOpenURL(url) {
                if #available(iOS 10, *) {
                    UIApplication.shared.open(url)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
        }
        else if indexPath.row == 1{
            if (MFMessageComposeViewController.canSendText()) {
                let controller = MFMessageComposeViewController()
                controller.body = ""
                controller.recipients = ["+233243817622"]
                controller.messageComposeDelegate = self
                self.present(controller, animated: true, completion: nil)
            }
        }
        else if indexPath.row == 2{
            if MFMailComposeViewController.canSendMail() {
                let toRecipients = ["info@ayefroinc.com"]
                let mc: MFMailComposeViewController = MFMailComposeViewController()
                mc.mailComposeDelegate = self
                mc.setToRecipients(toRecipients)
                mc.isNavigationBarHidden = true
                self.present(mc, animated: false, completion: nil)
            }
            else
            {
                let alert: UIAlertView = UIAlertView(title: "", message: "Mail app not install on your device. Please send an email to info@ayefroinc.com.com", delegate: nil, cancelButtonTitle: "OK");
                alert.show()
            }
        }
        else if indexPath.row == 3{
            let youtubeId = "EfwMRB-_3yg"
            var youtubeUrl = NSURL(string:"youtube://\(youtubeId)")!
            if UIApplication.shared.canOpenURL(youtubeUrl as URL){
                UIApplication.shared.openURL(youtubeUrl as URL)
            } else{
                youtubeUrl = NSURL(string:"https://www.youtube.com/watch?v=\(youtubeId)")!
                UIApplication.shared.openURL(youtubeUrl as URL)
            }
        }
        else if indexPath.row == 4{
            let backItem = UIBarButtonItem()
            backItem.title = "Back"
            backItem.tintColor = UIColor.black
            navigationItem.backBarButtonItem = backItem
            let storyboard =  UIStoryboard(name: "Main", bundle: nil)
            let promo = storyboard.instantiateViewController(withIdentifier: "FAQViewController") as! FAQViewController
            self.navigationController?.pushViewController(promo, animated: true)
        }
    }
    
    
    
    @IBAction func faqBtnClk(_ sender: UIButton) {
        let backItem = UIBarButtonItem()
        backItem.title = "Back"
        backItem.tintColor = UIColor.black
        navigationItem.backBarButtonItem = backItem
        let storyboard =  UIStoryboard(name: "Main", bundle: nil)
        let promo = storyboard.instantiateViewController(withIdentifier: "FAQViewController") as! FAQViewController
        self.navigationController?.pushViewController(promo, animated: true)
    }
    
    @IBAction func smsBtnClk(_ sender: UIButton) {
        if (MFMessageComposeViewController.canSendText()) {
            let controller = MFMessageComposeViewController()
            controller.body = ""
            controller.recipients = ["+233243817622"]
            controller.messageComposeDelegate = self
            self.present(controller, animated: true, completion: nil)
        }
    }
    
    @IBAction func mailBtnClk(_ sender: UIButton) {
        if MFMailComposeViewController.canSendMail() {
            let toRecipients = ["info@ayefroinc.com"]
            let mc: MFMailComposeViewController = MFMailComposeViewController()
            mc.mailComposeDelegate = self
            mc.setToRecipients(toRecipients)
            mc.isNavigationBarHidden = true
            self.present(mc, animated: false, completion: nil)
        }
        else
        {
            let alert: UIAlertView = UIAlertView(title: "", message: "Mail app not install on your device. Please send an email to info@ayefroinc.com.com", delegate: nil, cancelButtonTitle: "OK");
            alert.show()
        }

    }
    
    @IBAction func videoTutBtnClk(_ sender: UIButton) {
        let youtubeId = "EfwMRB-_3yg"
        var youtubeUrl = NSURL(string:"youtube://\(youtubeId)")!
        if UIApplication.shared.canOpenURL(youtubeUrl as URL){
            UIApplication.shared.openURL(youtubeUrl as URL)
        } else{
            youtubeUrl = NSURL(string:"https://www.youtube.com/watch?v=\(youtubeId)")!
            UIApplication.shared.openURL(youtubeUrl as URL)
        }
    }
    
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        switch result.rawValue {
        case MFMailComposeResult.cancelled.rawValue:
            //NSLog("Mail Cancelled")
            self.dismiss(animated: false, completion: nil)
            break
        case MFMailComposeResult.saved.rawValue:
            //NSLog("Mail Saved")
            self.dismiss(animated: false, completion: nil)
            break
        case MFMailComposeResult.failed.rawValue:
            //NSLog("Mail Sent Fail", [error!.localizedDescription])
            self.dismiss(animated: false, completion: nil)
            break
        case MFMailComposeResult.sent.rawValue:
            //NSLog("Mail Sent")
            self.dismiss(animated: false, completion: nil)
            break
            
        default:
            break
        }
    }
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        switch (result) {
        case MessageComposeResult.cancelled:
            print("Message was cancelled")
            self.dismiss(animated: false, completion: nil)
        case MessageComposeResult.failed:
            print("Message failed")
            self.dismiss(animated: false, completion: nil)
        case MessageComposeResult.sent:
            print("Message was sent")
            self.dismiss(animated: false, completion: nil)
        default:
            break;
        }
    }
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
