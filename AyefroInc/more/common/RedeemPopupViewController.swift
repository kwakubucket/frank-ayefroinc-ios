//
//  RedeemPopupViewController.swift
//  AyefroInc
//
//  Created by Vijay Darkonde on 12/01/18.
//  Copyright © 2018 Inceptive Consulting Pvt. Ltd. All rights reserved.
//

import UIKit
import Alamofire
import Branch

class RedeemPopupViewController: UIViewController{
    
    @IBOutlet weak var networkTF: NiceTextField!
    @IBOutlet weak var phoneNumberTF: NiceTextField!
    @IBOutlet var popUpView: UIView!
    
    var a_id : String = ""
    var delegate : redeemPoints!
    var redeemOptions: String = "Redeem notification"
    var points : Int = 0
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    override init(nibName nibNameOrNil: String!, bundle nibBundleOrNil: Bundle!) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        self.popUpView.layer.cornerRadius = 5
        self.popUpView.layer.shadowOpacity = 0.0
        self.popUpView.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        
//        let tap3 = UITapGestureRecognizer(target: self, action: #selector(self.tapOnProfilePic))
//        self.view.isUserInteractionEnabled = true
//        self.view.addGestureRecognizer(tap3)
    }
    
    
    func showInView(_ aView: UIView!, animated: Bool)
    {
        aView.addSubview(self.view)
        if animated
        {
            self.showAnimate()
        }
    }
    
    func showAnimate()
    {
        self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view.alpha = 0.0;
        UIView.animate(withDuration: 0.25, animations: {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        });
    }
    
    @objc func tapOnProfilePic(){
        removeAnimate()
    }
    
    func removeAnimate()
    {
        UIView.animate(withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view.alpha = 0.0;
        }, completion:{(finished : Bool)  in
            if (finished)
            {
                self.view.removeFromSuperview()
            }
        });
    }
    
    @IBAction func backBtnClk(_ sender: AnyObject) {
        networkTF.resignFirstResponder()
        phoneNumberTF.resignFirstResponder()
        removeAnimate()
    }
    
    @IBAction func saveBtnClk(_ sender: AnyObject) {
        reloadValue = true
        networkTF.resignFirstResponder()
        phoneNumberTF.resignFirstResponder()
        if networkTF.text?.isEmpty == true
        {
            networkTF.layer.borderColor = UIColor.red.cgColor
            networkTF.layer.borderWidth = 1
        }
        else if phoneNumberTF.text?.isEmpty == true
        {
            phoneNumberTF.layer.borderColor = UIColor.red.cgColor
            phoneNumberTF.layer.borderWidth = 1
        }
        else
        {
            self.view.showActivityView(withLabel: "Loading")
            DispatchQueue.main.async {
//                let name = userFirstName + " " + userLastName + " \r"
//                let userNumber = PhoneNumber as! String
//                let points = "\rPoints redeemed - " + String(self.points)
//                let rechargeNumber =  " \r\rPhone number to send airtime - " + self.phoneNumberTF.text!
//                let network = " \rNetwork of phone number - " + self.networkTF.text!
//                let body = name + userNumber + points + rechargeNumber + network
                
                let parameters : [String: Any] = [
                    "User_Id": userID,
                    "Points_Redeem": self.points,
                    "Reciever_Phone": self.phoneNumberTF.text!,
                    "Reciever_Network": self.networkTF.text!
                ]
                
                let manager = Alamofire.SessionManager.default
                manager.session.configuration.timeoutIntervalForRequest = 120
                
                manager.request(baseURL + "api/EmailsAPI/RedeemMail", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
                    
                    switch(response.result) {
                    case .success(_):
                        if response.result.value != nil{
                            //print(response.result.value as Any)
                        }
                        Branch.getInstance().redeemRewards(self.points)
                        self.view.hideActivityView()
                        self.removeAnimate()
                        let refreshAlert = UIAlertController(title: "Congratulations!", message: "You will receive your reward soon. Keep inviting friends and family to keep winning.", preferredStyle: UIAlertControllerStyle.alert)
                        
                        refreshAlert.addAction(UIAlertAction(title: "OK", style: .cancel , handler: { (action: UIAlertAction!) in
                            self.delegate.redeemSuccess()
                        }))
                        
                        self.present(refreshAlert, animated: true, completion: nil)
                        
                        
                        break
                        
                    case .failure(_):
                        print(response.error!.localizedDescription)
                        let alert: UIAlertView = UIAlertView(title: "", message: response.error!.localizedDescription, delegate: nil, cancelButtonTitle: "OK");
                        alert.show()
                        self.view.hideActivityView()
                        print(response.result.error as Any)
                        break
                        
                    }
                }
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}


