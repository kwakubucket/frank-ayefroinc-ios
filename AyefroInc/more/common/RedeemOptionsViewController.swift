//
//  RedeemOptionsViewController.swift
//  AyefroInc
//
//  Created by Vijay Darkonde on 12/01/18.
//  Copyright © 2018 Inceptive Consulting Pvt. Ltd. All rights reserved.
//

import UIKit

protocol redeemPoints {
    func redeemSuccess()
}

class RedeemOptionsViewController: UIViewController, redeemPoints {
    
    
    @IBOutlet weak var firstRedeem: UIButton!
    @IBOutlet weak var secondRedeem: UIButton!
    @IBOutlet weak var thirdRedeem: UIButton!
    @IBOutlet weak var fourthRedeem: UIButton!
    @IBOutlet weak var lastRedeem: UIButton!
    @IBOutlet weak var scrollHeight: NSLayoutConstraint!
    
    var popViewController : RedeemPopupViewController!
    var popViewController1 : BigRedeemViewController!

    var points: Int! = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Redeem Options"
        let redeem = UIImage(named: "Redeem")
        let noredeem = UIImage(named: "noredeem")
        self.navigationController?.navigationBar.tintColor = UIColor.black
        if points >= 50{
            self.firstRedeem.setImage(redeem, for: UIControlState.normal)
        }
        else{
            self.firstRedeem.setImage(noredeem, for: UIControlState.normal)
        }
        if points >= 100{
            self.secondRedeem.setImage(redeem, for: UIControlState.normal)
        }
        else{
            self.secondRedeem.setImage(noredeem, for: UIControlState.normal)
        }
        if points >= 200{
            self.thirdRedeem.setImage(redeem, for: UIControlState.normal)
        }
        else{
            self.thirdRedeem.setImage(noredeem, for: UIControlState.normal)
        }
        if points >= 1500{
            self.fourthRedeem.setImage(redeem, for: UIControlState.normal)
        }
        else{
            self.fourthRedeem.setImage(noredeem, for: UIControlState.normal)
        }
        if points >= 20000{
            self.lastRedeem.setImage(redeem, for: UIControlState.normal)
        }
        else{
            self.lastRedeem.setImage(noredeem, for: UIControlState.normal)
        }
        
        if UIScreen.main.bounds.size.width == 414
        {
        }
        else if UIScreen.main.bounds.size.width == 375 && UIScreen.main.bounds.size.height == 667
        {
        }
        else if UIScreen.main.bounds.size.width == 320 && UIScreen.main.bounds.size.height == 568
        {
            scrollHeight.constant = 50
        }
        else if UIScreen.main.bounds.size.width == 320 && UIScreen.main.bounds.size.height == 480
        {
            scrollHeight.constant = 100
        }
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func firstRedeemBtnClk(_ sender: UIButton) {
        if points >= 50{
            self.popViewController = self.storyboard?.instantiateViewController(withIdentifier: "RedeemPopupViewController")as! RedeemPopupViewController
            self.popViewController.delegate = self
            self.popViewController.points = 50
            self.popViewController.showInView(self.view, animated: true)
        }
    }
    
    @IBAction func secondRedeemBtnClk(_ sender: UIButton) {
        if points >= 100{
            self.popViewController = self.storyboard?.instantiateViewController(withIdentifier: "RedeemPopupViewController")as! RedeemPopupViewController
            self.popViewController.delegate = self
            self.popViewController.points = 100
            self.popViewController.showInView(self.view, animated: true)
        }
        
    }
    
    @IBAction func thirdRedeemBtnClk(_ sender: UIButton) {
        if points >= 200{
            self.popViewController = self.storyboard?.instantiateViewController(withIdentifier: "RedeemPopupViewController")as! RedeemPopupViewController
            self.popViewController.delegate = self
            self.popViewController.points = 200
            self.popViewController.showInView(self.view, animated: true)
        }
    }
    
    @IBAction func fourthRedeemBtnClk(_ sender: UIButton) {
        if points >= 1500{
            self.popViewController = self.storyboard?.instantiateViewController(withIdentifier: "RedeemPopupViewController")as! RedeemPopupViewController
            self.popViewController.delegate = self
            self.popViewController.points = 1500
            self.popViewController.showInView(self.view, animated: true)
        }
    }
    
    @IBAction func lastRedeemBtnClk(_ sender: UIButton) {
        if points >= 20000{
            self.popViewController1 = self.storyboard?.instantiateViewController(withIdentifier: "BigRedeemViewController")as! BigRedeemViewController
            self.popViewController1.delegate = self
            self.popViewController1.points = 20000
            self.popViewController1.showInView(self.view, animated: true)
        }
        
    }
    
    
    func redeemSuccess(){
        self.navigationController?.popViewController(animated: true)
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
