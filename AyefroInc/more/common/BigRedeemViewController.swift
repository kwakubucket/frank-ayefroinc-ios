//
//  BigRedeemViewController.swift
//  AyefroInc
//
//  Created by Vijay Darkonde on 18/01/18.
//  Copyright © 2018 Inceptive Consulting Pvt. Ltd. All rights reserved.
//

import UIKit
import Alamofire
import Branch
import DLRadioButton

class BigRedeemViewController: UIViewController {
    

    @IBOutlet var popUpView: UIView!
    
    var a_id : String = ""
    var delegate : redeemPoints!
    var redeemOptions: String = "Redeem notification"
    var points : Int = 0
    var roleValue: String! = ""

    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    override init(nibName nibNameOrNil: String!, bundle nibBundleOrNil: Bundle!) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        self.popUpView.layer.cornerRadius = 5
        self.popUpView.layer.shadowOpacity = 0.0
        self.popUpView.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        
    }
    
    
    func showInView(_ aView: UIView!, animated: Bool)
    {
        aView.addSubview(self.view)
        if animated
        {
            self.showAnimate()
        }
    }
    
    func showAnimate()
    {
        self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view.alpha = 0.0;
        UIView.animate(withDuration: 0.25, animations: {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        });
    }
    
    @objc func tapOnProfilePic(){
        removeAnimate()
    }
    
    func removeAnimate()
    {
        UIView.animate(withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view.alpha = 0.0;
        }, completion:{(finished : Bool)  in
            if (finished)
            {
                self.view.removeFromSuperview()
            }
        });
    }
    
    @IBAction func backBtnClk(_ sender: AnyObject) {
        
        removeAnimate()
    }
    
    @IBAction func logSelectedButton(radioButton : DLRadioButton) {
        if (radioButton.isMultipleSelectionEnabled) {
            for button in radioButton.selectedButtons() {
                print(String(format: "%@ is selected.\n", button.titleLabel!.text!));
                roleValue = button.titleLabel!.text!
            }
        } else {
            print(String(format: "%@ is selected.\n", radioButton.selected()!.titleLabel!.text!));
            roleValue = radioButton.selected()!.titleLabel!.text!
        }
    }
    
    @IBAction func saveBtnClk(_ sender: AnyObject) {
        reloadValue = true
        if roleValue == ""{
            let alert: UIAlertView = UIAlertView(title: "", message: "Please any one device", delegate: nil, cancelButtonTitle: "OK");
            alert.show()
        }
        else
        {
            self.view.showActivityView(withLabel: "Loading")
            let prefs: UserDefaults? = UserDefaults.standard
            let PhoneNumber: String? = prefs?.string(forKey: "PhoneNumber")
            DispatchQueue.main.async {
                

                let parameters : [String: Any] = [
                    "User_Id":userID,
                    "Points_Redeem":self.points,
                    "Phone_Type": self.roleValue
                ]

                let manager = Alamofire.SessionManager.default
                manager.session.configuration.timeoutIntervalForRequest = 120

                manager.request(baseURL + "api/EmailsAPI/RedeemPhoneMail", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in

                    switch(response.result) {
                    case .success(_):
                        if response.result.value != nil{
                            //print(response.result.value as Any)
                        }
                        Branch.getInstance().redeemRewards(self.points)
                        self.view.hideActivityView()
                        self.removeAnimate()
                        let refreshAlert = UIAlertController(title: "Congratulations!", message: "You will receive your reward soon. Keep inviting friends and family to keep winning.", preferredStyle: UIAlertControllerStyle.alert)

                        refreshAlert.addAction(UIAlertAction(title: "OK", style: .cancel , handler: { (action: UIAlertAction!) in
                            self.delegate.redeemSuccess()
                        }))

                        self.present(refreshAlert, animated: true, completion: nil)


                        break

                    case .failure(_):
                        print(response.error!.localizedDescription)
                        let alert: UIAlertView = UIAlertView(title: "", message: response.error!.localizedDescription, delegate: nil, cancelButtonTitle: "OK");
                        alert.show()
                        self.view.hideActivityView()
                        print(response.result.error as Any)
                        break

                    }
                }
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}



