//
//  ReferEarnViewController.swift
//  AyefroInc
//
//  Created by Vijay Darkonde on 12/01/18.
//  Copyright © 2018 Inceptive Consulting Pvt. Ltd. All rights reserved.
//

import UIKit

class ReferEarnViewController: UIViewController, UIScrollViewDelegate {
    
    
    @IBOutlet weak var menuScrollView: UIScrollView!
    @IBOutlet weak var containerScrollView: UIScrollView!
    var kDefaultEdgeInsets = UIEdgeInsetsMake(8, 16, 8, 16)
    var SCREEN_WIDTH = UIScreen.main.bounds.size.width
    var btnArray = NSArray()

    
    var viewCount : Int! = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Refer and Redeem"
        reloadValue = true
        self.tabBarController?.tabBar.isHidden = true
        _ = ModelManager.instance.getUserLoginStatus()
        let path = Bundle.main.path(forResource: "countryCode", ofType: "json")
        do{
            let data = try Data(contentsOf: URL(fileURLWithPath: path!), options: .mappedIfSafe)
            let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves) as! NSDictionary
            let countries = jsonResult.object(forKey: "countries") as! NSDictionary
            let country = countries.object(forKey: "country") as! NSArray
            for i in 0 ..< country.count
            {
                let countryObj = country[i] as! NSDictionary
                let countryName = countryObj.object(forKey: "countryName") as! String
                if Country == countryName{
                    let currencySymbol = countryObj.object(forKey: "currencySymbol") as! String
                    UserDefaults.standard.setValue(currencySymbol, forKey: "currencySymbol")
                    print("\(UserDefaults.standard.value(forKey: "currencySymbol")!)")
                    break
                }
            }
            
        }
        catch{
            
        }
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.tabBarController?.tabBar.isHidden = true
        if reloadValue == true {
            reloadValue = false
            let storyboard =  UIStoryboard(name: "Main", bundle: nil)
            let vc1 = storyboard.instantiateViewController(withIdentifier: "InviteUserViewController") as! InviteUserViewController
            let vc2 = storyboard.instantiateViewController(withIdentifier: "RedeemPointsViewController") as! RedeemPointsViewController
            btnArray = ["Invite", "Redeem Points"]
            self.addButtonsInScrollMenu(btnArray)
            
            let controllerArray : NSArray = [vc1,vc2]
            self.addChildViewControllersOntoContainer(controllerArray)
            
            let border = CALayer()
            let width = CGFloat(0.5)
            border.borderColor = UIColor.lightGray.cgColor
            border.frame = CGRect(x: 0, y: menuScrollView.frame.size.height - width, width:  menuScrollView.frame.size.width, height: menuScrollView.frame.size.height)
            
            border.borderWidth = width
            menuScrollView.layer.addSublayer(border)
            menuScrollView.layer.masksToBounds = true
        }
    }
    
    
    func addButtonsInScrollMenu(_ buttonArray : NSArray)
    {
        let subViews = self.menuScrollView.subviews
        for subview in subViews{
            subview.removeFromSuperview()
        }
        
        let buttonHeight: CGFloat = self.menuScrollView.frame.size.height
        var cWidth: CGFloat = 0.0
        for i in 0..<buttonArray.count {
            let tagTitle: String = buttonArray[i] as! String
            let buttonWidth: CGFloat = self.view.frame.width / 2
            let button: UIButton = UIButton()
            button.frame = CGRect(x: cWidth, y: 0.0, width: buttonWidth, height: buttonHeight)
            button.setTitle(tagTitle, for: UIControlState())
            button.addTarget(self, action: #selector(EventsViewController.buttonPressed(_:)), for: UIControlEvents.touchUpInside)
            button.tag = i
            self.menuScrollView.addSubview(button)
            let bottomView: UIView = UIView(frame: CGRect(x: 0, y: button.frame.size.height-2, width: button.frame.size.width, height: 2))
            button.setTitleColor(UIColor.gray, for: UIControlState())
            button.setTitleColor(UIColor.black, for: UIControlState.selected)
            if UIScreen.main.bounds.size.width == 414
            {
                button.titleLabel!.font = UIFont.boldSystemFont(ofSize: 19.0)
            }
            if UIScreen.main.bounds.size.width == 375
            {
                button.titleLabel!.font = UIFont.boldSystemFont(ofSize: 16.0)
            }
            if UIScreen.main.bounds.size.width == 320 && UIScreen.main.bounds.size.height == 568
            {
                button.titleLabel!.font = UIFont.boldSystemFont(ofSize: 13.0)
                
            }
            if UIScreen.main.bounds.size.width == 320 && UIScreen.main.bounds.size.height == 480
            {
                button.titleLabel!.font = UIFont.boldSystemFont(ofSize: 11.0)
                
            }
            bottomView.backgroundColor = UIColor.black
            bottomView.tag = 1001
            
            button.addSubview(bottomView)
            if i == 0 {
                button.isSelected = true
                bottomView.isHidden = false
            } else {
                bottomView.isHidden = true
            }
            cWidth += buttonWidth
            
        }
        print("scroll menu width->\(cWidth)")
        self.menuScrollView.contentSize = CGSize(width: cWidth, height: self.menuScrollView.frame.size.height)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.backgroundColor = UIColor.white
        UIApplication.shared.statusBarView?.backgroundColor = UIColor.white
    }
    
    
    @objc func buttonPressed(_ sender: UIButton)
    {
        let senderbtn: UIButton = (sender as UIButton)
        
        let buttonWidth: Float = 0.0
        for subView : AnyObject in self.menuScrollView.subviews {
            
            let btn: UIButton = (subView as? UIButton)!
            let bottomView: UIView = btn.viewWithTag(1001)!
            if btn.tag == senderbtn.tag {
                btn.isSelected = true
                bottomView.isHidden = false
            } else {
                btn.isSelected = false
                bottomView.isHidden = true
                
            }
        }
        
        let yy : CGFloat = 0.0
        self.containerScrollView.setContentOffset(CGPoint(x: SCREEN_WIDTH * CGFloat(senderbtn.tag), y: 0), animated: true)
        let xx : CGFloat = (SCREEN_WIDTH * CGFloat(senderbtn.tag)) * (CGFloat(buttonWidth) / SCREEN_WIDTH) - CGFloat(buttonWidth)
        self.menuScrollView.scrollRectToVisible(CGRect(x: xx, y: yy, width: SCREEN_WIDTH, height: self.menuScrollView.frame.size.height), animated: true)
    }
    
    func addChildViewControllersOntoContainer(_ controllersArr : NSArray)
    {
        for i in 0..<controllersArr.count {
            let vc: UIViewController = (controllersArr[i] as! UIViewController)
            var frame: CGRect = CGRect(x: 0, y: 0, width: self.containerScrollView.frame.size.width, height: self.containerScrollView.frame.size.height)
            frame.origin.x = SCREEN_WIDTH * CGFloat(i)
            vc.view.frame = frame
            self.addChildViewController(vc)
            self.containerScrollView.addSubview(vc.view)
            vc.didMove(toParentViewController: self)
        }
        self.containerScrollView.contentSize = CGSize(width: SCREEN_WIDTH * CGFloat(controllersArr.count) + 1.0, height: self.containerScrollView.frame.size.height)
        self.containerScrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
        self.containerScrollView.isPagingEnabled = true
        self.containerScrollView.delegate = self as UIScrollViewDelegate
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView)
    {
        let page: CGFloat = (scrollView.contentOffset.x / SCREEN_WIDTH)
        
        
        var btn: UIButton
        var buttonWidth: CGFloat = 0.0
        for subView in self.menuScrollView.subviews {
            btn = (subView as? UIButton)!
            let bottomView: UIView = btn.viewWithTag(1001)!
            if btn.tag == Int(page) {
                btn.isSelected = true
                buttonWidth = btn.frame.size.width
                bottomView.isHidden = false
            } else {
                btn.isSelected = false
                bottomView.isHidden = true
                
            }
        }
        let yy : CGFloat = 0.0
        let xx : CGFloat = scrollView.contentOffset.x * (CGFloat(buttonWidth) / SCREEN_WIDTH ) - CGFloat(buttonWidth)
        self.menuScrollView.scrollRectToVisible(CGRect(x: CGFloat(xx), y: yy, width: SCREEN_WIDTH, height: self.menuScrollView.frame.size.height), animated: true)
    }
    
    
}



