//
//  DataObject.swift
//  RATreeViewExamples
//
//  Created by Rafal Augustyniak on 22/11/15.
//  Copyright © 2015 com.Augustyniak. All rights reserved.
//

import Foundation


class DataObject
{
    let name : String
    let imageName : String
    var children : [DataObject]

    init(name : String,imageName : String, children: [DataObject]) {
        self.name = name
        self.children = children
        self.imageName = imageName
    }
 
    convenience init(name : String,imageName : String) {
        self.init(name: name, imageName : imageName, children: [DataObject]())
    }

    func addChild(_ child : DataObject) {
        self.children.append(child)
    }

    func removeChild(_ child : DataObject) {
        self.children = self.children.filter( {$0 !== child})
    }
}



