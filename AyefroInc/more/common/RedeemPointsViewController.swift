//
//  RedeemPointsViewController.swift
//  AyefroInc
//
//  Created by Vijay Darkonde on 18/01/18.
//  Copyright © 2018 Inceptive Consulting Pvt. Ltd. All rights reserved.
//

import UIKit
import Branch

class RedeemPointsViewController: UIViewController{
    
    @IBOutlet weak var scrollHeight: NSLayoutConstraint!
    @IBOutlet weak var pointsLbl: UILabel!
    
    var points: Int! = 0

    
    override func viewDidLoad() {
        super.viewDidLoad()
        //        let action = "mycustom"
        //        Branch.getInstance().userCompletedAction(action)
        // Do any additional setup after loading the view.
        self.tabBarController?.tabBar.isHidden = true
        
        if UIScreen.main.bounds.size.width == 414
        {
            scrollHeight.constant = 50
        }
        else if UIScreen.main.bounds.size.width == 375 && UIScreen.main.bounds.size.height == 667
        {
            scrollHeight.constant = 100
        }
        else if UIScreen.main.bounds.size.width == 320 && UIScreen.main.bounds.size.height == 568
        {
            scrollHeight.constant = 200
        }
        else if UIScreen.main.bounds.size.width == 320 && UIScreen.main.bounds.size.height == 480
        {
            scrollHeight.constant = 300
        }
        
        Branch.getInstance().setIdentity(userID)
        Branch.getInstance().loadRewards { (changed, error) in
            if (error == nil) {
                let creditsForBucket = Branch.getInstance().getCredits()
                print("credit for bucket: \(creditsForBucket)")
                self.pointsLbl.text = String(creditsForBucket)
                self.points = creditsForBucket
            }
        }
    }
    
    @IBAction func redeemClk(_ sender: UIButton) {
        let backItem = UIBarButtonItem()
        backItem.title = "Back"
        backItem.tintColor = UIColor.black
        navigationItem.backBarButtonItem = backItem
        let storyboard =  UIStoryboard(name: "Main", bundle: nil)
        let promo = storyboard.instantiateViewController(withIdentifier: "RedeemOptionsViewController") as! RedeemOptionsViewController
        promo.points = self.points
        self.navigationController?.pushViewController(promo, animated: true)
    }
    
    
    @IBAction func inviteClk(_ sender: UIButton) {
        let buo = BranchUniversalObject(canonicalIdentifier: "/ayefroinc")
        buo.canonicalUrl = "https://ayefroinc.com/"
        buo.title = "Ayefro Inc"
        buo.contentDescription = "The biggest wedding and events app - Ayefro Inc."
        buo.imageUrl = "https://ayefroinc.files.wordpress.com/2017/11/cropped-logo3.png"
        
        
        let lp: BranchLinkProperties = BranchLinkProperties()
        lp.channel = "facebook"
        lp.feature = "sharing"
        lp.campaign = "Ayefro Inc"
        lp.stage = "new user"
        lp.tags = ["one", "two", "three"]
        
        lp.addControlParam("$desktop_url", withValue: "https://ayefroinc.com/")
        
        lp.addControlParam("$ios_url", withValue: "https://itunes.apple.com/us/app/ayefro-inc/id1339116784?ls=1&mt=8")
        lp.addControlParam("$android_url", withValue: "https://play.google.com/store/apps/details?id=com.ayefroinc")
        lp.addControlParam("$match_duration", withValue: "2000")
        
        lp.addControlParam("custom_data", withValue: "yes")
        lp.addControlParam("look_at", withValue: "this")
        lp.addControlParam("nav_to", withValue: "over here")
        lp.addControlParam("random", withValue: UUID.init().uuidString)
        
        
        buo.getShortUrl(with: lp) { (url, error) in
            print(url ?? "")
        }
        
        let message = "Ayefro Inc is giving away iPhone X and Samsung Galaxy S8, Instant airtime and more. Don’t miss out! Download now."
        
        buo.showShareSheet(with: lp, andShareText: message, from: self) { (activityType, completed) in
            print(activityType ?? "")
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}

