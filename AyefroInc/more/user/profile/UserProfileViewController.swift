//
//  UserProfileViewController.swift
//  AyefroInc
//
//  Created by Vijay Darkonde on 21/12/17.
//  Copyright © 2017 Inceptive Consulting Pvt. Ltd. All rights reserved.
//

import UIKit
import SDWebImage
import Alamofire


class UserProfileViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var userBtn: UIButton!
    @IBOutlet var profilePic: UIImageView!
    @IBOutlet var firstNameTF: NiceTextField!
    @IBOutlet var lastNameTF: NiceTextField!
    @IBOutlet var PhoneNumTF: NiceTextField!
    @IBOutlet var EmailTF: NiceTextField!
    
    var base64Str : String! = ""
    let imagePicker = UIImagePickerController()
    var reviewArr : NSArray!

    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "My Profile"
        self.tabBarController?.tabBar.isHidden = true
        
        imagePicker.delegate = self
        
        profilePic.layer.cornerRadius = profilePic.frame.size.height/2
        profilePic.layer.masksToBounds = true
        
        let prefs: UserDefaults? = UserDefaults.standard
        let PhoneNumber: String? = prefs?.string(forKey: "PhoneNumber")
        
        _ = ModelManager.instance.getUserLoginStatus()
        firstNameTF.text = userFirstName
        lastNameTF.text = userLastName
        PhoneNumTF.text = PhoneNumber
        EmailTF.text = userEmail
        let url = URL(string: image)
        let str: String? = Bundle.main.path(forResource: "loading_spinner", ofType: "gif")
        let fileData = NSData(contentsOfFile: str!)
        if url != nil{
            self.profilePic.sd_setImage(with: url, placeholderImage: UIImage.sd_animatedGIF(with: fileData! as Data), options: SDWebImageOptions.allowInvalidSSLCertificates, progress: nil, completed: nil)
            self.userBtn.setTitle("", for: UIControlState.normal)
        }
        else{
            self.profilePic.image = UIImage(named: "user")
            self.userBtn.setTitle("Upload", for: UIControlState.normal)
        }
        getProfileInfo()
    }
    
    func getProfileInfo()  {
        DispatchQueue.main.async {
            let manager = Alamofire.SessionManager.default
            manager.session.configuration.timeoutIntervalForRequest = 120
            
            manager.request( baseURL + "api/Events/UserProfile/?UserId=" + userID, method: .get, parameters: ["":""], encoding: URLEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
                
                switch(response.result) {
                case .success(_):
                    if response.result.value != nil{
                        //print(response.result.value as Any)
                    }
                    let jsonResults : NSDictionary
                    jsonResults = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! NSDictionary
                    let status = jsonResults["status"] as! String
                    if status == "Success"{
                        let userInfo = jsonResults["userInfo"] as! NSArray
                        let reviews = jsonResults["reviews"] as! NSArray
                        self.reviewArr = reviews
                    }
                    
                    break
                    
                case .failure(_):
                    print(response.error!.localizedDescription)
                    let alert: UIAlertView = UIAlertView(title: "", message: response.error!.localizedDescription, delegate: nil, cancelButtonTitle: "OK");
                    alert.show()
                    self.view.hideActivityView()
                    print(response.result.error as Any)
                    break
                    
                }
            }
        }
    }
    
    @IBAction func addImageBtnClk(_ sender: UIButton)     {
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallary()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera))
        {
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openGallary()
    {
        imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        imagePicker.allowsEditing = false
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    internal func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            profilePic.image = pickedImage
            let imageData: NSData = UIImageJPEGRepresentation(pickedImage, 0.4)! as NSData
            let imageStr = imageData.base64EncodedString(options: .lineLength64Characters)
            base64Str = imageStr
            self.userBtn.setTitle("", for: UIControlState.normal)
        }
        
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func reviewBtnClk(_ sender: UIButton) {
        let backItem = UIBarButtonItem()
        backItem.title = "Back"
        backItem.tintColor = UIColor.black
        navigationItem.backBarButtonItem = backItem
        let storyboard =  UIStoryboard(name: "Main", bundle: nil)
        let promo = storyboard.instantiateViewController(withIdentifier: "ReviewsViewController") as! ReviewsViewController
        promo.dataArr = self.reviewArr
        promo.redirectioValue = "User"
        self.navigationController?.pushViewController(promo, animated: true)
    }
    
    @IBAction func updateProfileBtnClk(_ sender: UIButton) {
        if (firstNameTF.text?.trimmingCharacters(in: .whitespaces).isEmpty)!{
            let alert: UIAlertView = UIAlertView(title: "", message: "Please Enter First Name", delegate: nil, cancelButtonTitle: "OK");
            alert.show()
        }
        else if (lastNameTF.text?.trimmingCharacters(in: .whitespaces).isEmpty)!{
            let alert: UIAlertView = UIAlertView(title: "", message: "Please Enter Last Name", delegate: nil, cancelButtonTitle: "OK");
            alert.show()
        }
        else{
            DispatchQueue.main.async {
                self.view.showActivityView(withLabel: "Loading")
                let parameters : [String: Any] = [
                    "User_Id": userID,
                    "FirstName": self.firstNameTF.text!,
                    "LastName": self.lastNameTF.text!,
                    "Image": self.base64Str as String,
                    ]
                
                let manager = Alamofire.SessionManager.default
                manager.session.configuration.timeoutIntervalForRequest = 120
                
                manager.request(baseURL + "api/AccountAPI/UserProfileUpdate", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
                    
                    switch(response.result) {
                    case .success(_):
                        if response.result.value != nil{
                            //print(response.result.value as Any)
                        }
                        let jsonResults : NSDictionary
                        jsonResults = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! NSDictionary
                        let firstName = jsonResults["firstName"] as! String
                        let lastName = jsonResults["lastName"] as! String
                        var image = ""
                        if (jsonResults["image"] as? String) != nil
                        {
                            image = jsonResults["image"] as! String
                        }
                        
                        let productInfo = ProductInfo()
                        productInfo.userFirstName = firstName
                        productInfo.userLastName = lastName
                        productInfo.image = image
                        productInfo.companyName = ""
                        productInfo.companyAddress = ""
                        productInfo.userID = userID
                        let isData = ModelManager.instance.updateUserInfo(productInfo)
                        if isData == true{
                            let alert: UIAlertView = UIAlertView(title: "Profile Updated Successfully.", message: "", delegate: nil, cancelButtonTitle: "OK");
                            alert.show()
                            self.navigationController?.popViewController(animated: true)
                        }
                        self.view.hideActivityView()
                        break
                        
                    case .failure(_):
                        print(response.error!.localizedDescription)
                        let alert: UIAlertView = UIAlertView(title: "", message: response.error!.localizedDescription, delegate: nil, cancelButtonTitle: "OK");
                        alert.show()
                        self.view.hideActivityView()
                        print(response.result.error as Any)
                        break
                        
                    }
                }
            }
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    

}
