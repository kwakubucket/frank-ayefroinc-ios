//
//  BrowesGalleryViewController.swift
//  AyefroInc
//
//  Created by Vijay Darkonde on 06/12/17.
//  Copyright © 2017 Inceptive Consulting Pvt. Ltd. All rights reserved.
//

import UIKit
import SDWebImage
import Alamofire


class BrowesGalleryViewController: UIViewController, UICollectionViewDelegateFlowLayout, UICollectionViewDelegate, UICollectionViewDataSource {
    
    var imageName: [String] = []
    var NameArr: [String] = []
    var idArr : [String] = []
    
    @IBOutlet var imageCollection: UICollectionView!
    @IBOutlet var loader: UIImageView!
    @IBOutlet var errorView: UIView!
    @IBOutlet var noInternetView: UIView!
    @IBOutlet var retryBtn: UIButton!


    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Gallery"
        self.tabBarController?.tabBar.isHidden = true
        
        let str: String? = Bundle.main.path(forResource: "loading_spinner", ofType: "gif")
        let fileData = NSData(contentsOfFile: str!)
        loader.image = UIImage.sd_animatedGIF(with: fileData! as Data)
        self.errorView.isHidden = true
        self.imageCollection.isHidden =  true
        self.noInternetView.isHidden = true
        self.loader.isHidden = false
        self.retryBtn.layer.cornerRadius = 5
        

    }
    
    override func viewWillAppear(_ animated: Bool) {
        getAllImages()
    }
    
    func getAllImages()  {
        self.imageName = []
        self.NameArr = []
        self.idArr = []
        self.imageCollection.reloadData()
        DispatchQueue.main.async {
            let manager = Alamofire.SessionManager.default
            manager.session.configuration.timeoutIntervalForRequest = 120
            manager.request( baseURL + "api/Albums/GetProviderAlbum/?UserId=" + userID, method: .get, parameters: ["":""], encoding: URLEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
                
                switch(response.result) {
                case .success(_):
                    if response.result.value != nil{
                        //print(response.result.value as Any)
                    }
                    
                    let jsonResults : NSArray
                    jsonResults = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! NSArray
                    if jsonResults.count == 0{
                        self.errorView.isHidden = false
                        self.imageCollection.isHidden =  true
                        self.noInternetView.isHidden = true
                        self.loader.isHidden = true
                    }
                    else{
                        self.errorView.isHidden = true
                        self.noInternetView.isHidden = true
                        self.imageCollection.isHidden =  false
                        self.loader.isHidden = true
                        for i in 0 ..< jsonResults.count{
                            let data = jsonResults[i] as! NSDictionary
                            let CategoryTitle = data["image"] as! String
                            var providerCompany = ""
                            if (data["providerCompany"] as? String) != nil
                            {
                                providerCompany = data["providerCompany"] as! String
                            }
                            let providerFirstName = data["providerFirstName"] as! String
                            let providerLastName = data["providerLastName"] as! String
                            let serviceProviderId = data["serviceProviderId"] as! String
                            if providerCompany == ""{
                                let name = providerFirstName + " " + providerLastName
                                self.NameArr.append(name)
                            }
                            else{
                                self.NameArr.append(providerCompany)
                            }
                            self.imageName.append(CategoryTitle)
                            self.idArr.append(serviceProviderId)
                        }
                        self.imageCollection.reloadData()
                    }
                    
                    break
                    
                case .failure(_):
                    if response.error?.localizedDescription == "The Internet connection appears to be offline."
                    {
                        self.errorView.isHidden = true
                        self.imageCollection.isHidden = true
                        self.noInternetView.isHidden = false
                        self.loader.isHidden = true
                    }
                    else{
                        self.loader.isHidden = true
                        let alert: UIAlertView = UIAlertView(title: "", message: response.error!.localizedDescription, delegate: nil, cancelButtonTitle: "OK");
                        alert.show()
                        print(response.result.error as Any)
                    }
                    break
                    
                }
            }
        }
    }
    
    @IBAction func retryBtnClk(_ sender: UIButton) {
        getAllImages()
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GalleryCell", for: indexPath) as! GalleryCell
        let str: String? = Bundle.main.path(forResource: "loading_spinner", ofType: "gif")
        let fileData = NSData(contentsOfFile: str!)
        
        cell.galleryImg.sd_setImage(with: URL(string: imageName[indexPath.item]), placeholderImage: UIImage.sd_animatedGIF(with: fileData! as Data), options: SDWebImageOptions.allowInvalidSSLCertificates, progress: nil, completed: nil)
        cell.userName.text = NameArr[indexPath.row]
        cell.layer.cornerRadius = 2
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imageName.count
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let backItem = UIBarButtonItem()
        backItem.title = "Back"
        backItem.tintColor = UIColor.black
        navigationItem.backBarButtonItem = backItem
        let storyboard =  UIStoryboard(name: "Main", bundle: nil)
        let promo = storyboard.instantiateViewController(withIdentifier: "ViewGalleryController") as! ViewGalleryController
        promo.headerTitle = NameArr[indexPath.row]
        promo.vendorID = self.idArr[indexPath.item]
        self.navigationController?.pushViewController(promo, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if UIScreen.main.bounds.size.width == 414
        {
            let collectionViewWidthB: CGFloat? = collectionView.frame.size.width
            let width = collectionViewWidthB! / 3.5
            let cellSizeB = CGSize(width: width, height: width)
            return cellSizeB
        }
        else if UIScreen.main.bounds.size.width == 375 && UIScreen.main.bounds.size.height == 667
        {
            let collectionViewWidthB: CGFloat? = collectionView.frame.size.width
            let width = collectionViewWidthB! / 3.5
            let cellSizeB = CGSize(width: width, height: width)
            return cellSizeB
        }
        else if UIScreen.main.bounds.size.width == 320 && UIScreen.main.bounds.size.height == 568
        {
            let collectionViewWidthB: CGFloat? = collectionView.frame.size.width
            let width = collectionViewWidthB! / 3.5
            let cellSizeB = CGSize(width: width, height: width)
            return cellSizeB
        }
        else if UIScreen.main.bounds.size.width == 320 && UIScreen.main.bounds.size.height == 480
        {
            let collectionViewWidthB: CGFloat? = collectionView.frame.size.width
            let width = collectionViewWidthB! / 3.5
            let cellSizeB = CGSize(width: width, height: width)
            return cellSizeB
        }
        else{
            let collectionViewWidthB: CGFloat? = collectionView.frame.size.width
            let width = collectionViewWidthB! / 3.5
            let cellSizeB = CGSize(width: width, height: width)
            return cellSizeB
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}


