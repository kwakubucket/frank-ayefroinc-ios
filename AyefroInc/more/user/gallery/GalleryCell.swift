//
//  GalleryCell.swift
//  AyefroInc
//
//  Created by Vijay Darkonde on 06/12/17.
//  Copyright © 2017 Inceptive Consulting Pvt. Ltd. All rights reserved.
//

import UIKit

class GalleryCell: UICollectionViewCell {
    
    @IBOutlet var galleryImg: UIImageView!
    @IBOutlet var userName: UILabel!
}
