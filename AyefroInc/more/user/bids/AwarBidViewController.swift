//
//  AwarBidViewController.swift
//  AyefroInc
//
//  Created by Vijay Darkonde on 22/12/17.
//  Copyright © 2017 Inceptive Consulting Pvt. Ltd. All rights reserved.
//

import UIKit
import ExpandableTableViewController
import Alamofire
import MessageUI

class AwarBidViewController: ExpandableTableViewController, ExpandableTableViewDelegate, emailSend , MFMailComposeViewControllerDelegate{
    
    var eventID : String = ""
    var header : String = ""
    var serviceID : String! = ""
    var error = UILabel()

    var BidCostArr : [String] = []
    var BidDescArr : [String] = []
    var BidSpUserIdArr : [String] = []
    var BidSpUserNameArr: [String] = []
    var AwardedArr : [Bool] = []
    var serviceIDArr: [String] = []
    var coverView = UIView()
    var screenRect = UIScreen.main.bounds
    var category: [String] = ["View Gallery","Contact Vendor"]
    var menuOptionImageNameArray : [String] = []
    
    var vendorID: String! = ""
    var popViewController : ContactPopUpViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.title = ""
        self.expandableTableView.expandableDelegate = self
        
        coverView = UIView(frame: screenRect)
        let config = FTConfiguration.shared
        config.textColor = UIColor.black
        config.backgoundTintColor = UIColor.white
        config.borderColor = UIColor.lightGray
        config.menuWidth = self.view.frame.width - 10
        config.menuSeparatorColor = UIColor.lightGray
        config.textAlignment = .left
        config.textFont = UIFont(name: "Helvetica Neue", size: 17)!
        config.menuRowHeight = 50
        config.cornerRadius = 6
        
        error = UILabel(frame: CGRect(x: 0, y: (self.view.frame.size.height/2) - 50, width: self.view.frame.size.width, height: 50))
        error.backgroundColor = UIColor.clear
        error.textAlignment = NSTextAlignment.center
        error.text = "No Bid(s) for this Service"
        error.font = UIFont(name:"HelveticaNeue-Bold", size: 17.0)!
        error.textColor = UIColor(red: 85.0/255.0, green: 85.0/255.0, blue: 85.0/255.0, alpha: 1.0)
        navigationController?.view.addSubview(error)
        
        let tapGesture1: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(ForumDetailViewController.removeView))
        self.view.addGestureRecognizer(tapGesture1)
        
        self.expandableTableView.tableFooterView = UIView()
        
        buildTestData {
            
        }
        self.error.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.error.removeFromSuperview()
    }
    
    @objc func removeView()  {
        coverView.removeFromSuperview()
    }

    func buildTestData(then: @escaping () -> Void) {
        BidCostArr  = []
        BidDescArr  = []
        BidSpUserIdArr  = []
        BidSpUserNameArr = []
        AwardedArr  = []
        serviceIDArr = []
        // Data from `data.json`
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 120
        
        manager.request(baseURL + "api/EventPlaceBidsAPI/GetServiceBidList/?userid=" + userID + "&eventId=" + self.eventID + "&ServiceId=" + self.serviceID, method: .get, parameters: ["":""], encoding: URLEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case .success(_):
                if response.result.value != nil{
                    //print(response.result.value as Any)
                    let jsonResults : NSDictionary
                    jsonResults = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! NSDictionary
                    if (jsonResults["EventDetails"] as? NSArray) != nil
                    {
                        let EventDetails = jsonResults["EventDetails"] as! NSArray
                        if EventDetails.count != 0{
                            for i in 0 ..< EventDetails.count{
                                let dict = EventDetails[i] as! NSDictionary
                                let ServiceId = dict["ServiceId"] as! Int
                                let ServiceName = dict["ServiceName"] as! String
                                self.title = ServiceName
                                let BidCost = dict["BidCost"] as! Int
                                let BidDesc = dict["BidDesc"] as! String
                                let BidSpUserId = dict["BidSpUserId"] as! String
                                let BidSpUserName = dict["BidSpUserName"] as! String
                                let Awarded = dict["Awarded"] as! Bool
                                
                                self.serviceIDArr.append(String(ServiceId))
                                self.BidCostArr.append(String(BidCost))
                                self.BidDescArr.append(BidDesc)
                                self.BidSpUserIdArr.append(BidSpUserId)
                                self.BidSpUserNameArr.append(BidSpUserName)
                                self.AwardedArr.append(Awarded)
                                
                            }
                            self.error.isHidden = true
                            self.tableView.reloadData()
                        }
                        else{
                            self.error.isHidden = false
                        }
                        
                    }
                    else{
                        var status = ""
                        if (jsonResults["status"] as? String) != nil
                        {
                            status = jsonResults["status"] as! String
                            if status == "Failed"{
                                var Message = ""
                                if (jsonResults["Message"] as? String) != nil
                                {
                                    Message = jsonResults["Message"] as! String
                                    if Message == "Unauthorized"{
                                        _ = ModelManager.instance.deleteUserTable()
                                        let storyboard =  UIStoryboard(name: "Main", bundle: nil)
                                        let promo = storyboard.instantiateViewController(withIdentifier: "LandingPageViewController") as! LandingPageViewController
                                        self.navigationController?.pushViewController(promo, animated: true)
                                    }
                                }
                            }
                        }
                    }
                    
                    
                    
                }
                break
                
            case .failure(_):
                print(response.error!.localizedDescription)
                let alert: UIAlertView = UIAlertView(title: "", message: response.error!.localizedDescription, delegate: nil, cancelButtonTitle: "OK");
                alert.show()
                self.view.hideActivityView()
                print(response.result.error as Any)
                
                break
                
            }
        }
        
    }

    @IBAction func expandBtnClk(_ sender: UIButton) {
        let point = self.tableView.convert(CGPoint.zero, from: sender)
        if let indexPath = self.tableView.indexPathForRow(at: point) {
            let birthdateCell : BidsTitleCell = expandableTableView.cellForRow(at: indexPath) as! BidsTitleCell
            let index = self.expandableIndexPathForIndexPath(indexPath)
            if expandableTableView.isCellExpandedAtExpandableIndexPath(index) == true{
                birthdateCell.arrowImg.image = UIImage(named: "uarrow")
                unexpandCellAtIndexPath(indexPath, tableView: expandableTableView)
            }
            else{
                birthdateCell.arrowImg.image = UIImage(named: "darrow")
                expandCellAtIndexPath(indexPath, tableView: self.expandableTableView)
                
            }
        }
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: - Init
    
    override func tableView(_: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if cell.responds(to: #selector(setter: UIView.preservesSuperviewLayoutMargins)){
            cell.preservesSuperviewLayoutMargins = false
        }
        
        if cell.responds(to: #selector(setter: UIView.layoutMargins)){
            cell.layoutMargins = UIEdgeInsets.zero
        }
    }
    
    func getcontact()  {
        if self.vendorID != ""{
            DispatchQueue.main.async {
                let manager = Alamofire.SessionManager.default
                manager.session.configuration.timeoutIntervalForRequest = 120
                
                manager.request( baseURL + "api/Albums/GetContactDetails/?providerId=" + self.vendorID, method: .get, parameters: ["":""], encoding: URLEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
                    
                    switch(response.result) {
                    case .success(_):
                        if response.result.value != nil{
                            //print(response.result.value as Any)
                        }
                        
                        let jsonResults : NSArray
                        jsonResults = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! NSArray
                        let dict = jsonResults[0] as! NSDictionary
                        let phoneNumber = dict["phoneNumber"] as! String
                        let email = dict["email"] as! String
                        
                        self.popViewController = self.storyboard?.instantiateViewController(withIdentifier: "ContactPopUpViewController")as! ContactPopUpViewController
                        self.popViewController.phoneNum = phoneNumber
                        self.popViewController.emailID = email
                        self.popViewController.delegate = self
                        self.popViewController.showInView(self.view, animated: true)
                        
                        break
                        
                    case .failure(_):
                        if response.error?.localizedDescription == "The Internet connection appears to be offline."
                        {
                            let alert: UIAlertView = UIAlertView(title: "", message: response.error!.localizedDescription, delegate: nil, cancelButtonTitle: "OK");
                            alert.show()
                        }
                        else{
                            let alert: UIAlertView = UIAlertView(title: "", message: response.error!.localizedDescription, delegate: nil, cancelButtonTitle: "OK");
                            alert.show()
                            print(response.result.error as Any)
                        }
                        break
                        
                    }
                }
            }
        }
        else{
            let alert: UIAlertView = UIAlertView(title: "", message: "No data available", delegate: nil, cancelButtonTitle: "OK");
            alert.show()
        }
    }
    
    func emailBox(email: String){
        if MFMailComposeViewController.canSendMail() {
            let toRecipients : [String] = [email]
            let mc: MFMailComposeViewController = MFMailComposeViewController()
            mc.mailComposeDelegate = self
            mc.setToRecipients(toRecipients)
            mc.isNavigationBarHidden = true
            self.present(mc, animated: false, completion: nil)
        }
        else
        {
            let alert: UIAlertView = UIAlertView(title: "", message: "Mail app not install on your device. Please send an email to info@ayefroinc.com.com", delegate: nil, cancelButtonTitle: "OK");
            alert.show()
        }
    }
    
    func viewGallery()  {
        let backItem = UIBarButtonItem()
        backItem.title = "Back"
        backItem.tintColor = UIColor.black
        navigationItem.backBarButtonItem = backItem
        let storyboard =  UIStoryboard(name: "Main", bundle: nil)
        let promo = storyboard.instantiateViewController(withIdentifier: "ViewGalleryController") as! ViewGalleryController
        promo.vendorID = vendorID
        self.navigationController?.pushViewController(promo, animated: true)
        
    }
    
    @IBAction func awardBtnClk(_ sender: UIButton) {
        let point = self.tableView.convert(CGPoint.zero, from: sender)
        var name = ""
        var cost = ""
        var servID = ""
        if let indexPath = self.tableView.indexPathForRow(at: point) {
            vendorID = BidSpUserIdArr[indexPath.row]
            name = BidSpUserNameArr[indexPath.row]
            cost = self.BidCostArr[indexPath.row]
            servID = self.serviceIDArr[indexPath.row]
        }
        
        var serviceArr : [[String:String]] = []
        
        let piePrice:[String:String] = [
            "serviceId":servID,
            "serviceCost": cost
        ]
        serviceArr.append(piePrice)
        
        DispatchQueue.main.async {
            let parameters : [String: Any] = [
                "Cost": cost,
                "EventsId": self.eventID,
                "ServiceProviderId": self.vendorID,
                "UserId": userID,
                "ServiceCost": serviceArr,
            ]
            let manager = Alamofire.SessionManager.default
            manager.session.configuration.timeoutIntervalForRequest = 120
            
            manager.request(baseURL + "api/Orders/PostOrder", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
                
                switch(response.result) {
                case .success(_):
                    if response.result.value != nil{
                        //print(response.result.value as Any)
                        
                        
                    }
                    
                    let jsonResults : NSDictionary
                    jsonResults = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! NSDictionary
                    let status = jsonResults["status"] as! String
                    if status == "Success"{
                        let message = "Thank you for awarding the service to " + name + ". We hope you enjoy the experience. Kindly provide your valuable feedback about the quality of service provided."
                        let alert: UIAlertView = UIAlertView(title: "", message: message, delegate: nil, cancelButtonTitle: "OK");
                        alert.show()
                        self.buildTestData {
                            
                        }
                    }
                    else{
                        let alert: UIAlertView = UIAlertView(title: "", message: "You cannot apply for this event since all services have already been awarded.", delegate: nil, cancelButtonTitle: "OK");
                        alert.show()
                    }
                    
                    
                    break
                    
                case .failure(_):
                    print(response.error!.localizedDescription)
                    let alert: UIAlertView = UIAlertView(title: "", message: response.error!.localizedDescription, delegate: nil, cancelButtonTitle: "OK");
                    alert.show()
                    print(response.result.error as Any)
                    break
                    
                }
            }
        }
    }
    

    @IBAction func btnClk(_ sender: UIButton) {
        let point = self.tableView.convert(CGPoint.zero, from: sender)
        if let indexPath = self.tableView.indexPathForRow(at: point) {
            vendorID = BidSpUserIdArr[indexPath.row]
        }
        
        
        coverView.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        self.view.addSubview(coverView)
        FTPopOverMenu.showForSender(sender: sender, with: category, menuImageArray: menuOptionImageNameArray, done: { (selectedIndex) -> () in
            if selectedIndex == 0
            {
                self.viewGallery()
            }
            if selectedIndex == 1
            {
                self.getcontact()
            }
            self.coverView.removeFromSuperview()
        }) {
            self.coverView.removeFromSuperview()
        }

    }
    
    
    // MARK: - Expandable Table View Controller Delegate
    
    // MARK: - Rows
    func expandableTableView(_ expandableTableView: ExpandableTableView, numberOfRowsInSection section: Int) -> Int {
        return BidSpUserIdArr.count
    }
    
    func expandableTableView(_ expandableTableView: ExpandableTableView, cellForRowAtExpandableIndexPath expandableIndexPath: ExpandableIndexPath) -> UITableViewCell {
        
        var cell: BidsTitleCell!
        cell = expandableTableView.dequeueReusableCellWithIdentifier("BidsTitleCell", forIndexPath: expandableIndexPath) as! BidsTitleCell
        cell.providerName.text = self.BidSpUserNameArr[expandableIndexPath.row]
        let prefs: UserDefaults? = UserDefaults.standard
        let currencySymbol = prefs?.string(forKey: "currencySymbol")
        cell.budgetLbl.text = currencySymbol! + " " + self.BidCostArr[expandableIndexPath.row]
        if AwardedArr[expandableIndexPath.row] == true{
            cell.awardImg.image = UIImage(named: "awarded")
        }
        else{
            cell.awardImg.image = UIImage(named: "award button")
        }
        
        
        return cell
    }
    
    
    func expandableTableView(_ expandableTableView: ExpandableTableView, heightForRowAtExpandableIndexPath expandableIndexPath: ExpandableIndexPath) -> CGFloat {
        return 100
    }
    
    func expandableTableView(_ expandableTableView: ExpandableTableView, estimatedHeightForRowAtExpandableIndexPath expandableIndexPath: ExpandableIndexPath) -> CGFloat {
        return 100
        
    }
    
    func expandableTableView(_ expandableTableView: ExpandableTableView, didSelectRowAtExpandableIndexPath expandableIndexPath: ExpandableIndexPath) {

    }
    
    // MARK: - SubRows
    func expandableTableView(_ expandableTableView: ExpandableTableView, numberOfSubRowsInRowAtExpandableIndexPath expandableIndexPath: ExpandableIndexPath) -> Int {
        return 1
    }
    
    func expandableTableView(_ expandableTableView: ExpandableTableView, subCellForRowAtExpandableIndexPath expandableIndexPath: ExpandableIndexPath) -> UITableViewCell {
        var cell: BidDetailCell!
        cell = expandableTableView.dequeueReusableCellWithIdentifier("BidDetailCell", forIndexPath: expandableIndexPath) as! BidDetailCell
        cell.descriptionLbl.text = self.BidDescArr[expandableIndexPath.row]
        cell.descriptionLbl.layer.borderColor = UIColor.gray.cgColor
        cell.descriptionLbl.layer.borderWidth = 0.5
        cell.descriptionLbl.layer.cornerRadius = 5
        return cell

    }
    
   
    func expandableTableView(_ expandableTableView: ExpandableTableView, heightForSubRowAtExpandableIndexPath expandableIndexPath: ExpandableIndexPath) -> CGFloat {
        return 120.0
    }
    
    func expandableTableView(_ expandableTableView: ExpandableTableView, estimatedHeightForSubRowAtExpandableIndexPath expandableIndexPath: ExpandableIndexPath) -> CGFloat {
        return 120.0
    }
    
    func expandableTableView(_ expandableTableView: ExpandableTableView, didSelectSubRowAtExpandableIndexPath expandableIndexPath: ExpandableIndexPath){
        
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        switch result.rawValue {
        case MFMailComposeResult.cancelled.rawValue:
            //NSLog("Mail Cancelled")
            self.dismiss(animated: false, completion: nil)
            break
        case MFMailComposeResult.saved.rawValue:
            //NSLog("Mail Saved")
            self.dismiss(animated: false, completion: nil)
            break
        case MFMailComposeResult.failed.rawValue:
            //NSLog("Mail Sent Fail", [error!.localizedDescription])
            self.dismiss(animated: false, completion: nil)
            break
        case MFMailComposeResult.sent.rawValue:
            //NSLog("Mail Sent")
            self.dismiss(animated: false, completion: nil)
            break
            
        default:
            break
        }
    }
   
}

