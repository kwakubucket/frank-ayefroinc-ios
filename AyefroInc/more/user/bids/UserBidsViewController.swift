//
//  UserBidsViewController.swift
//  AyefroInc
//
//  Created by Vijay Darkonde on 13/12/17.
//  Copyright © 2017 Inceptive Consulting Pvt. Ltd. All rights reserved.
//

import UIKit
import Alamofire

class UserBidsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var prototypeEntitiesFromJSON: [BidEventEntity] = []
    
    
    @IBOutlet var loader: UIImageView!
    @IBOutlet var noInternetView: UIView!
    @IBOutlet var errorView: UIView!
    @IBOutlet var tableView: UITableView!
    @IBOutlet var pushBtn: UIButton!
    @IBOutlet var retryBtn: UIButton!
    
    var refreshControl: UIRefreshControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //        self.tableView.register(BidEventsCell.self, forCellReuseIdentifier: "Cell")
        self.tableView.register(UINib.init(nibName: "BidEventsCell", bundle: nil), forCellReuseIdentifier: "BidEventsCell")
        self.tableView.fd_debugLogEnabled = true
        self.tableView.tableFooterView = UIView()
        self.tabBarController?.tabBar.isHidden = true
//        self.shyNavBarManager.scrollView = self.tableView;
       
        self.title = "Bids"
        
        pushBtn.layer.cornerRadius = 5
        retryBtn.layer.cornerRadius = 5
        self.errorView.isHidden = true
        self.tableView.isHidden = true
        self.noInternetView.isHidden = true
        let str: String? = Bundle.main.path(forResource: "loading_spinner", ofType: "gif")
        let fileData = NSData(contentsOfFile: str!)
        loader.image = UIImage.sd_animatedGIF(with: fileData! as Data)
        
        buildTestData {
            
        }
    }
    
    override func viewDidLayoutSubviews() {
        self.automaticallyAdjustsScrollViewInsets = false
//        self.shyNavBarManager.scrollView = self.tableView
    }
    
    
    func buildTestData(then: @escaping () -> Void) {
        DispatchQueue.main.async {
            
            // Data from `data.json`
            self.prototypeEntitiesFromJSON = []
            let manager = Alamofire.SessionManager.default
            manager.session.configuration.timeoutIntervalForRequest = 120
            
            manager.request(baseURL + "api/ServiceProviderMappingsAPI/GetEventBidDetails/?UserId=" + userID, method: .get, parameters: ["":""], encoding: URLEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
                
                switch(response.result) {
                case .success(_):
                    if response.result.value != nil{
                        //print(response.result.value as Any)
                        let jsonResults : NSDictionary
                        jsonResults = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! NSDictionary
                        var EventDetails = NSArray()
                        
                        if (jsonResults["EventDetails"] as? NSArray) != nil
                        {
                            EventDetails = jsonResults["EventDetails"] as! NSArray
                            if EventDetails.count == 0{
                                self.errorView.isHidden = false
                                self.tableView.isHidden = true
                                self.noInternetView.isHidden = true
                                self.loader.isHidden = true
                            }
                            else{
                                self.errorView.isHidden = true
                                self.tableView.isHidden = false
                                self.noInternetView.isHidden = true
                                self.loader.isHidden = true
                                let jsonArr = NSMutableArray()
                                for i in 0 ..< EventDetails.count{
                                    let dicWithoutNulls = EventDetails[i] as! NSDictionary

                                    let outputDict = self.removeNSNull(from: dicWithoutNulls.mutableCopy() as! NSMutableDictionary)

                                    jsonArr.add(outputDict)
                                }

                                let arr = NSArray(array: jsonArr)
                                for i in 0 ..< arr.count{
                                    self.prototypeEntitiesFromJSON.append(BidEventEntity(dictionary: arr[i] as! [AnyHashable : Any] ))
                                }
                            }
                        }
                        else{
                            var status = ""
                            if (jsonResults["status"] as? String) != nil
                            {
                                status = jsonResults["status"] as! String
                                if status == "Failed"{
                                    var Message = ""
                                    if (jsonResults["Message"] as? String) != nil
                                    {
                                        Message = jsonResults["Message"] as! String
                                        if Message == "Unauthorized"{
                                            _ = ModelManager.instance.deleteUserTable()
                                            let storyboard =  UIStoryboard(name: "Main", bundle: nil)
                                            let promo = storyboard.instantiateViewController(withIdentifier: "LandingPageViewController") as! LandingPageViewController
                                            self.navigationController?.pushViewController(promo, animated: true)
                                        }
                                    }
                                }
                            }
                        }

                        
                        
                    }
                    self.tableView.reloadData()
                    break
                    
                case .failure(_):
                    print(response.error!.localizedDescription)
                    
                    if response.error?.localizedDescription == "The Internet connection appears to be offline."
                    {
                        self.errorView.isHidden = true
                        self.tableView.isHidden = true
                        self.noInternetView.isHidden = false
                        self.loader.isHidden = true
                    }
                    else{
                        self.loader.isHidden = true
                        let alert: UIAlertView = UIAlertView(title: "", message: response.error!.localizedDescription, delegate: nil, cancelButtonTitle: "OK");
                        alert.show()
                        self.view.hideActivityView()
                        print(response.result.error as Any)
                    }
                    break
                    
                }
            }
        }
    }
    
    func removeNSNull(from dict: NSMutableDictionary) -> NSDictionary {
        let mutableDict = dict
        let keysWithEmptString = dict.filter { $0.1 is NSNull }.map { $0.0 }
        for key in keysWithEmptString {
            mutableDict[key] = ""
        }
        
        return mutableDict
    }
    
    // MARK: - UITableViewDataSource
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let count = prototypeEntitiesFromJSON.count
        if count != 0 {
            return count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "BidEventsCell") as! BidEventsCell
        if prototypeEntitiesFromJSON.count != 0{
            configureCell(cell, at: indexPath)
        }
        return cell
    }
    
    
    func configureCell(_ cell: BidEventsCell, at indexPath: IndexPath) {
        if prototypeEntitiesFromJSON.count != 0{
            cell.fd_enforceFrameLayout = true
            cell.entity = prototypeEntitiesFromJSON[indexPath.row]
            
        }
        
    }
    
    // MARK: - UITableViewDelegate
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return tableView.fd_heightForCell(withIdentifier: "BidEventsCell") { cell in
            self.configureCell(cell as! BidEventsCell, at: indexPath)
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let backItem = UIBarButtonItem()
        backItem.title = "Back"
        backItem.tintColor = UIColor.black
        navigationItem.backBarButtonItem = backItem
        let storyboard =  UIStoryboard(name: "Main", bundle: nil)
        let promo = storyboard.instantiateViewController(withIdentifier: "BidServicesViewController") as! BidServicesViewController
        promo.eventID = String(prototypeEntitiesFromJSON[indexPath.row].eventId)
        promo.header = prototypeEntitiesFromJSON[indexPath.row].eventName
        self.navigationController?.pushViewController(promo, animated: true)
    }

    
    @IBAction func pushProfileBtnClk(_ sender: Any) {
        let storyboard =  UIStoryboard(name: "Main", bundle: nil)
        let promo = storyboard.instantiateViewController(withIdentifier: "UserAddEventViewController") as! UserAddEventViewController
        self.navigationController?.pushViewController(promo, animated: true)
    }
    
    @IBAction func retryBtnClk(_ sender: Any) {
        self.loader.isHidden = false
        self.errorView.isHidden = true
        self.tableView.isHidden = true
        self.noInternetView.isHidden = true
        buildTestData {
            
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
}


