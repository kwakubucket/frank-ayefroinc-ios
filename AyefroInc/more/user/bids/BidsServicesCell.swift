//
//  BidsServicesCell.swift
//  AyefroInc
//
//  Created by Vijay Darkonde on 22/12/17.
//  Copyright © 2017 Inceptive Consulting Pvt. Ltd. All rights reserved.
//

import UIKit

class BidsServicesCell: UITableViewCell {
    
    @IBOutlet weak var serviceLbl: UILabel!
    @IBOutlet weak var bidLbl: UILabel!
    @IBOutlet weak var viewAllLbl: UILabel!
    @IBOutlet weak var awardedImg: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
