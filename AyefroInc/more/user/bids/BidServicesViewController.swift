//
//  BidServicesViewController.swift
//  AyefroInc
//
//  Created by Vijay Darkonde on 22/12/17.
//  Copyright © 2017 Inceptive Consulting Pvt. Ltd. All rights reserved.
//

import UIKit
import Alamofire

class BidServicesViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    
    var eventID : String = ""
    var header : String = ""
    
    
    var BidcountArr: [String] = []
    var EventServiceIdArr: [String] = []
    var EventServiceNameArr: [String] = []
    var awardedArr: [Bool] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = true
        self.title = self.header
        // Do any additional setup after loading the view.
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        buildTestData {
            
        }
    }
    
    func buildTestData(then: @escaping () -> Void) {
        BidcountArr = []
        EventServiceIdArr = []
        EventServiceNameArr = []
        awardedArr = []
        // Data from `data.json`
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 120
        
        manager.request(baseURL + "api/EventPlaceBidsAPI/GetEventServiceList/?userid=" + userID + "&eventId=" + self.eventID, method: .get, parameters: ["":""], encoding: URLEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case .success(_):
                if response.result.value != nil{
                    //print(response.result.value as Any)
                    let jsonResults : NSDictionary
                    jsonResults = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! NSDictionary
                    var EventDetails = NSArray()
                    if (jsonResults["EventDetails"] as? NSArray) != nil
                    {
                        let EventDetails = jsonResults["EventDetails"] as! NSArray
                        for i in 0 ..< EventDetails.count{
                            let dict = EventDetails[i] as! NSDictionary
                            let EventId = dict["Bidcount"] as! Int
                            let EventServiceId = dict["EventServiceId"] as! Int
                            let EventServiceName = dict["EventServiceName"] as! String
                            let Bidcount = dict["Bidcount"] as! Int
                            let Awarded = dict["Awarded"] as! Bool

                            
                            self.awardedArr.append(Awarded)
                            self.EventServiceIdArr.append(String(EventServiceId))
                            self.EventServiceNameArr.append(EventServiceName)
                            self.BidcountArr.append(String(Bidcount))
                            
                        }
                        self.tableView.reloadData()
                    }
                    else{
                        var status = ""
                        if (jsonResults["status"] as? String) != nil
                        {
                            status = jsonResults["status"] as! String
                            if status == "Failed"{
                                var Message = ""
                                if (jsonResults["Message"] as? String) != nil
                                {
                                    Message = jsonResults["Message"] as! String
                                    if Message == "Unauthorized"{
                                        _ = ModelManager.instance.deleteUserTable()
                                        let storyboard =  UIStoryboard(name: "Main", bundle: nil)
                                        let promo = storyboard.instantiateViewController(withIdentifier: "LandingPageViewController") as! LandingPageViewController
                                        self.navigationController?.pushViewController(promo, animated: true)
                                    }
                                }
                            }
                        }
                    }
                    
                    
                    
                }
                break
                
            case .failure(_):
                print(response.error!.localizedDescription)
                let alert: UIAlertView = UIAlertView(title: "", message: response.error!.localizedDescription, delegate: nil, cancelButtonTitle: "OK");
                alert.show()
                self.view.hideActivityView()
                print(response.result.error as Any)
                
                break
                
            }
        }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return EventServiceNameArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BidsServicesCell", for: indexPath) as! BidsServicesCell
        cell.serviceLbl.text = EventServiceNameArr[indexPath.row]
        cell.bidLbl.text = BidcountArr[indexPath.row] + " Bid(s)"
        if self.awardedArr[indexPath.row] == true{
            cell.awardedImg.isHidden = false
            cell.viewAllLbl.isHidden = true
        }
        else{
            cell.awardedImg.isHidden = true
            cell.viewAllLbl.isHidden = false
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if EventServiceIdArr[indexPath.row] != "" && EventServiceNameArr[indexPath.row] != "" && self.eventID != ""{
            let backItem = UIBarButtonItem()
            backItem.title = "Back"
            backItem.tintColor = UIColor.black
            navigationItem.backBarButtonItem = backItem
            let storyboard =  UIStoryboard(name: "Main", bundle: nil)
            let promo = storyboard.instantiateViewController(withIdentifier: "AwarBidViewController") as! AwarBidViewController
            promo.serviceID = self.EventServiceIdArr[indexPath.row]
            promo.header = self.EventServiceNameArr[indexPath.row]
            promo.eventID = self.eventID
            self.navigationController?.pushViewController(promo, animated: true)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}
