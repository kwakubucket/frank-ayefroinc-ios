//
//  BidsTitleCell.swift
//  AyefroInc
//
//  Created by Vijay Darkonde on 22/12/17.
//  Copyright © 2017 Inceptive Consulting Pvt. Ltd. All rights reserved.
//

import UIKit

class BidsTitleCell: UITableViewCell {
    
    @IBOutlet weak var providerName: UILabel!
    @IBOutlet weak var budgetLbl: UILabel!
    @IBOutlet weak var arrowImg: UIImageView!
    @IBOutlet weak var awardImg: UIImageView!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
