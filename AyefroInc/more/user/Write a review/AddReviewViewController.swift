//
//  AddReviewViewController.swift
//  AyefroInc
//
//  Created by Vijay Darkonde on 23/12/17.
//  Copyright © 2017 Inceptive Consulting Pvt. Ltd. All rights reserved.
//

import UIKit
import KMPlaceholderTextView
import Alamofire

class AddReviewViewController: UIViewController {
    
    @IBOutlet var rateYourExperienceLbl: UILabel!
    @IBOutlet weak var reviewTxtView: KMPlaceholderTextView!
    @IBOutlet weak var floatRatingView: FloatRatingView!
    
    var ratingValue : Double = 0.0
    var eventID: String! = ""
    var ServiceProviderId: String! = ""
    var vender_name: String = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        floatRatingView.backgroundColor = UIColor.clear
        floatRatingView.delegate = self
        floatRatingView.type = .wholeRatings
        floatRatingView.rating = 0
        floatRatingView.contentMode = UIViewContentMode.scaleAspectFit
        
        reviewTxtView.layer.borderColor = UIColor.lightGray.cgColor
        reviewTxtView.layer.borderWidth = 0.5
        reviewTxtView.layer.cornerRadius = 7
        
        self.title = "Write a Review"
        self.rateYourExperienceLbl.text = "Rate your Experience with " + vender_name
    }
    
    
    @IBAction func saveBtnCLk(_ sender: UIButton) {
        if ratingValue == 0.0{
            let alert: UIAlertView = UIAlertView(title: "", message: "Select number of stars", delegate: nil, cancelButtonTitle: "OK");
            alert.show()
        }
        else if (reviewTxtView.text.trimmingCharacters(in: .whitespaces).isEmpty){
            let alert: UIAlertView = UIAlertView(title: "", message: "Please Enter Review", delegate: nil, cancelButtonTitle: "OK");
            alert.show()
        }
        else{
            DispatchQueue.main.async {
                let eventid = Int(self.eventID)!
                let parameters : [String: Any] = [
                    "Review": self.reviewTxtView.text!,
                    "UserId": userID,
                    "EventId": eventid,
                    "ServiceProviderId": self.ServiceProviderId,
                    "Ratings": self.ratingValue,
                    ]
                let manager = Alamofire.SessionManager.default
                manager.session.configuration.timeoutIntervalForRequest = 120
                
                manager.request(baseURL + "api/ServiceProviderReviewsAPI/ServiceProviderReview", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
                    
                    switch(response.result) {
                    case .success(_):
                        if response.result.value != nil{
                            //print(response.result.value as Any)
                            
                        }
                        
                        let jsonResults : NSDictionary
                        jsonResults = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! NSDictionary
                        let status = jsonResults["status"] as! String
                        if status == "Success"{
                            let alert: UIAlertView = UIAlertView(title: "", message: "Thank you for review. It will be published soon.", delegate: nil, cancelButtonTitle: "OK");
                            alert.show()
                        }
                        else{
                            let alert: UIAlertView = UIAlertView(title: "", message: "You have already added a review for this service provider, for this event.", delegate: nil, cancelButtonTitle: "OK");
                            alert.show()
                        }
                        
                       
                        
                        self.navigationController?.popViewController(animated: true)
                        break
                        
                    case .failure(_):
                        print(response.error!.localizedDescription)
                        let alert: UIAlertView = UIAlertView(title: "", message: response.error!.localizedDescription, delegate: nil, cancelButtonTitle: "OK");
                        alert.show()
                        print(response.result.error as Any)
                        break
                        
                    }
                }
            }
        }
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

extension AddReviewViewController: FloatRatingViewDelegate {
    
    // MARK: FloatRatingViewDelegate
    
    func floatRatingView(_ ratingView: FloatRatingView, isUpdating rating: Double) {
        self.ratingValue = rating
    }
    
    func floatRatingView(_ ratingView: FloatRatingView, didUpdate rating: Double) {
        self.ratingValue = rating
    }
    
}

