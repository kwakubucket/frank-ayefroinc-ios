//
//  CompletedEventViewController.swift
//  AyefroInc
//
//  Created by Vijay Darkonde on 22/12/17.
//  Copyright © 2017 Inceptive Consulting Pvt. Ltd. All rights reserved.
//

import UIKit
import Alamofire

class CompletedEventViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    
    @IBOutlet weak var errorView: UIView!
    @IBOutlet weak var eventTableView: UITableView!
    var menuArr : [String] = []
    var idArr : [String] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "My Events"
        self.tabBarController?.tabBar.isHidden = true
        getProfileInfo()
        self.eventTableView.tableFooterView = UIView()
        self.eventTableView.register(UITableViewCell.self, forCellReuseIdentifier: "Cell")
        self.errorView.isHidden = true
        self.eventTableView.isHidden = true
        
    }
    
    func getProfileInfo()  {
        DispatchQueue.main.async {
            let manager = Alamofire.SessionManager.default
            manager.session.configuration.timeoutIntervalForRequest = 120
            
            manager.request( baseURL + "api/Events/UserProfile/?UserId=" + userID, method: .get, parameters: ["":""], encoding: URLEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
                
                switch(response.result) {
                case .success(_):
                    if response.result.value != nil{
                        //print(response.result.value as Any)
                    }
                    let jsonResults : NSDictionary
                    jsonResults = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! NSDictionary
                    let status = jsonResults["status"] as! String
                    if status == "Success"{
                        let userInfo = jsonResults["events"] as! NSArray
                        if userInfo.count != 0{
                            for i in 0 ..< userInfo.count{
                                let dict = userInfo[i] as! NSDictionary
                                let EventName = dict["EventName"] as! String
                                let id = dict["Id"] as! Int
                                self.menuArr.append(EventName)
                                self.idArr.append(String(id))
                            }
                            self.errorView.isHidden = true
                            self.eventTableView.isHidden = false
                        }
                        else{
                            self.errorView.isHidden = false
                            self.eventTableView.isHidden = true
                        }
                        
                    }
                    self.eventTableView.reloadData()
                    break
                    
                case .failure(_):
                    print(response.error!.localizedDescription)
                    let alert: UIAlertView = UIAlertView(title: "", message: response.error!.localizedDescription, delegate: nil, cancelButtonTitle: "OK");
                    alert.show()
                    self.view.hideActivityView()
                    print(response.result.error as Any)
                    break
                    
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as UITableViewCell
        cell.accessoryType = UITableViewCellAccessoryType.disclosureIndicator
        cell.textLabel?.text = menuArr[indexPath.row]
        cell.textLabel?.font = UIFont(name:"HelveticaNeue-Bold", size:20)
        cell.textLabel?.textColor = UIColor.darkGray
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        cell.textLabel?.lineBreakMode = .byWordWrapping
        cell.textLabel?.numberOfLines = 0
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let cellText = menuArr[indexPath.row]
        let cellFont: UIFont? = UIFont(name:"HelveticaNeue-Bold", size: 18)!
        let attributedText = NSAttributedString(string: cellText, attributes: [NSAttributedStringKey.font: cellFont as Any])
        let rect: CGRect = attributedText.boundingRect(with: CGSize(width: tableView.bounds.size.width, height: CGFloat.greatestFiniteMagnitude), options: .usesLineFragmentOrigin, context: nil)
        return rect.size.height + 40
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let backItem = UIBarButtonItem()
        backItem.title = "Back"
        backItem.tintColor = UIColor.black
        navigationItem.backBarButtonItem = backItem
        let storyboard =  UIStoryboard(name: "Main", bundle: nil)
        let promo = storyboard.instantiateViewController(withIdentifier: "UserEventDetailViewController") as! UserEventDetailViewController
        promo.header = menuArr[indexPath.row]
        promo.eventID = self.idArr[indexPath.row]
        self.navigationController?.pushViewController(promo, animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    

}
