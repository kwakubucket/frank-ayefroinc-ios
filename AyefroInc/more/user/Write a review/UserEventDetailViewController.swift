//
//  UserEventDetailViewController.swift
//  AyefroInc
//
//  Created by Vijay Darkonde on 23/12/17.
//  Copyright © 2017 Inceptive Consulting Pvt. Ltd. All rights reserved.
//

import UIKit
import Alamofire

struct MyCurrency {
    var currencySymbolString = ""
}

class UserEventDetailViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var prototypeEntitiesFromJSON: [EventEntity] = []
    
    
   
    @IBOutlet var errorView: UIView!
    @IBOutlet var tableView: UITableView!
    var eventID: String! = ""
    var header: String! = ""
    var refreshControl: UIRefreshControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.fd_debugLogEnabled = true
        self.tableView.tableFooterView = UIView()
        self.errorView.isHidden = true
        self.tableView.isHidden = true
        self.title = header
        self.tableView.register(UINib.init(nibName: "ServiceProviderCell", bundle: nil), forCellReuseIdentifier: "ServiceProviderCell")
        buildTestData {
            
        }
    }
    
    override func viewDidLayoutSubviews() {
        self.automaticallyAdjustsScrollViewInsets = false
    }
    
    
    
    func buildTestData(then: @escaping () -> Void) {
        DispatchQueue.main.async {
            let manager = Alamofire.SessionManager.default
            manager.session.configuration.timeoutIntervalForRequest = 120
            // Data from `data.json`
            self.prototypeEntitiesFromJSON = []
            manager.request(baseURL + "api/Events/ProfileEventDetails?EventId=" + self.eventID, method: .get, parameters: ["":""], encoding: URLEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
                
                switch(response.result) {
                case .success(_):
                    if response.result.value != nil{
                        //print(response.result.value as Any)
                        let jsonResults : NSDictionary
                        jsonResults = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! NSDictionary
                        let providerName = jsonResults["providerName"] as! NSArray
                        if providerName.count == 0{
                            self.errorView.isHidden = false
                            self.tableView.isHidden = true
                        }
                        else{
                            self.errorView.isHidden = true
                            self.tableView.isHidden = false
                            let jsonArr = NSMutableArray()
                            for i in 0 ..< providerName.count{
                                let dicWithoutNulls = providerName[i] as! NSDictionary
                                
                                let outputDict = self.removeNSNull(from: dicWithoutNulls.mutableCopy() as! NSMutableDictionary)
                                
                                jsonArr.add(outputDict)
                            }
                            
                            let arr = NSArray(array: jsonArr)
                            for i in 0 ..< arr.count{
                                self.prototypeEntitiesFromJSON.append(EventEntity(dictionary: arr[i] as! [AnyHashable : Any] ))
                            }
                        }
                    }
                    self.tableView.reloadData()
                    break
                    
                case .failure(_):
                    print(response.error!.localizedDescription)
                    let alert: UIAlertView = UIAlertView(title: "", message: response.error!.localizedDescription, delegate: nil, cancelButtonTitle: "OK");
                    alert.show()
                    self.view.hideActivityView()
                    print(response.result.error as Any)
                    break
                    
                }
            }
        }
    }
    
    func removeNSNull(from dict: NSMutableDictionary) -> NSDictionary {
        let mutableDict = dict
        let keysWithEmptString = dict.filter { $0.1 is NSNull }.map { $0.0 }
        for key in keysWithEmptString {
            mutableDict[key] = ""
        }
        
        return mutableDict
    }
    
    // MARK: - UITableViewDataSource
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let count = prototypeEntitiesFromJSON.count
        if count != 0 {
            return count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "ServiceProviderCell") as! ServiceProviderCell
        if prototypeEntitiesFromJSON.count != 0{
            configureCell(cell, at: indexPath)
        }
        return cell
    }
    
    
    func configureCell(_ cell: ServiceProviderCell, at indexPath: IndexPath) {
        cell.fd_enforceFrameLayout = true
        if prototypeEntitiesFromJSON.count != 0{
            cell.entity = prototypeEntitiesFromJSON[indexPath.row]
        }
        
    }
    
    // MARK: - UITableViewDelegate
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tableView.fd_heightForCell(withIdentifier: "ServiceProviderCell") { cell in
            self.configureCell(cell as! ServiceProviderCell, at: indexPath)
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! ServiceProviderCell
        
        let backItem = UIBarButtonItem()
        backItem.title = "Back"
        backItem.tintColor = UIColor.black
        navigationItem.backBarButtonItem = backItem
        let storyboard =  UIStoryboard(name: "Main", bundle: nil)
        let promo = storyboard.instantiateViewController(withIdentifier: "AddReviewViewController") as! AddReviewViewController
        promo.eventID = self.eventID
        promo.ServiceProviderId = self.prototypeEntitiesFromJSON[indexPath.row].value(forKey: "ServiecProviderId") as! String
        promo.vender_name = cell.userNameLbl.text!
        self.navigationController?.pushViewController(promo, animated: true)
    }
    
//    class AppConstant {
//        private init() {}
//        class func cards() -> String { return currencySymbolString }
//    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
}



