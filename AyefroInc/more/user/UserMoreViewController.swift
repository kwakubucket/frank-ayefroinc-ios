//
//  UserMoreViewController.swift
//  AyefroInc
//
//  Created by Vijay Darkonde on 04/12/17.
//  Copyright © 2017 Inceptive Consulting Pvt. Ltd. All rights reserved.
//

import UIKit
import RATreeView
import SDWebImage
import Alamofire


class UserMoreViewController: UIViewController , RATreeViewDelegate, RATreeViewDataSource {
    
    @IBOutlet var dataView: UIView!
    @IBOutlet var profileImageView: UIImageView!
    @IBOutlet var nameLabel: UILabel!
    var treeView : RATreeView!
    var data : [DataObject]
    var editButton : UIBarButtonItem!
    
    var eventNameValueFromNotification: String = ""
    
    convenience init() {
        self.init(nibName : nil, bundle: nil)
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        data = TreeViewController.commonInit()
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        data = TreeViewController.commonInit()
        super.init(coder: aDecoder)
    }
    
    let fvc = UIView()
    let lblView = UIView()
    let textLbl = UILabel()
    let icon = UIImageView()
    let closeicon = UIImageView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        overlays()
        _ = ModelManager.instance.getUserLoginStatus()
        let url = URL(string: image)
        let str: String? = Bundle.main.path(forResource: "loading_spinner", ofType: "gif")
        let fileData = NSData(contentsOfFile: str!)
        nameLabel.text = userFirstName
        if url != nil{
            self.profileImageView.sd_setImage(with: url, placeholderImage: UIImage.sd_animatedGIF(with: fileData! as Data), options: SDWebImageOptions.allowInvalidSSLCertificates, progress: nil, completed: nil)
        }
        else{
            self.profileImageView.image = UIImage(named: "user")
        }
        profileImageView.layer.cornerRadius = profileImageView.frame.height/2
        profileImageView.layer.masksToBounds = true
        
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            
            if submitBitNotification == true{
                if eve_id != "" && eve_Name != ""{
                    let backItem = UIBarButtonItem()
                    backItem.title = "Back"
                    backItem.tintColor = UIColor.black
                    self.navigationItem.backBarButtonItem = backItem
                    let storyboard =  UIStoryboard(name: "Main", bundle: nil)
                    let promo = storyboard.instantiateViewController(withIdentifier: "BidServicesViewController") as! BidServicesViewController
                    promo.eventID = eve_id
                    promo.header = eve_Name
                    self.navigationController?.pushViewController(promo, animated: true)
                }
            }
        }
        
        
        view.backgroundColor = .white
        setupTreeView()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        getAllTopic()
        self.navigationController?.navigationBar.setBackgroundImage(nil, for: .default)
        self.navigationController?.navigationBar.shadowImage = nil
        self.tabBarController?.tabBar.isHidden = false
    }
    
    func overlays() {
        
        let showValue = UserDefaults.standard.value(forKey: "MoreOverlay") as! String
        if showValue == "true" {
            fvc.frame = CGRect(x: self.view.frame.origin.x, y: self.view.frame.origin.y, width: self.view.frame.size.width, height: self.view.frame.size.height)
            fvc.backgroundColor = UIColor(red:0.0/255.0, green:0.0/255.0, blue:0.0/255.0, alpha:0.2)
            
            
            lblView.frame = CGRect(x: self.dataView.frame.origin.y + (44 * 2), y: self.view.frame.size.height - 130, width: self.view.frame.size.width - 40, height: 60)
            lblView.backgroundColor = UIColor(red:98.0/255.0, green:151.0/255.0, blue:207.0/255.0, alpha: 1.0)
            lblView.layer.cornerRadius = 15
            
            
            
            textLbl.text = "Share your wedding and other event experiences with the world"
            textLbl.numberOfLines = 0
            textLbl.lineBreakMode = .byWordWrapping
            textLbl.font = UIFont(name:"HelveticaNeue-Bold", size: 17.0)!
            textLbl.layer.cornerRadius = 15
            textLbl.backgroundColor = UIColor(red:98.0/255.0, green:151.0/255.0, blue:207.0/255.0, alpha: 1.0)
            textLbl.textColor = UIColor.white
            textLbl.frame = CGRect(x: lblView.frame.origin.x , y: 0, width: lblView.frame.size.width - 40, height: 60)
            lblView.addSubview(textLbl)
            
            icon.image = UIImage(named: "drr")
            icon.frame = CGRect(x: self.view.frame.origin.x + 40, y: self.view.frame.size.height - 80, width: 30, height: 30)
            fvc.addSubview(icon)
            
            closeicon.image = UIImage(named: "closeb")
            closeicon.frame = CGRect(x: lblView.frame.size.width - 30, y: 5, width: 20, height: 20)
            lblView.addSubview(closeicon)
            
            fvc.addSubview(lblView)
            self.tabBarController?.view.addSubview(fvc)
            
            let tap1 = UITapGestureRecognizer(target: self, action: #selector(self.tapOnProfilePic))
            fvc.isUserInteractionEnabled = true
            fvc.addGestureRecognizer(tap1)
            
            let tap2 = UITapGestureRecognizer(target: self, action: #selector(self.tapOnProfilePic))
            lblView.isUserInteractionEnabled = true
            lblView.addGestureRecognizer(tap2)
            
            let tap3 = UITapGestureRecognizer(target: self, action: #selector(self.tapOnProfilePic))
            closeicon.isUserInteractionEnabled = true
            closeicon.addGestureRecognizer(tap3)
            
        }
        
    }
    
    @objc func tapOnProfilePic(){
        fvc.removeFromSuperview()
//        UserDefaults.standard.setValue("false", forKey: "DashboardOverlay")
    }
    
    func setupTreeView() -> Void {
        treeView = RATreeView(frame: CGRect(x: 0, y: self.dataView.frame.height + (self.navigationController?.navigationBar.frame.height)!, width: self.view.frame.size.width, height: self.view.frame.size.height - ((self.tabBarController?.tabBar.frame.size.height)! + self.dataView.frame.height)))
        treeView.register(UINib(nibName: String(describing: TreeTableViewCell.self), bundle: nil), forCellReuseIdentifier: String(describing: TreeTableViewCell.self))
        treeView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        treeView.delegate = self;
        treeView.dataSource = self;
        treeView.treeFooterView = UIView()
        treeView.backgroundColor = .white
        
        view.addSubview(treeView)
    }
    
    //MARK: RATreeView data source
    
    @IBAction func profileBtnClk(_ sender: Any) {
        let backItem = UIBarButtonItem()
        backItem.title = "Back"
        backItem.tintColor = UIColor.black
        navigationItem.backBarButtonItem = backItem
        let storyboard =  UIStoryboard(name: "Main", bundle: nil)
        let promo = storyboard.instantiateViewController(withIdentifier: "UserProfileViewController") as! UserProfileViewController
        self.navigationController?.pushViewController(promo, animated: true)
    }
    
    
    func treeView(_ treeView: RATreeView, numberOfChildrenOfItem item: Any?) -> Int {
        if let item = item as? DataObject {
            return item.children.count
        } else {
            return self.data.count
        }
    }
    
    func treeView(_ treeView: RATreeView, child index: Int, ofItem item: Any?) -> Any {
        if let item = item as? DataObject {
            return item.children[index]
        } else {
            return data[index] as AnyObject
        }
    }
    
    func treeView(_ treeView: RATreeView, cellForItem item: Any?) -> UITableViewCell {
        let cell = treeView.dequeueReusableCell(withIdentifier: String(describing: TreeTableViewCell.self)) as! TreeTableViewCell
        let item = item as! DataObject
        
        let level = treeView.levelForCell(forItem: item)
        cell.selectionStyle = .none
        cell.setup(withTitle: item.name, level: level,imageName: item.imageName,childCount: item.children.count, additionalButtonHidden: false)
        
        
        return cell
    }
    
    //MARK: RATreeView delegate
    func treeView(_ treeView: RATreeView, didSelectRowForItem item: Any) {
        let item = item as! DataObject
        let backItem = UIBarButtonItem()
        backItem.title = "Back"
        backItem.tintColor = UIColor.black
        navigationItem.backBarButtonItem = backItem
        
        print(item.name)
        if (item.name == "Add Event"){
            let storyboard =  UIStoryboard(name: "Main", bundle: nil)
            let promo = storyboard.instantiateViewController(withIdentifier: "UserAddEventViewController") as! UserAddEventViewController
            self.navigationController?.pushViewController(promo, animated: true)
        }
        if (item.name == "Write a Review"){
            let storyboard =  UIStoryboard(name: "Main", bundle: nil)
            let promo = storyboard.instantiateViewController(withIdentifier: "CompletedEventViewController") as! CompletedEventViewController
            self.navigationController?.pushViewController(promo, animated: true)
        }
        if (item.name == "Refer and Redeem"){
            let storyboard =  UIStoryboard(name: "Main", bundle: nil)
            let promo = storyboard.instantiateViewController(withIdentifier: "ReferEarnViewController") as! ReferEarnViewController
            self.navigationController?.pushViewController(promo, animated: true)
        }
        if (item.name == "Bids"){
            let storyboard =  UIStoryboard(name: "Main", bundle: nil)
            let promo = storyboard.instantiateViewController(withIdentifier: "UserBidsViewController") as! UserBidsViewController
            self.navigationController?.pushViewController(promo, animated: true)
        }
        if (item.name == "Gallery"){
            let storyboard =  UIStoryboard(name: "Main", bundle: nil)
            let promo = storyboard.instantiateViewController(withIdentifier: "BrowesGalleryViewController") as! BrowesGalleryViewController
            self.navigationController?.pushViewController(promo, animated: true)
        }
        if (item.name == "Favourites"){
            let storyboard =  UIStoryboard(name: "Main", bundle: nil)
            let promo = storyboard.instantiateViewController(withIdentifier: "FavListViewController") as! FavListViewController
            self.navigationController?.pushViewController(promo, animated: true)
        }
        if (item.name == "My Profile"){
            let storyboard =  UIStoryboard(name: "Main", bundle: nil)
            let promo = storyboard.instantiateViewController(withIdentifier: "UserProfileViewController") as! UserProfileViewController
            self.navigationController?.pushViewController(promo, animated: true)
        }
        if (item.name == "Pay to Use"){
            let storyboard =  UIStoryboard(name: "Main", bundle: nil)
            let promo = storyboard.instantiateViewController(withIdentifier: "PayToUseViewController") as! PayToUseViewController
            self.navigationController?.pushViewController(promo, animated: true)
        }
        
        if (item.name == "Notifications"){
            UIApplication.shared.openURL(NSURL(string:UIApplicationOpenSettingsURLString)! as URL)
        }
        if (item.name == "About Us"){
            let storyboard =  UIStoryboard(name: "Main", bundle: nil)
            let promo = storyboard.instantiateViewController(withIdentifier: "AboutUsViewController") as! AboutUsViewController
            self.navigationController?.pushViewController(promo, animated: true)
        }
        if (item.name == "Privacy Policy"){
            let storyboard =  UIStoryboard(name: "Main", bundle: nil)
            let promo = storyboard.instantiateViewController(withIdentifier: "PrivacyPolicyViewController") as! PrivacyPolicyViewController
            self.navigationController?.pushViewController(promo, animated: true)
        }
        if (item.name == "Terms & Conditions"){
            let storyboard =  UIStoryboard(name: "Main", bundle: nil)
            let promo = storyboard.instantiateViewController(withIdentifier: "TermsAndConditionViewController") as! TermsAndConditionViewController
            self.navigationController?.pushViewController(promo, animated: true)
        }
        if (item.name == "Blog"){
            let storyboard =  UIStoryboard(name: "Main", bundle: nil)
            let promo = storyboard.instantiateViewController(withIdentifier: "BlogViewController") as! BlogViewController
            promo.blogURL = "http://www.ayefroinc.com/blog"
            self.navigationController?.pushViewController(promo, animated: true)
//            self.tabBarController?.selectedIndex = 2
        }
        if (item.name == "Join the Discussion"){
            self.tabBarController?.selectedIndex = 2
        }
        if (item.name == "Verify Account"){
            let storyboard =  UIStoryboard(name: "Main", bundle: nil)
            let promo = storyboard.instantiateViewController(withIdentifier: "VerifyAccountViewController") as! VerifyAccountViewController
            self.navigationController?.pushViewController(promo, animated: true)
        }
        if (item.name == "Contact Us"){
            let storyboard =  UIStoryboard(name: "Main", bundle: nil)
            let promo = storyboard.instantiateViewController(withIdentifier: "ContactUsViewController") as! ContactUsViewController
            self.navigationController?.pushViewController(promo, animated: true)
        }
        if (item.name == "Share"){
            let textToShare = "Download Ayefro Inc App from Play Store: https://play.google.com/store/apps/details?id=com.ayefroinc"
            
            if let myWebsite = NSURL(string: "https://play.google.com/store/apps/details?id=com.ayefroinc") {
                let objectsToShare = [textToShare, myWebsite] as [Any] as [Any]
                let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
                activityVC.popoverPresentationController?.sourceView = treeView
                self.present(activityVC, animated: true, completion: nil)
            }
            
        }
        
    }
    
    
    func getAllTopic()  {
        servicesDescriptionArr = []
        servicesArr = []
        servicesIDArr = []
        DispatchQueue.main.async {
            
            let manager = Alamofire.SessionManager.default
            manager.session.configuration.timeoutIntervalForRequest = 120
            
            manager.request( baseURL + "api/Events/GetAllServices", method: .get, parameters: ["":""], encoding: URLEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
                
                switch(response.result) {
                case .success(_):
                    if response.result.value != nil{
                        //print(response.result.value as Any)
                    }
                    let jsonResults : NSArray
                    jsonResults = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! NSArray
                    for i in 0 ..< jsonResults.count{
                        let data = jsonResults[i] as! NSDictionary
                        let categoryID = data["id"] as! Int
                        let CategoryTitle = data["serviceName"] as! String
                        var serviceDescription = ""
                        if (data["serviceDescription"] as? String) != nil {
                            serviceDescription = data["serviceDescription"] as! String
                        }
                        let Active = data["active"] as! Bool
                        if Active == true{
                            servicesArr.append(CategoryTitle)
                            servicesIDArr.append(String(categoryID))
                            servicesDescriptionArr.append(serviceDescription)
                        }
                    }
                    
                    break
                    
                case .failure(_):
                    print(response.error!.localizedDescription)
                    let alert: UIAlertView = UIAlertView(title: "", message: response.error!.localizedDescription, delegate: nil, cancelButtonTitle: "OK");
                    alert.show()
                    self.view.hideActivityView()
                    print(response.result.error as Any)
                    break
                    
                }
            }
        }
    }
}


private extension TreeViewController {
    
    static func commonInit() -> [DataObject] {
        
        let addEvents = DataObject(name: "Add Event", imageName: "plus")
        let Bids = DataObject(name: "Bids", imageName: "list")
        let writereview = DataObject(name: "Write a Review", imageName: "edit")
        
        var My_Event_Center = DataObject(name: "My Event Center                                  \u{25BC}", imageName: "calendar", children: [addEvents, Bids, writereview])
        if UIScreen.main.bounds.size.width == 414
        {
            My_Event_Center = DataObject(name: "My Event Center                                  \u{25BC}", imageName: "calendar", children: [addEvents, Bids, writereview])
        }
        else if UIScreen.main.bounds.size.width == 375 && UIScreen.main.bounds.size.height == 667
        {
            My_Event_Center = DataObject(name: "My Event Center                                  \u{25BC}", imageName: "calendar", children: [addEvents, Bids, writereview])
        }
        else if UIScreen.main.bounds.size.width == 320 && UIScreen.main.bounds.size.height == 568
        {
            My_Event_Center = DataObject(name: "My Event Center                    \u{25BC}", imageName: "calendar", children: [ addEvents, Bids, writereview])
        }
        else if UIScreen.main.bounds.size.width == 320 && UIScreen.main.bounds.size.height == 480
        {
            My_Event_Center = DataObject(name: "My Event Center                    \u{25BC}", imageName: "calendar", children: [ addEvents, Bids, writereview])
        }
        
        
        let Gallery = DataObject(name: "Gallery", imageName: "gallery")
        let Fav = DataObject(name: "Favourites", imageName: "favourite")
        
        let profile = DataObject(name: "My Profile", imageName: "myprofile")
        let notification = DataObject(name: "Notifications", imageName: "notifications")
        var Setting = DataObject(name: "Settings                                                \u{25BC}", imageName: "settings", children: [ profile, notification])
        if UIScreen.main.bounds.size.width == 414
        {
            Setting = DataObject(name: "Settings                                                \u{25BC}", imageName: "settings", children: [profile, notification])
        }
        else if UIScreen.main.bounds.size.width == 375 && UIScreen.main.bounds.size.height == 667
        {
            Setting = DataObject(name: "Settings                                                \u{25BC}", imageName: "settings", children: [profile, notification])
        }
        else if UIScreen.main.bounds.size.width == 320 && UIScreen.main.bounds.size.height == 568
        {
            Setting = DataObject(name: "Settings                                  \u{25BC}", imageName: "settings", children: [profile, notification])
        }
        else if UIScreen.main.bounds.size.width == 320 && UIScreen.main.bounds.size.height == 480
        {
            Setting = DataObject(name: "Settings                                  \u{25BC}", imageName: "settings", children: [profile, notification])
        }
        let refer = DataObject(name: "Refer and Redeem", imageName: "referearn")
        let about = DataObject(name: "About Us", imageName: "aboutus")
        let pp = DataObject(name: "Privacy Policy", imageName: "policy")
        let tc = DataObject(name: "Terms & Conditions", imageName: "terms")
        var AboutUs = DataObject(name: "About Us                                              \u{25BC}", imageName: "information",  children: [ about, pp, tc])
        if UIScreen.main.bounds.size.width == 414
        {
            AboutUs = DataObject(name: "About Us                                              \u{25BC}", imageName: "information",  children: [ about, pp, tc])
        }
        else if UIScreen.main.bounds.size.width == 375 && UIScreen.main.bounds.size.height == 667
        {
            AboutUs = DataObject(name: "About Us                                              \u{25BC}", imageName: "information",  children: [ about, pp, tc])
        }
        else if UIScreen.main.bounds.size.width == 320 && UIScreen.main.bounds.size.height == 568
        {
            AboutUs = DataObject(name: "About Us                                \u{25BC}", imageName: "information",  children: [ about, pp, tc])
        }
        else if UIScreen.main.bounds.size.width == 320 && UIScreen.main.bounds.size.height == 480
        {
            AboutUs = DataObject(name: "About Us                                \u{25BC}", imageName: "information",  children: [ about, pp, tc])
        }
        
        let Blog = DataObject(name: "Blog", imageName: "blog")
        let discussion = DataObject(name: "Join the Discussion", imageName: "forum")
        let contactUs = DataObject(name: "Contact Us", imageName: "contactus")
        let Share = DataObject(name: "Share", imageName: "share")
        

        
        
        return [My_Event_Center, Gallery, Fav, Setting, refer, AboutUs, Blog, discussion, contactUs, Share ]
    }
    
}


