//
//  FavVendorCell.swift
//  AyefroInc
//
//  Created by Vijay Darkonde on 20/12/17.
//  Copyright © 2017 Inceptive Consulting Pvt. Ltd. All rights reserved.
//

import UIKit

class FavVendorCell: UICollectionViewCell {
    
    @IBOutlet weak var userImg: UIImageView!
    @IBOutlet weak var userNameLbl: UILabel!
    @IBOutlet weak var ratingLbl: UILabel!
    
}
