//
//  ViewController.swift
//  ExpandableTableViewController
//
//  Created by enric.macias.lopez on 08/21/2015.
//  Copyright (c) 2015 enric.macias.lopez. All rights reserved.
//

import UIKit
import ExpandableTableViewController

enum TableViewRows: Int {
    case text = 0, datePicker, list
}


struct Model {
    let serviceId: String
    let serviceDesc: String
    let serviceCount: Int
    
    static func jsonArray(array : [Model]) -> String
    {
        return "[" + array.map {$0.jsonRepresentation}.joined(separator: ",") + "]"
    }
    
    var jsonRepresentation : String {
        return "{\"serviceId\":\"\(serviceId)\",\"serviceDesc\":\"\(serviceDesc)\",\"serviceCount\":\"\(serviceCount)\"}"
    }
}

class DemoTableViewController: ExpandableTableViewController, ExpandableTableViewDelegate, UITextViewDelegate, UITextFieldDelegate {
    
    var button = UIButton()
    var delegate: AddServicesForEvent!
    
    
    var selectedService: [Model] = []
    
    
    var selectedID: [String] = []
    var isSelected: [String] = []
    var selectedIndex: [Int] = []

    var amountArr : [String] = []
    var descriptionArr : [String] = []
    
    var servicersLoadArr : NSArray!
 
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.title = "Select Services"
        self.expandableTableView.expandableDelegate = self
        
        button = UIButton(frame: CGRect(x: 0, y: (self.view.frame.size.height - 50), width: self.view.frame.size.width, height: 50))
        button.backgroundColor = UIColor(red: 85.0/255.0, green: 85.0/255.0, blue: 85.0/255.0, alpha: 1.0)
        button.setTitle("ADD SERVICES", for: .normal)
        button.titleLabel?.textColor = UIColor.white
        button.titleLabel?.font = UIFont(name:"HelveticaNeue-Bold", size: 17.0)!
        button.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        navigationController?.view.addSubview(button)
        
        
        
        if servicesArr.count != 0{
            for _ in 0 ..< servicesArr.count{
                self.isSelected.append("false")
                self.amountArr.append("0")
                self.descriptionArr.append("")
            }
            
            for i in 0 ..< servicersLoadArr.count{
                let dict = servicersLoadArr[i] as! NSDictionary
                let servID = dict["serviceId"] as! String
                let amount = dict["serviceCount"] as! String
                let serviceDescription = dict["serviceDesc"] as! String
                let index = servicesIDArr.index(of: servID)
//                let index = Int(servID)! - 1
                self.isSelected[index!] = "true"
                self.selectedIndex.append(index!)
                self.amountArr[index!] = amount
                self.descriptionArr[index!] = serviceDescription
                self.selectedID.append(servID)
            }
        }
        else{
            let alert: UIAlertView = UIAlertView(title: "", message: "Services not loaded yet, please try again.", delegate: nil, cancelButtonTitle: "OK");
            alert.show()
        }
        
        let backbutton = UIButton(type: .system)
        backbutton.setTitle(" Back", for: .normal)
        backbutton.setImage(UIImage(named: "backbtn"), for: .normal)
        backbutton.addTarget(self, action: #selector(reset), for: .touchUpInside)
        backbutton.tintColor = UIColor.black
        backbutton.contentHorizontalAlignment = .left
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: backbutton)
    }
    
    @objc func reset()  {
        if selectedID.count == 0 {
            self.navigationController?.popViewController(animated: true)
        }
        else{
            let refreshAlert = UIAlertController(title: "", message: "Are you sure you want to EXIT without adding the selected services?", preferredStyle: UIAlertControllerStyle.alert)
            
            refreshAlert.addAction(UIAlertAction(title: "Discard", style: .cancel , handler: { (action: UIAlertAction!) in
                self.navigationController?.popViewController(animated: true)
            }))
            
            
            refreshAlert.addAction(UIAlertAction(title: "Continue", style: .default, handler: { (action: UIAlertAction!) in
            }))
            
            self.present(refreshAlert, animated: true, completion: nil)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.expandableTableView.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: (self.view.frame.size.height - 50))
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.button.removeFromSuperview()
    }
    
    @objc func buttonAction(_ sender: UIButton!) {
        do{
            unexpandAllCells()
            print(self.selectedID)
            selectedService = []
            if selectedID.count != 0{
                for i in 0 ..< selectedID.count{
                    
                    let index = self.selectedIndex[i]
                    let amount = self.amountArr[index]
                    if amount == "0" || (amount.trimmingCharacters(in: .whitespaces).isEmpty){
                        let alert: UIAlertView = UIAlertView(title: "", message: "You have selected a service but you have not specified an amount. Please check and try again", delegate: nil, cancelButtonTitle: "OK");
                        alert.show()
                        break
                    }
                    else{
                        let desc = self.descriptionArr[index]
                        if desc == "" || (desc.trimmingCharacters(in: .whitespaces).isEmpty){
                            let alert: UIAlertView = UIAlertView(title: "", message: "You have selected a service but you have not specified an description. Please check and try again", delegate: nil, cancelButtonTitle: "OK");
                            alert.show()
                            break
                        }
                        else{
                            if i == selectedID.count - 1{
                                selectedService.append(Model.init(serviceId: selectedID[i], serviceDesc: desc, serviceCount: Int(amount)!))
                                let jsonArray = Model.jsonArray(array: selectedService)
                                let jsonData = jsonArray.data(using: .utf8)
                                let dictionary = try? JSONSerialization.jsonObject(with: jsonData!, options: .mutableLeaves) as! NSArray
                                self.delegate.getServiceArray(arr: dictionary as! NSArray)
                                self.navigationController?.popViewController(animated: true)
                            }
                            else{
                                selectedService.append(Model.init(serviceId: selectedID[i], serviceDesc: desc, serviceCount: Int(amount)!))
                            }
                        }
                    }
                }
                
            }
            else{
                let alert: UIAlertView = UIAlertView(title: "", message: "Kindly tick the services you require, fill in extra details and add amount", delegate: nil, cancelButtonTitle: "OK");
                alert.show()
            }
        }
        catch{
            
        }
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: - Init
    
    override func tableView(_: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if cell.responds(to: #selector(setter: UIView.preservesSuperviewLayoutMargins)){
            cell.preservesSuperviewLayoutMargins = false
        }
        
        if cell.responds(to: #selector(setter: UIView.layoutMargins)){
            cell.layoutMargins = UIEdgeInsets.zero
        }
    }
    
    @IBAction func btnClk(_ sender: UIButton) {
        let position = self.tableView.convert(CGPoint.zero, from: sender)
        let pickerCellIndexPath : IndexPath = expandableTableView.indexPathForRow(at: position)!
        
        // Creates the index path for the value text
        let birthdateCell : BirthdateTableViewCell = expandableTableView.cellForRow(at: pickerCellIndexPath) as! BirthdateTableViewCell
        let index = self.expandableIndexPathForIndexPath(pickerCellIndexPath)
        if birthdateCell.btn.imageView?.image == UIImage(named: "check-mark")
        {
            birthdateCell.btn.setImage(UIImage(named: "circle"), for: UIControlState.normal)
            if expandableTableView.isCellExpandedAtExpandableIndexPath(index) == true{
                unexpandCellAtIndexPath(pickerCellIndexPath, tableView: expandableTableView)
                birthdateCell.arrowImgf.image = UIImage(named: "darrow")
                if let index = selectedID.index(of: servicesIDArr[index.row]) {
                    self.selectedID.remove(at: index)
                }
                
                self.isSelected[index.row] = "false"
                self.amountArr[index.row] = "0"
                self.descriptionArr[index.row] = ""
                if let index = selectedIndex.index(of: index.row) {
                    selectedIndex.remove(at: index)
                }
                
            }
            else{
                if let index = selectedID.index(of: servicesIDArr[index.row]) {
                    self.selectedID.remove(at: index)
                }
                self.isSelected[index.row] = "false"
                self.amountArr.append("0")
                self.descriptionArr.append("")
                if let index = selectedIndex.index(of: index.row) {
                    selectedIndex.remove(at: index)
                }
            }
        }
        else{
            birthdateCell.btn.setImage(UIImage(named: "check-mark"), for: UIControlState.normal)
            if expandableTableView.isCellExpandedAtExpandableIndexPath(index) == false{
                unexpandAllCells()
                let ind = index.row
                let indexP = IndexPath(row: ind, section: 0)
                expandCellAtIndexPath(indexP, tableView: self.expandableTableView)
                self.selectedID.append(servicesIDArr[index.row])
                self.isSelected[index.row] = "true"
                self.selectedIndex.append(index.row)
                birthdateCell.arrowImgf.image = UIImage(named: "uarrow")
            }
            else{
                unexpandAllCells()
                self.selectedID.append(servicesIDArr[index.row])
                self.selectedIndex.append(index.row)
                self.isSelected[index.row] = "true"
            }
        }
    }
    
    
    // MARK: - Expandable Table View Controller Delegate
    
    // MARK: - Rows
    func expandableTableView(_ expandableTableView: ExpandableTableView, numberOfRowsInSection section: Int) -> Int {
        return servicesArr.count
    }
    
    func expandableTableView(_ expandableTableView: ExpandableTableView, cellForRowAtExpandableIndexPath expandableIndexPath: ExpandableIndexPath) -> UITableViewCell {
        var cell: BirthdateTableViewCell!
        cell = expandableTableView.dequeueReusableCellWithIdentifier("BirthdateCell", forIndexPath: expandableIndexPath) as! BirthdateTableViewCell
        if isSelected[expandableIndexPath.row] == "true"{
            cell.btn.setImage(UIImage(named: "check-mark"), for: UIControlState.normal)
        }
        else{
            cell.btn.setImage(UIImage(named: "circle"), for: UIControlState.normal)
        }
        cell.btn.tintColor = UIColor.black
        cell.serviceLbl.text = servicesArr[expandableIndexPath.row]
        let prefs: UserDefaults? = UserDefaults.standard
        let currencySymbol = prefs?.string(forKey: "currencySymbol")
        if amountArr[expandableIndexPath.row] == "0"{
            cell.budgetLbl.text = currencySymbol! + " 0"
        }
        else{
            cell.budgetLbl.text = currencySymbol! + " " +  amountArr[expandableIndexPath.row]
        }
        cell.descriptionlbl.text = self.descriptionArr[expandableIndexPath.row]
        return cell
    }
    
    
    func expandableTableView(_ expandableTableView: ExpandableTableView, heightForRowAtExpandableIndexPath expandableIndexPath: ExpandableIndexPath) -> CGFloat {
        return 70
    }
    
    func expandableTableView(_ expandableTableView: ExpandableTableView, estimatedHeightForRowAtExpandableIndexPath expandableIndexPath: ExpandableIndexPath) -> CGFloat {
        return 70
        
    }
    
    func expandableTableView(_ expandableTableView: ExpandableTableView, didSelectRowAtExpandableIndexPath expandableIndexPath: ExpandableIndexPath) {
        let birthdateCell : BirthdateTableViewCell = expandableTableView.cellForRowAtIndexPath(expandableIndexPath) as! BirthdateTableViewCell
        if expandableTableView.isCellExpandedAtExpandableIndexPath(expandableIndexPath) == true{
            unexpandAllCells()
            let index = expandableIndexPath.row
            let indexPath = IndexPath(row: index, section: 0)
            expandCellAtIndexPath(indexPath, tableView: self.expandableTableView)
            birthdateCell.arrowImgf.image = UIImage(named: "uarrow")
        }
        else{
            birthdateCell.arrowImgf.image = UIImage(named: "darrow")
        }
    }
    
    // MARK: - SubRows
    func expandableTableView(_ expandableTableView: ExpandableTableView, numberOfSubRowsInRowAtExpandableIndexPath expandableIndexPath: ExpandableIndexPath) -> Int {
        return 1
    }
    
    func expandableTableView(_ expandableTableView: ExpandableTableView, subCellForRowAtExpandableIndexPath expandableIndexPath: ExpandableIndexPath) -> UITableViewCell {
        var cell: DatePickerTableViewCell!
        
        cell = expandableTableView.dequeueReusableCellWithIdentifier("DatePickerCell", forIndexPath: expandableIndexPath) as! DatePickerTableViewCell
        cell.budgetTF.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        
        if amountArr[expandableIndexPath.row] == "0"{
            cell.budgetTF.text = ""
        }
        else{
            cell.budgetTF.text = amountArr[expandableIndexPath.row]
        }
        if descriptionArr[expandableIndexPath.row] == ""{
            cell.descriptionTf.text = ""
        }
        else{
            cell.descriptionTf.text = descriptionArr[expandableIndexPath.row]
        }
        cell.descriptionTf.placeholder = servicesDescriptionArr[expandableIndexPath.row]
        cell.descriptionTf.layer.borderColor = UIColor.gray.cgColor
        cell.descriptionTf.layer.borderWidth = 0.5
        cell.descriptionTf.layer.cornerRadius = 5
        cell.descriptionTf.delegate = self
        cell.budgetTF.delegate = self
        return cell
    }
    
    
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        let position : CGPoint = textField.convert(textField.frame.origin, to: self.expandableTableView)
        let pickerCellIndexPath : IndexPath = tableView.indexPathForRow(at: CGPoint(x: position.x, y:position.y - 160))!
        let valueCellIndexPath = IndexPath(row: pickerCellIndexPath.row - 1, section: 0)
        
        let birthdateCell : BirthdateTableViewCell = tableView.cellForRow(at: valueCellIndexPath) as! BirthdateTableViewCell
        let prefs: UserDefaults? = UserDefaults.standard
        let currencySymbol = prefs?.string(forKey: "currencySymbol")
        birthdateCell.budgetLbl.text = currencySymbol! + " " +  textField.text!
        
        self.amountArr[valueCellIndexPath.row] = textField.text!
    }
    
    
    func expandableTableView(_ expandableTableView: ExpandableTableView, heightForSubRowAtExpandableIndexPath expandableIndexPath: ExpandableIndexPath) -> CGFloat {
        return 185.0
    }
    
    func expandableTableView(_ expandableTableView: ExpandableTableView, estimatedHeightForSubRowAtExpandableIndexPath expandableIndexPath: ExpandableIndexPath) -> CGFloat {
        return 185.0
    }
    
    func expandableTableView(_ expandableTableView: ExpandableTableView, didSelectSubRowAtExpandableIndexPath expandableIndexPath: ExpandableIndexPath){
        
    }
    
    func textViewDidChange(_ textView: UITextView) {
        print(textView.text)
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        let position : CGPoint = textView.convert(textView.frame.origin, to: self.expandableTableView)
        let pickerCellIndexPath : IndexPath = self.expandableTableView.indexPathForRow(at: CGPoint(x: position.x, y:position.y - 100))!
        let valueCellIndexPath = IndexPath(row: pickerCellIndexPath.row, section: 0)
        
        let birthdateCell : BirthdateTableViewCell = self.expandableTableView.cellForRow(at: valueCellIndexPath) as! BirthdateTableViewCell
        birthdateCell.descriptionlbl.text =  textView.text!
        self.descriptionArr[valueCellIndexPath.row] = textView.text!
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        
        let newLength = text.utf16.count + string.utf16.count - range.length
        return newLength <= 8 // Bool
    }
    
    
}

