//
//  UserAddEventViewController.swift
//  AyefroInc
//
//  Created by Vijay Darkonde on 14/12/17.
//  Copyright © 2017 Inceptive Consulting Pvt. Ltd. All rights reserved.
//

import UIKit
import Alamofire

protocol AddServicesForEvent {
    func getServiceArray(arr: NSArray)
}


class UserAddEventViewController: UIViewController, AddServicesForEvent {
    
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var eventNameTF: NiceTextField!
    @IBOutlet var userLocTF: NiceTextField!
    @IBOutlet var eventLocTF: NiceTextField!
    @IBOutlet var eventDateTF: NiceTextField!
    @IBOutlet var servicesLbl: UILabel!
    @IBOutlet var budgetLbl: UILabel!
    @IBOutlet var scrollHeight: NSLayoutConstraint!
    var category: [String] = ["Coming Soon..."]
    var menuOptionImageNameArray : [String] = []
    
    var selectedServices = NSArray()
    var totalBudgetValue: String = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Add New Event"
        self.tabBarController?.tabBar.isHidden = true

        let button = UIButton(type: .system)
        button.setTitle(" Back", for: .normal)
        button.setImage(UIImage(named: "backbtn"), for: .normal)
        button.addTarget(self, action: #selector(reset), for: .touchUpInside)
        button.tintColor = UIColor.black
        button.contentHorizontalAlignment = .left
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: button)
        
        let config = FTConfiguration.shared
        config.textColor = UIColor.white
        config.backgoundTintColor = UIColor.black
        config.borderColor = UIColor.lightGray
        config.menuWidth = self.view.frame.width - 10
        config.menuSeparatorColor = UIColor.lightGray
        config.textAlignment = .left
        config.textFont = UIFont(name: "Helvetica Neue", size: 15)!
        config.menuRowHeight = 60
        config.cornerRadius = 6
        
        if UIScreen.main.bounds.size.width == 414
        {
            config.textFont = UIFont(name: "Helvetica Neue", size: 15)!
        }
        else if UIScreen.main.bounds.size.width == 375 && UIScreen.main.bounds.size.height == 667
        {
            config.textFont = UIFont(name: "Helvetica Neue", size: 15)!
        }
        else if UIScreen.main.bounds.size.width == 320 && UIScreen.main.bounds.size.height == 568
        {
            config.textFont = UIFont(name: "Helvetica Neue", size: 13)!
            scrollHeight.constant = 100
        }
        else if UIScreen.main.bounds.size.width == 320 && UIScreen.main.bounds.size.height == 480
        {
            config.textFont = UIFont(name: "Helvetica Neue", size: 15)!
            scrollHeight.constant = 160
        }
        
    }
    
    @objc func reset()  {
        if eventNameTF.text == "" && userLocTF.text == "" && eventDateTF.text == ""  && eventLocTF.text == "" && selectedServices.count == 0{
            self.navigationController?.popViewController(animated: true)
        }
        else{
            let refreshAlert = UIAlertController(title: "", message: "Are you sure you want to exit without creating your event?", preferredStyle: UIAlertControllerStyle.alert)
            
            refreshAlert.addAction(UIAlertAction(title: "Discard", style: .cancel , handler: { (action: UIAlertAction!) in
                self.navigationController?.popViewController(animated: true)
            }))
            
            
            refreshAlert.addAction(UIAlertAction(title: "Continue", style: .default, handler: { (action: UIAlertAction!) in
            }))
            
            self.present(refreshAlert, animated: true, completion: nil)
        }
    }
    
    @objc func startDate(sender:UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        let date = dateFormatter.string(from: sender.date)
        eventDateTF.text = date
    }
    
    @IBAction func calImgBtnClk(_ sender: UIButton) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        let date = dateFormatter.string(from: NSDate() as Date)
        eventDateTF.text = date
        let datePickerView:UIDatePicker = UIDatePicker()
        datePickerView.datePickerMode = UIDatePickerMode.date
        datePickerView.minimumDate = NSDate() as Date
        eventDateTF.inputView = datePickerView
        eventDateTF.becomeFirstResponder()
        datePickerView.addTarget(self, action: #selector(UserAddEventViewController.startDate), for: UIControlEvents.valueChanged)
    }
    
    @IBAction func dateTFClk(_ sender: NiceTextField) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        let date = dateFormatter.string(from: NSDate() as Date)
        eventDateTF.text = date
        let datePickerView:UIDatePicker = UIDatePicker()
        datePickerView.datePickerMode = UIDatePickerMode.date
        datePickerView.minimumDate = NSDate() as Date
        sender.inputView = datePickerView
        datePickerView.addTarget(self, action: #selector(UserAddEventViewController.startDate), for: UIControlEvents.valueChanged)
    }
    
    @IBAction func eventLocInfoBtnClk(_ sender: UIButton) {
        category = ["Vendors need this information to calculate their costs"]
        FTPopOverMenu.showForSender(sender: sender, with: category, menuImageArray: menuOptionImageNameArray, done: { (selectedIndex) -> () in
            print(selectedIndex)
            
        }) {
        }
    }
    
    @IBAction func userLocInfoBtnClk(_ sender: UIButton) {
        category = ["This will help us to find you the right vendors"]
        FTPopOverMenu.showForSender(sender: sender, with: category, menuImageArray: menuOptionImageNameArray, done: { (selectedIndex) -> () in
            print(selectedIndex)
            
        }) {
        }
    }
    
    @IBAction func eventNameInfoBtnClk(_ sender: UIButton) {
        category = ["Example: Corporate Event, Wedding, Birthday Party, etc"]
        FTPopOverMenu.showForSender(sender: sender, with: category, menuImageArray: menuOptionImageNameArray, done: { (selectedIndex) -> () in
            print(selectedIndex)
            
        }) {
        }
    }
    
    @IBAction func servicesBtnClk(_ sender: UIButton) {
        let backItem = UIBarButtonItem()
        backItem.title = "Back"
        backItem.tintColor = UIColor.black
        navigationItem.backBarButtonItem = backItem
        let storyboard =  UIStoryboard(name: "Main", bundle: nil)
        let promo = storyboard.instantiateViewController(withIdentifier: "DemoTableViewController") as! DemoTableViewController
        promo.delegate = self
        promo.servicersLoadArr = selectedServices
        self.navigationController?.pushViewController(promo, animated: true)
    }
    
    @IBAction func submitEventBtnClk(_ sender: UIButton) {
        if (eventNameTF.text?.trimmingCharacters(in: .whitespaces).isEmpty)!{
            let alert: UIAlertView = UIAlertView(title: "", message: "Please add your event name", delegate: nil, cancelButtonTitle: "OK");
            alert.show()
        }
        else if (eventLocTF.text?.trimmingCharacters(in: .whitespaces).isEmpty)!{
            let alert: UIAlertView = UIAlertView(title: "", message: "Please add your event location", delegate: nil, cancelButtonTitle: "OK");
            alert.show()
        }
        else if (userLocTF.text?.trimmingCharacters(in: .whitespaces).isEmpty)!{
            let alert: UIAlertView = UIAlertView(title: "", message: "Please add your location", delegate: nil, cancelButtonTitle: "OK");
            alert.show()
        }
        else if (eventDateTF.text?.trimmingCharacters(in: .whitespaces).isEmpty)!{
            let alert: UIAlertView = UIAlertView(title: "", message: "Please add your event date", delegate: nil, cancelButtonTitle: "OK");
            alert.show()
        }
        else if selectedServices.count == 0{
            let alert: UIAlertView = UIAlertView(title: "", message: "Please add your services", delegate: nil, cancelButtonTitle: "OK");
            alert.show()
        }
        else{
            postEvent()
        }
    }
    
    func getServiceArray(arr: NSArray)  {
        
        self.servicesLbl.text = String(arr.count) + " Services"
        var budget : Double = 0
        
        for i in 0 ..< arr.count{
            let dict = arr[i] as! NSDictionary
            let amount = dict["serviceCount"] as! String
            budget = budget + Double(amount)!
            
        }
        selectedServices = arr
        let prefs: UserDefaults? = UserDefaults.standard
        let currencySymbol = prefs?.string(forKey: "currencySymbol")
        let aa = Int(budget)
        self.totalBudgetValue = String(aa)
        self.budgetLbl.text = currencySymbol! + " " + String(budget.cleanValue)
    }
    
    func postEvent()  {
        
        DispatchQueue.main.async {
            self.view.showActivityView(withLabel: "Loading")
            let newDate = self.convertDateFormater(self.eventDateTF.text!)
            let parameters : [String: Any] = [
                "EventName": self.eventNameTF.text!,
                "EventDescription": "Publishing First Event",
                "EventBudget": self.totalBudgetValue,
                "UserLocation": self.userLocTF.text!,
                "EventLocation": self.eventLocTF.text!,
                "serviceCount": self.selectedServices,
                "Images": [],
                "Guest": "0",
                "EventDate": newDate,
                "UserId": userID
            ]
            
            let manager = Alamofire.SessionManager.default
            manager.session.configuration.timeoutIntervalForRequest = 120
            
            manager.request(baseURL + "api/Events/PostEvents", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
                
                switch(response.result) {
                case .success(_):
                    if response.result.value != nil{
                        //print(response.result.value as Any)
                    }
                    let message = "Thank you " + userFirstName + "."
                    let alert: UIAlertView = UIAlertView(title: message, message: "Your event has been added and all related service providers will be notified. You will be receiving bids soon.", delegate: nil, cancelButtonTitle: "OK");
                    alert.show()
                    self.navigationController?.popViewController(animated: true)
                    self.view.hideActivityView()
                    break
                    
                case .failure(_):
                    print(response.error!.localizedDescription)
                    let alert: UIAlertView = UIAlertView(title: "", message: response.error!.localizedDescription, delegate: nil, cancelButtonTitle: "OK");
                    alert.show()
                    self.view.hideActivityView()
                    print(response.result.error as Any)
                    break
                    
                }
            }
        }
    }
    
    func convertDateFormater(_ date: String) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        let date = dateFormatter.date(from: date)
        dateFormatter.dateFormat = "MM/dd/yyyy"
        return  dateFormatter.string(from: date!)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}

extension Double
{
    var cleanValue: String
    {
        return self.truncatingRemainder(dividingBy: 1) == 0 ? String(format: "%.0f", self) : String(self)
    }
}
