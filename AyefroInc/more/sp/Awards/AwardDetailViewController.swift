//
//  AwardDetailViewController.swift
//  AyefroInc
//
//  Created by Vijay Darkonde on 05/12/17.
//  Copyright © 2017 Inceptive Consulting Pvt. Ltd. All rights reserved.
//

import UIKit
import Alamofire
import SDWebImage
import MessageUI

class AwardDetailViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, emailSend , MFMailComposeViewControllerDelegate{
    
    @IBOutlet var firstView: UIView!
    @IBOutlet var userName: UILabel!
    @IBOutlet var eventName: UILabel!
   
    @IBOutlet var profilePicImg: UIImageView!
    @IBOutlet var tableView: UITableView!
    
    
    var categoryArr : [String] = []
    var budgetArr : [String] = []
    var serviceProviderID : String = ""
    
    var orderID : String = ""
    var popViewController : ContactPopUpViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        firstView.dropShadow()
        self.title = "Order Details"
        self.tableView.tableFooterView = UIView()
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        self.tabBarController?.tabBar.isHidden = true
        profilePicImg.layer.cornerRadius = profilePicImg.frame.height/2
        profilePicImg.layer.masksToBounds = true

        getAllAwardedDetail()
        self.userName.text = ""
        self.eventName.text = ""
    }
    
    func getAllAwardedDetail()  {
        DispatchQueue.main.async {
            let manager = Alamofire.SessionManager.default
            manager.session.configuration.timeoutIntervalForRequest = 120
            manager.request( baseURL + "api/Orders/GetServiceProviderOrderDetails/?orderId=" + self.orderID, method: .get, parameters: ["":""], encoding: URLEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
                
                switch(response.result) {
                case .success(_):
                    if response.result.value != nil{
                        //print(response.result.value as Any)
                    }
                    
                    let jsonResults : NSDictionary
                    jsonResults = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! NSDictionary
                    let orderDetails = jsonResults["orderDetails"] as! NSArray
                    
                    let orderServiceArray = jsonResults["orderServiceArray"] as! NSArray
                    
                    if orderDetails.count != 0{
                        let orderDetailDict = orderDetails[0] as! NSDictionary
                        let EventDescription = orderDetailDict["EventDescription"] as! String
                        let EventName = orderDetailDict["EventName"] as! String
                        let FirstName = orderDetailDict["FirstName"] as! String
                        let LastName = orderDetailDict["LastName"] as! String
                        
                        var image = ""
                        if (orderDetailDict["image"] as? String) != nil{
                            image = orderDetailDict["image"] as! String
                        }
                        
                        let Guest = orderDetailDict["Guest"] as! Int
                        self.serviceProviderID = orderDetailDict["userId"] as! String
                        
                        self.userName.text = FirstName + " " + LastName
                        self.eventName.text = "Event Name: " + EventName
                        
                        let url = URL(string: image)
                        let str: String? = Bundle.main.path(forResource: "loading_spinner", ofType: "gif")
                        let fileData = NSData(contentsOfFile: str!)
                        if url != nil{
                            self.profilePicImg.sd_setImage(with: url, placeholderImage: UIImage.sd_animatedGIF(with: fileData! as Data), options: SDWebImageOptions.allowInvalidSSLCertificates, progress: nil, completed: nil)
                        }
                        else{
                            self.profilePicImg.image = UIImage(named: "user")
                        }
                        
                        for i in 0 ..< orderServiceArray.count{
                            let data = orderServiceArray[i] as! NSDictionary
                            let ServiceName = data["ServiceName"] as! String
                            let cost = data["Cost"] as! Int
                            self.categoryArr.append(ServiceName)
                            self.budgetArr.append(String(cost))
                        }
                    }
                    else{
                        let alert: UIAlertView = UIAlertView(title: "", message: "No data available", delegate: nil, cancelButtonTitle: "OK");
                        alert.show()
                    }
                    
                    
                    
                    
                    
                    self.tableView.reloadData()
                    
                    break
                    
                case .failure(_):
                    if response.error?.localizedDescription == "The Internet connection appears to be offline."
                    {
                        let alert: UIAlertView = UIAlertView(title: "", message: response.error!.localizedDescription, delegate: nil, cancelButtonTitle: "OK");
                        alert.show()
                    }
                    else{
                        let alert: UIAlertView = UIAlertView(title: "", message: response.error!.localizedDescription, delegate: nil, cancelButtonTitle: "OK");
                        alert.show()
                        print(response.result.error as Any)
                    }
                    break
                    
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categoryArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as UITableViewCell
        cell = UITableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: "cell")
        let prefs: UserDefaults? = UserDefaults.standard
        let currencySymbol: String? = prefs?.string(forKey: "currencySymbol")
        let codedLabel:UILabel = UILabel()
        codedLabel.frame = CGRect(x: 20, y: 0, width: cell.frame.size.width - 100, height: cell.frame.size.height)
        codedLabel.textAlignment = .left
        codedLabel.text = categoryArr[indexPath.row]
        codedLabel.numberOfLines = 0
        codedLabel.textColor=UIColor.darkGray
        codedLabel.font=UIFont.systemFont(ofSize: 17)
        codedLabel.lineBreakMode = .byWordWrapping
        cell.contentView.addSubview(codedLabel)
        codedLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint(item: codedLabel, attribute: NSLayoutAttribute.centerY, relatedBy: NSLayoutRelation.equal, toItem: cell.contentView, attribute: NSLayoutAttribute.centerY, multiplier: 1, constant: 0).isActive = true
        NSLayoutConstraint(item: codedLabel, attribute: NSLayoutAttribute.leading, relatedBy: NSLayoutRelation.equal, toItem: cell.contentView, attribute: NSLayoutAttribute.leading, multiplier: 1, constant: 10).isActive = true
        NSLayoutConstraint(item: codedLabel, attribute: NSLayoutAttribute.trailing, relatedBy: NSLayoutRelation.equal, toItem: cell.detailTextLabel, attribute: NSLayoutAttribute.leading, multiplier: 1, constant: 10).isActive = true
//        cell.textLabel?.text = categoryArr[indexPath.row]
        cell.detailTextLabel?.text = budgetArr[indexPath.row] + " " + currencySymbol!
        return cell
    }
    
    @IBAction func getcontactBtnClk(_ sender: UIButton) {
        sender.isUserInteractionEnabled = false
        if self.serviceProviderID != ""{
            DispatchQueue.main.async {
                let manager = Alamofire.SessionManager.default
                manager.session.configuration.timeoutIntervalForRequest = 120
                manager.request( baseURL + "api/Albums/GetContactDetails/?providerId=" + self.serviceProviderID, method: .get, parameters: ["":""], encoding: URLEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
                    
                    switch(response.result) {
                    case .success(_):
                        if response.result.value != nil{
                            //print(response.result.value as Any)
                        }
                        
                        let jsonResults : NSArray
                        jsonResults = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! NSArray
                        if jsonResults.count != 0{
                            let dict = jsonResults[0] as! NSDictionary
                            let phoneNumber = dict["phoneNumber"] as! String
                            let email = dict["email"] as! String
                            
                            self.popViewController = self.storyboard?.instantiateViewController(withIdentifier: "ContactPopUpViewController")as! ContactPopUpViewController
                            self.popViewController.phoneNum = phoneNumber
                            self.popViewController.emailID = email
                            self.popViewController.delegate = self
                            self.popViewController.showInView(self.view, animated: true)
                        }
                        else{
                            let alert: UIAlertView = UIAlertView(title: "", message: "No data available", delegate: nil, cancelButtonTitle: "OK");
                            alert.show()
                        }
                        sender.isUserInteractionEnabled = true

                        break
                        
                    case .failure(_):
                        if response.error?.localizedDescription == "The Internet connection appears to be offline."
                        {
                            let alert: UIAlertView = UIAlertView(title: "", message: response.error!.localizedDescription, delegate: nil, cancelButtonTitle: "OK");
                            alert.show()
                        }
                        else{
                            let alert: UIAlertView = UIAlertView(title: "", message: response.error!.localizedDescription, delegate: nil, cancelButtonTitle: "OK");
                            alert.show()
                            print(response.result.error as Any)
                        }
                        break
                        
                    }
                }
            }
        }
        else{
            let alert: UIAlertView = UIAlertView(title: "", message: "No data available", delegate: nil, cancelButtonTitle: "OK");
            alert.show()
        }
    }
    
    func emailBox(email: String){
        if MFMailComposeViewController.canSendMail() {
            let toRecipients : [String] = [email]
            let mc: MFMailComposeViewController = MFMailComposeViewController()
            mc.mailComposeDelegate = self
            mc.setToRecipients(toRecipients)
            mc.isNavigationBarHidden = true
            self.present(mc, animated: false, completion: nil)
        }
        else
        {
            let alert: UIAlertView = UIAlertView(title: "", message: "Mail app not install on your device. Please send an email to info@ayefroinc.com.com", delegate: nil, cancelButtonTitle: "OK");
            alert.show()
        }
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        switch result.rawValue {
        case MFMailComposeResult.cancelled.rawValue:
            //NSLog("Mail Cancelled")
            self.dismiss(animated: false, completion: nil)
            break
        case MFMailComposeResult.saved.rawValue:
            //NSLog("Mail Saved")
            self.dismiss(animated: false, completion: nil)
            break
        case MFMailComposeResult.failed.rawValue:
            //NSLog("Mail Sent Fail", [error!.localizedDescription])
            self.dismiss(animated: false, completion: nil)
            break
        case MFMailComposeResult.sent.rawValue:
            //NSLog("Mail Sent")
            self.dismiss(animated: false, completion: nil)
            break
            
        default:
            break
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}
