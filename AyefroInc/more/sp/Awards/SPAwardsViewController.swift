//
//  SPAwardsViewController.swift
//  AyefroInc
//
//  Created by Vijay Darkonde on 24/11/17.
//  Copyright © 2017 Inceptive Consulting Pvt. Ltd. All rights reserved.
//

import UIKit
import Alamofire

class SPAwardsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    
    
    @IBOutlet var loader: UIImageView!
    @IBOutlet var errorView: UIView!
    @IBOutlet var noInternetView: UIView!
    @IBOutlet var retryBtn: UIButton!
    @IBOutlet var btn1: UIButton!
    @IBOutlet var btn2: UIButton!
    @IBOutlet var btn3: UIButton!
    @IBOutlet var btn4: UIButton!
    @IBOutlet weak var eventListTable: UITableView!
    
    var eventName : [String] = []
    var budget: [String] = []
    var orderIdArr : [String] = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "My Awards"
        self.tabBarController?.tabBar.isHidden = true
        
        let str: String? = Bundle.main.path(forResource: "loading_spinner", ofType: "gif")
        let fileData = NSData(contentsOfFile: str!)
        loader.image = UIImage.sd_animatedGIF(with: fileData! as Data)
        self.errorView.isHidden = true
        self.noInternetView.isHidden = true
        self.eventListTable.isHidden = true
        self.loader.isHidden = false
        self.retryBtn.layer.cornerRadius = 5
        
        btn1.layer.borderColor = UIColor.gray.cgColor
        btn1.layer.borderWidth = 0.5
        
        btn2.layer.borderColor = UIColor.gray.cgColor
        btn2.layer.borderWidth = 0.5
        
        btn3.layer.borderColor = UIColor.gray.cgColor
        btn3.layer.borderWidth = 0.5
        
        btn4.layer.borderColor = UIColor.gray.cgColor
        btn4.layer.borderWidth = 0.5
        
        getAllAwarded()
        self.eventListTable.tableFooterView = UIView()
        self.eventListTable.register(UITableViewCell.self, forCellReuseIdentifier: "Cell")
        self.eventListTable.dataSource = self
        self.eventListTable.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.setBackgroundImage(nil, for: .default)
        self.navigationController?.navigationBar.shadowImage = nil
    }
    
    
    func getAllAwarded()  {
        DispatchQueue.main.async {
            let manager = Alamofire.SessionManager.default
            manager.session.configuration.timeoutIntervalForRequest = 120
            manager.request( baseURL + "api/Orders/GetOrders/?providerID=" + userID, method: .get, parameters: ["":""], encoding: URLEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
                
                switch(response.result) {
                case .success(_):
                    if response.result.value != nil{
                        //print(response.result.value as Any)
                    }
                    
                    let jsonResults : NSArray
                    jsonResults = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! NSArray
                    
                    if jsonResults.count == 0{
                        self.errorView.isHidden = false
                        self.eventListTable.isHidden = true
                        self.noInternetView.isHidden = true
                        self.loader.isHidden = true
                    }
                    else{
                        self.errorView.isHidden = true
                        self.noInternetView.isHidden = true
                        self.eventListTable.isHidden = false
                        self.loader.isHidden = true
                        for i in 0 ..< jsonResults.count{
                            let data = jsonResults[i] as! NSDictionary
                            let eventName = data["eventName"] as! String
                            let cost = data["cost"] as! Int
                            let orderId = data["orderId"] as! Int
                            self.orderIdArr.append(String(orderId))
                            self.eventName.append(eventName)
                            self.budget.append(String(cost))
                        }
                        self.eventListTable.reloadData()
                    }
                    
                    break
                    
                case .failure(_):
                    if response.error?.localizedDescription == "The Internet connection appears to be offline."
                    {
                        self.errorView.isHidden = true
                        //                        self.imageCollection.isHidden = true
                        self.noInternetView.isHidden = false
                        self.loader.isHidden = true
                    }
                    else{
                        self.loader.isHidden = true
                        let alert: UIAlertView = UIAlertView(title: "", message: response.error!.localizedDescription, delegate: nil, cancelButtonTitle: "OK");
                        alert.show()
                        print(response.result.error as Any)
                    }
                    break
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return eventName.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "Cell"
        var cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier)
        if cell != nil {
            cell = UITableViewCell(style: .value1, reuseIdentifier: cellIdentifier)
        }
        cell?.accessoryType = UITableViewCellAccessoryType.disclosureIndicator
        cell?.selectionStyle = .none
        let prefs: UserDefaults? = UserDefaults.standard
        let currencySymbol: String? = prefs?.string(forKey: "currencySymbol")
        cell?.textLabel?.text = eventName[indexPath.row]
        cell?.detailTextLabel?.text = budget[indexPath.row] + " " + currencySymbol!
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let backItem = UIBarButtonItem()
        backItem.title = "Back"
        backItem.tintColor = UIColor.black
        navigationItem.backBarButtonItem = backItem
        let destinationVC = storyboard?.instantiateViewController(withIdentifier: "AwardDetailViewController") as! AwardDetailViewController
        destinationVC.orderID = self.orderIdArr[indexPath.row]
        self.navigationController?.pushViewController(destinationVC, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70.0
    }

    
    @IBAction func retryBtnClk(_ sender: Any) {
        getAllAwarded()
    }
    
    @IBAction func verifyProfileBtnClk(_ sender: UIButton) {
        let backItem = UIBarButtonItem()
        backItem.title = "Back"
        backItem.tintColor = UIColor.black
        navigationItem.backBarButtonItem = backItem
        let storyboard =  UIStoryboard(name: "Main", bundle: nil)
        let promo = storyboard.instantiateViewController(withIdentifier: "VerifyAccountViewController") as! VerifyAccountViewController
        self.navigationController?.pushViewController(promo, animated: true)
    }
    
    @IBAction func updateGalleryBtnClk(_ sender: UIButton) {
        let backItem = UIBarButtonItem()
        backItem.title = "Back"
        backItem.tintColor = UIColor.black
        navigationItem.backBarButtonItem = backItem
        let storyboard =  UIStoryboard(name: "Main", bundle: nil)
        let promo = storyboard.instantiateViewController(withIdentifier: "SPGalleryViewController") as! SPGalleryViewController
        self.navigationController?.pushViewController(promo, animated: true)
    }
    
    @IBAction func promoteBtnClk(_ sender: UIButton) {
        let backItem = UIBarButtonItem()
        backItem.title = "Back"
        backItem.tintColor = UIColor.black
        navigationItem.backBarButtonItem = backItem
        let storyboard =  UIStoryboard(name: "Main", bundle: nil)
        let promo = storyboard.instantiateViewController(withIdentifier: "PromotionController") as! PromotionController
        self.navigationController?.pushViewController(promo, animated: true)
    }
    
    @IBAction func eventBtnClk(_ sender: UIButton) {
        let backItem = UIBarButtonItem()
        backItem.title = "Back"
        backItem.tintColor = UIColor.black
        navigationItem.backBarButtonItem = backItem
        let storyboard =  UIStoryboard(name: "Main", bundle: nil)
        let promo = storyboard.instantiateViewController(withIdentifier: "EventsViewController") as! EventsViewController
        self.navigationController?.pushViewController(promo, animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
