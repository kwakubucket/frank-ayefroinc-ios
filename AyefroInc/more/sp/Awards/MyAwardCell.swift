//
//  MyAwardCell.swift
//  AyefroInc
//
//  Created by Vijay Darkonde on 26/12/17.
//  Copyright © 2017 Inceptive Consulting Pvt. Ltd. All rights reserved.
//

import UIKit

class MyAwardCell: UITableViewCell {
    
    @IBOutlet weak var budgetLabel: UILabel!
    @IBOutlet weak var eventNameLabel: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
