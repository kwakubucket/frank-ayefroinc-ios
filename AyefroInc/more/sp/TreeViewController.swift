//
//  TreeViewController.swift
//  RATreeViewExamples
//
//  Created by Rafal Augustyniak on 21/11/15.
//  Copyright © 2015 com.Augustyniak. All rights reserved.
//


import UIKit
import RATreeView
import SDWebImage
import Alamofire


class TreeViewController: UIViewController, RATreeViewDelegate, RATreeViewDataSource {

    @IBOutlet var dataView: UIView!
    @IBOutlet var profileImageView: UIImageView!
    @IBOutlet var nameLabel: UILabel!
    var treeView : RATreeView!
    var data : [DataObject]
    var editButton : UIBarButtonItem!
    
    let fvc = UIView()
    let lblView = UIView()
    let textLbl = UILabel()
    let icon = UIImageView()
    let closeicon = UIImageView()

    convenience init() {
        self.init(nibName : nil, bundle: nil)
    }

    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        data = TreeViewController.commonInit()
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }

    required init?(coder aDecoder: NSCoder) {
        data = TreeViewController.commonInit()
        super.init(coder: aDecoder)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .white
        setupTreeView()
        //overlays()
        _ = ModelManager.instance.getUserLoginStatus()
        let path = Bundle.main.path(forResource: "countryCode", ofType: "json")
        do{
            let data = try Data(contentsOf: URL(fileURLWithPath: path!), options: .mappedIfSafe)
            let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves) as! NSDictionary
            let countries = jsonResult.object(forKey: "countries") as! NSDictionary
            let country = countries.object(forKey: "country") as! NSArray
            for i in 0 ..< country.count
            {
                let countryObj = country[i] as! NSDictionary
                let countryName = countryObj.object(forKey: "countryName") as! String
                if Country == countryName{
                    let currencySymbol = countryObj.object(forKey: "currencySymbol") as! String
                    UserDefaults.standard.setValue(currencySymbol, forKey: "currencySymbol")
                    print("\(UserDefaults.standard.value(forKey: "currencySymbol")!)")
                    break
                }
            }
            
            
        }
        catch{
            
        }
        
        if eventNotification == true{
            if e_id != ""
            {
                
                let backItem = UIBarButtonItem()
                backItem.title = "Back"
                backItem.tintColor = UIColor.black
                self.navigationItem.backBarButtonItem = backItem
                let storyboard =  UIStoryboard(name: "Main", bundle: nil)
                let promo = storyboard.instantiateViewController(withIdentifier: "EventDetailsViewController") as! EventDetailsViewController
                promo.eventID = e_id
                promo.eventTitle = e_Name
                self.navigationController?.pushViewController(promo, animated: true)
                eventNotification = false
            }
            else{
                
            }
        }
        
        if imageUploadSelf == true{
            let backItem = UIBarButtonItem()
            backItem.title = "Back"
            backItem.tintColor = UIColor.black
            self.navigationItem.backBarButtonItem = backItem
            let storyboard =  UIStoryboard(name: "Main", bundle: nil)
            let promo = storyboard.instantiateViewController(withIdentifier: "SPGalleryViewController") as! SPGalleryViewController
            self.navigationController?.pushViewController(promo, animated: true)
            imageUploadSelf = false
        }
        
        if awardNotification == true{
            let backItem = UIBarButtonItem()
            backItem.title = "Back"
            backItem.tintColor = UIColor.black
            self.navigationItem.backBarButtonItem = backItem
            let storyboard =  UIStoryboard(name: "Main", bundle: nil)
            let destinationVC = storyboard.instantiateViewController(withIdentifier: "AwardDetailViewController") as! AwardDetailViewController
            destinationVC.orderID = ord_id
            self.navigationController?.pushViewController(destinationVC, animated: true)
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        _ = ModelManager.instance.getUserLoginStatus()
        let url = URL(string: image)
        let str: String? = Bundle.main.path(forResource: "loading_spinner", ofType: "gif")
        let fileData = NSData(contentsOfFile: str!)
        nameLabel.text = userFirstName
        if url != nil{
            self.profileImageView.sd_setImage(with: url, placeholderImage: UIImage.sd_animatedGIF(with: fileData! as Data), options: SDWebImageOptions.allowInvalidSSLCertificates, progress: nil, completed: nil)
        }
        else{
            self.profileImageView.image = UIImage(named: "user")
        }
        profileImageView.layer.cornerRadius = profileImageView.frame.height/2
        profileImageView.layer.masksToBounds = true
        getAllTopic()
        getAllLocation()
        self.navigationController?.navigationBar.setBackgroundImage(nil, for: .default)
        self.navigationController?.navigationBar.shadowImage = nil
        self.tabBarController?.tabBar.isHidden = false
        UIApplication.shared.statusBarView?.backgroundColor = UIColor(red: 247.0/255.0, green: 247.0/255.0, blue: 247.0/255.0, alpha: 1.0)

    }

    func setupTreeView() -> Void {
        treeView = RATreeView(frame: CGRect(x: 0, y: self.dataView.frame.height + (self.navigationController?.navigationBar.frame.height)!, width: self.view.frame.size.width, height: self.view.frame.size.height - ((self.tabBarController?.tabBar.frame.size.height)! + self.dataView.frame.height)))
        treeView.register(UINib(nibName: String(describing: TreeTableViewCell.self), bundle: nil), forCellReuseIdentifier: String(describing: TreeTableViewCell.self))
        treeView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        treeView.delegate = self;
        treeView.dataSource = self;
        
        treeView.treeFooterView = UIView()
        treeView.backgroundColor = .white
        
        view.addSubview(treeView)
    }

    //MARK: RATreeView data source

    @IBAction func profileBtnClk(_ sender: Any) {
        let backItem = UIBarButtonItem()
        backItem.title = "Back"
        backItem.tintColor = UIColor.black
        navigationItem.backBarButtonItem = backItem
        let storyboard =  UIStoryboard(name: "Main", bundle: nil)
        let promo = storyboard.instantiateViewController(withIdentifier: "SPProfileViewController") as! SPProfileViewController
        self.navigationController?.pushViewController(promo, animated: true)
    }
    
    func overlays() {
        
        let showValue = UserDefaults.standard.value(forKey: "MoreOverlay") as! String
        if showValue == "true" {
            fvc.frame = CGRect(x: self.view.frame.origin.x, y: self.view.frame.origin.y, width: self.view.frame.size.width, height: self.view.frame.size.height)
            fvc.backgroundColor = UIColor(red:0.0/255.0, green:0.0/255.0, blue:0.0/255.0, alpha:0.2)
            
            
            lblView.frame = CGRect(x:self.view.frame.origin.x + 20, y: self.dataView.frame.size.height, width: self.view.frame.size.width - 40, height: 40)
            lblView.backgroundColor = UIColor(red:98.0/255.0, green:151.0/255.0, blue:207.0/255.0, alpha: 1.0)
            lblView.layer.cornerRadius = 15
            
            
            textLbl.text = "Find all upcoming events here"
            textLbl.numberOfLines = 0
            textLbl.lineBreakMode = .byWordWrapping
            textLbl.font = UIFont(name:"HelveticaNeue-Bold", size: 17.0)!
            textLbl.layer.cornerRadius = 15
            textLbl.backgroundColor = UIColor(red:98.0/255.0, green:151.0/255.0, blue:207.0/255.0, alpha: 1.0)
            textLbl.textColor = UIColor.white
            textLbl.frame = CGRect(x: lblView.frame.origin.x , y: 0, width: lblView.frame.size.width - 40, height: 40)
            lblView.addSubview(textLbl)
            
            icon.image = UIImage(named: "drr")
            icon.frame = CGRect(x: (self.view.frame.size.width / 2) - 30, y: self.dataView.frame.size.height + 10 , width: 30, height: 30)
            fvc.addSubview(icon)
            
            closeicon.image = UIImage(named: "closeb")
            closeicon.frame = CGRect(x: lblView.frame.size.width - 30, y: 5, width: 20, height: 20)
            lblView.addSubview(closeicon)
            
            fvc.addSubview(lblView)
            self.tabBarController?.view.addSubview(fvc)
            
            let tap1 = UITapGestureRecognizer(target: self, action: #selector(self.tapOnProfilePic))
            fvc.isUserInteractionEnabled = true
            fvc.addGestureRecognizer(tap1)
            
            let tap2 = UITapGestureRecognizer(target: self, action: #selector(self.tapOnProfilePic))
            lblView.isUserInteractionEnabled = true
            lblView.addGestureRecognizer(tap2)
            
            let tap3 = UITapGestureRecognizer(target: self, action: #selector(self.tapOnProfilePic))
            closeicon.isUserInteractionEnabled = true
            closeicon.addGestureRecognizer(tap3)
            
        }
        
    }
    
    @objc func tapOnProfilePic(){
        fvc.removeFromSuperview()
//        UserDefaults.standard.setValue("false", forKey: "DashboardOverlay")
    }
    
    
    func treeView(_ treeView: RATreeView, numberOfChildrenOfItem item: Any?) -> Int {
        if let item = item as? DataObject {
            return item.children.count
        } else {
            return self.data.count
        }
    }

    func treeView(_ treeView: RATreeView, child index: Int, ofItem item: Any?) -> Any {
        if let item = item as? DataObject {
            return item.children[index]
        } else {
            return data[index] as AnyObject
        }
    }

    func treeView(_ treeView: RATreeView, cellForItem item: Any?) -> UITableViewCell {
        let cell = treeView.dequeueReusableCell(withIdentifier: String(describing: TreeTableViewCell.self)) as! TreeTableViewCell
        let item = item as! DataObject

        let level = treeView.levelForCell(forItem: item)
        cell.selectionStyle = .none
        cell.setup(withTitle: item.name, level: level,imageName: item.imageName,childCount: item.children.count, additionalButtonHidden: false)
        
        
        return cell
    }
    
    func treeView(_ treeView: RATreeView, editingStyleForRowForItem item: Any) -> UITableViewCellEditingStyle {
        return UITableViewCellEditingStyle.none
    }

    //MARK: RATreeView delegate
    func treeView(_ treeView: RATreeView, didSelectRowForItem item: Any) {
        let item = item as! DataObject
        let backItem = UIBarButtonItem()
        backItem.title = "Back"
        backItem.tintColor = UIColor.black
        navigationItem.backBarButtonItem = backItem
        
        print(item.name)
        if (item.name == "Events"){
            let storyboard =  UIStoryboard(name: "Main", bundle: nil)
            let promo = storyboard.instantiateViewController(withIdentifier: "EventsViewController") as! EventsViewController
            self.navigationController?.pushViewController(promo, animated: true)
        }
        if (item.name == "My Profile"){
            let storyboard =  UIStoryboard(name: "Main", bundle: nil)
            let promo = storyboard.instantiateViewController(withIdentifier: "SPProfileViewController") as! SPProfileViewController
            self.navigationController?.pushViewController(promo, animated: true)
        }
        if (item.name == "My Gallery"){
            let storyboard =  UIStoryboard(name: "Main", bundle: nil)
            let promo = storyboard.instantiateViewController(withIdentifier: "SPGalleryViewController") as! SPGalleryViewController
            self.navigationController?.pushViewController(promo, animated: true)
        }
        if (item.name == "Promote Your Business"){
            let storyboard =  UIStoryboard(name: "Main", bundle: nil)
            let promo = storyboard.instantiateViewController(withIdentifier: "PromotionController") as! PromotionController
            self.navigationController?.pushViewController(promo, animated: true)
        }
        if (item.name == "My Awards"){
            let storyboard =  UIStoryboard(name: "Main", bundle: nil)
            let promo = storyboard.instantiateViewController(withIdentifier: "SPAwardsViewController") as! SPAwardsViewController
            self.navigationController?.pushViewController(promo, animated: true)
        }
        if (item.name == "Notifications"){
            UIApplication.shared.openURL(NSURL(string:UIApplicationOpenSettingsURLString)! as URL)
        }
        if (item.name == "About Us"){
            let storyboard =  UIStoryboard(name: "Main", bundle: nil)
            let promo = storyboard.instantiateViewController(withIdentifier: "AboutUsViewController") as! AboutUsViewController
            self.navigationController?.pushViewController(promo, animated: true)
        }
        if (item.name == "Refer and Redeem"){
            let storyboard =  UIStoryboard(name: "Main", bundle: nil)
            let promo = storyboard.instantiateViewController(withIdentifier: "ReferEarnViewController") as! ReferEarnViewController
            self.navigationController?.pushViewController(promo, animated: true)
        }
        if (item.name == "Privacy Policy"){
            let storyboard =  UIStoryboard(name: "Main", bundle: nil)
            let promo = storyboard.instantiateViewController(withIdentifier: "PrivacyPolicyViewController") as! PrivacyPolicyViewController
            self.navigationController?.pushViewController(promo, animated: true)
        }
        if (item.name == "Terms & Conditions"){
            let storyboard =  UIStoryboard(name: "Main", bundle: nil)
            let promo = storyboard.instantiateViewController(withIdentifier: "TermsAndConditionViewController") as! TermsAndConditionViewController
            self.navigationController?.pushViewController(promo, animated: true)
        }
        if (item.name == "Blog"){
            let storyboard =  UIStoryboard(name: "Main", bundle: nil)
            let promo = storyboard.instantiateViewController(withIdentifier: "BlogViewController") as! BlogViewController
            promo.blogURL = "http://www.ayefroinc.com/blog"
            self.navigationController?.pushViewController(promo, animated: true)
        }
        if (item.name == "Join the Discussion"){
            self.tabBarController?.selectedIndex = 2
        }
        if (item.name == "Verify Account"){
            let storyboard =  UIStoryboard(name: "Main", bundle: nil)
            let promo = storyboard.instantiateViewController(withIdentifier: "VerifyAccountViewController") as! VerifyAccountViewController
            self.navigationController?.pushViewController(promo, animated: true)
        }
        if (item.name == "Contact Us"){
            let storyboard =  UIStoryboard(name: "Main", bundle: nil)
            let promo = storyboard.instantiateViewController(withIdentifier: "ContactUsViewController") as! ContactUsViewController
            self.navigationController?.pushViewController(promo, animated: true)
        }
        if (item.name == "Share"){
            let textToShare = "Download Ayefro Inc App from Play Store: https://play.google.com/store/apps/details?id=com.ayefroinc"

            if let myWebsite = NSURL(string: "https://play.google.com/store/apps/details?id=com.ayefroinc") {
                let objectsToShare = [textToShare, myWebsite] as [Any] as [Any]
                let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
                activityVC.popoverPresentationController?.sourceView = treeView
                self.present(activityVC, animated: true, completion: nil)
            }
        }
        
    }
    
    func getAllTopic()  {
        servicesArr = []
        servicesIDArr = []
        DispatchQueue.main.async {
            let manager = Alamofire.SessionManager.default
            manager.session.configuration.timeoutIntervalForRequest = 120
            
            manager.request( baseURL + "api/Events/GetAllServices", method: .get, parameters: ["":""], encoding: URLEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
                
                switch(response.result) {
                case .success(_):
                    if response.result.value != nil{
                        //print(response.result.value as Any)
                    }
                    let jsonResults : NSArray
                    jsonResults = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! NSArray
                    for i in 0 ..< jsonResults.count{
                        let data = jsonResults[i] as! NSDictionary
                        let categoryID = data["id"] as! Int
                        let CategoryTitle = data["serviceName"] as! String
                        let Active = data["active"] as! Bool
                        if Active == true{
                            servicesArr.append(CategoryTitle)
                            servicesIDArr.append(String(categoryID))
                        }
                    }
                    
                    break
                    
                case .failure(_):
                    print(response.error!.localizedDescription)
                    let alert: UIAlertView = UIAlertView(title: "", message: response.error!.localizedDescription, delegate: nil, cancelButtonTitle: "OK");
                    alert.show()
                    self.view.hideActivityView()
                    print(response.result.error as Any)
                    break
                    
                }
            }
        }
    }
    
    func getAllLocation()  {
        locationArr = []
        DispatchQueue.main.async {
            let manager = Alamofire.SessionManager.default
            manager.session.configuration.timeoutIntervalForRequest = 120
            
            manager.request( baseURL + "api/Events/GetUserlocations/?userid=" + userID, method: .get, parameters: ["":""], encoding: URLEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
                
                switch(response.result) {
                case .success(_):
                    if response.result.value != nil{
                        //print(response.result.value as Any)
                    }
                    let jsonResults : NSDictionary
                    jsonResults = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! NSDictionary
                    let data = jsonResults["Data"] as! NSArray
                    for i in 0 ..< data.count{
                        if (data[i] as? String) != nil
                        {
                            locationArr.append(data[i] as! String)
                        }
                    }
                    
                    break
                    
                case .failure(_):
                    print(response.error!.localizedDescription)
                    let alert: UIAlertView = UIAlertView(title: "", message: response.error!.localizedDescription, delegate: nil, cancelButtonTitle: "OK");
                    alert.show()
                    self.view.hideActivityView()
                    print(response.result.error as Any)
                    break
                    
                }
            }
        }
    }
}


private extension TreeViewController {

    static func commonInit() -> [DataObject] {
        
        
        let Events = DataObject(name: "Events", imageName: "list")
        let award = DataObject(name: "My Awards", imageName: "history")
        var My_Event_Center = DataObject(name: "My Event Center                                  \u{25BC}", imageName: "calendar", children:[ Events, award])
        if UIScreen.main.bounds.size.width == 414
        {
            My_Event_Center = DataObject(name: "My Event Center                                             \u{25BC}", imageName: "calendar", children:[ Events, award])
        }
        else if UIScreen.main.bounds.size.width == 375 && UIScreen.main.bounds.size.height == 667
        {
            My_Event_Center = DataObject(name: "My Event Center                                  \u{25BC}", imageName: "calendar", children:[ Events, award])
        }
        else if UIScreen.main.bounds.size.width == 320 && UIScreen.main.bounds.size.height == 568
        {
            My_Event_Center = DataObject(name: "My Event Center                    \u{25BC}", imageName: "calendar", children: [ Events, award])
        }
        else if UIScreen.main.bounds.size.width == 320 && UIScreen.main.bounds.size.height == 480
        {
            My_Event_Center = DataObject(name: "My Event Center                    \u{25BC}", imageName: "calendar", children: [ Events, award])
        }
        
        let profile = DataObject(name: "My Profile", imageName: "myprofile")
        let gallery = DataObject(name: "My Gallery", imageName: "gallery")
        let paytouse = DataObject(name: "Promote Your Business", imageName: "paytouse")
        let notification = DataObject(name: "Notifications", imageName: "notifications")
        var Setting = DataObject(name: "Settings                                                \u{25BC}", imageName: "settings", children: [ profile, gallery, paytouse, notification])
        if UIScreen.main.bounds.size.width == 414
        {
            Setting = DataObject(name: "Settings                                                            \u{25BC}", imageName: "settings", children: [ profile, gallery, paytouse, notification])
        }
        else if UIScreen.main.bounds.size.width == 375 && UIScreen.main.bounds.size.height == 667
        {
            Setting = DataObject(name: "Settings                                                \u{25BC}", imageName: "settings", children: [ profile, gallery, paytouse, notification])
        }
        else if UIScreen.main.bounds.size.width == 320 && UIScreen.main.bounds.size.height == 568
        {
            Setting = DataObject(name: "Settings                                  \u{25BC}", imageName: "settings", children: [ profile, gallery, paytouse, notification])
        }
        else if UIScreen.main.bounds.size.width == 320 && UIScreen.main.bounds.size.height == 480
        {
            Setting = DataObject(name: "Settings                                  \u{25BC}", imageName: "settings", children: [ profile, gallery, paytouse, notification])
        }
        let refer = DataObject(name: "Refer and Redeem", imageName: "referearn")
        let about = DataObject(name: "About Us", imageName: "aboutus")
        let pp = DataObject(name: "Privacy Policy", imageName: "policy")
        let tc = DataObject(name: "Terms & Conditions", imageName: "terms")
        var AboutUs = DataObject(name: "About Us                                              \u{25BC}", imageName: "information",  children: [ about, pp, tc])
        if UIScreen.main.bounds.size.width == 414
        {
            AboutUs = DataObject(name: "About Us                                                          \u{25BC}", imageName: "information",  children: [ about, pp, tc])
        }
        else if UIScreen.main.bounds.size.width == 375 && UIScreen.main.bounds.size.height == 667
        {
            AboutUs = DataObject(name: "About Us                                              \u{25BC}", imageName: "information",  children: [ about, pp, tc])
        }
        else if UIScreen.main.bounds.size.width == 320 && UIScreen.main.bounds.size.height == 568
        {
            AboutUs = DataObject(name: "About Us                                \u{25BC}", imageName: "information",  children: [ about, pp, tc])
        }
        else if UIScreen.main.bounds.size.width == 320 && UIScreen.main.bounds.size.height == 480
        {
            AboutUs = DataObject(name: "About Us                                \u{25BC}", imageName: "information",  children: [ about, pp, tc])
        }
        
        let Blog = DataObject(name: "Blog", imageName: "blog")
        let discussion = DataObject(name: "Join the Discussion", imageName: "forum")
        let verifyAccount = DataObject(name: "Verify Account", imageName: "verify")
        let contactUs = DataObject(name: "Contact Us", imageName: "contactus")
        let Share = DataObject(name: "Share", imageName: "share")
        
        

        return [My_Event_Center, Setting, refer, AboutUs, Blog, discussion, verifyAccount, contactUs, Share]
    }

}

