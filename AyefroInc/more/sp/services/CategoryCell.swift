//
//  CategoryCell.swift
//  AyefroInc
//
//  Created by Vijay Darkonde on 05/12/17.
//  Copyright © 2017 Inceptive Consulting Pvt. Ltd. All rights reserved.
//

import UIKit

class CategoryCell: UITableViewCell {
    
    
    @IBOutlet var titleLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
