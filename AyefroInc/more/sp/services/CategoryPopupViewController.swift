//
//  CategoryPopupViewController.swift
//  AyefroInc
//
//  Created by Vijay Darkonde on 01/12/17.
//  Copyright © 2017 Inceptive Consulting Pvt. Ltd. All rights reserved.
//

import UIKit

class CategoryPopupViewController: UIViewController, UITableViewDelegate, UITableViewDataSource{
    
    @IBOutlet var tableView: UITableView!
    
    var delagate: addServices!
    var categoryArr : [String] = []
    var categoryIDArr : [String] = []
    var selectedCategory : [String] = []
    
    var loaded : Bool = false
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    override init(nibName nibNameOrNil: String!, bundle nibBundleOrNil: Bundle!) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.categoryArr = servicesArr
        self.categoryIDArr = servicesIDArr
        
        self.title = "Select Services"
        
        self.tableView.tableFooterView = UIView()
        self.tableView.setEditing(true, animated: true)
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "Cell")
        
        for i in 0 ..< selectedCategory.count{
            if let indexof = categoryIDArr.index(of: selectedCategory[i]) {
                let indexPath = IndexPath(row: indexof, section: 0)
                tableView.selectRow(at: indexPath, animated: true, scrollPosition: .middle)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categoryArr.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as UITableViewCell
        cell.textLabel?.text = categoryArr[indexPath.row]
        cell.textLabel?.lineBreakMode = .byWordWrapping
        cell.textLabel?.numberOfLines = 0
        cell.textLabel?.backgroundColor = UIColor.clear
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedCategory.append(categoryIDArr[indexPath.row])
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        
        print("deselected  \(categoryArr[indexPath.row])")
        categoryIDArr = categoryIDArr.filter{$0 != categoryArr[indexPath.row]}
        let serviceid = categoryIDArr[indexPath.row]
        if let indexof = selectedCategory.index(of: serviceid) {
            selectedCategory.remove(at: indexof)
        }
    }
    

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let cellText = categoryArr[indexPath.row]
        let cellFont: UIFont? = UIFont(name:"HelveticaNeue-Bold", size: 17.0)!
        let attributedText = NSAttributedString(string: cellText, attributes: [NSAttributedStringKey.font: cellFont as Any])
        let rect: CGRect = attributedText.boundingRect(with: CGSize(width: tableView.bounds.size.width, height: CGFloat.greatestFiniteMagnitude), options: .usesLineFragmentOrigin, context: nil)
        return rect.size.height + 20
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
        return unsafeBitCast(3, to: UITableViewCellEditingStyle.self)
    }
    
    @IBAction func saveBtnClk(_ sender: AnyObject) {
        print(selectedCategory)
        delagate.addService(services: String(selectedCategory.count), idArr: selectedCategory)
        self.navigationController?.popViewController(animated: true)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}


