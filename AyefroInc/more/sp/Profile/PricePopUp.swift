//
//  PricePopUp.swift
//  AyefroInc
//
//  Created by Vijay Darkonde on 06/12/17.
//  Copyright © 2017 Inceptive Consulting Pvt. Ltd. All rights reserved.
//

import UIKit

class PricePopUp: UIViewController {
    
    @IBOutlet var popUpView: UIView!
    @IBOutlet var innerView: UIView!
    @IBOutlet var currencyLbl: UILabel!
    @IBOutlet var amountPrice: UITextField!
    
    var delegate: MinimunPrice!
    var minPrice: String = ""
    
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    override init(nibName nibNameOrNil: String!, bundle nibBundleOrNil: Bundle!) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        self.popUpView.layer.cornerRadius = 5
        self.popUpView.layer.shadowOpacity = 0.0
        self.popUpView.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        
        innerView.layer.borderColor = UIColor.gray.cgColor
        innerView.layer.borderWidth = 0.5
        
        amountPrice.text = minPrice

        let prefs: UserDefaults? = UserDefaults.standard
        let currencySymbol: String? = prefs?.string(forKey: "currencySymbol")
        currencyLbl.text = currencySymbol
    }
    
    func showInView(_ aView: UIView!, animated: Bool)
    {
        aView.addSubview(self.view)
        if animated
        {
            self.showAnimate()
        }
    }
    
    func showAnimate()
    {
        self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view.alpha = 0.0;
        UIView.animate(withDuration: 0.25, animations: {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        });
    }
    
    func removeAnimate()
    {
        UIView.animate(withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view.alpha = 0.0;
        }, completion:{(finished : Bool)  in
            if (finished)
            {
                self.view.removeFromSuperview()
            }
        });
    }
    
    @IBAction func backBtnClk(_ sender: AnyObject) {
        removeAnimate()
    }
    
    @IBAction func saveBtnClk(_ sender: UIButton) {
        if (amountPrice.text?.trimmingCharacters(in: .whitespaces).isEmpty)!{
            let alert: UIAlertView = UIAlertView(title: "", message: "Please Enter Amount", delegate: nil, cancelButtonTitle: "OK");
            alert.show()
        }
        else{
            self.delegate.minPrice(price: amountPrice.text!)
            self.removeAnimate()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}


