//
//  URLPopUp.swift
//  AyefroInc
//
//  Created by Vijay Darkonde on 06/12/17.
//  Copyright © 2017 Inceptive Consulting Pvt. Ltd. All rights reserved.
//

import UIKit

class URLPopUp: UIViewController {
    
    @IBOutlet var popUpView: UIView!
    @IBOutlet var fbURLTF: UITextField!
    @IBOutlet var instagramURLTF: UITextField!
    @IBOutlet var twitterURLTF: UITextField!
    @IBOutlet var websiteURLTF: UITextField!
    
    var delegate: URLLinks!
    var fbURl: String = ""
    var instaURl: String = ""
    var twitterURl: String = ""
    var webURl: String = ""

    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    override init(nibName nibNameOrNil: String!, bundle nibBundleOrNil: Bundle!) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        self.popUpView.layer.cornerRadius = 5
        self.popUpView.layer.shadowOpacity = 0.0
        self.popUpView.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        
        fbURLTF.text = fbURl
        instagramURLTF.text = instaURl
        twitterURLTF.text = twitterURl
        websiteURLTF.text = webURl

    }
    
    func showInView(_ aView: UIView!, animated: Bool)
    {
        aView.addSubview(self.view)
        if animated
        {
            self.showAnimate()
        }
    }
    
    func showAnimate()
    {
        self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view.alpha = 0.0;
        UIView.animate(withDuration: 0.25, animations: {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        });
    }
    
    func removeAnimate()
    {
        UIView.animate(withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view.alpha = 0.0;
        }, completion:{(finished : Bool)  in
            if (finished)
            {
                self.view.removeFromSuperview()
            }
        });
    }
    
    @IBAction func backBtnClk(_ sender: AnyObject) {
        removeAnimate()
    }
    
    @IBAction func saveBtnClk(_ sender: UIButton) {
        if fbURLTF.text != "" && validateUrl(urlString: fbURLTF.text! as NSString) == false{
            let alert: UIAlertView = UIAlertView(title: "", message: "Please Enter Valid Facebook URL.", delegate: nil, cancelButtonTitle: "OK");
            alert.show()
        }
        else if instagramURLTF.text != "" && validateUrl(urlString: instagramURLTF.text! as NSString) == false{
            let alert: UIAlertView = UIAlertView(title: "", message: "Please Enter Valid Instagram URL.", delegate: nil, cancelButtonTitle: "OK");
            alert.show()
        }
        else if twitterURLTF.text != "" && validateUrl(urlString: twitterURLTF.text! as NSString) == false{
            let alert: UIAlertView = UIAlertView(title: "", message: "Please Enter Valid Twitter URL.", delegate: nil, cancelButtonTitle: "OK");
            alert.show()
        }
        else if websiteURLTF.text != "" && validateUrl(urlString: websiteURLTF.text! as NSString) == false{
            let alert: UIAlertView = UIAlertView(title: "", message: "Please Enter Valid Website URL.", delegate: nil, cancelButtonTitle: "OK");
            alert.show()
        }
        else{
            delegate.urls(fb: fbURLTF.text!, insta: instagramURLTF.text!, twitter: twitterURLTF.text!, website: websiteURLTF.text!)
            removeAnimate()
        }
    }
    
    func validateUrl (urlString: NSString) -> Bool {
        let urlRegEx = "((?:http|https)://)?(?:www\\.)?[\\w\\d\\-_]+\\.\\w{2,3}(\\.\\w{2})?(/(?<=/)(?:[\\w\\d\\-./_]+)?)?"
        return NSPredicate(format: "SELF MATCHES %@", urlRegEx).evaluate(with: urlString)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}


