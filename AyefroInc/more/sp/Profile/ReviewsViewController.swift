//
//  ReviewsViewController.swift
//  AyefroInc
//
//  Created by Vijay Darkonde on 06/12/17.
//  Copyright © 2017 Inceptive Consulting Pvt. Ltd. All rights reserved.
//

import UIKit
import SDWebImage
import Alamofire


class ReviewsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet var tableView: UITableView!
    @IBOutlet var errorView: UIView!
    
    var dataArr : NSArray!
    var redirectioValue : String! = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.tableFooterView = UIView()
        self.title = "Reviews"
        self.tableView.register(UINib.init(nibName: "ReviewCell", bundle: nil), forCellReuseIdentifier: "ReviewCell")
        
        if dataArr.count != 0{
            self.tableView.isHidden =  false
            self.errorView.isHidden = true
        }
        else{
            self.errorView.isHidden = false
            self.tableView.isHidden =  true
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if dataArr.count != 0{
            return dataArr.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ReviewCell", for: indexPath) as! ReviewCell

        let dict = dataArr[indexPath.row] as! NSDictionary
        var fName = ""
        if redirectioValue == "User"{
            fName = dict["FirstName"] as! String
        }
        else{
            fName = dict["FirstNme"] as! String
        }
        if dict["Verified"] as! Bool {
            cell.verifiedEventImage.isHidden = false
            cell.nameLblTrailingConstraint.constant = 90
        } else {
            cell.verifiedEventImage.isHidden = true
            cell.nameLblTrailingConstraint.constant = 0
        }
        let lName = dict["LastName"] as! String
        let descrition = dict["Review"] as! String
        let rating = dict["Ratings"] as! Double
        let created_at = dict["created_at"] as! String
        var pic = ""
        if (dict["UserImage"] as? String) != nil{
            pic = dict["UserImage"] as! String
        }
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
        let date = formatter.date(from: created_at)
        formatter.dateFormat = "yyyy/dd/MM"
        let strDate = formatter.string(from: date!)
        cell.nameLbl.text = fName + " " + lName
        cell.descriptionLbl.text = descrition
        cell.descriptionLbl.numberOfLines = 0
        cell.descriptionLbl.lineBreakMode = .byWordWrapping
        cell.ratingLbl.text = String(rating)
        cell.dateLbl.text = strDate
        let url = URL(string: pic)
        let str: String? = Bundle.main.path(forResource: "loading_spinner", ofType: "gif")
        let fileData = NSData(contentsOfFile: str!)
        if url != nil{
            cell.profilePic.sd_setImage(with: url, placeholderImage: UIImage.sd_animatedGIF(with: fileData! as Data), options: SDWebImageOptions.allowInvalidSSLCertificates, progress: nil, completed: nil)
        }
        else{
            cell.profilePic.image = UIImage(named: "user")
        }
        cell.profilePic.layer.cornerRadius = cell.profilePic.frame.height/2
        cell.profilePic.layer.masksToBounds = true
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let dict = dataArr[indexPath.row] as! NSDictionary
        var fName = ""
        if redirectioValue == "User"{
            fName = dict["FirstName"] as! String
        }
        else{
            fName = dict["FirstNme"] as! String
        }
        let descrition = dict["Review"] as! String

        let f_Name = fName
        let cellFont: UIFont? = UIFont(name:"HelveticaNeue-Bold", size: 18)!
        let attributedText = NSAttributedString(string: f_Name, attributes: [NSAttributedStringKey.font: cellFont as Any])
        let fNamerect: CGRect = attributedText.boundingRect(with: CGSize(width: tableView.bounds.size.width, height: CGFloat.greatestFiniteMagnitude), options: .usesLineFragmentOrigin, context: nil)

        let descritionTxt = descrition
        let descritionTxtattributedText = NSAttributedString(string: descritionTxt, attributes: [NSAttributedStringKey.font: cellFont as Any])
        let descritionTxtrect: CGRect = descritionTxtattributedText.boundingRect(with: CGSize(width: tableView.bounds.size.width, height: CGFloat.greatestFiniteMagnitude), options: .usesLineFragmentOrigin, context: nil)
        let he = fNamerect.size.height +  descritionTxtrect.size.height + fNamerect.size.height + fNamerect.size.height + 45
        
        
        return he
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
   

}
