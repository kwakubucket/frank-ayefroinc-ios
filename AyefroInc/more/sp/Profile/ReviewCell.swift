//
//  ReviewCell.swift
//  AyefroInc
//
//  Created by Vijay Darkonde on 07/12/17.
//  Copyright © 2017 Inceptive Consulting Pvt. Ltd. All rights reserved.
//

import UIKit

class ReviewCell: UITableViewCell {
    
    @IBOutlet var cardView: UIView!
    @IBOutlet var profilePic: UIImageView!
    @IBOutlet var nameLbl: UILabel!
    @IBOutlet var dateLbl: UILabel!
    @IBOutlet var descriptionLbl: UILabel!
    @IBOutlet var ratingLbl: UILabel!
    @IBOutlet var verifiedEventImage: UIImageView!
    @IBOutlet var nameLblTrailingConstraint: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
