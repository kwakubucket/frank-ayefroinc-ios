//
//  SPProfileViewController.swift
//  AyefroInc
//
//  Created by Vijay Darkonde on 24/11/17.
//  Copyright © 2017 Inceptive Consulting Pvt. Ltd. All rights reserved.
//

import UIKit
import SDWebImage
import Alamofire

protocol CompanyDescription {
    func companyInfo(txt: String)
}
protocol MinimunPrice {
    func minPrice(price: String)
}
protocol URLLinks {
    func urls(fb: String, insta: String, twitter: String, website: String)
}


class SPProfileViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITableViewDelegate, UITableViewDataSource, addServices, CompanyDescription, MinimunPrice, URLLinks {
    
    @IBOutlet var rateLbl: UILabel!
    @IBOutlet var profilePic: UIImageView!
    @IBOutlet var firstNameTF: NiceTextField!
    @IBOutlet var lastNameTF: NiceTextField!
    @IBOutlet var PhoneNumTF: NiceTextField!
    @IBOutlet var EmailTF: NiceTextField!
    @IBOutlet var companyNameTF: NiceTextField!
    @IBOutlet var cityNameTF: NiceTextField!
    @IBOutlet var tableView: UITableView!
    @IBOutlet var uploadBtn: UIButton!
    @IBOutlet weak var verifiedImg: UIImageView!
    
    var base64Str : String! = ""
    let imagePicker = UIImagePickerController()
    var menuArr : [String] = ["What services do you provide?", "Describe your company", "Starting price", "Social media and Website", "Reviews"]
    var popViewController : CategoryPopupViewController!
    var popViewController1 : CompanyDetailPopup!
    var popViewController2 : PricePopUp!
    var popViewController3 : URLPopUp!
    var CompanyDescription = ""
    var FacebookUrl = ""
    var InstagramUrl = ""
    var TwitterUrl = ""
    var Website = ""
    var StartingPrice = ""
    var Verified = false
    var reviewArr : NSArray!
    var selectedCategory: [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "My Profile"
        self.tabBarController?.tabBar.isHidden = true
        
        imagePicker.delegate = self
        
        profilePic.layer.cornerRadius = profilePic.frame.size.height/2
        profilePic.layer.masksToBounds = true
        let prefs: UserDefaults? = UserDefaults.standard
        let PhoneNumber: String? = prefs?.string(forKey: "PhoneNumber")
        
        _ = ModelManager.instance.getUserLoginStatus()
        firstNameTF.text = userFirstName
        lastNameTF.text = userLastName
        PhoneNumTF.text = PhoneNumber
        EmailTF.text = userEmail
        companyNameTF.text = companyName
        cityNameTF.text = companyAddress
        let url = URL(string: image)
        let str: String? = Bundle.main.path(forResource: "loading_spinner", ofType: "gif")
        let fileData = NSData(contentsOfFile: str!)
        if url != nil{
            self.profilePic.sd_setImage(with: url, placeholderImage: UIImage.sd_animatedGIF(with: fileData! as Data), options: SDWebImageOptions.allowInvalidSSLCertificates, progress: nil, completed: nil)
            self.uploadBtn.setTitle("", for: UIControlState.normal)
        }
        else{
            self.profilePic.image = UIImage(named: "user")
            self.uploadBtn.setTitle("Upload", for: UIControlState.normal)
        }
        
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "Cell")
        self.tableView.tableFooterView = UIView()
        
        self.rateLbl.text = ""
        self.verifiedImg.image = nil
        getProfileInfo()
    }
    
    func getProfileInfo()  {
        DispatchQueue.main.async {
            let manager = Alamofire.SessionManager.default
            manager.session.configuration.timeoutIntervalForRequest = 120
            manager.request( baseURL + "api/ServiceProviderMappingsAPI/GetPerticularServiceProviderMapping/" + userID, method: .get, parameters: ["":""], encoding: URLEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
                
                switch(response.result) {
                case .success(_):
                    if response.result.value != nil{
                        //print(response.result.value as Any)
                    }
                    let jsonResults : NSDictionary
                    jsonResults = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! NSDictionary
                    
                    if (jsonResults["status"] as! String == "Success") {
                        let ServiceProviderMapping = jsonResults["ServiceProviderMapping"] as! NSArray
                        let getServiceProviderReview = jsonResults["getServiceProviderReview"] as! NSArray
                        let profileinfo = jsonResults["profileinfo"] as! NSArray
                        let profileDict = profileinfo[0] as! NSDictionary
                        let AverageRatings = profileDict["AverageRatings"] as! Double
                    
                        var FirstName = ""
                        if (profileDict["FirstName"] as? String) != nil
                        {
                            FirstName = profileDict["FirstName"] as! String
                        }
                    
                        var LastName = ""
                        if (profileDict["LastName"] as? String) != nil
                        {
                            LastName = profileDict["LastName"] as! String
                        }
                    
                        var image = ""
                        if (profileDict["image"] as? String) != nil
                        {
                            image = profileDict["image"] as! String
                        }
                    
                        var companyLocation = ""
                        if (profileDict["companyLocation"] as? String) != nil
                        {
                            companyLocation = jsonResults["companyLocation"] as! String
                        }
                    
                        var companyName = ""
                        if (profileDict["companyName"] as? String) != nil
                        {
                            companyName = jsonResults["companyName"] as! String
                        }
                    
                        let productInfo = ProductInfo()
                        productInfo.userFirstName = FirstName
                        productInfo.userLastName = LastName
                        productInfo.image = image
                        productInfo.companyName = companyName
                        productInfo.companyAddress = companyLocation
                        productInfo.userID = userID
                        let isData = ModelManager.instance.updateUserInfo(productInfo)
                    

                    
                        self.reviewArr = getServiceProviderReview
                        for i in 0 ..< ServiceProviderMapping.count{
                            let data = ServiceProviderMapping[i] as! NSDictionary
                            let ServiceId = data["ServiceId"] as! String
                            let Active = data["selected"] as! Bool
                            if Active == true{
                                self.selectedCategory.append(ServiceId)
                            }
                        }
                        if AverageRatings == 0{
                            self.rateLbl.text = String(AverageRatings)
                        }
                        else{
                            self.rateLbl.text = String(AverageRatings)
                        }
                        if (profileDict["CompanyDescription"] as? String) != nil
                        {
                            self.CompanyDescription = profileDict["CompanyDescription"] as! String
                        }
                        if (profileDict["FacebookUrl"] as? String) != nil
                        {
                            self.FacebookUrl = profileDict["FacebookUrl"] as! String
                        }
                        if (profileDict["InstagramUrl"] as? String) != nil
                        {
                            self.InstagramUrl = profileDict["InstagramUrl"] as! String
                        }
                    
                        if (profileDict["Twitter"] as? String) != nil
                        {
                            self.TwitterUrl = profileDict["Twitter"] as! String
                        }
                    
                        if (profileDict["Website"] as? String) != nil
                        {
                            self.Website = profileDict["Website"] as! String
                        }
                        var Price = 0
                        if (profileDict["StartingPrice"] as? Int) != nil
                        {
                            Price = profileDict["StartingPrice"] as! Int
                            self.StartingPrice = String(Price)
                        }
                        if (profileDict["Verified"] as? Bool) != nil
                        {
                            self.Verified = profileDict["Verified"] as! Bool
                        }
                    
                        if self.Verified == true{
                            self.verifiedImg.image = UIImage(named: "Verified")
                        }
                        else{
                            self.verifiedImg.image = nil
                        }
                    
                        self.menuArr = ["What services do you provide? " + "\(self.selectedCategory.count) Services", "Describe your company", "Starting price", "Social media and Website", "Reviews"]
                        self.tableView.reloadData()
                    }
                    break
                    
                case .failure(_):
                    print(response.error!.localizedDescription)
                    let alert: UIAlertView = UIAlertView(title: "", message: response.error!.localizedDescription, delegate: nil, cancelButtonTitle: "OK");
                    alert.show()
                    self.view.hideActivityView()
                    print(response.result.error as Any)
                    break
                    
                }
            }
        }
    }
    
    
    
    @IBAction func addImageBtnClk(_ sender: UIButton)     {
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallary()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera))
        {
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openGallary()
    {
        imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        imagePicker.allowsEditing = false
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    internal func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            profilePic.image = pickedImage
            self.uploadBtn.setTitle("", for: UIControlState.normal)
            let imageData: NSData = UIImageJPEGRepresentation(pickedImage, 0.4)! as NSData
            let imageStr = imageData.base64EncodedString(options: .lineLength64Characters)
            base64Str = imageStr
            self.uploadBtn.setTitle("", for: UIControlState.normal)
        }
        
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as UITableViewCell
        cell.accessoryType = UITableViewCellAccessoryType.disclosureIndicator
        cell.textLabel?.text = menuArr[indexPath.row]
        cell.textLabel?.font = UIFont(name:"HelveticaNeue-Bold", size:18)
        cell.textLabel?.textColor = UIColor.darkGray
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        cell.textLabel?.lineBreakMode = .byWordWrapping
        cell.textLabel?.numberOfLines = 0
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let cellText = menuArr[indexPath.row]
        let cellFont: UIFont? = UIFont(name:"HelveticaNeue-Bold", size: 18)!
        let attributedText = NSAttributedString(string: cellText, attributes: [NSAttributedStringKey.font: cellFont as Any])
        let rect: CGRect = attributedText.boundingRect(with: CGSize(width: tableView.bounds.size.width, height: CGFloat.greatestFiniteMagnitude), options: .usesLineFragmentOrigin, context: nil)
        return rect.size.height + 40
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let backItem = UIBarButtonItem()
        backItem.title = "Back"
        backItem.tintColor = UIColor.black
        navigationItem.backBarButtonItem = backItem
        if indexPath.row == 0{
            self.popViewController = self.storyboard?.instantiateViewController(withIdentifier: "CategoryPopupViewController")as! CategoryPopupViewController
            self.popViewController.delagate = self
            self.popViewController.selectedCategory = selectedCategory
            self.navigationController?.pushViewController(self.popViewController, animated: true)
        }
        if indexPath.row == 1{
            self.popViewController1 = self.storyboard?.instantiateViewController(withIdentifier: "CompanyDetailPopup")as! CompanyDetailPopup
            self.popViewController1.delegate = self
            self.popViewController1.companyDescription = self.CompanyDescription
            self.popViewController1.showInView(self.view, animated: true)
        }
        if indexPath.row == 2{
            self.popViewController2 = self.storyboard?.instantiateViewController(withIdentifier: "PricePopUp")as! PricePopUp
            self.popViewController2.delegate = self
            if self.StartingPrice == "0" || self.StartingPrice == ""{
                self.popViewController2.minPrice = ""
            }
            else{
                self.popViewController2.minPrice = self.StartingPrice
            }
            self.popViewController2.showInView(self.view, animated: true)
        }
        if indexPath.row == 3{
            self.popViewController3 = self.storyboard?.instantiateViewController(withIdentifier: "URLPopUp")as! URLPopUp
            self.popViewController3.delegate = self
            self.popViewController3.fbURl = self.FacebookUrl
            self.popViewController3.instaURl = self.InstagramUrl
            self.popViewController3.twitterURl = self.TwitterUrl
            self.popViewController3.webURl = self.Website
            self.popViewController3.showInView(self.view, animated: true)
        }
        if indexPath.row == 4{
            let storyboard =  UIStoryboard(name: "Main", bundle: nil)
            let promo = storyboard.instantiateViewController(withIdentifier: "ReviewsViewController") as! ReviewsViewController
            promo.dataArr = self.reviewArr
            promo.redirectioValue = "SP"
            self.navigationController?.pushViewController(promo, animated: true)
        }
    }
    
    func addService(services: String, idArr: [String]) {
        selectedCategory = idArr
        
       menuArr = ["What services do you provide? " + "\(idArr.count) Services", "Describe your company", "Starting price", "Social media and Website", "Reviews"]
        tableView.reloadData()
    }
    
    func companyInfo(txt: String)  {
        self.CompanyDescription = txt
    }
    
    func minPrice(price: String)  {
        self.StartingPrice = price
    }
    
    func urls(fb: String, insta: String, twitter: String, website: String)  {
        self.FacebookUrl = fb
        self.InstagramUrl = insta
        self.TwitterUrl = twitter
        self.Website = website
    }
    
    @IBAction func saveBtnClk(_ sender: UIButton) {
        if (firstNameTF.text?.trimmingCharacters(in: .whitespaces).isEmpty)!{
            let alert: UIAlertView = UIAlertView(title: "", message: "Please Enter First Name", delegate: nil, cancelButtonTitle: "OK");
            alert.show()
        }
        else if (lastNameTF.text?.trimmingCharacters(in: .whitespaces).isEmpty)!{
            let alert: UIAlertView = UIAlertView(title: "", message: "Please Enter Last Name", delegate: nil, cancelButtonTitle: "OK");
            alert.show()
        }
        else{
            DispatchQueue.main.async {
                self.view.showActivityView(withLabel: "Loading")
                let parameters : [String: Any] = [
                    "ProviderId": userID,
                    "ServiceId": self.selectedCategory as [String],
                    "ProviderName": self.firstNameTF.text as! String,
                    "ProviderLastName": self.lastNameTF.text as! String,
                    "CompanyName": self.companyNameTF.text as! String,
                    "CompanyLocation": self.cityNameTF.text as! String,
                    "ProviderImage": self.base64Str as String,
                    "CompanyDescription": self.CompanyDescription as String,
                    "StartingPrice": self.StartingPrice as String,
                    "FacebookUrl": self.FacebookUrl as String,
                    "InstagramUrl": self.InstagramUrl as String,
                    "Twitter": self.TwitterUrl as String,
                    "Website": self.Website as String
                ]
                let manager = Alamofire.SessionManager.default
                manager.session.configuration.timeoutIntervalForRequest = 120
                
                manager.request(baseURL + "api/ServiceProviderMappingsAPI/ServiceProviderMapping", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
                    
                    switch(response.result) {
                    case .success(_):
                        if response.result.value != nil{
                            //print(response.result.value as Any)
                        }
                        let jsonResults : NSDictionary
                        jsonResults = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! NSDictionary
                        let firstName = jsonResults["firstName"] as! String
                        let lastName = jsonResults["lastName"] as! String
                        var image = ""
                        if (jsonResults["image"] as? String) != nil
                        {
                            image = jsonResults["image"] as! String
                        }
                        
                        
                        let productInfo = ProductInfo()
                        productInfo.userFirstName = firstName
                        productInfo.userLastName = lastName
                        productInfo.image = image
                        productInfo.companyName = self.companyNameTF.text
                        productInfo.companyAddress = self.cityNameTF.text
                        productInfo.userID = userID
                        let isData = ModelManager.instance.updateUserInfo(productInfo)
                        if isData == true{
                            let alert: UIAlertView = UIAlertView(title: "Profile Updated Successfully.", message: "", delegate: nil, cancelButtonTitle: "OK");
                            alert.show()
                            self.navigationController?.popViewController(animated: true)
                        }
                        self.view.hideActivityView()
                        break
                        
                    case .failure(_):
                        print(response.error!.localizedDescription)
                        let alert: UIAlertView = UIAlertView(title: "", message: response.error!.localizedDescription, delegate: nil, cancelButtonTitle: "OK");
                        alert.show()
                        self.view.hideActivityView()
                        print(response.result.error as Any)
                        break
                        
                    }
                }
            }
        }
        
    }
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
