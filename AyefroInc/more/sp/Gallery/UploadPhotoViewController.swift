//
//  UploadPhotoViewController.swift
//  AyefroInc
//
//  Created by Vijay Darkonde on 05/12/17.
//  Copyright © 2017 Inceptive Consulting Pvt. Ltd. All rights reserved.
//

import UIKit
import KMPlaceholderTextView
import Alamofire


protocol promoteArr
{
    func getPromoteArr(aim: String, budget: String, timespan: String, startDate: String, endDate: String)
}


class UploadPhotoViewController: UIViewController,UIImagePickerControllerDelegate, UINavigationControllerDelegate, addServices, promoteArr {

    @IBOutlet var addimgbtn: UIButton!
    @IBOutlet var pictureView: UIImageView!
    @IBOutlet var descriptionTV: KMPlaceholderTextView!
    @IBOutlet var servicesLbl: UILabel!
    @IBOutlet var view1: UIView!
    @IBOutlet var view2: UIView!
    @IBOutlet var view3: UIView!
    @IBOutlet var uploadBtn: UIButton!
    
    var base64Str : String! = ""
    let imagePicker = UIImagePickerController()
    var popViewController : CategoryPopupViewController!
    var selectedCategory: [String] = []
    var promotionAim: String! = ""
    var budget: String! = "0"
    var timespan: String! = ""
    var imgArr : [String] = []
    var selectedImage : UIImage!
    var promotionStartDate: String! = ""
    var promotionEndDate: String! = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imagePicker.delegate = self
        
        descriptionTV.layer.borderColor = UIColor.gray.cgColor
        descriptionTV.layer.borderWidth = 0.5
        descriptionTV.layer.cornerRadius = 5
        
        
        pictureView.layer.borderColor = UIColor.gray.cgColor
        pictureView.layer.borderWidth = 0.5
        pictureView.layer.cornerRadius = 5
        
        uploadBtn.layer.cornerRadius = 5
        
//        view1.dropShadow()
//        view2.dropShadow()
//        view3.dropShadow()
        
        self.title = "Add Picture"
        self.uploadBtn.setTitle("Upload", for: UIControlState.normal)
    }

    @IBAction func addImgBtnClk(_ sender: Any) {
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallary()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }

    
    @IBAction func servicesBtnClk(_ sender: UIButton) {
        let backItem = UIBarButtonItem()
        backItem.title = "Back"
        backItem.tintColor = UIColor.black
        navigationItem.backBarButtonItem = backItem
        self.popViewController = self.storyboard?.instantiateViewController(withIdentifier: "CategoryPopupViewController")as! CategoryPopupViewController
        self.popViewController.delagate = self
        self.popViewController.selectedCategory = self.selectedCategory
        self.navigationController?.pushViewController(self.popViewController, animated: true)
    }

    @IBAction func uploadImgBtnClk(_ sender: UIButton) {
        if base64Str == ""{
            let alert: UIAlertView = UIAlertView(title: "", message: "Please Select Image.", delegate: nil, cancelButtonTitle: "OK");
            alert.show()
        }
        else if (descriptionTV.text.trimmingCharacters(in: .whitespaces).isEmpty){
            let alert: UIAlertView = UIAlertView(title: "", message: "Please Enter Description.", delegate: nil, cancelButtonTitle: "OK");
            alert.show()
        }
        else if selectedCategory.count == 0{
            let alert: UIAlertView = UIAlertView(title: "", message: "Please Select at least One Service.", delegate: nil, cancelButtonTitle: "OK");
            alert.show()
        }
        else{
            self.imgArr.append(base64Str)
            let services = self.selectedCategory.joined(separator: ",")
            DispatchQueue.main.async {
                let parameters : [String: Any] = [
                    "ServiceProviderId": userID as String,
                    "Sevices": services as String,
                    "PromotionAim": self.promotionAim as String,
                    "Budget": self.budget as String,
                    "Timespan": self.timespan as String,
                    "ImageDesc":self.descriptionTV.text as String,
                    "PromotionStartDate": self.promotionStartDate as String,
                    "PromotionEndDate": self.promotionEndDate as String
                ]
                
                Alamofire.upload(multipartFormData: { (multipartFormData) in
                    multipartFormData.append(UIImageJPEGRepresentation(self.selectedImage, 0.5)!, withName: "image", fileName: "swift_file.jpeg", mimeType: "image/jpeg")
                    for (key, value) in parameters {
                        multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
                    }
                    
                }, to: baseURL + "api/Albums/PostAlbum")
                { (result) in
                    switch result {
                    case .success(let upload, _, _):
                        
                        
                        upload.uploadProgress(closure: { (Progress) in
                            print("Upload Progress: \(Progress.fractionCompleted)")
                        })
                        
                        upload.responseJSON { response in
                            
                        }
                        
                        let refreshAlert = UIAlertController(title: "", message: "Image is uploading. Please check after some time. Do you want to upload another?", preferredStyle: UIAlertControllerStyle.alert)

                        refreshAlert.addAction(UIAlertAction(title: "No, Let's Go Back", style: .cancel , handler: { (action: UIAlertAction!) in
                            self.navigationController?.popViewController(animated: true)
                        }))

                        refreshAlert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (action: UIAlertAction!) in
                            self.descriptionTV.text = ""
                            self.selectedCategory = []
                            self.pictureView.image = nil
                            self.servicesLbl.text = "0 Services"
                            self.uploadBtn.setTitle("Upload", for: UIControlState.normal)
                        }))

                        self.present(refreshAlert, animated: true, completion: nil)

                    case .failure(let encodingError):
                        //self.delegate?.showFailAlert()
                        print(encodingError)
                    }
                    
                }

            }
        }
    }
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera))
        {
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openGallary()
    {
        imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        imagePicker.allowsEditing = false
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    internal func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            pictureView.image = pickedImage
            selectedImage = pickedImage
            pictureView.layer.cornerRadius = 5
            let imageData: NSData = UIImageJPEGRepresentation(pickedImage, 0.4)! as NSData
            let imageStr = imageData.base64EncodedString(options: .lineLength64Characters)
            base64Str = imageStr
            self.uploadBtn.setTitle("", for: UIControlState.normal)
        }
        
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func addService(services: String, idArr: [String]) {
        selectedCategory = idArr
        servicesLbl.text = services + " Services"
    }
    
    
    func getPromoteArr(aim: String, budget: String, timespan: String, startDate: String, endDate: String){
        self.promotionAim = aim
        self.budget = budget
        self.timespan = timespan
        self.promotionStartDate = startDate
        self.promotionEndDate = endDate
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let backItem = UIBarButtonItem()
        backItem.title = "Back"
        backItem.tintColor = UIColor.black
        navigationItem.backBarButtonItem = backItem
        let destinationVC = segue.destination as! PromoteViewController
        destinationVC.delegate = self
        // This will show in the next view controller being pushed
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}
