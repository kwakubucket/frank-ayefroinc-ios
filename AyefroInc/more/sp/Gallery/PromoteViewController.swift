//
//  PromoteViewController.swift
//  AyefroInc
//
//  Created by Vijay Darkonde on 05/12/17.
//  Copyright © 2017 Inceptive Consulting Pvt. Ltd. All rights reserved.
//

import UIKit
import DLRadioButton
import Alamofire

protocol cancelProto {
    func cancelTransaction()
}

protocol successProto {
    func successTrasaction()
}


class PromoteViewController: UIViewController, UITextFieldDelegate, cancelProto, successProto {
    
    @IBOutlet weak var radioBtn4: DLRadioButton!
    @IBOutlet weak var radioBtn3: DLRadioButton!
    @IBOutlet weak var radioBtn2: DLRadioButton!
    @IBOutlet weak var radioBtn: DLRadioButton!
    @IBOutlet var firstCard: UIView!
    @IBOutlet var secondCard: UIView!
    @IBOutlet var thirdCard: UIView!
    @IBOutlet var startTF: NiceTextField!
    @IBOutlet var endTF: NiceTextField!
    @IBOutlet var currencyLbl: UILabel!
    @IBOutlet var budgetTF: NiceTextField!
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var daysValue: UILabel!
    @IBOutlet var perDayBudget: UILabel!
    @IBOutlet var scrollHeight: NSLayoutConstraint!
    
    
    var delegate: promoteArr!
    var start_Date: Date!
    var end_Date: Date!
    var roleValue: String! = ""
    var days : Int = 0
    var currencySymbol: String! = ""
    var promotionText: String! = ""
    var startDate: String! = ""
    var endDate: String! = ""
    var budgetValue: String! = ""
    var timespan: String! = ""
    var FacebookUrl = ""
    var InstagramUrl = ""
    var TwitterUrl = ""
    var Website = ""
    var checkoutURL = ""

    override func viewDidLoad() {
        super.viewDidLoad()

//        firstCard.dropShadow()
//        secondCard.dropShadow()
//        thirdCard.dropShadow()
        
        let prefs: UserDefaults? = UserDefaults.standard
        currencySymbol = prefs?.string(forKey: "currencySymbol")
        currencyLbl.text = currencySymbol
        
        budgetTF.isEnabled = false
        endTF.isEnabled = false
        budgetTF.delegate = self
        budgetTF.addDoneOnKeyboardWithTarget(self, action: #selector(self.onDoneBtnClk))
        
        self.title = "PROMOTE"
        
        if UIScreen.main.bounds.size.width == 414
        {
            scrollHeight.constant =  100
        }
        else if UIScreen.main.bounds.size.width == 375 && UIScreen.main.bounds.size.height == 667
        {
            scrollHeight.constant =  200
        }
        else if UIScreen.main.bounds.size.width == 320 && UIScreen.main.bounds.size.height == 568
        {
            scrollHeight.constant =  280
        }
        else if UIScreen.main.bounds.size.width == 320 && UIScreen.main.bounds.size.height == 480
        {
            scrollHeight.constant = 360
        }

        if promotionText == "Increase brand Awareness"{
            radioBtn.isSelected = true
            roleValue = "Increase brand Awareness"
        }
        if promotionText == "Get more website visits"{
            radioBtn2.isSelected = true
            roleValue = "Get more website visits"
        }
        if promotionText == "Get more calls for your business"{
            radioBtn3.isSelected = true
            roleValue = "Get more calls for your business"

        }
        if promotionText == "Promote On Social Media"{
            radioBtn4.isSelected = true
            roleValue = "Promote On Social Media"
        }
        if timespan != ""{
            self.daysValue.text = "\(timespan as String)" + " day(s)"
            self.days = Int(timespan)!
            if budgetValue != ""{
                self.budgetTF.text = budgetValue
                let price = Int(budgetValue!)
                let perday : Double = Double(price!) / Double(self.days)
                let x = perday.rounded(toPlaces: 2)  // x becomes 0.1235 under Swift 2
                perDayBudget.text = "You will spend " + currencySymbol! + " " + String(x) + " a day"
            }
            if startDate != ""{
                let sDate = convertDate(startDate)
                self.startTF.text = sDate
                let eDate = convertDate(endDate)
                self.endTF.text = eDate
                start_Date = convertStringToDate(startDate)
                end_Date = convertStringToDate(endDate)
                budgetTF.isEnabled = true
                endTF.isEnabled = true
            }
        }
        
        getProfileInfo()
        
    }
    
    func getProfileInfo()  {
        DispatchQueue.main.async {
            let manager = Alamofire.SessionManager.default
            manager.session.configuration.timeoutIntervalForRequest = 120
            
            manager.request( baseURL + "api/ServiceProviderMappingsAPI/GetPerticularServiceProviderMapping/" + userID, method: .get, parameters: ["":""], encoding: URLEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
                
                switch(response.result) {
                case .success(_):
                    if response.result.value != nil{
                        //print(response.result.value as Any)
                    }
                    let jsonResults : NSDictionary
                    jsonResults = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! NSDictionary
                    let profileinfo = jsonResults["profileinfo"] as! NSArray
                    let profileDict = profileinfo[0] as! NSDictionary
                    
                    if (profileDict["FacebookUrl"] as? String) != nil
                    {
                        self.FacebookUrl = profileDict["FacebookUrl"] as! String
                    }
                    if (profileDict["InstagramUrl"] as? String) != nil
                    {
                        self.InstagramUrl = profileDict["InstagramUrl"] as! String
                    }
                    
                    if (profileDict["Twitter"] as? String) != nil
                    {
                        self.TwitterUrl = profileDict["Twitter"] as! String
                    }
                    
                    if (profileDict["Website"] as? String) != nil
                    {
                        self.Website = profileDict["Website"] as! String
                    }
                    
                    
                    break
                    
                case .failure(_):
                    print(response.error!.localizedDescription)
                    let alert: UIAlertView = UIAlertView(title: "", message: response.error!.localizedDescription, delegate: nil, cancelButtonTitle: "OK");
                    alert.show()
                    self.view.hideActivityView()
                    print(response.result.error as Any)
                    break
                    
                }
            }
        }
    }
    
    @objc func onDoneBtnClk(){
        
        budgetTF.resignFirstResponder()
        if budgetTF.text != ""{
            let price = Int(budgetTF.text!)
            let perday : Double = Double(price!) / Double(self.days)
            if perday < 1{
                let message = "Per day promotion charge should be more than 1 " + currencySymbol!
                let alert: UIAlertView = UIAlertView(title: "", message: message, delegate: nil, cancelButtonTitle: "OK");
                alert.show()
            }
            else{
                let x = perday.rounded(toPlaces: 2)  // x becomes 0.1235 under Swift 2
                perDayBudget.text = "You will spend " + currencySymbol! + " " + String(x) + " a day"
            }
        }
        
    }
    
    @IBAction func startDateTFClk(_ sender: NiceTextField) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        let date = dateFormatter.string(from: NSDate() as Date)
        startTF.text = date
        start_Date = NSDate() as Date
        endTF.isEnabled = true
        let datePickerView:UIDatePicker = UIDatePicker()
        datePickerView.datePickerMode = UIDatePickerMode.date
        datePickerView.minimumDate = NSDate() as Date
        sender.inputView = datePickerView
        datePickerView.addTarget(self, action: #selector(FilterViewController.startDate), for: UIControlEvents.valueChanged)
        if endTF.text != ""{
            let dateValue = compareDate(dateInitial: start_Date, dateFinal: end_Date)
            if dateValue == false{
                let alert: UIAlertView = UIAlertView(title: "", message: "End Date should be greater than Start Date.", delegate: nil, cancelButtonTitle: "OK");
                alert.show()
            }
            else{
                let calendar = NSCalendar.current
                let date1 = calendar.startOfDay(for: start_Date)
                let date2 = calendar.startOfDay(for: end_Date)
                let components = calendar.dateComponents([.day], from: date1, to: date2)
                self.days = components.day! + 1
                daysValue.text = "\(String(describing: components.day! + 1))" + " day(s)"
                if budgetTF.text != ""{
                    let price = Int(budgetTF.text!)
                    let perday : Double = Double(price!) / Double(self.days)
                    if perday < 1{
                        let message = "Per day promotion charge should be more than 1 " + currencySymbol!
                        let alert: UIAlertView = UIAlertView(title: "", message: message, delegate: nil, cancelButtonTitle: "OK");
                        alert.show()
                    }
                    else{
                        let x = perday.rounded(toPlaces: 2)  // x becomes 0.1235 under Swift 2
                        perDayBudget.text = "You will spend " + currencySymbol! + " " + String(x) + " a day"
                    }
                }
            }
        }
    }
    
    @IBAction func endDateTFClk(_ sender: NiceTextField) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        let date = dateFormatter.string(from: start_Date)
        endTF.text = date
        end_Date = start_Date
        let datePickerView:UIDatePicker = UIDatePicker()
        datePickerView.datePickerMode = UIDatePickerMode.date
        datePickerView.minimumDate = start_Date
        sender.inputView = datePickerView
        datePickerView.addTarget(self, action: #selector(FilterViewController.endDate), for: UIControlEvents.valueChanged)
        
        if startTF.text != ""{
            let dateValue = compareDate(dateInitial: start_Date, dateFinal: end_Date)
            if dateValue == false{
                let alert: UIAlertView = UIAlertView(title: "", message: "End Date should be greater than Start Date.", delegate: nil, cancelButtonTitle: "OK");
                alert.show()
            }
            else{
                let calendar = NSCalendar.current
                let date1 = calendar.startOfDay(for: start_Date)
                let date2 = calendar.startOfDay(for: end_Date)
                let components = calendar.dateComponents([.day], from: date1, to: date2)
                self.days = components.day! + 1
                daysValue.text = "\(String(describing: components.day! + 1))" + " day(s)"
                if budgetTF.text != ""{
                    let price = Int(budgetTF.text!)
                    let perday : Double = Double(price!) / Double(self.days)
                    if perday < 1{
                        let message = "Per day promotion charge should be more than 1 " + currencySymbol!
                        let alert: UIAlertView = UIAlertView(title: "", message: message, delegate: nil, cancelButtonTitle: "OK");
                        alert.show()
                    }
                    else{
                        let x = perday.rounded(toPlaces: 2)  // x becomes 0.1235 under Swift 2
                        perDayBudget.text = "You will spend " + currencySymbol! + " " + String(x) + " a day"
                    }
                }
            }
        }
        budgetTF.isEnabled = true
    }
    
    @IBAction func startdatecalbtnclk(_ sender: UIButton) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        let date = dateFormatter.string(from: NSDate() as Date)
        startTF.text = date
        start_Date = NSDate() as Date
        endTF.isEnabled = true
        let datePickerView:UIDatePicker = UIDatePicker()
        datePickerView.datePickerMode = UIDatePickerMode.date
        datePickerView.minimumDate = NSDate() as Date
        startTF.inputView = datePickerView
        startTF.becomeFirstResponder()
        datePickerView.addTarget(self, action: #selector(FilterViewController.startDate), for: UIControlEvents.valueChanged)
        if endTF.text != ""{
            let dateValue = compareDate(dateInitial: start_Date, dateFinal: end_Date)
            if dateValue == false{
                let alert: UIAlertView = UIAlertView(title: "", message: "End Date should be greater than Start Date.", delegate: nil, cancelButtonTitle: "OK");
                alert.show()
            }
            else{
                
                let calendar = NSCalendar.current
                let date1 = calendar.startOfDay(for: start_Date)
                let date2 = calendar.startOfDay(for: end_Date)
                let components = calendar.dateComponents([.day], from: date1, to: date2)
                self.days = components.day! + 1
                daysValue.text = "\(String(describing: components.day! + 1))" + " day(s)"
                if budgetTF.text != ""{
                    let price = Int(budgetTF.text!)
                    let perday : Double = Double(price!) / Double(self.days)
                    if perday < 1{
                        let message = "Per day promotion charge should be more than 1 " + currencySymbol!
                        let alert: UIAlertView = UIAlertView(title: "", message: message, delegate: nil, cancelButtonTitle: "OK");
                        alert.show()
                    }
                    else{
                        let x = perday.rounded(toPlaces: 2)  // x becomes 0.1235 under Swift 2
                        perDayBudget.text = "You will spend " + currencySymbol! + " " + String(x) + " a day"
                    }
                }
            }
        }
    }
    
    @IBAction func enddatecalbtnclk(_ sender: UIButton) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        let date = dateFormatter.string(from: start_Date)
        endTF.text = date
        end_Date = start_Date
        let datePickerView:UIDatePicker = UIDatePicker()
        datePickerView.datePickerMode = UIDatePickerMode.date
        datePickerView.minimumDate = start_Date
        endTF.inputView = datePickerView
        endTF.becomeFirstResponder()
        datePickerView.addTarget(self, action: #selector(FilterViewController.endDate), for: UIControlEvents.valueChanged)
        
        if startTF.text != ""{
            let dateValue = compareDate(dateInitial: start_Date, dateFinal: end_Date)
            if dateValue == false{
                let alert: UIAlertView = UIAlertView(title: "", message: "End Date should be greater than Start Date.", delegate: nil, cancelButtonTitle: "OK");
                alert.show()
            }
            else{
                let calendar = NSCalendar.current
                let date1 = calendar.startOfDay(for: start_Date)
                let date2 = calendar.startOfDay(for: end_Date)
                let components = calendar.dateComponents([.day], from: date1, to: date2)
                self.days = components.day! + 1
                daysValue.text = "\(String(describing: components.day! + 1))" + " day(s)"
                if budgetTF.text != ""{
                    let price = Int(budgetTF.text!)
                    let perday : Double = Double(price!) / Double(self.days)
                    if perday < 1{
                        let message = "Per day promotion charge should be more than 1 " + currencySymbol!
                        let alert: UIAlertView = UIAlertView(title: "", message: message, delegate: nil, cancelButtonTitle: "OK");
                        alert.show()
                    }
                    else{
                        let x = perday.rounded(toPlaces: 2)  // x becomes 0.1235 under Swift 2
                        perDayBudget.text = "You will spend " + currencySymbol! + " " + String(x) + " a day"
                    }
                }
            }
        }
        budgetTF.isEnabled = true
        
    }
    
    @IBAction func logSelectedButton(radioButton : DLRadioButton) {
        if (radioButton.isMultipleSelectionEnabled) {
            for button in radioButton.selectedButtons() {
                print(String(format: "%@ is selected.\n", button.titleLabel!.text!));
                roleValue = button.titleLabel!.text!
            }
        } else {
            print(String(format: "%@ is selected.\n", radioButton.selected()!.titleLabel!.text!));
            roleValue = radioButton.selected()!.titleLabel!.text!
        }
    }
    
    @IBAction func applyBtnClk(_ sender: UIButton) {
        if roleValue == ""{
            let alert: UIAlertView = UIAlertView(title: "", message: "Please Select at least One Promotion Aim.", delegate: nil, cancelButtonTitle: "OK");
            alert.show()
        }
        else if startTF.text! == "" || endTF.text == ""{
            let alert: UIAlertView = UIAlertView(title: "", message: "Enter Valid Start and End Dates.", delegate: nil, cancelButtonTitle: "OK");
            alert.show()
        }
        else if startTF.text! != "" && endTF.text != ""{
            let dateValue = compareDate(dateInitial: start_Date, dateFinal: end_Date )
            if dateValue == false{
                let alert: UIAlertView = UIAlertView(title: "", message: "End Date should be greater than Start Date.", delegate: nil, cancelButtonTitle: "OK");
                alert.show()
            }
            else if budgetTF.text == ""{
                let alert: UIAlertView = UIAlertView(title: "", message: "Please Enter Valid Budget.", delegate: nil, cancelButtonTitle: "OK");
                alert.show()
            }
            else{
                if roleValue == "Get more website visits" && self.Website == ""{
                    let alert: UIAlertView = UIAlertView(title: "", message: "Please update website link from My Profile", delegate: nil, cancelButtonTitle: "OK");
                    alert.show()
                }
                else if roleValue == "Promote On Social Media" && self.FacebookUrl == "" && self.TwitterUrl == "" && self.InstagramUrl == ""{
                    let alert: UIAlertView = UIAlertView(title: "", message: "Please update social media link from My Profile", delegate: nil, cancelButtonTitle: "OK");
                    alert.show()
                }
                else{
                    self.payment()
                    
//                    let sDate = convertDateFormater(startTF.text!)
//                    let eDate = convertDateFormater(endTF.text!)
//                    delegate.getPromoteArr(aim: roleValue, budget: budgetTF.text!, timespan: String(self.days), startDate: sDate, endDate: eDate)
//                    self.navigationController?.popViewController(animated: true)

                }
                
            }
        }
    }
    
    func payment()  {
        self.view.showActivityView(withLabel: "Loading")
        let parameters : [String: Any] = [
            "invoice": [
                "items":[
                    "item_1": [
                        "name": "Image Promotion",
                        "quantity": 1,
                        "unit_price": self.budgetTF.text!,
                        "total_price": self.budgetTF.text!,
                        "description": ""
                    ]
                ],
                "taxes": [
                    
                ],
                "total_amount": self.budgetTF.text!,
                "description": ""
            ],
            "store": [
                "name": "Ayefro Inc.",
                "tagline": "Event Planner",
                "postal_address": "House No. 4, Plot No. 54, Ashongman Estate",
                "phone": "+233243817622",
                "logo_url": "https://ayefroinc.files.wordpress.com/2017/11/cropped-logo3.png",
                "website_url": "https://ayefroinc.com/"
            ],
            "custom_data": [
                
            ],
            "actions": [
                "cancel_url": "https://ayefroinc.com/about/",
                "return_url": "https://ayefroinc.com/"
            ]
            
        ]
        
        
        let headers = [
            "Authorization": "Basic cGptZHd6aXM6dHZ6d25uZGw=",
            "Content-Type": "application/json"
        ]
        
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 120
        
        manager.request("https://api.hubtel.com/v1/merchantaccount/onlinecheckout/invoice/create", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case .success(_):
                if response.result.value != nil{
                    //print(response.result.value as Any)
                }
                let jsonResults : NSDictionary
                jsonResults = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! NSDictionary
                let response_code = jsonResults["response_code"] as! String
                if response_code == "00"{
                    self.view.hideActivityView()
                    let response_text = jsonResults["response_text"] as! String
                    let token = jsonResults["token"] as! String
                    self.checkoutURL = response_text
                    //                                UIApplication.shared.openURL(NSURL(string: self.checkoutURL)! as URL)
                    let backItem = UIBarButtonItem()
                    backItem.title = "Back"
                    backItem.tintColor = UIColor.black
                    self.navigationItem.backBarButtonItem = backItem
                    let storyboard =  UIStoryboard(name: "Main", bundle: nil)
                    let promo = storyboard.instantiateViewController(withIdentifier: "CheckOutViewController") as! CheckOutViewController
                    promo.webViewurl = self.checkoutURL
                    promo.cancelDel = self
                    promo.successDel = self
                    promo.token = token
                    self.navigationController?.pushViewController(promo, animated: true)
                }
                
                
                break
                
            case .failure(_):
                print(response.error!.localizedDescription)
                let alert: UIAlertView = UIAlertView(title: "", message: response.error!.localizedDescription, delegate: nil, cancelButtonTitle: "OK");
                alert.show()
                self.view.hideActivityView()
                print(response.result.error as Any)
                break
                
            }
        }
    }
    
    func convertDateFormater(_ date: String) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        let date = dateFormatter.date(from: date)
        dateFormatter.dateFormat = "MM/dd/yyyy"
        return  dateFormatter.string(from: date!)
    }
    
    func convertDate(_ date: String) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        let date = dateFormatter.date(from: date)
        dateFormatter.dateFormat = "dd/MM/yyyy"
        return  dateFormatter.string(from: date!)
        
    }
    
    func convertStringToDate(_ date: String) -> Date
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        let date = dateFormatter.date(from: date)
        return date!
        
    }
    
    @objc func startDate(sender:UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        let date = dateFormatter.string(from: sender.date)
        start_Date = sender.date
        if endTF.text != ""{
            let dateValue = compareDate(dateInitial: start_Date, dateFinal: end_Date)
            if dateValue == false{
                let alert: UIAlertView = UIAlertView(title: "", message: "End Date should be greater than Start Date.", delegate: nil, cancelButtonTitle: "OK");
                alert.show()
            }
            else{
                startTF.text = date
                let calendar = NSCalendar.current
                let date1 = calendar.startOfDay(for: start_Date)
                let date2 = calendar.startOfDay(for: end_Date)
                let components = calendar.dateComponents([.day], from: date1, to: date2)
                self.days = components.day! + 1
                daysValue.text = "\(String(describing: components.day! + 1))" + " day(s)"
                if budgetTF.text != ""{
                    let price = Int(budgetTF.text!)
                    let perday : Double = Double(price!) / Double(self.days)
                    if perday < 1{
                        let message = "Per day promotion charge should be more than 1 " + currencySymbol!
                        let alert: UIAlertView = UIAlertView(title: "", message: message, delegate: nil, cancelButtonTitle: "OK");
                        alert.show()
                    }
                    else{
                        let x = perday.rounded(toPlaces: 2)  // x becomes 0.1235 under Swift 2
                        perDayBudget.text = "You will spend " + currencySymbol! + " " + String(x) + " a day"
                    }
                }
            }
        }
        else{
            startTF.text = date
        }
    }
    
    
    func compareDate(dateInitial:Date, dateFinal:Date) -> Bool {
        let order = Calendar.current.compare(dateInitial, to: dateFinal, toGranularity: .day)
        
        switch order {
        case .orderedAscending:
            print("\(dateFinal) is after \(dateInitial)")
            return true
        case .orderedDescending:
            print("\(dateFinal) is before \(dateInitial)")
            return false
        default:
            print("\(dateFinal) is the same as \(dateInitial)")
            return true
        }
    }
    
    @objc func endDate(sender:UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        let date = dateFormatter.string(from: sender.date)
        end_Date = sender.date
        endTF.text = date
        if startTF.text != ""{
            let dateValue = compareDate(dateInitial: start_Date, dateFinal: end_Date)
            if dateValue == false{
                let alert: UIAlertView = UIAlertView(title: "", message: "End Date should be greater than Start Date.", delegate: nil, cancelButtonTitle: "OK");
                alert.show()
            }
            else{
                let calendar = NSCalendar.current
                let date1 = calendar.startOfDay(for: start_Date)
                let date2 = calendar.startOfDay(for: end_Date)
                let components = calendar.dateComponents([.day], from: date1, to: date2)
                self.days = components.day! + 1
                daysValue.text = "\(String(describing: components.day! + 1))" + " day(s)"
                if budgetTF.text != ""{
                    let price = Int(budgetTF.text!)
                    let perday : Double = Double(price!) / Double(self.days)
                    if perday < 1{
                        let message = "Per day promotion charge should be more than 1 " + currencySymbol!
                        let alert: UIAlertView = UIAlertView(title: "", message: message, delegate: nil, cancelButtonTitle: "OK");
                        alert.show()
                    }
                    else{
                        let x = perday.rounded(toPlaces: 2)  // x becomes 0.1235 under Swift 2
                        perDayBudget.text = "You will spend " + currencySymbol! + " " + String(x) + " a day"
                    }
                }
            }
        }
        budgetTF.isEnabled = true

    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        
        let newLength = text.utf16.count + string.utf16.count - range.length
        return newLength <= 8 // Bool
    }
    
    func cancelTransaction(){
        let alert: UIAlertView = UIAlertView(title: "", message: "Transaction failed", delegate: nil, cancelButtonTitle: "OK");
        alert.show()
    }
    
    func successTrasaction(){
        let sDate = convertDateFormater(startTF.text!)
        let eDate = convertDateFormater(endTF.text!)
        delegate.getPromoteArr(aim: roleValue, budget: budgetTF.text!, timespan: String(self.days), startDate: sDate, endDate: eDate)
        self.navigationController?.popViewController(animated: true)

        let alert: UIAlertView = UIAlertView(title: "", message: "Transaction successful", delegate: nil, cancelButtonTitle: "OK");
        alert.show()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

extension UIView {
    
    func dropShadow(color: UIColor, opacity: Float = 0.5, offSet: CGSize, radius: CGFloat = 1, scale: Bool = true) {
        self.layer.masksToBounds = false
        self.layer.shadowColor = color.cgColor
        self.layer.shadowOpacity = opacity
        self.layer.shadowOffset = offSet
        self.layer.shadowRadius = radius
        
        self.layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
        self.layer.shouldRasterize = true
        self.layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
}

extension Double {
    /// Rounds the double to decimal places value
    func rounded(toPlaces places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}

