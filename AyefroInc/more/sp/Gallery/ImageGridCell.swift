//
//  ImageGridCell.swift
//  AyefroInc
//
//  Created by Vijay Darkonde on 04/12/17.
//  Copyright © 2017 Inceptive Consulting Pvt. Ltd. All rights reserved.
//

import UIKit

class ImageGridCell: UICollectionViewCell {
    
    @IBOutlet var galleryImg: UIImageView!
    
}
