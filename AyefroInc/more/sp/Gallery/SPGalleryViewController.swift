//
//  SPGalleryViewController.swift
//  AyefroInc
//
//  Created by Vijay Darkonde on 24/11/17.
//  Copyright © 2017 Inceptive Consulting Pvt. Ltd. All rights reserved.
//

import UIKit
import SDWebImage
import Alamofire

class SPGalleryViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {
    
    var imageName: [String] = []
    var imageId: [String] = []

    @IBOutlet var imageCollection: UICollectionView!
    @IBOutlet var loader: UIImageView!
    @IBOutlet var errorView: UIView!
    @IBOutlet var noInternetView: UIView!
    @IBOutlet var retryBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "My Gallery"
        self.tabBarController?.tabBar.isHidden = true
    
        let str: String? = Bundle.main.path(forResource: "loading_spinner", ofType: "gif")
        let fileData = NSData(contentsOfFile: str!)
        loader.image = UIImage.sd_animatedGIF(with: fileData! as Data)
        // Do any additional setup after loading the view.
        self.errorView.isHidden = true
        self.imageCollection.isHidden =  true
        self.noInternetView.isHidden = true
        self.loader.isHidden = false
        self.retryBtn.layer.cornerRadius = 5
    }
    
    override func viewWillAppear(_ animated: Bool) {
        getAllImages()
    }
    
    func getAllImages()  {
        self.imageName = []
        self.imageCollection.reloadData()
        DispatchQueue.main.async {
            let manager = Alamofire.SessionManager.default
            manager.session.configuration.timeoutIntervalForRequest = 120
            manager.request( baseURL + "api/Albums/MyGAllery?providerId=" + userID, method: .get, parameters: ["":""], encoding: URLEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
                
                switch(response.result) {
                case .success(_):
                    if response.result.value != nil{
                        //print(response.result.value as Any)
                    }
                    
                    let jsonResults : NSArray
                    jsonResults = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! NSArray
                    self.errorView.isHidden = false
                    
                    if jsonResults.count == 0{
                        self.errorView.isHidden = false
                        self.imageCollection.isHidden =  true
                        self.noInternetView.isHidden = true
                        self.loader.isHidden = true
                    }
                    else{
                        self.errorView.isHidden = true
                        self.noInternetView.isHidden = true
                        self.imageCollection.isHidden =  false
                        self.loader.isHidden = true
                        for i in 0 ..< jsonResults.count{
                            let data = jsonResults[i] as! NSDictionary
                            let CategoryTitle = data["image"] as! String
                            let imageId = data["imageId"] as! Int
                            self.imageId.append(String(imageId))
                            self.imageName.append(CategoryTitle)
                        }
                        self.imageCollection.reloadData()
                    }
                    
                    break
                    
                case .failure(_):
                    if response.error?.localizedDescription == "The Internet connection appears to be offline."
                    {
                        self.errorView.isHidden = true
                        self.imageCollection.isHidden = true
                        self.noInternetView.isHidden = false
                        self.loader.isHidden = true
                    }
                    else{
                        self.loader.isHidden = true
                        let alert: UIAlertView = UIAlertView(title: "", message: response.error!.localizedDescription, delegate: nil, cancelButtonTitle: "OK");
                        alert.show()
                        print(response.result.error as Any)
                    }
                    break
                    
                }
            }
        }
    }
    
    @IBAction func retryBtnClk(_ sender: UIButton) {
        getAllImages()
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageGridCell", for: indexPath) as! ImageGridCell
        let str: String? = Bundle.main.path(forResource: "loading_spinner", ofType: "gif")
        let fileData = NSData(contentsOfFile: str!)
        
        cell.galleryImg.sd_setImage(with: URL(string: imageName[indexPath.item]), placeholderImage: UIImage.sd_animatedGIF(with: fileData! as Data), options: SDWebImageOptions.allowInvalidSSLCertificates, progress: nil, completed: nil)
        cell.layer.cornerRadius = 2
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imageName.count
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let backItem = UIBarButtonItem()
        backItem.title = "Back"
        backItem.tintColor = UIColor.black
        navigationItem.backBarButtonItem = backItem
//        let storyboard =  UIStoryboard(name: "Main", bundle: nil)
//        let promo = storyboard.instantiateViewController(withIdentifier: "ImageDisplayViewController") as! ImageDisplayViewController
//        print(imageName[indexPath.item])
//        promo.imageURL = imageName[indexPath.item]
//        promo.imageID = imageId[indexPath.item]
//        promo.deleteShow = true
//        self.navigationController?.pushViewController(promo, animated: true)
        let storyboard =  UIStoryboard(name: "Main", bundle: nil)
        let promo = storyboard.instantiateViewController(withIdentifier: "ImageDetailViewController") as! ImageDetailViewController
        promo.vendorID = userID
        promo.QuestionId = imageId[indexPath.item]
        self.navigationController?.pushViewController(promo, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if UIScreen.main.bounds.size.width == 414
        {
            let collectionViewWidthB: CGFloat? = collectionView.frame.size.width
            let width = collectionViewWidthB! / 3.5
            let cellSizeB = CGSize(width: width, height: width)
            return cellSizeB
        }
        else if UIScreen.main.bounds.size.width == 375 && UIScreen.main.bounds.size.height == 667
        {
            let collectionViewWidthB: CGFloat? = collectionView.frame.size.width
            let width = collectionViewWidthB! / 3.5
            let cellSizeB = CGSize(width: width, height: width)
            return cellSizeB
        }
        else if UIScreen.main.bounds.size.width == 320 && UIScreen.main.bounds.size.height == 568
        {
            let collectionViewWidthB: CGFloat? = collectionView.frame.size.width
            let width = collectionViewWidthB! / 3.5
            let cellSizeB = CGSize(width: width, height: width)
            return cellSizeB
        }
        else if UIScreen.main.bounds.size.width == 320 && UIScreen.main.bounds.size.height == 480
        {
            let collectionViewWidthB: CGFloat? = collectionView.frame.size.width
            let width = collectionViewWidthB! / 3.5
            let cellSizeB = CGSize(width: width, height: width)
            return cellSizeB
        }
        else{
            let collectionViewWidthB: CGFloat? = collectionView.frame.size.width
            let width = collectionViewWidthB! / 3.5
            let cellSizeB = CGSize(width: width, height: width)
            return cellSizeB
        }
       
    }
    
    
    
    @IBAction func addPictureBtnClk(_ sender: UIButton) {
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let backItem = UIBarButtonItem()
        backItem.title = "Back"
        backItem.tintColor = UIColor.black
        navigationItem.backBarButtonItem = backItem
        let destinationVC = segue.destination as! UploadPhotoViewController
        // This will show in the next view controller being pushed
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
