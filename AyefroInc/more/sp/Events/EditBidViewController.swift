//
//  EditBidViewController.swift
//  AyefroInc
//
//  Created by Vijay Darkonde on 16/12/17.
//  Copyright © 2017 Inceptive Consulting Pvt. Ltd. All rights reserved.
//

import UIKit
import ExpandableTableViewController
import Alamofire


class EditBidViewController: ExpandableTableViewController, ExpandableTableViewDelegate, UITextViewDelegate, UITextFieldDelegate, subscribetionProto {
    
    var button = UIButton()
    var delegate: AddServicesForEvent!
    var selectedService: [BidModel] = []

    @IBOutlet var vendorDescription: UITextField!
    
    @IBOutlet var footerView: UIView!
    
    var category: [String] = ["Coming Soon..."]
    var menuOptionImageNameArray : [String] = []
    var selectedID: [String] = []
    var isSelected: [String] = []
    var amountArr : [String] = []
    var descriptionArr : [String] = []
    var servicesName: [String] = []
    var serviceID : [String] = []
    var userAmountArr : [String] = []
    var selectedIndex: [Int] = []

    var eventID : String = ""
    var eventTitle : String = ""
    var servicersLoadArr : NSArray!
    var dictArr: NSArray!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = eventTitle
        self.expandableTableView.expandableDelegate = self
        self.expandableTableView.tableFooterView = footerView
        button = UIButton(frame: CGRect(x: 0, y: (self.view.frame.size.height - 50), width: self.view.frame.size.width, height: 50))
        button.backgroundColor = UIColor(red: 85.0/255.0, green: 85.0/255.0, blue: 85.0/255.0, alpha: 1.0)
        button.setTitle("UPDATE BID", for: .normal)
        button.titleLabel?.textColor = UIColor.white
        button.titleLabel?.font = UIFont(name:"HelveticaNeue-Bold", size: 17.0)!
        button.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        navigationController?.view.addSubview(button)
        
        
        
        let backbutton = UIButton(type: .system)
        backbutton.setTitle(" Back", for: .normal)
        backbutton.setImage(UIImage(named: "backbtn"), for: .normal)
        backbutton.addTarget(self, action: #selector(reset), for: .touchUpInside)
        backbutton.tintColor = UIColor.black
        backbutton.contentHorizontalAlignment = .left
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: backbutton)
        
        let config = FTConfiguration.shared
        config.textColor = UIColor.white
        config.backgoundTintColor = UIColor.black
        config.borderColor = UIColor.lightGray
        config.menuWidth = self.view.frame.width - 10
        config.menuSeparatorColor = UIColor.lightGray
        config.textAlignment = .left
        config.textFont = UIFont(name: "Helvetica Neue", size: 17)!
        config.menuRowHeight = 50
        config.cornerRadius = 6
        
        buildTestData {
            
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.setBackgroundImage(nil, for: .default)
        self.navigationController?.navigationBar.shadowImage = nil
    }
    
    
    @objc func reset()  {
        self.navigationController?.popViewController(animated: true)
    }
    
    func getSubscription()  {
        DispatchQueue.main.async {
            let date = self.convertDateFormater()
            let manager = Alamofire.SessionManager.default
            manager.session.configuration.timeoutIntervalForRequest = 120
            
            manager.request( baseURL + "api/SubscriptionMappings/GetSubscriptionDetail/?userid=" + userID + "&date=" + date, method: .get, parameters: ["":""], encoding: URLEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
                
                switch(response.result) {
                case .success(_):
                    if response.result.value != nil{
                        //print(response.result.value as Any)
                    }
                    let jsonResults : NSDictionary
                    jsonResults = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! NSDictionary
                    let Success = jsonResults["Success"] as! String
                    if Success == "Success"{
                        if (jsonResults["SubDetails"] as? NSArray) != nil{
                            let SubDetails = jsonResults["SubDetails"] as! NSArray
                            let dict = SubDetails[0] as! NSDictionary
                            let SubPlanId = dict["SubPlanId"] as! Int
                            let SubPlanName = dict["SubPlanName"] as! String
                            UserDefaults.standard.setValue(SubPlanId, forKey: "SubPlanId")
                            UserDefaults.standard.setValue(SubPlanName, forKey: "SubPlanName")
                            let IsActive = dict["IsActive"] as! Bool
                            var activeState = ""
                            if IsActive == true{
                                activeState = "true"
                            }
                            else{
                                activeState = "false"
                            }
                            UserDefaults.standard.setValue(activeState, forKey: "IsSubscriptionActive")
                        }
                        else{
                            let IsActive = jsonResults["IsActive"] as! String
                            UserDefaults.standard.setValue(IsActive, forKey: "IsSubscriptionActive")
                            
                        }
                    }
                    else{
                        let IsActive = jsonResults["IsActive"] as! String
                        UserDefaults.standard.setValue(IsActive, forKey: "IsSubscriptionActive")
                    }
                    break
                    
                case .failure(_):
                    print(response.error!.localizedDescription)
                    let alert: UIAlertView = UIAlertView(title: "", message: response.error!.localizedDescription, delegate: nil, cancelButtonTitle: "OK");
                    alert.show()
                    self.view.hideActivityView()
                    print(response.result.error as Any)
                    break
                    
                }
            }
        }
    }
    
    func convertDateFormater() -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy"
        return  dateFormatter.string(from: Date())
    }
    
    func buildTestData(then: @escaping () -> Void) {
        DispatchQueue.main.async {
            let manager = Alamofire.SessionManager.default
            manager.session.configuration.timeoutIntervalForRequest = 120
            manager.request(baseURL + "api/EventPlaceBidsAPI/GetBidByproviderID/?providerID=" + userID + "&eventId=" + self.eventID, method: .get, parameters: ["":""], encoding: URLEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in

                switch(response.result) {
                case .success(_):
                    if response.result.value != nil{
                        //print(response.result.value as Any)
                        let jsonResults : NSDictionary
                        jsonResults = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! NSDictionary
                        let eventData = jsonResults["EventDetails"] as! NSArray
                        if eventData.count != 0{
                            let dict = eventData[0] as! NSDictionary
                            let EventServicesArray = dict["EventServicesArray"] as! NSArray
                            for i in 0 ..< EventServicesArray.count{
                                let dict = EventServicesArray[i] as! NSDictionary
                                let servID = dict["serviceId"] as! Int
                                let amount = dict["serviceAmount"] as! Int
                                let serviceName = dict["serviceName"] as! String
                                var serviceDescription = ""
                                if (dict["ServiceDesc"] as? String) != nil{
                                    serviceDescription = dict["ServiceDesc"] as! String
                                }
                                self.serviceID.append(String(servID))
                                self.servicesName.append(serviceName)
                                self.descriptionArr.append(serviceDescription)
                                self.userAmountArr.append(String(amount))
                                self.isSelected.append("false")
                                self.amountArr.append("0")
                            }
                            let BidDetails = jsonResults["BidDetails"] as! NSArray
                            let bidDict = BidDetails[0] as! NSDictionary
                            var BidDescription = ""
                            if (bidDict["BidDescription"] as? String) != nil
                            {
                                BidDescription = bidDict["BidDescription"] as! String
                            }
                            self.vendorDescription.text = BidDescription
                            let serviceslist = bidDict["serviceslist"] as! NSArray
                            for i in 0 ..< serviceslist.count{
                                let dict = serviceslist[i] as! NSDictionary
                                let ServiceId = dict["ServiceId"] as! Int
                                let ServiceCost = dict["ServiceCost"] as! Int
                                let indexOfA = self.serviceID.index(of: String(ServiceId))
                                self.isSelected[indexOfA!] = "true"
                                self.selectedIndex.append(i)
                                self.amountArr[indexOfA!] = String(ServiceCost)
                                self.selectedID.append(String(ServiceId))
                                self.selectedService.append(BidModel.init(serviceId: String(ServiceId), serviceCost: Int(ServiceCost)))
                            }
                            if EventServicesArray.count == 0 && serviceslist.count == 0{
                                let refreshAlert = UIAlertController(title: "", message: "All services has been awarded", preferredStyle: UIAlertControllerStyle.alert)
                                
                                refreshAlert.addAction(UIAlertAction(title: "OK", style: .cancel , handler: { (action: UIAlertAction!) in
                                    self.navigationController?.popViewController(animated: true)
                                }))
                                
                                
                                
                                
                                self.present(refreshAlert, animated: true, completion: nil)
                            }
                            self.expandableTableView.reloadData()
                        }
                        else
                        {
                            let alert: UIAlertView = UIAlertView(title: "", message: "Data not found", delegate: nil, cancelButtonTitle: "OK");
                            alert.show()
                        }
                        
                    }
                    self.tableView.reloadData()
                    break
                    
                case .failure(_):
                    print(response.error!.localizedDescription)
                    if response.error?.localizedDescription == "The Internet connection appears to be offline."
                    {
                        let alert: UIAlertView = UIAlertView(title: "", message: response.error!.localizedDescription, delegate: nil, cancelButtonTitle: "OK");
                        alert.show()
                        self.view.hideActivityView()
                        print(response.result.error as Any)
                    }
                    else{
                        let alert: UIAlertView = UIAlertView(title: "", message: response.error!.localizedDescription, delegate: nil, cancelButtonTitle: "OK");
                        alert.show()
                        self.view.hideActivityView()
                        print(response.result.error as Any)
                    }
                    break
                    
                }
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.expandableTableView.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: (self.view.frame.size.height - 50))
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.button.removeFromSuperview()
    }
    
    @objc func buttonAction(_ sender: UIButton!) {
        unexpandAllCells()
        print(self.selectedID)
        selectedService = []
        if selectedID.count != 0{
            for i in 0 ..< selectedID.count{
                let index = self.selectedIndex[i]
                let amount = self.amountArr[index]
                if amount == "0"{
                    let alert: UIAlertView = UIAlertView(title: "", message: "You have selected a service but you have not specified an amount. Please check and try again", delegate: nil, cancelButtonTitle: "OK");
                    alert.show()
                    break
                }
                else{
                    if (vendorDescription.text?.trimmingCharacters(in: .whitespaces).isEmpty)!{
                        let alert: UIAlertView = UIAlertView(title: "", message: "Please indicate why you should be hired for the job", delegate: nil, cancelButtonTitle: "OK");
                        alert.show()
                        break
                    }
                    else{
                        if i == selectedID.count - 1{
                            self.selectedService.append(BidModel.init(serviceId: selectedID[i], serviceCost: Int(amount)!))
                            let jsonArray = BidModel.jsonArray(array: selectedService)
                            let jsonData = jsonArray.data(using: .utf8)
                            let dictionary = try? JSONSerialization.jsonObject(with: jsonData!, options: .mutableLeaves) as! NSArray
                            
                            self.dictArr = dictionary!
                            let subscriptionID = UserDefaults.standard.string(forKey: "SubPlanId")
                            if UserDefaults.standard.string(forKey: "IsSubscriptionActive") == "false" {
                                let refreshAlert = UIAlertController(title: "", message: "To place your bid, you need to subscribe to a package. Do you want to subscribe?", preferredStyle: UIAlertControllerStyle.alert)
                                
                                refreshAlert.addAction(UIAlertAction(title: "No", style: .cancel , handler: { (action: UIAlertAction!) in
                                    self.navigationController?.popViewController(animated: true)
                                }))
                                
                                
                                refreshAlert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (action: UIAlertAction!) in
                                    let backItem = UIBarButtonItem()
                                    backItem.title = "Back"
                                    backItem.tintColor = UIColor.black
                                    self.navigationItem.backBarButtonItem = backItem
                                    let storyboard =  UIStoryboard(name: "Main", bundle: nil)
                                    let promo = storyboard.instantiateViewController(withIdentifier: "SubscriptionViewController") as! SubscriptionViewController
                                    promo.delegate = self
                                    promo.servicesCount = self.selectedID.count
                                    self.navigationController?.pushViewController(promo, animated: true)
                                }))
                                
                                self.present(refreshAlert, animated: true, completion: nil)
                            }
                            else if (subscriptionID == "2" && selectedID.count > 4) || (subscriptionID == "3" && selectedID.count > 2){
                                let refreshAlert = UIAlertController(title: "", message: "Your current subscription plan cannot apply for all the the services you have selected. Do you want to upgrade?", preferredStyle: UIAlertControllerStyle.alert)
                                
                                refreshAlert.addAction(UIAlertAction(title: "No", style: .cancel , handler: { (action: UIAlertAction!) in
                                    
                                }))
                                
                                
                                refreshAlert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (action: UIAlertAction!) in
                                    let backItem = UIBarButtonItem()
                                    backItem.title = "Back"
                                    backItem.tintColor = UIColor.black
                                    self.navigationItem.backBarButtonItem = backItem
                                    let storyboard =  UIStoryboard(name: "Main", bundle: nil)
                                    let promo = storyboard.instantiateViewController(withIdentifier: "SubscriptionViewController") as! SubscriptionViewController
                                    promo.delegate = self
                                    promo.servicesCount = self.selectedID.count
                                    self.navigationController?.pushViewController(promo, animated: true)
                                }))
                                self.present(refreshAlert, animated: true, completion: nil)
                            }
                            else{
                                submitBid(servicesArr: self.dictArr!)
                            }
//                            submitBid(servicesArr: dictionary!)
                        }
                        else{
                            self.selectedService.append(BidModel.init(serviceId: selectedID[i], serviceCost: Int(amount)!))
                        }
                    }
                }
            }
        }
        else{
            let alert: UIAlertView = UIAlertView(title: "", message: "Kindly tick the services you require, fill in extra details and add amount", delegate: nil, cancelButtonTitle: "OK");
            alert.show()
        }
        
    }
    
    func subscribtionSuccess(){
        submitBid(servicesArr: self.dictArr!)
    }
    
    func submitBid(servicesArr: NSArray)  {
        var sum : Int = 0
        for i in 0..<servicesArr.count {
            let asd = (servicesArr[i] as! AnyObject).value(forKey: "serviceCost") as! String
            sum = sum + Int(asd)!
        }
        print(sum)
        DispatchQueue.main.async {
            self.view.showActivityView(withLabel: "Loading")
            let parameters : [String: Any] = [
                "EventId": self.eventID,
                "Cost": String(sum),
                "Description": self.vendorDescription.text!,
                "ServiceProviderId": userID,
                "Attachments": [],
                "ServiceCost": servicesArr,
                
                ]
            let manager = Alamofire.SessionManager.default
            manager.session.configuration.timeoutIntervalForRequest = 120
            
            manager.request(baseURL + "api/EventPlaceBidsAPI/UpdateEventPlaceBid", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
                
                switch(response.result) {
                case .success(_):
                    if response.result.value != nil{
                        //print(response.result.value as Any)
                    }
                    let message = "Thank you " + userFirstName + " for your bid. You will be notified if you are selected."
                    let alert: UIAlertView = UIAlertView(title: "", message: message, delegate: nil, cancelButtonTitle: "OK");
                    alert.show()
                    self.navigationController?.popViewController(animated: true)
                    self.view.hideActivityView()
                    break
                    
                case .failure(_):
                    print(response.error!.localizedDescription)
                    let alert: UIAlertView = UIAlertView(title: "", message: response.error!.localizedDescription, delegate: nil, cancelButtonTitle: "OK");
                    alert.show()
                    self.view.hideActivityView()
                    print(response.result.error as Any)
                    break
                    
                }
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: - Init
    
    override func tableView(_: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if cell.responds(to: #selector(setter: UIView.preservesSuperviewLayoutMargins)){
            cell.preservesSuperviewLayoutMargins = false
        }
        
        if cell.responds(to: #selector(setter: UIView.layoutMargins)){
            cell.layoutMargins = UIEdgeInsets.zero
        }
    }
    
    @IBAction func btnClk(_ sender: UIButton) {
        let position : CGPoint = sender.convert(sender.frame.origin, to: self.expandableTableView)
        let pickerCellIndexPath : IndexPath = expandableTableView.indexPathForRow(at: position)!
        
        // Creates the index path for the value text
        let birthdateCell : EventDetailTitleCell = expandableTableView.cellForRow(at: pickerCellIndexPath) as! EventDetailTitleCell
        let index = self.expandableIndexPathForIndexPath(pickerCellIndexPath)
        if birthdateCell.btn.imageView?.image == UIImage(named: "check-mark")
        {
            birthdateCell.btn.setImage(UIImage(named: "circle"), for: UIControlState.normal)
            if expandableTableView.isCellExpandedAtExpandableIndexPath(index) == true{
                unexpandCellAtIndexPath(pickerCellIndexPath, tableView: expandableTableView)
                if let index = selectedID.index(of: self.serviceID[index.row]) {
                    selectedID.remove(at: index)
                }
                self.isSelected[index.row] = "false"
                if let index = selectedIndex.index(of: index.row) {
                    selectedIndex.remove(at: index)
                }
                birthdateCell.arrowImgf.image = UIImage(named: "darrow")
            }
            else{
                if let index = selectedID.index(of: self.serviceID[index.row]) {
                    selectedID.remove(at: index)
                }
                self.isSelected[index.row] = "false"
                if let index = selectedIndex.index(of: index.row) {
                    selectedIndex.remove(at: index)
                }
            }
        }
        else{
            birthdateCell.btn.setImage(UIImage(named: "check-mark"), for: UIControlState.normal)
            if expandableTableView.isCellExpandedAtExpandableIndexPath(index) == false{
                unexpandAllCells()
                let ind = index.row
                let indexP = IndexPath(row: ind, section: 0)
                expandCellAtIndexPath(indexP, tableView: self.expandableTableView)
                self.selectedID.append(self.serviceID[index.row])
                self.isSelected[index.row] = "true"
                let budget = birthdateCell.budgetLbl.text!.components(separatedBy: " ")
                 birthdateCell.arrowImgf.image = UIImage(named: "uarrow")
                self.selectedIndex.append(index.row)
            }
            else{
                unexpandAllCells()
                self.selectedIndex.append(index.row)
                self.selectedID.append(self.serviceID[index.row])
                let budget = birthdateCell.budgetLbl.text!.components(separatedBy: " ")
                self.isSelected[index.row] = "true"
            }
        }
    }
    
    
    // MARK: - Expandable Table View Controller Delegate
    
    // MARK: - Rows
    func expandableTableView(_ expandableTableView: ExpandableTableView, numberOfRowsInSection section: Int) -> Int {
        return servicesName.count
    }
    
    func expandableTableView(_ expandableTableView: ExpandableTableView, cellForRowAtExpandableIndexPath expandableIndexPath: ExpandableIndexPath) -> UITableViewCell {
        var cell: EventDetailTitleCell!
        cell = expandableTableView.dequeueReusableCellWithIdentifier("EventDetailTitleCell", forIndexPath: expandableIndexPath) as! EventDetailTitleCell
        if isSelected[expandableIndexPath.row] == "true"{
            cell.btn.setImage(UIImage(named: "check-mark"), for: UIControlState.normal)
        }
        else{
            cell.btn.setImage(UIImage(named: "circle"), for: UIControlState.normal)
        }
        cell.btn.tintColor = UIColor.black
        cell.serviceLbl.text = self.servicesName[expandableIndexPath.row]
        let prefs: UserDefaults? = UserDefaults.standard
        let currencySymbol = prefs?.string(forKey: "currencySymbol")
        if amountArr[expandableIndexPath.row] == "0"{
            cell.newBudgetLbl.text = currencySymbol! + " 0"
        }
        else{
            cell.newBudgetLbl.text = currencySymbol! + " " + amountArr[expandableIndexPath.row]
        }
        
        cell.budgetLbl.text = currencySymbol! + " " + userAmountArr[expandableIndexPath.row]
        
        return cell
    }
    
    
    func expandableTableView(_ expandableTableView: ExpandableTableView, heightForRowAtExpandableIndexPath expandableIndexPath: ExpandableIndexPath) -> CGFloat {
        return 73
    }
    
    func expandableTableView(_ expandableTableView: ExpandableTableView, estimatedHeightForRowAtExpandableIndexPath expandableIndexPath: ExpandableIndexPath) -> CGFloat {
        return 73
        
    }
    
    func expandableTableView(_ expandableTableView: ExpandableTableView, didSelectRowAtExpandableIndexPath expandableIndexPath: ExpandableIndexPath) {
        let birthdateCell : EventDetailTitleCell = expandableTableView.cellForRowAtIndexPath(expandableIndexPath) as! EventDetailTitleCell
        if expandableTableView.isCellExpandedAtExpandableIndexPath(expandableIndexPath) == true{
            unexpandAllCells()
            let index = expandableIndexPath.row
            let indexPath = IndexPath(row: index, section: 0)
            expandCellAtIndexPath(indexPath, tableView: self.expandableTableView)
            birthdateCell.arrowImgf.image = UIImage(named: "uarrow")
        }
        else{
            birthdateCell.arrowImgf.image = UIImage(named: "darrow")
        }
    }
    
    // MARK: - SubRows
    func expandableTableView(_ expandableTableView: ExpandableTableView, numberOfSubRowsInRowAtExpandableIndexPath expandableIndexPath: ExpandableIndexPath) -> Int {
        return 1
    }
    
    func expandableTableView(_ expandableTableView: ExpandableTableView, subCellForRowAtExpandableIndexPath expandableIndexPath: ExpandableIndexPath) -> UITableViewCell {
        var cell: ExtraDetailsCel!
        
        cell = expandableTableView.dequeueReusableCellWithIdentifier("ExtraDetailsCel", forIndexPath: expandableIndexPath) as! ExtraDetailsCel
        cell.amountTF.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        if amountArr[expandableIndexPath.row] == "0"{
            cell.amountTF.text = ""
        }
        else{
            cell.amountTF.text = amountArr[expandableIndexPath.row]
        }
        cell.descriptionTV.text = descriptionArr[expandableIndexPath.row]
        cell.descriptionTV.layer.borderColor = UIColor.gray.cgColor
        cell.descriptionTV.layer.borderWidth = 0.5
        cell.descriptionTV.layer.cornerRadius = 5
        cell.descriptionTV.delegate = self
        cell.amountTF.delegate = self
        return cell
    }
    
    
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        let position : CGPoint = textField.convert(textField.frame.origin, to: self.expandableTableView)
        let pickerCellIndexPath : IndexPath = tableView.indexPathForRow(at: CGPoint(x: position.x, y:position.y - 160))!
        let valueCellIndexPath = IndexPath(row: pickerCellIndexPath.row - 1, section: 0)
        
        let birthdateCell : EventDetailTitleCell = tableView.cellForRow(at: valueCellIndexPath) as! EventDetailTitleCell
        let prefs: UserDefaults? = UserDefaults.standard
        let currencySymbol = prefs?.string(forKey: "currencySymbol")
        birthdateCell.newBudgetLbl.text = currencySymbol! + " " +  textField.text!
        
        self.amountArr[valueCellIndexPath.row] = textField.text!
    }
    
    
    func expandableTableView(_ expandableTableView: ExpandableTableView, heightForSubRowAtExpandableIndexPath expandableIndexPath: ExpandableIndexPath) -> CGFloat {
        return 185.0
    }
    
    func expandableTableView(_ expandableTableView: ExpandableTableView, estimatedHeightForSubRowAtExpandableIndexPath expandableIndexPath: ExpandableIndexPath) -> CGFloat {
        return 185.0
    }
    
    func expandableTableView(_ expandableTableView: ExpandableTableView, didSelectSubRowAtExpandableIndexPath expandableIndexPath: ExpandableIndexPath){
        
    }
    
    func textViewDidChange(_ textView: UITextView) {
        print(textView.text)
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        
        let newLength = text.utf16.count + string.utf16.count - range.length
        return newLength <= 8 // Bool
    }
    
    @IBAction func infoBtnClk(_ sender: UIButton) {
        category = ["What makes your company unique?"]
        FTPopOverMenu.showForSender(sender: sender, with: category, menuImageArray: menuOptionImageNameArray, done: { (selectedIndex) -> () in
            print(selectedIndex)
            
        }) {
        }
    }
    
}



