//
//  SubscriptionCell.swift
//  AyefroInc
//
//  Created by Vijay Darkonde on 06/01/18.
//  Copyright © 2018 Inceptive Consulting Pvt. Ltd. All rights reserved.
//

import UIKit

class SubscriptionCell: UITableViewCell {
    
    
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var packTitle: UILabel!
    @IBOutlet weak var amount: UILabel!
    @IBOutlet weak var packDetails: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
