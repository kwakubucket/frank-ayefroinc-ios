//
//  EventDetailTitleCell.swift
//  AyefroInc
//
//  Created by Vijay Darkonde on 15/12/17.
//  Copyright © 2017 Inceptive Consulting Pvt. Ltd. All rights reserved.
//

import UIKit

class EventDetailTitleCell: UITableViewCell {
    
    @IBOutlet var btn: UIButton!
    @IBOutlet var serviceLbl: UILabel!
    @IBOutlet var budgetLbl: UILabel!
    @IBOutlet var newBudgetLbl: UILabel!
    @IBOutlet weak var arrowImgf: UIImageView!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
