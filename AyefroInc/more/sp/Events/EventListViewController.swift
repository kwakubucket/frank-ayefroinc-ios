//
//  EventListViewController.swift
//  AyefroInc
//
//  Created by Vijay Darkonde on 24/11/17.
//  Copyright © 2017 Inceptive Consulting Pvt. Ltd. All rights reserved.
//

import UIKit
import Alamofire


class EventListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var prototypeEntitiesFromJSON: [EventListEntity] = []
    
    
    @IBOutlet var loader: UIImageView!
    @IBOutlet var noInternetView: UIView!
    @IBOutlet var errorView: UIView!
    @IBOutlet var tableView: UITableView!
    @IBOutlet var pushBtn: UIButton!
    @IBOutlet var retryBtn: UIButton!
    
    var refreshControl: UIRefreshControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.register(UINib.init(nibName: "EventsCell", bundle: nil), forCellReuseIdentifier: "EventsCell")
        self.tableView.fd_debugLogEnabled = true
        self.tableView.tableFooterView = UIView()
        //        self.shyNavBarManager.scrollView = self.tableView;
        refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: "Reloading")
        refreshControl.addTarget(self, action: #selector(refresh), for: UIControlEvents.valueChanged)
        tableView.addSubview(refreshControl)
        
        pushBtn.layer.cornerRadius = 5
        retryBtn.layer.cornerRadius = 5
        self.errorView.isHidden = true
        self.tableView.isHidden = true
        self.noInternetView.isHidden = true
        let str: String? = Bundle.main.path(forResource: "loading_spinner", ofType: "gif")
        let fileData = NSData(contentsOfFile: str!)
        loader.image = UIImage.sd_animatedGIF(with: fileData! as Data)
        
        buildTestData {
            
        }
    }
    
    override func viewDidLayoutSubviews() {
        self.automaticallyAdjustsScrollViewInsets = false
    }
    
    @objc func refresh(){
        refreshControl.endRefreshing()
        DispatchQueue.main.async {
            let manager = Alamofire.SessionManager.default
            manager.session.configuration.timeoutIntervalForRequest = 120
            // Data from `data.json`
            self.prototypeEntitiesFromJSON = []
            manager.request(baseURL + "api/Events/GetEventsByProvider/?providerId=" + userID, method: .get, parameters: ["":""], encoding: URLEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
                
                switch(response.result) {
                case .success(_):
                    if response.result.value != nil{
                        //print(response.result.value as Any)
                        let jsonResults : NSArray
                        jsonResults = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! NSArray
                        if jsonResults.count == 0{
                            self.errorView.isHidden = false
                            self.tableView.isHidden = true
                            self.noInternetView.isHidden = true
                            self.loader.isHidden = true
                        }
                        else{
                            self.errorView.isHidden = true
                            self.tableView.isHidden = false
                            self.noInternetView.isHidden = true
                            self.loader.isHidden = true
                            let jsonArr = NSMutableArray()
                            for i in 0 ..< jsonResults.count{
                                let dicWithoutNulls = jsonResults[i] as! NSDictionary
                                
                                let outputDict = self.removeNSNull(from: dicWithoutNulls.mutableCopy() as! NSMutableDictionary)
                                
                                jsonArr.add(outputDict)
                            }
                            
                            let arr = NSArray(array: jsonArr)
                            for i in 0 ..< arr.count{
                                self.prototypeEntitiesFromJSON.append(EventListEntity(dictionary: arr[i] as! [AnyHashable : Any] ))
                            }
                        }
                    }
                    self.tableView.reloadData()
                    break
                    
                case .failure(_):
                    print(response.error!.localizedDescription)
                    
                    if response.error?.localizedDescription == "The Internet connection appears to be offline."
                    {
                        self.errorView.isHidden = true
                        self.tableView.isHidden = true
                        self.noInternetView.isHidden = false
                        self.loader.isHidden = true
                    }
                    else{
                        self.loader.isHidden = true
                        let alert: UIAlertView = UIAlertView(title: "", message: response.error!.localizedDescription, delegate: nil, cancelButtonTitle: "OK");
                        alert.show()
                        self.view.hideActivityView()
                        print(response.result.error as Any)
                    }
                    break
                    
                }
            }
        }
    }
    
    func buildTestData(then: @escaping () -> Void) {
        DispatchQueue.main.async {
            let manager = Alamofire.SessionManager.default
            manager.session.configuration.timeoutIntervalForRequest = 120
            // Data from `data.json`
            self.prototypeEntitiesFromJSON = []
            manager.request(baseURL + "api/Events/GetEventsByProvider/?providerId=" + userID, method: .get, parameters: ["":""], encoding: URLEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
                
                switch(response.result) {
                case .success(_):
                    if response.result.value != nil{
                        //print(response.result.value as Any)
                        let jsonResults : NSArray
                        jsonResults = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! NSArray
                        
                        if jsonResults.count == 0{
                            self.errorView.isHidden = false
                            self.tableView.isHidden = true
                            self.noInternetView.isHidden = true
                            self.loader.isHidden = true
                        }
                        else{
                            self.errorView.isHidden = true
                            self.tableView.isHidden = false
                            self.noInternetView.isHidden = true
                            self.loader.isHidden = true
                            let jsonArr = NSMutableArray()
                            for i in 0 ..< jsonResults.count{
                                let dicWithoutNulls = jsonResults[i] as! NSDictionary
                                
                                let outputDict = self.removeNSNull(from: dicWithoutNulls.mutableCopy() as! NSMutableDictionary)
                                
                                jsonArr.add(outputDict)
                            }
                            
                            let arr = NSArray(array: jsonArr)
                            for i in 0 ..< arr.count{
                                self.prototypeEntitiesFromJSON.append(EventListEntity(dictionary: arr[i] as! [AnyHashable : Any] ))
                            }
                        }
                    }
                    self.tableView.reloadData()
                    break
                    
                case .failure(_):
                    print(response.error!.localizedDescription)
                    
                    if response.error?.localizedDescription == "The Internet connection appears to be offline."
                    {
                        self.errorView.isHidden = true
                        self.tableView.isHidden = true
                        self.noInternetView.isHidden = false
                        self.loader.isHidden = true
                    }
                    else{
                        self.loader.isHidden = true
                        let alert: UIAlertView = UIAlertView(title: "", message: response.error!.localizedDescription, delegate: nil, cancelButtonTitle: "OK");
                        alert.show()
                        self.view.hideActivityView()
                        print(response.result.error as Any)
                    }
                    break
                    
                }
            }
        }
    }
    
    func removeNSNull(from dict: NSMutableDictionary) -> NSDictionary {
        let mutableDict = dict
        let keysWithEmptString = dict.filter { $0.1 is NSNull }.map { $0.0 }
        for key in keysWithEmptString {
            mutableDict[key] = ""
        }
        
        return mutableDict
    }
    
    // MARK: - UITableViewDataSource
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let count = prototypeEntitiesFromJSON.count
        if count != 0 {
            return count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "EventsCell") as! EventsCell
        if prototypeEntitiesFromJSON.count != 0{
            configureCell(cell, at: indexPath)
        }
        return cell
    }
    
    
    func configureCell(_ cell: EventsCell, at indexPath: IndexPath) {
        if prototypeEntitiesFromJSON.count != 0{
            cell.fd_enforceFrameLayout = true
            cell.entity = prototypeEntitiesFromJSON[indexPath.row]
            cell.hideBtn.addTarget(self, action: #selector(self.hideBtnClk), for: UIControlEvents.touchUpInside)
            cell.detailBtn.setTitle("Details", for: UIControlState.normal)
            cell.detailBtn.addTarget(self, action: #selector(self.detailsEvent), for: UIControlEvents.touchUpInside)
        }
        
    }
    
    // MARK: - UITableViewDelegate
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return tableView.fd_heightForCell(withIdentifier: "EventsCell") { cell in
            self.configureCell(cell as! EventsCell, at: indexPath)
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard =  UIStoryboard(name: "Main", bundle: nil)
        let promo = storyboard.instantiateViewController(withIdentifier: "EventDetailsViewController") as! EventDetailsViewController
        promo.eventID = String(prototypeEntitiesFromJSON[indexPath.row].eventId)
        promo.eventTitle = prototypeEntitiesFromJSON[indexPath.row].eventName
        self.navigationController?.pushViewController(promo, animated: true)
    }
    
    
    @objc func detailsEvent(_ sender: UIButton) {
        let backItem = UIBarButtonItem()
        backItem.title = "Back"
        backItem.tintColor = UIColor.black
        navigationItem.backBarButtonItem = backItem
        let point = self.tableView.convert(CGPoint.zero, from: sender)
        var event_id = ""
        var eventName = ""
        if let indexPath = self.tableView.indexPathForRow(at: point) {
            event_id = String(self.prototypeEntitiesFromJSON[indexPath.row].value(forKey: "eventId") as! Int)
            eventName = self.prototypeEntitiesFromJSON[indexPath.row].value(forKey: "eventName") as! String
        }
        let storyboard =  UIStoryboard(name: "Main", bundle: nil)
        let promo = storyboard.instantiateViewController(withIdentifier: "EventDetailsViewController") as! EventDetailsViewController
        promo.eventID = event_id
        promo.eventTitle = eventName
        self.navigationController?.pushViewController(promo, animated: true)
    }
    
    @objc func hideBtnClk(_ sender: UIButton) {
        DispatchQueue.main.async {
            let refreshAlert = UIAlertController(title: "", message: "Are you sure you want to HIDE this event, You can always find hidden events by clicking on the FILTER icon.", preferredStyle: UIAlertControllerStyle.alert)
            
            refreshAlert.addAction(UIAlertAction(title: "No, I don’t want to hide", style: .cancel , handler: { (action: UIAlertAction!) in
                
            }))
            
            
            refreshAlert.addAction(UIAlertAction(title: "Yes, I want to hide", style: .default, handler: { (action: UIAlertAction!) in
                let point = self.tableView.convert(CGPoint.zero, from: sender)
                var event_id = ""
                if let indexPath = self.tableView.indexPathForRow(at: point) {
                    event_id = String(self.prototypeEntitiesFromJSON[indexPath.row].value(forKey: "eventId") as! Int)
                    
                }
                
                let manager = Alamofire.SessionManager.default
                manager.session.configuration.timeoutIntervalForRequest = 120
                
                let parameters : [String: String] = [:]
                
                manager.request(baseURL + "api/Events/hideEvents/?providerId=" + userID + "&EventId=" + event_id, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
                    
                    switch(response.result) {
                    case .success(_):
                        if response.result.value != nil{
                            //print(response.result.value as Any)
                        }
                        let alert: UIAlertView = UIAlertView(title: "", message: "This event is now hidden", delegate: nil, cancelButtonTitle: "OK");
                        alert.show()
                        self.refresh()
                        break
                        
                    case .failure(_):
                        print(response.error!.localizedDescription)
                        let alert: UIAlertView = UIAlertView(title: "", message: response.error!.localizedDescription, delegate: nil, cancelButtonTitle: "OK");
                        alert.show()
                        self.view.hideActivityView()
                        print(response.result.error as Any)
                        break
                        
                    }
                    
                }
            }))
            self.present(refreshAlert, animated: true, completion: nil)
            
        }
    }
    
    @IBAction func pushProfileBtnClk(_ sender: Any) {
        let storyboard =  UIStoryboard(name: "Main", bundle: nil)
        let promo = storyboard.instantiateViewController(withIdentifier: "SPProfileViewController") as! SPProfileViewController
        self.navigationController?.pushViewController(promo, animated: true)
    }
    
    @IBAction func retryBtnClk(_ sender: Any) {
        self.loader.isHidden = false
        self.errorView.isHidden = true
        self.tableView.isHidden = true
        self.noInternetView.isHidden = true
        buildTestData {
            
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
}


