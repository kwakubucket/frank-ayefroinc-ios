//
//  FilterViewController.swift
//  AyefroInc
//
//  Created by Vijay Darkonde on 29/11/17.
//  Copyright © 2017 Inceptive Consulting Pvt. Ltd. All rights reserved.
//

import UIKit
import Alamofire

protocol addServices
{
    func addService(services: String, idArr: [String])
}

class FilterViewController: UIViewController, addServices{
    
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var firstCard: UIView!
    @IBOutlet var locationTF: SearchTextField!
    
    @IBOutlet var secondCard: UIView!
    @IBOutlet var serviceBtn: UIButton!
    
    @IBOutlet var thirdCard: UIView!
    @IBOutlet var startTF: NiceTextField!
    @IBOutlet var endTF: NiceTextField!
    
    
    @IBOutlet var forthCard: UIView!
    @IBOutlet var currencyLbl: UILabel!
    @IBOutlet var currencyLbl2: UILabel!
    @IBOutlet var minPriceTF: NiceTextField!
    @IBOutlet var maxPriceTF: NiceTextField!
    
    @IBOutlet weak var scollHeight: NSLayoutConstraint!
    @IBOutlet var fifthCard: UIView!
    @IBOutlet var checkBox: VKCheckbox!
    
    var delegate: filterEvents!
    var checkBoxValue: Bool! = false
    let date = Date()
    var btnclk : String! = ""
    var screenRect = UIScreen.main.bounds
    var coverView = UIView()
    var popViewController : CategoryPopupViewController!
    var start_Date: Date!
    var end_Date: Date!
    var selectedCategory: [String] = []

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Filter Events"

//        firstCard.dropShadow()
//        secondCard.dropShadow()
//        thirdCard.dropShadow()
//        forthCard.dropShadow()
//        fifthCard.dropShadow()
        
        checkBox.checkboxValueChangedBlock = {
            isOn in
            print("Basic checkbox is \(isOn ? "ON" : "OFF")")
            self.checkBoxValue = isOn
        }
        
        coverView = UIView(frame: screenRect)
        
        
        let tapGesture1: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(ForumDetailViewController.removeView))
        self.view.addGestureRecognizer(tapGesture1)
        
        
        let prefs: UserDefaults? = UserDefaults.standard
        let currencySymbol: String? = prefs?.string(forKey: "currencySymbol")
        
        currencyLbl.text = currencySymbol
        currencyLbl2.text = currencySymbol
        
        locationTF.filterStrings(locationArr)
        
        if UIScreen.main.bounds.size.width == 414
        {
            
        }
        else if UIScreen.main.bounds.size.width == 375 && UIScreen.main.bounds.size.height == 667
        {
            scollHeight.constant = 0
            scrollView.isScrollEnabled = false
        }
        else if UIScreen.main.bounds.size.width == 320 && UIScreen.main.bounds.size.height == 568
        {
            scollHeight.constant = 60
        }
        else if UIScreen.main.bounds.size.width == 320 && UIScreen.main.bounds.size.height == 480
        {
            scollHeight.constant = 160
        }
        
    }
    
    override func viewDidLayoutSubviews() {
        if UIScreen.main.bounds.size.width == 414
        {
            self.scrollView.isUserInteractionEnabled = true
            self.scrollView.contentSize = CGSize(width: 414, height: 400)
        }
        else if UIScreen.main.bounds.size.width == 375 && UIScreen.main.bounds.size.height == 667
        {
            self.scrollView.isUserInteractionEnabled = true
            self.scrollView.contentSize = CGSize(width: 375, height: 604)
        }
        else if UIScreen.main.bounds.size.width == 320 && UIScreen.main.bounds.size.height == 568
        {
            self.scrollView.isUserInteractionEnabled = true
            self.scrollView.contentSize = CGSize(width: 320, height: 660)
        }
        else
        {
            self.scrollView.isUserInteractionEnabled = true
            self.scrollView.contentSize = CGSize(width: 320, height: 560)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.setBackgroundImage(nil, for: .default)
        self.navigationController?.navigationBar.shadowImage = nil
        UIApplication.shared.statusBarView?.backgroundColor = UIColor(red: 247.0/255.0, green: 247.0/255.0, blue: 247.0/255.0, alpha: 1.0)

    }
    
    @objc func removeView()  {
        coverView.removeFromSuperview()
    }
    
    @IBAction func applyBtnClk(_ sender: Any) {
        if (startTF.text != "" && endTF.text == ""){
            let alert: UIAlertView = UIAlertView(title: "", message: "Please Enter End Date.", delegate: nil, cancelButtonTitle: "OK");
            alert.show()
        }
        else if (endTF.text != "" && startTF.text == ""){
            let alert: UIAlertView = UIAlertView(title: "", message: "Please Enter Start Date.", delegate: nil, cancelButtonTitle: "OK");
            alert.show()
        }
        else if (endTF.text != "" && startTF.text != ""){
            let dateValue = compareDate(dateInitial: start_Date, dateFinal: end_Date)
            if dateValue == false{
                let alert: UIAlertView = UIAlertView(title: "", message: "End Date should be greater than Start Date.", delegate: nil, cancelButtonTitle: "OK");
                alert.show()
            }
            else{
                if (minPriceTF.text != "" && maxPriceTF.text == ""){
                    let alert: UIAlertView = UIAlertView(title: "", message: "Please Enter Max Price.", delegate: nil, cancelButtonTitle: "OK");
                    alert.show()
                }
                else if (maxPriceTF.text != "" && minPriceTF.text == ""){
                    let alert: UIAlertView = UIAlertView(title: "", message: "Please Enter Min Price.", delegate: nil, cancelButtonTitle: "OK");
                    alert.show()
                }
                else if (minPriceTF.text != "" && maxPriceTF.text != ""){
                    let minPrice = Int(minPriceTF.text!)
                    let maxPrice = Int(maxPriceTF.text!)
                    
                    if (minPrice! > maxPrice!){
                        let alert: UIAlertView = UIAlertView(title: "", message: "Max Price should be greater than Min Price.", delegate: nil, cancelButtonTitle: "OK");
                        alert.show()
                    }
                    else{
                        self.applyFilter()
                    }
                }
                else{
                    self.applyFilter()
                }
            }
        }
        else if (minPriceTF.text != "" && maxPriceTF.text == ""){
            let alert: UIAlertView = UIAlertView(title: "", message: "Please Enter Max Price.", delegate: nil, cancelButtonTitle: "OK");
            alert.show()
        }
        else if (maxPriceTF.text != "" && minPriceTF.text == ""){
            let alert: UIAlertView = UIAlertView(title: "", message: "Please Enter Min Price.", delegate: nil, cancelButtonTitle: "OK");
            alert.show()
        }
        else if (minPriceTF.text != "" && maxPriceTF.text != ""){
            let minPrice = Int(minPriceTF.text!)
            let maxPrice = Int(maxPriceTF.text!)
            
            if (minPrice! > maxPrice!){
                let alert: UIAlertView = UIAlertView(title: "", message: "Max Price should be greater than Min Price.", delegate: nil, cancelButtonTitle: "OK");
                alert.show()
            }
            else{
                self.applyFilter()
            }
        }
        else{
            self.otherValidation()
        }
    }
    
    func otherValidation()  {
        if (locationTF.text?.trimmingCharacters(in: .whitespaces).isEmpty)! && selectedCategory.count == 0 && self.checkBoxValue == false{
            // string contains non-whitespace characters
            let alert: UIAlertView = UIAlertView(title: "Please select at least one filter", message: "", delegate: nil, cancelButtonTitle: "OK");
            alert.show()
        }
        
        else{
            self.applyFilter()
        }
    }
    
    func applyFilter()  {
        self.view.showActivityView(withLabel: "Loading")
        DispatchQueue.main.async {
            let parameters : [String: Any] = [
                "location": self.locationTF.text as! String,
                "services": self.selectedCategory as [String],
                "min_price_range": self.minPriceTF.text as! String,
                "max_price_range": self.maxPriceTF.text as! String,
                "event_start_date_range":self.startTF.text as! String,
                "event_end_date_range":self.endTF.text as! String,
                "show_all_hidden_event": self.checkBoxValue,
                "providerId": userID
            ]
            
            let manager = Alamofire.SessionManager.default
            manager.session.configuration.timeoutIntervalForRequest = 120
            
            manager.request(baseURL + "api/Events/searchEvents", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
                
                switch(response.result) {
                case .success(_):
                    if response.result.value != nil{
                        //print(response.result.value as Any)
                    }
                    let jsonResults : NSArray
                    jsonResults = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! NSArray
                    self.view.hideActivityView()
                    if jsonResults.count != 0{
                        self.navigationController?.popViewController(animated: false)
                        self.delegate.filterEventView(arr: jsonResults)
                    }
                    else{
                        let alert: UIAlertView = UIAlertView(title: "", message: "No Events with this Selected Filter", delegate: nil, cancelButtonTitle: "OK");
                        alert.show()
                    }
                    break
                    
                case .failure(_):
                    print(response.error!.localizedDescription)
                    let alert: UIAlertView = UIAlertView(title: "", message: response.error!.localizedDescription, delegate: nil, cancelButtonTitle: "OK");
                    alert.show()
                    self.view.hideActivityView()
                    print(response.result.error as Any)
                    break
                    
                }
            }
        }
    }
    
    @IBAction func serviceBtnClk(_ sender: Any) {
        let backItem = UIBarButtonItem()
        backItem.title = "Back"
        backItem.tintColor = UIColor.black
        navigationItem.backBarButtonItem = backItem
        self.popViewController = self.storyboard?.instantiateViewController(withIdentifier: "CategoryPopupViewController")as! CategoryPopupViewController
        self.popViewController.delagate = self
        self.popViewController.selectedCategory = self.selectedCategory
        self.navigationController?.pushViewController(self.popViewController, animated: true)
    }
    
    @IBAction func startDateTFClk(_ sender: NiceTextField) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        let date = dateFormatter.string(from: NSDate() as Date)
        startTF.text = date
        start_Date = NSDate() as Date
        
        let datePickerView:UIDatePicker = UIDatePicker()
        datePickerView.datePickerMode = UIDatePickerMode.date
        sender.inputView = datePickerView
        datePickerView.addTarget(self, action: #selector(FilterViewController.startDate), for: UIControlEvents.valueChanged)
    }
    
    @IBAction func endDateTFClk(_ sender: NiceTextField) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        let date = dateFormatter.string(from: NSDate() as Date)
        endTF.text = date
        end_Date = NSDate() as Date
        let datePickerView:UIDatePicker = UIDatePicker()
        datePickerView.datePickerMode = UIDatePickerMode.date
        sender.inputView = datePickerView
        datePickerView.addTarget(self, action: #selector(FilterViewController.endDate), for: UIControlEvents.valueChanged)
    }
    
    @IBAction func startdatecalbtnclk(_ sender: UIButton) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        let date = dateFormatter.string(from: NSDate() as Date)
        startTF.text = date
        start_Date = NSDate() as Date
        
        let datePickerView:UIDatePicker = UIDatePicker()
        datePickerView.datePickerMode = UIDatePickerMode.date
        startTF.inputView = datePickerView
        startTF.becomeFirstResponder()
        datePickerView.addTarget(self, action: #selector(FilterViewController.startDate), for: UIControlEvents.valueChanged)
    }
    
    @IBAction func enddatecalbtnclk(_ sender: UIButton) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        let date = dateFormatter.string(from: NSDate() as Date)
        endTF.text = date
        end_Date = NSDate() as Date
        let datePickerView:UIDatePicker = UIDatePicker()
        datePickerView.datePickerMode = UIDatePickerMode.date
        endTF.inputView = datePickerView
        endTF.becomeFirstResponder()
        datePickerView.addTarget(self, action: #selector(FilterViewController.endDate), for: UIControlEvents.valueChanged)
    }
    
    
    @objc func startDate(sender:UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        let date = dateFormatter.string(from: sender.date)
        start_Date = sender.date
        startTF.text = date
    }
    
    
    func compareDate(dateInitial:Date, dateFinal:Date) -> Bool {
        let order = Calendar.current.compare(dateInitial, to: dateFinal, toGranularity: .day)
        
        switch order {
        case .orderedAscending:
            print("\(dateFinal) is after \(dateInitial)")
            return true
        case .orderedDescending:
            print("\(dateFinal) is before \(dateInitial)")
            return false
        default:
            print("\(dateFinal) is the same as \(dateInitial)")
            return true
        }
    }

    @objc func endDate(sender:UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        let date = dateFormatter.string(from: sender.date)
        end_Date = sender.date
        endTF.text = date
    }
    
    func addService(services: String, idArr: [String]) {
        selectedCategory = idArr
        self.serviceBtn.setTitle(services + " Services", for: UIControlState.normal)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

