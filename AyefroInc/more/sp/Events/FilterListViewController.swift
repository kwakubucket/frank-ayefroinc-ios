//
//  FilterListViewController.swift
//  AyefroInc
//
//  Created by Vijay Darkonde on 29/11/17.
//  Copyright © 2017 Inceptive Consulting Pvt. Ltd. All rights reserved.
//

import UIKit
import Alamofire

class FilterListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var prototypeEntitiesFromJSON: [FilterEntity] = []
    
    
    @IBOutlet var tableView: UITableView!
    var jsonResults: NSArray!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Filter Events"
        
        self.tableView.register(UINib.init(nibName: "FilterEventCell", bundle: nil), forCellReuseIdentifier: "FilterEventCell")
        self.tableView.fd_debugLogEnabled = true
        self.tableView.tableFooterView = UIView()
        
        let jsonArr = NSMutableArray()
        for i in 0 ..< jsonResults.count{
            let dicWithoutNulls = jsonResults[i] as! NSDictionary
            
            let outputDict = self.removeNSNull(from: dicWithoutNulls.mutableCopy() as! NSMutableDictionary)
            
            jsonArr.add(outputDict)
        }
        
        let arr = NSArray(array: jsonArr)
        for i in 0 ..< arr.count{
            self.prototypeEntitiesFromJSON.append(FilterEntity(dictionary: arr[i] as! [AnyHashable : Any] ))
        }
        
        self.tableView.reloadData()
        
    }
    
    override func viewDidLayoutSubviews() {
        self.automaticallyAdjustsScrollViewInsets = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.setBackgroundImage(nil, for: .default)
        self.navigationController?.navigationBar.shadowImage = nil
        UIApplication.shared.statusBarView?.backgroundColor = UIColor(red: 247.0/255.0, green: 247.0/255.0, blue: 247.0/255.0, alpha: 1.0)

    }
    
    func removeNSNull(from dict: NSMutableDictionary) -> NSDictionary {
        let mutableDict = dict
        let keysWithEmptString = dict.filter { $0.1 is NSNull }.map { $0.0 }
        for key in keysWithEmptString {
            mutableDict[key] = ""
        }
        
        return mutableDict
    }
    
    // MARK: - UITableViewDataSource
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let count = prototypeEntitiesFromJSON.count
        if count != 0 {
            return count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FilterEventCell") as? FilterEventCell
        if prototypeEntitiesFromJSON.count != 0{
            configureCell(cell!, at: indexPath)
        }
        return cell!
    }
    
    
    func configureCell(_ cell: FilterEventCell, at indexPath: IndexPath) {
        if prototypeEntitiesFromJSON.count != 0{
            cell.fd_enforceFrameLayout = true
            cell.entity = prototypeEntitiesFromJSON[indexPath.row]
            cell.hideBtn.addTarget(self, action: #selector(self.hideBtnClk), for: UIControlEvents.touchUpInside)
            cell.detailBtn.setTitle("Details", for: UIControlState.normal)
            cell.detailBtn.addTarget(self, action: #selector(self.detailsEvent), for: UIControlEvents.touchUpInside)
        }
        
    }
    
    @objc func detailsEvent(_ sender: UIButton) {
        let backItem = UIBarButtonItem()
        backItem.title = "Back"
        backItem.tintColor = UIColor.black
        navigationItem.backBarButtonItem = backItem
        let point = self.tableView.convert(CGPoint.zero, from: sender)
        var event_id = ""
        var eventName = ""
        if let indexPath = self.tableView.indexPathForRow(at: point) {
            event_id = String(self.prototypeEntitiesFromJSON[indexPath.row].value(forKey: "eventId") as! Int)
            eventName = self.prototypeEntitiesFromJSON[indexPath.row].value(forKey: "eventName") as! String
        }
        let storyboard =  UIStoryboard(name: "Main", bundle: nil)
        let promo = storyboard.instantiateViewController(withIdentifier: "EventDetailsViewController") as! EventDetailsViewController
        promo.eventID = event_id
        promo.eventTitle = eventName
        self.navigationController?.pushViewController(promo, animated: true)
    }
    
    // MARK: - UITableViewDelegate
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return tableView.fd_heightForCell(withIdentifier: "FilterEventCell") { cell in
            self.configureCell(cell as! FilterEventCell, at: indexPath)
        }
    }
    
    @objc func hideBtnClk(_ sender: UIButton) {
        let point = self.tableView.convert(CGPoint.zero, from: sender)
        var event_id = ""
        var isHidden : Bool! = false
        if let indexPath = self.tableView.indexPathForRow(at: point) {
            event_id = String(self.prototypeEntitiesFromJSON[indexPath.row].value(forKey: "eventId") as! Int)
            isHidden = self.prototypeEntitiesFromJSON[indexPath.row].value(forKey: "isEventHide") as! Bool
            if isHidden == true{
                let dict = self.prototypeEntitiesFromJSON[indexPath.row]
                dict.setValue(false, forKey: "isEventHide")
            }
            else{
                let dict = self.prototypeEntitiesFromJSON[indexPath.row]
                dict.setValue(true, forKey: "isEventHide")
            }
        }
        
        self.tableView.reloadData()
        
        DispatchQueue.main.async {
            let parameters : [String: String] = [:]
            let manager = Alamofire.SessionManager.default
            manager.session.configuration.timeoutIntervalForRequest = 120
            
            manager.request(baseURL + "api/Events/hideEvents/?providerId=" + userID + "&EventId=" + event_id, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
                
                switch(response.result) {
                case .success(_):
                    if response.result.value != nil{
                        //print(response.result.value as Any)
                    }
                    if isHidden == true{
                        let alert: UIAlertView = UIAlertView(title: "", message: "This event will no longer be hidden.", delegate: nil, cancelButtonTitle: "OK");
                        alert.show()
                    }
                    else{
                        let alert: UIAlertView = UIAlertView(title: "", message: "This event will be hidden until you unhide.", delegate: nil, cancelButtonTitle: "OK");
                        alert.show()
                    }
                    
                    
                    break
                    
                case .failure(_):
                    print(response.error!.localizedDescription)
                    let alert: UIAlertView = UIAlertView(title: "", message: response.error!.localizedDescription, delegate: nil, cancelButtonTitle: "OK");
                    alert.show()
                    self.view.hideActivityView()
                    print(response.result.error as Any)
                    break
                    
                }
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
}

