//
//  ExtraDetailsCel.swift
//  AyefroInc
//
//  Created by Vijay Darkonde on 15/12/17.
//  Copyright © 2017 Inceptive Consulting Pvt. Ltd. All rights reserved.
//

import UIKit

class ExtraDetailsCel: UITableViewCell {
    
    @IBOutlet var descriptionTV: UITextView!
    @IBOutlet var amountTF: NiceTextField!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
