//
//  SubscriptionViewController.swift
//  AyefroInc
//
//  Created by Vijay Darkonde on 06/01/18.
//  Copyright © 2018 Inceptive Consulting Pvt. Ltd. All rights reserved.
//

import UIKit
import Alamofire

class SubscriptionViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, cancelProto, successProto {
    
    
    var imgArr: [String] = ["bronze.png","silver.png","gold.png"]
    var amountArr: [String] = ["20","50","80"]
    var packTitleArr: [String] = ["Bronze Pack", "Silver Pack", "Gold Pack"]
    var pactDetailArr: [String] = ["Bid for up to 2 Services per event in a month","Bid for up to 4 Services per event in a month","Bid for unlimited number of Services per event in a month"]
    
    @IBOutlet var tableView: UITableView!
    var checkoutURL = ""
    var subPlanID : String = ""
    var delegate: subscribetionProto!
    var servicesCount: Int! = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Subscribe to a Package"

        self.tableView.register(UINib.init(nibName: "SubscriptionCell", bundle: nil), forCellReuseIdentifier: "SubscriptionCell")
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return packTitleArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SubscriptionCell", for: indexPath) as! SubscriptionCell
        cell.imgView.image = UIImage(named: imgArr[indexPath.row])
        cell.packTitle.text = packTitleArr[indexPath.row]
        cell.packDetails.text = pactDetailArr[indexPath.row]
        cell.amount.text = "GHS " +  amountArr[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        postSubscrptioData()
        if indexPath.row == 0 {
            subPlanID = "3"
            if servicesCount < 3{
                suscribetionPayment(amount: amountArr[indexPath.row], desc: packTitleArr[indexPath.row])
            }
            else{
                let refreshAlert = UIAlertController(title: "", message: "To bid for more than two services, please select silver or gold pack.", preferredStyle: UIAlertControllerStyle.alert)

                refreshAlert.addAction(UIAlertAction(title: "OK", style: .cancel , handler: { (action: UIAlertAction!) in

                }))

                self.present(refreshAlert, animated: true, completion: nil)
            }
        }
        if indexPath.row == 1 {
            subPlanID = "2"
            if servicesCount < 5{
                suscribetionPayment(amount: amountArr[indexPath.row], desc: packTitleArr[indexPath.row])
            }
            else{
                let refreshAlert = UIAlertController(title: "", message: "To bid for more than four services, please select gold pack.", preferredStyle: UIAlertControllerStyle.alert)

                refreshAlert.addAction(UIAlertAction(title: "OK", style: .cancel , handler: { (action: UIAlertAction!) in

                }))

                self.present(refreshAlert, animated: true, completion: nil)
            }
        }
        if indexPath.row == 2{
            subPlanID = "1"
            suscribetionPayment(amount: amountArr[indexPath.row], desc: packTitleArr[indexPath.row])
        }
        
    }
    
    func suscribetionPayment(amount: String, desc: String)  {
        self.view.showActivityView(withLabel: "Loading")
        let parameters : [String: Any] = [
            "invoice": [
                "items":[
                    "item_1": [
                        "name": desc,
                        "quantity": 1,
                        "unit_price": amount,
                        "total_price": amount,
                        "description": ""
                    ]
                ],
                "taxes": [
                    
                ],
                "total_amount": amount,
                "description": ""
            ],
            "store": [
                "name": "Ayefro Inc.",
                "tagline": "Event Planner",
                "postal_address": "House No. 4, Plot No. 54, Ashongman Estate",
                "phone": "+233243817622",
                "logo_url": "https://ayefroinc.files.wordpress.com/2017/11/cropped-logo3.png",
                "website_url": "https://ayefroinc.com/"
            ],
            "custom_data": [
                
            ],
            "actions": [
                "cancel_url": "https://ayefroinc.com/about/",
                "return_url": "https://ayefroinc.com/"
            ]
            
        ]
        
        
        let headers = [
            "Authorization": "Basic cGptZHd6aXM6dHZ6d25uZGw=",
            "Content-Type": "application/json"
        ]
        
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 120
        
        manager.request("https://api.hubtel.com/v1/merchantaccount/onlinecheckout/invoice/create", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case .success(_):
                if response.result.value != nil{
                    //print(response.result.value as Any)
                }
                let jsonResults : NSDictionary
                jsonResults = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! NSDictionary
                let response_code = jsonResults["response_code"] as! String
                if response_code == "00"{
                    let response_text = jsonResults["response_text"] as! String
                    let token = jsonResults["token"] as! String
                    self.checkoutURL = response_text
                    self.view.hideActivityView()
                    let backItem = UIBarButtonItem()
                    backItem.title = "Back"
                    backItem.tintColor = UIColor.black
                    self.navigationItem.backBarButtonItem = backItem
                    let storyboard =  UIStoryboard(name: "Main", bundle: nil)
                    let promo = storyboard.instantiateViewController(withIdentifier: "CheckOutViewController") as! CheckOutViewController
                    promo.webViewurl = self.checkoutURL
                    promo.cancelDel = self
                    promo.successDel = self
                    promo.token = token
                    self.navigationController?.pushViewController(promo, animated: true)
                }
                
                
                break
                
            case .failure(_):
                print(response.error!.localizedDescription)
                let alert: UIAlertView = UIAlertView(title: "", message: response.error!.localizedDescription, delegate: nil, cancelButtonTitle: "OK");
                alert.show()
                self.view.hideActivityView()
                print(response.result.error as Any)
                break
                
            }
        }
    }
    
    func cancelTransaction(){
        let alert: UIAlertView = UIAlertView(title: "", message: "Transaction failed", delegate: nil, cancelButtonTitle: "OK");
        alert.show()
    }
    
    func successTrasaction(){
        postSubscrptioData()
    }
    
    func convertDateFormater() -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy"
        return  dateFormatter.string(from: Date())
    }
    
    var tenDaysfromNow: Date {
        return (Calendar.current as NSCalendar).date(byAdding: .day, value: 30, to: Date(), options: [])!
    }
    
    func next30DaysDateFormater() -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy"
        return  dateFormatter.string(from: tenDaysfromNow)
    }
    
    func postSubscrptioData()  {
        DispatchQueue.main.async {
            self.view.showActivityView(withLabel: "Loading")
            let parameters : [String: Any] = [
                "SubPlanId": self.subPlanID,
                "SubStartDate": self.convertDateFormater(),
                "SubEndDate": self.next30DaysDateFormater(),
                "UserId": userID,
                ]
            
            let manager = Alamofire.SessionManager.default
            manager.session.configuration.timeoutIntervalForRequest = 120
            
            manager.request(baseURL + "api/SubscriptionMappings/PostSubscription", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
                
                switch(response.result) {
                case .success(_):
                    if response.result.value != nil{
                        //print(response.result.value as Any)
                    }
                    self.view.hideActivityView()
                    self.delegate.subscribtionSuccess()
                    self.navigationController?.popViewController(animated: true)
                    
                    break
                    
                case .failure(_):
                    print(response.error!.localizedDescription)
                    let alert: UIAlertView = UIAlertView(title: "", message: response.error!.localizedDescription, delegate: nil, cancelButtonTitle: "OK");
                    alert.show()
                    self.view.hideActivityView()
                    print(response.result.error as Any)
                    break
                    
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 220
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}
