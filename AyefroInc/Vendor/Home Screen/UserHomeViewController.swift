//
//  UserHomeViewController.swift
//  AyefroInc
//
//  Created by Vijay Darkonde on 14/11/17.
//  Copyright © 2017 Inceptive Consulting Pvt. Ltd. All rights reserved.
//

import UIKit
import ImageSlideshow
import SDWebImage
import Alamofire
import MessageUI
import SystemConfiguration

protocol emailSend {
    func emailBox(email: String)
}

class UserHomeViewController: UIViewController , UISearchControllerDelegate, UISearchResultsUpdating, UISearchBarDelegate, UITableViewDelegate, UITableViewDataSource, UICollectionViewDataSource, UICollectionViewDelegate, emailSend,  MFMailComposeViewControllerDelegate {
    
    
    @IBOutlet weak var twitterX: NSLayoutConstraint!
    @IBOutlet weak var fbX: NSLayoutConstraint!
    @IBOutlet weak var instaX: NSLayoutConstraint!
    @IBOutlet weak var actionImg: UIImageView!
    @IBOutlet weak var sponsoredImg: UIImageView!
    @IBOutlet weak var twitterBtn: UIButton!
    @IBOutlet weak var instaBtn: UIButton!
    @IBOutlet weak var fbBtn: UIButton!
    @IBOutlet weak var linksView: UIView!
    @IBOutlet weak var vendorCollectionView: UICollectionView!
    @IBOutlet weak var imageSlider: ImageSlideshow!
    @IBOutlet weak var scrollHeight: NSLayoutConstraint!
    var searchController : UISearchController!
    var tableView = UITableView()
    var sdWebImageSource = [SDWebImageSource(urlString: "https://images.unsplash.com/photo-1432679963831-2dab49187847?w=1080")!, SDWebImageSource(urlString: "https://images.unsplash.com/photo-1447746249824-4be4e1b76d66?w=1080")!, SDWebImageSource(urlString: "https://images.unsplash.com/photo-1463595373836-6e0b0a8ee322?w=1080")!]
    
    var imgArr : [String] = []
    var nameArr : [String] = []
    var ratingArr : [String] = []
    var idArr : [String] = []
    
    var searchID : [String] = []
    var searchName : [String] = []
    var searchType: [String] = []
    
    var ImageUrlArr : [String] = []
    var PromotionAimArr : [String] = []
    var ProviderIdArr : [String] = []
    var WebsiteLinkArr : [String] = []
    var contactDetailsArr : [String] = []
    var SocialMedia_facebookArr : [String] = []
    var SocialMedia_InstagramArr : [String] = []
    var SocialMedia_twitterArr : [String] = []
    var selectedImageIndex : Int = 0
    var popViewController : ContactPopUpViewController!

    var fbURL : String! = ""
    var instaURL: String! = ""
    var twitter: String! = ""

    var email: String! = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.searchController = UISearchController(searchResultsController:  nil)
        
        self.tableView.frame = self.view.frame
        self.tableView.isHidden = true
        
        self.searchController.searchResultsUpdater = self
        self.searchController.delegate = self
        self.searchController.searchBar.delegate = self
        self.searchController.hidesNavigationBarDuringPresentation = false
        self.searchController.dimsBackgroundDuringPresentation = false
        self.searchController.searchBar.placeholder = "Search by Name, Category or Location"
        self.navigationItem.titleView = searchController.searchBar
        
        self.definesPresentationContext = true
        
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "Cell")
        tableView.dataSource = self
        tableView.delegate = self
        self.tableView.tableFooterView = UIView()
        self.view.addSubview(tableView)
        
        if UIScreen.main.bounds.size.width == 414
        {
            let textFieldInsideSearchBar = self.searchController.searchBar.value(forKey: "searchField") as! UITextField
            textFieldInsideSearchBar.font = UIFont.systemFont(ofSize: 16)
        }
        else if UIScreen.main.bounds.size.width == 375 && UIScreen.main.bounds.size.height == 667
        {
            let textFieldInsideSearchBar = self.searchController.searchBar.value(forKey: "searchField") as! UITextField
            textFieldInsideSearchBar.font = UIFont.systemFont(ofSize: 14)
        }
        else if UIScreen.main.bounds.size.width == 320 && UIScreen.main.bounds.size.height == 568
        {
            let textFieldInsideSearchBar = self.searchController.searchBar.value(forKey: "searchField") as! UITextField
            textFieldInsideSearchBar.font = UIFont.systemFont(ofSize: 12)
        }
        else if UIScreen.main.bounds.size.width == 320 && UIScreen.main.bounds.size.height == 480
        {
            let textFieldInsideSearchBar = self.searchController.searchBar.value(forKey: "searchField") as! UITextField
            textFieldInsideSearchBar.font = UIFont.systemFont(ofSize: 12)
        }
        else{
            let textFieldInsideSearchBar = self.searchController.searchBar.value(forKey: "searchField") as! UITextField
            textFieldInsideSearchBar.font = UIFont.systemFont(ofSize: 14)
        }
        
        let tapGesture1: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UserHomeViewController.removeView))
        self.view.addGestureRecognizer(tapGesture1)
        
        let recognizer = UITapGestureRecognizer(target: self, action: #selector(UserHomeViewController.didTap))
        imageSlider.addGestureRecognizer(recognizer)
        
        
        if UIScreen.main.bounds.size.width == 320 && UIScreen.main.bounds.size.height == 568
        {
            scrollHeight.constant = 50
        }
        else if UIScreen.main.bounds.size.width == 320 && UIScreen.main.bounds.size.height == 480
        {
            scrollHeight.constant = 100
        }
        
        self.linksView.isHidden = true
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            if imageNotification == true{
                let backItem = UIBarButtonItem()
                backItem.title = "Back"
                backItem.tintColor = UIColor.black
                self.navigationItem.backBarButtonItem = backItem
                let storyboard =  UIStoryboard(name: "Main", bundle: nil)
                let promo = storyboard.instantiateViewController(withIdentifier: "ImageDetailViewController") as! ImageDetailViewController
                promo.vendorID = v_id
                promo.QuestionId = i_id
                imageNotification = true
                self.navigationController?.pushViewController(promo, animated: true)
            }
            
            if imageUpload == true{
                let backItem = UIBarButtonItem()
                backItem.title = "Back"
                backItem.tintColor = UIColor.black
                self.navigationItem.backBarButtonItem = backItem
                let storyboard =  UIStoryboard(name: "Main", bundle: nil)
                let promo = storyboard.instantiateViewController(withIdentifier: "ViewGalleryController") as! ViewGalleryController
                promo.vendorID = ven_id
                self.navigationController?.pushViewController(promo, animated: true)
                imageUpload = false
            }
        }
        
        if isConnectedToNetwork() == true{
            imageSlider.currentPageChanged = { page in
                self.selectedImageIndex = page
                if self.PromotionAimArr[page] == "Get more calls for your business"{
                    self.actionImg.image = UIImage(named: "callnow.png")
                }
                else{
                    self.actionImg.image = UIImage(named: "learnmore.png")
                }
            }
        }
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        _ = ModelManager.instance.getUserLoginStatus()
        imageSlider.backgroundColor = UIColor.white
        imageSlider.pageControl.currentPageIndicatorTintColor = UIColor.clear
        imageSlider.pageControl.pageIndicatorTintColor = UIColor.clear
        imageSlider.slideshowInterval = 10.0
        imageSlider.contentScaleMode = UIViewContentMode.scaleAspectFit
        imageSlider.activityIndicator = nil
        selectedImageIndex = 0
        imageSlider.setCurrentPage(0, animated: true)
        
        self.tabBarController?.tabBar.isHidden = false
        getAllImages()
        sdWebImageSource = []
        
        actionImg.image = nil
        sponsoredImg.image = nil
    }
    
    func isConnectedToNetwork() -> Bool {
        
        var zeroAddress = sockaddr_in(sin_len: 0, sin_family: 0, sin_port: 0, sin_addr: in_addr(s_addr: 0), sin_zero: (0, 0, 0, 0, 0, 0, 0, 0))
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        
        return (isReachable && !needsConnection)
        
    }
    
    @objc func didTap() {
        if isConnectedToNetwork() == true{
            print(self.ProviderIdArr)
            print(self.PromotionAimArr)
            if self.PromotionAimArr.count != 0{
                if PromotionAimArr[self.selectedImageIndex] == "Increase brand Awareness"{
                    let backItem = UIBarButtonItem()
                    backItem.title = "Back"
                    backItem.tintColor = UIColor.black
                    navigationItem.backBarButtonItem = backItem
                    let storyboard =  UIStoryboard(name: "Main", bundle: nil)
                    let promo = storyboard.instantiateViewController(withIdentifier: "VendorDetailsViewController") as! VendorDetailsViewController
                    promo.vendorID = self.ProviderIdArr[self.selectedImageIndex]
                    promo.poptoRoot = true
                    self.navigationController?.pushViewController(promo, animated: true)
                }
                if PromotionAimArr[self.selectedImageIndex] == "Get more website visits"{
                    
                    var websiteList = WebsiteLinkArr[selectedImageIndex]
                    if websiteList != ""{
                        let refreshAlert = UIAlertController(title: "", message: "Click to view website", preferredStyle: UIAlertControllerStyle.alert)
                        
                        refreshAlert.addAction(UIAlertAction(title: "No", style: .cancel , handler: { (action: UIAlertAction!) in
                            
                        }))
                        
                        refreshAlert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (action: UIAlertAction!) in
                            let str = websiteList.contains(find: "http://")
                            let str1 = websiteList.contains(find: "https://")
                            if str == false && str1 == false{
                                websiteList = "http://" + websiteList
                            }
                            guard let url = URL(string: websiteList) else {
                                return //be safe
                            }
                            
                            if #available(iOS 10.0, *) {
                                UIApplication.shared.open(url, options: [:], completionHandler: nil)
                            } else {
                                UIApplication.shared.openURL(url)
                            }
                        }))
                        
                        self.present(refreshAlert, animated: true, completion: nil)
                        
                    }
                    else{
                        let alert: UIAlertView = UIAlertView(title: "", message: "Website link not updated by service provider", delegate: nil, cancelButtonTitle: "OK");
                        alert.show()
                    }
                }
                if PromotionAimArr[self.selectedImageIndex] == "Get more calls for your business"{
                    self.imageSlider.isUserInteractionEnabled = false
                    DispatchQueue.main.async {
                        let manager = Alamofire.SessionManager.default
                        manager.session.configuration.timeoutIntervalForRequest = 120
                        
                        manager.request( baseURL + "api/Albums/GetContactDetails/?providerId=" + self.ProviderIdArr[self.selectedImageIndex], method: .get, parameters: ["":""], encoding: URLEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
                            
                            switch(response.result) {
                            case .success(_):
                                if response.result.value != nil{
                                    //print(response.result.value as Any)
                                }
                                
                                let jsonResults : NSArray
                                jsonResults = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! NSArray
                                let dict = jsonResults[0] as! NSDictionary
                                let phoneNumber = dict["phoneNumber"] as! String
                                let email = dict["email"] as! String
                                
                                self.popViewController = self.storyboard?.instantiateViewController(withIdentifier: "ContactPopUpViewController")as! ContactPopUpViewController
                                self.popViewController.phoneNum = phoneNumber
                                self.popViewController.emailID = email
                                self.popViewController.delegate = self
                                self.popViewController.showInView(self.view, animated: true)
                                self.imageSlider.isUserInteractionEnabled = true
                                break
                                
                            case .failure(_):
                                if response.error?.localizedDescription == "The Internet connection appears to be offline."
                                {
                                    let alert: UIAlertView = UIAlertView(title: "", message: response.error!.localizedDescription, delegate: nil, cancelButtonTitle: "OK");
                                    alert.show()
                                }
                                else{
                                    let alert: UIAlertView = UIAlertView(title: "", message: response.error!.localizedDescription, delegate: nil, cancelButtonTitle: "OK");
                                    alert.show()
                                    print(response.result.error as Any)
                                }
                                break
                                
                            }
                        }
                    }
                }
                if PromotionAimArr[self.selectedImageIndex] == "Promote On Social Media"{
                    self.linksView.isHidden = false
                    self.fbURL = SocialMedia_facebookArr[self.selectedImageIndex]
                    self.instaURL = SocialMedia_InstagramArr[self.selectedImageIndex]
                    self.twitter = SocialMedia_twitterArr[self.selectedImageIndex]
                    
                    if self.fbURL == ""{
                        self.fbBtn.setImage(UIImage(named:"fbgray"), for: UIControlState.normal)
                    }
                    else{
                        self.fbBtn.setImage(UIImage(named:"facebook"), for: UIControlState.normal)
                    }
                    if self.instaURL == ""{
                        self.instaBtn.setImage(UIImage(named:"instagray"), for: UIControlState.normal)
                    }
                    else{
                        self.instaBtn.setImage(UIImage(named:"instagram-1"), for: UIControlState.normal)
                    }
                    if self.twitter == ""{
                        self.twitterBtn.setImage(UIImage(named:"twittergray"), for: UIControlState.normal)
                    }
                    else{
                        self.twitterBtn.setImage(UIImage(named:"twitter"), for: UIControlState.normal)
                    }
                    
                    if self.fbURL == "" && self.instaURL == "" && self.twitter != ""{
                        self.twitterX.constant = 0.4
                    }
                    else if self.fbURL == "" && self.instaURL != "" && self.twitter != ""{
                        self.instaX.constant = 0.4
                        self.twitterX.constant = 0
                    }
                    else if self.instaURL == "" && self.twitter != ""{
                        self.twitterX.constant = 0
                    }
                    
                    
                }
            }
        }
        
        
    }
    
    func emailBox(email: String){
        if MFMailComposeViewController.canSendMail() {
            let toRecipients : [String] = [email]
            let mc: MFMailComposeViewController = MFMailComposeViewController()
            mc.mailComposeDelegate = self
            mc.setToRecipients(toRecipients)
            mc.isNavigationBarHidden = true
            self.present(mc, animated: false, completion: nil)
        }
        else
        {
            let alert: UIAlertView = UIAlertView(title: "", message: "Mail app not install on your device. Please send an email to info@ayefroinc.com.com", delegate: nil, cancelButtonTitle: "OK");
            alert.show()
        }
    }
    
    @objc func removeView()  {
        linksView.isHidden = true
    }
    
    func getAllImages()  {
        ImageUrlArr = []
        PromotionAimArr = []
        ProviderIdArr = []
        WebsiteLinkArr = []
        contactDetailsArr = []
        SocialMedia_facebookArr = []
        SocialMedia_twitterArr = []
        SocialMedia_InstagramArr = []
        sdWebImageSource = []
        
        DispatchQueue.main.async {
            let manager = Alamofire.SessionManager.default
            manager.session.configuration.timeoutIntervalForRequest = 120
            manager.request( baseURL + "api/PromotedImages/GetAdvertiseList", method: .get, parameters: ["":""], encoding: URLEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
                
                switch(response.result) {
                case .success(_):
                    if response.result.value != nil{
                        //print(response.result.value as Any)
                    }
                    self.getVendorDetails()
                    let jsonResults : NSDictionary
                    jsonResults = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! NSDictionary
                    let status = jsonResults["Success"] as! String
                    if status == "Success"{
                        let Advertiselist = jsonResults["Advertiselist"] as! NSArray
                        if Advertiselist.count != 0{
                            for i in 0 ..< Advertiselist.count{
                                let data = Advertiselist[i] as! NSDictionary
                                let ImageUrl = data["ImageUrl"] as! String
                                var PromotionAim = ""
                                if ( data["PromotionAim"] as? String) != nil{
                                    PromotionAim = data["PromotionAim"] as! String
                                }
                                
                                var ProviderId = ""
                                if ( data["ProviderId"] as? String) != nil{
                                    ProviderId = data["ProviderId"] as! String
                                }
                                
                                var WebsiteLink = ""
                                if ( data["WebsiteLink"] as? String) != nil{
                                    WebsiteLink = data["WebsiteLink"] as! String
                                }
                                
                                var contactDetails = ""
                                if ( data["contactDetails"] as? String) != nil{
                                    contactDetails = data["contactDetails"] as! String
                                }
                                
                                var SocialMedia_facebook = ""
                                if ( data["SocialMedia_facebook"] as? String) != nil{
                                    SocialMedia_facebook = data["SocialMedia_facebook"] as! String
                                }
                                
                                var SocialMedia_Instagram = ""
                                if ( data["SocialMedia_Instagram"] as? String) != nil{
                                    SocialMedia_Instagram = data["SocialMedia_Instagram"] as! String
                                }
                                
                                var SocialMedia_twitter = ""
                                if ( data["SocialMedia_twitter"] as? String) != nil{
                                    SocialMedia_twitter = data["SocialMedia_twitter"] as! String
                                }
                                
                                let str: String? = Bundle.main.path(forResource: "loading_spinner", ofType: "gif")
                                let fileData = NSData(contentsOfFile: str!)
                                
                                self.sdWebImageSource.append(SDWebImageSource(urlString: ImageUrl, placeholder: UIImage.sd_animatedGIF(with: fileData! as Data))!)
                                self.ImageUrlArr.append(ImageUrl)
                                self.PromotionAimArr.append(PromotionAim)
                                self.ProviderIdArr.append(ProviderId)
                                self.WebsiteLinkArr.append(WebsiteLink)
                                self.contactDetailsArr.append(contactDetails)
                                self.SocialMedia_facebookArr.append(SocialMedia_facebook)
                                self.SocialMedia_twitterArr.append(SocialMedia_twitter)
                                self.SocialMedia_InstagramArr.append(SocialMedia_Instagram)
                            }
                            if Advertiselist.count != 0{
                                self.sponsoredImg.image = UIImage(named:"sponsored.png")
                                if self.PromotionAimArr[0] == "Get more calls for your business"{
                                    self.actionImg.image = UIImage(named: "callnow.png")
                                }
                                else{
                                    self.actionImg.image = UIImage(named: "learnmore.png")
                                }
                            }
                            self.imageSlider.setImageInputs(self.sdWebImageSource)
                            self.vendorCollectionView.reloadData()
                            self.imageSlider.isUserInteractionEnabled = true
                            
                        }
                        else{
                            self.imageSlider.isUserInteractionEnabled = false
                        }
                    }
                    
                    
                    break
                    
                case .failure(_):
                    print(response.error!.localizedDescription)
                    let alert: UIAlertView = UIAlertView(title: "", message: response.error!.localizedDescription, delegate: nil, cancelButtonTitle: "OK");
                    alert.show()
                    self.view.hideActivityView()
                    print(response.result.error as Any)
                    break
                    
                }
            }
        }
    }
    
    func getVendorDetails()  {
        imgArr  = []
        nameArr  = []
        ratingArr  = []
        idArr = []
        self.vendorCollectionView.reloadData()
        DispatchQueue.main.async {
            let manager = Alamofire.SessionManager.default
            manager.session.configuration.timeoutIntervalForRequest = 120
            
            manager.request( baseURL + "api/AccountAPI/DashboardAPI/?UserId=" + userID, method: .get, parameters: ["":""], encoding: URLEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
                
                switch(response.result) {
                case .success(_):
                    if response.result.value != nil{
                        //print(response.result.value as Any)
                    }
                    self.getAllTopic()
                    let jsonResults : NSDictionary
                    jsonResults = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! NSDictionary
                    let status = jsonResults["status"] as! String
                    if status == "Success"{
                        let featuredVendors = jsonResults["featuredVendors"] as! NSArray
                        if featuredVendors.count != 0{
                            for i in 0 ..< featuredVendors.count{
                                let data = featuredVendors[i] as! NSDictionary
                                let firstName = data["FirstName"] as! String
                                let lastName = data["LastName"] as! String
                                let ratings = data["averageRatings"] as! Double
                                let id = data["Id"] as! String
                                var imageName = ""
                                if (data["image"] as? String) != nil{
                                    imageName = data["image"] as! String
                                }
                                self.imgArr.append(imageName)
                                self.nameArr.append(firstName + " " + lastName)
                                self.ratingArr.append(String(ratings))
                                self.idArr.append(id)
                            }
                            self.vendorCollectionView.reloadData()
                        }
                        else{
                            
                        }
                    }
                    
                    
                    break
                    
                case .failure(_):
                    print(response.error!.localizedDescription)
                    let alert: UIAlertView = UIAlertView(title: "", message: response.error!.localizedDescription, delegate: nil, cancelButtonTitle: "OK");
                    alert.show()
                    self.view.hideActivityView()
                    print(response.result.error as Any)
                    break
                    
                }
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FavVendorCell", for: indexPath) as! FavVendorCell
        let str: String? = Bundle.main.path(forResource: "loading_spinner", ofType: "gif")
        let fileData = NSData(contentsOfFile: str!)
        if imgArr[indexPath.item] != ""{
            cell.userImg.sd_setImage(with: URL(string: imgArr[indexPath.item]), placeholderImage: UIImage.sd_animatedGIF(with: fileData! as Data), options: SDWebImageOptions.allowInvalidSSLCertificates, progress: nil, completed: nil)
        }
        else{
            cell.userImg.image = UIImage(named: "user")
        }
        
        cell.layer.cornerRadius = 2
        cell.userNameLbl.text = nameArr[indexPath.item]
        cell.ratingLbl.text = ratingArr[indexPath.item]
        cell.userImg.layer.cornerRadius = cell.userImg.frame.height/2
        cell.userImg.layer.masksToBounds = true
        
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return nameArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    
    func updateSearchResults(for searchController: UISearchController) {
        var searchStr = searchController.searchBar.text
        if searchStr!.count > 1{
            print(searchController.searchBar.text!)
            searchStr = searchStr!.replacingOccurrences(of: " ", with: "%20")
            self.getSearchResult(searchText: searchStr!)
        }
        else{
            searchID = []
            searchName = []
            searchType = []
            self.tableView.reloadData()
        }
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        self.tableView.isHidden = false
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        self.tableView.isHidden = true
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchID.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "Cell")!
        cell = UITableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: "Cell")
        cell.accessoryType = UITableViewCellAccessoryType.disclosureIndicator

        let codedLabel:UILabel = UILabel()
        codedLabel.frame = CGRect(x: 20, y: 0, width: cell.frame.size.width - 100, height: cell.frame.size.height)
        codedLabel.textAlignment = .left
        codedLabel.text = searchName[indexPath.row]
        codedLabel.numberOfLines = 0
        codedLabel.textColor=UIColor.darkGray
        codedLabel.font=UIFont.systemFont(ofSize: 17)
        codedLabel.lineBreakMode = .byWordWrapping
        cell.contentView.addSubview(codedLabel)
        codedLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint(item: codedLabel, attribute: NSLayoutAttribute.centerY, relatedBy: NSLayoutRelation.equal, toItem: cell.contentView, attribute: NSLayoutAttribute.centerY, multiplier: 1, constant: 0).isActive = true
        NSLayoutConstraint(item: codedLabel, attribute: NSLayoutAttribute.leading, relatedBy: NSLayoutRelation.equal, toItem: cell.contentView, attribute: NSLayoutAttribute.leading, multiplier: 1, constant: 10).isActive = true
        NSLayoutConstraint(item: codedLabel, attribute: NSLayoutAttribute.trailing, relatedBy: NSLayoutRelation.equal, toItem: cell.detailTextLabel, attribute: NSLayoutAttribute.leading, multiplier: 1, constant: 10).isActive = true
        cell.detailTextLabel?.text = searchType[indexPath.row]
        cell.detailTextLabel?.textColor = UIColor.gray
        
        let tap1 = UITapGestureRecognizer(target: self, action: #selector(self.tapCellTap))
        cell.addGestureRecognizer(tap1)
        cell.isUserInteractionEnabled = true
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let cellText = searchName[indexPath.row]
        let cellFont: UIFont? = UIFont(name:"HelveticaNeue-Bold", size: 17.0)!
        let attributedText = NSAttributedString(string: cellText, attributes: [NSAttributedStringKey.font: cellFont as Any])
        let rect: CGRect = attributedText.boundingRect(with: CGSize(width: tableView.bounds.size.width-100, height: CGFloat.greatestFiniteMagnitude), options: .usesLineFragmentOrigin, context: nil)
        return rect.size.height + 30
    }
    
    @objc func tapCellTap(_ sender: UITapGestureRecognizer) {
        let backItem = UIBarButtonItem()
        backItem.title = "Back"
        backItem.tintColor = UIColor.black
        navigationItem.backBarButtonItem = backItem
        var cell: UITableViewCell?
        var view = sender.view
        while view != nil {
            if view is UITableViewCell {
                cell = view as? UITableViewCell
            }
            if view is UITableView {
                tableView = (view as? UITableView)!
            }
            view = view?.superview
        }
        
        if let indexPath = (cell != nil) ? tableView.indexPath(for: cell!) : nil {
            if searchType[indexPath.row] == "Vendor"{
                let backItem = UIBarButtonItem()
                backItem.title = "Back"
                backItem.tintColor = UIColor.black
                navigationItem.backBarButtonItem = backItem
                let storyboard =  UIStoryboard(name: "Main", bundle: nil)
                let promo = storyboard.instantiateViewController(withIdentifier: "VendorDetailsViewController") as! VendorDetailsViewController
                promo.vendorID = self.searchID[indexPath.row]
                promo.poptoRoot = true
                self.navigationController?.pushViewController(promo, animated: true)
            }
            if searchType[indexPath.row] ==  "Service"{
                let backItem = UIBarButtonItem()
                backItem.title = "Back"
                backItem.tintColor = UIColor.black
                navigationItem.backBarButtonItem = backItem
                let storyboard =  UIStoryboard(name: "Main", bundle: nil)
                let promo = storyboard.instantiateViewController(withIdentifier: "VendorListViewController") as! VendorListViewController
                promo.searchtype = self.searchType[indexPath.row]
                promo.serviceTD = self.searchID[indexPath.row]
                promo.header = self.searchName[indexPath.row]
                self.navigationController?.pushViewController(promo, animated: true)
            }
            if searchType[indexPath.row] ==  "Location"{
                let backItem = UIBarButtonItem()
                backItem.title = "Back"
                backItem.tintColor = UIColor.black
                navigationItem.backBarButtonItem = backItem
                let storyboard =  UIStoryboard(name: "Main", bundle: nil)
                let promo = storyboard.instantiateViewController(withIdentifier: "VendorListViewController") as! VendorListViewController
                promo.searchtype = self.searchType[indexPath.row]
                promo.location = self.searchName[indexPath.row]
                promo.header = self.searchName[indexPath.row]
                self.navigationController?.pushViewController(promo, animated: true)
            }
            self.searchController.isActive = false
            self.tableView.isHidden = true
        }
    }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
       
    }
    
    func getSearchResult(searchText: String)  {
        searchID = []
        searchName = []
        searchType = []
        tableView.reloadData()
        DispatchQueue.main.async {
            let manager = Alamofire.SessionManager.default
            manager.session.configuration.timeoutIntervalForRequest = 120
            manager.request( baseURL + "api/AccountAPI/searchVendors/?search=" + searchText + "&userid=" + userID, method: .get, parameters: ["":""], encoding: URLEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
                
                switch(response.result) {
                case .success(_):
                    if response.result.value != nil{
                        //print(response.result.value as Any)
                    }
                    let jsonResults : NSDictionary
                    jsonResults = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! NSDictionary
                    let status = jsonResults["status"] as! String
                    if status == "Success"{
                        self.searchID = []
                        self.searchName = []
                        self.searchType = []
                        self.tableView.reloadData()
                        let ServicesList = jsonResults["ServicesList"] as! NSArray
                        if ServicesList.count != 0{
                            for i in 0 ..< ServicesList.count{
                                let data = ServicesList[i] as! NSDictionary
                                let ServiceId = data["ServiceId"] as! Int
                                let ServiceName = data["ServiceName"] as! String
                                self.searchID.append(String(ServiceId))
                                self.searchName.append(ServiceName)
                                self.searchType.append("Service")
                            }
                        }
                        
                        let LocationList = jsonResults["LocationList"] as! NSArray
                        if LocationList.count != 0{
                            for i in 0 ..< LocationList.count{
                                let data = LocationList[i] as! NSDictionary
                                let UserId = data["UserId"] as! String
                                let UserLocation = data["UserLocation"] as! String
                                self.searchID.append(UserId)
                                self.searchName.append(UserLocation)
                                self.searchType.append("Location")
                            }
                        }
                        
                        let vendorsList = jsonResults["vendorsList"] as! NSArray
                        if vendorsList.count != 0{
                            for i in 0 ..< vendorsList.count{
                                let data = vendorsList[i] as! NSDictionary
                                let VendorId = data["VendorId"] as! String
                                let VendorName = data["VendorName"] as! String
                                self.searchID.append(VendorId)
                                self.searchName.append(VendorName)
                                self.searchType.append("Vendor")
                            }
                        }
                        self.tableView.reloadData()
                    }
                    
                    
                    break
                    
                case .failure(_):
                    print(response.error!.localizedDescription)
                    let alert: UIAlertView = UIAlertView(title: "", message: response.error!.localizedDescription, delegate: nil, cancelButtonTitle: "OK");
                    alert.show()
                    self.view.hideActivityView()
                    print(response.result.error as Any)
                    break
                    
                }
            }
        }
    }
    
    func getAllTopic()  {
        servicesArr = []
        servicesIDArr = []
        DispatchQueue.main.async {
            let manager = Alamofire.SessionManager.default
            manager.session.configuration.timeoutIntervalForRequest = 120
            
            manager.request( baseURL + "api/Events/GetAllServices", method: .get, parameters: ["":""], encoding: URLEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
                
                switch(response.result) {
                case .success(_):
                    if response.result.value != nil{
                        //print(response.result.value as Any)
                    }
                    let jsonResults : NSArray
                    jsonResults = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! NSArray
                    for i in 0 ..< jsonResults.count{
                        let data = jsonResults[i] as! NSDictionary
                        let categoryID = data["id"] as! Int
                        let CategoryTitle = data["serviceName"] as! String
                        let Active = data["active"] as! Bool
                        if Active == true{
                            servicesArr.append(CategoryTitle)
                            servicesIDArr.append(String(categoryID))
                        }
                    }
                    
                    break
                    
                case .failure(_):
                    print(response.error!.localizedDescription)
                    let alert: UIAlertView = UIAlertView(title: "", message: response.error!.localizedDescription, delegate: nil, cancelButtonTitle: "OK");
                    alert.show()
                    self.view.hideActivityView()
                    print(response.result.error as Any)
                    break
                    
                }
            }
        }
    }
    
    @IBAction func fbBtnClk(_ sender: UIButton) {
        if fbURL != ""{
            self.linksView.isHidden = true
            let str = fbURL.contains(find: "http://")
            if str == false{
                fbURL = "http://" + fbURL
            }
            guard let url = URL(string:  fbURL) else {
                return //be safe
            }
            
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
        else{
            let alert: UIAlertView = UIAlertView(title: "", message: "No social media channel yet for Facebook", delegate: nil, cancelButtonTitle: "OK");
            alert.show()
        }
    }
    
    @IBAction func instaBtnClk(_ sender: UIButton) {
        if instaURL != ""{
            self.linksView.isHidden = true
            
            let str = instaURL.contains(find: "http://")
            if str == false{
                instaURL = "http://" + instaURL
            }
            
            guard let url = URL(string: instaURL) else {
                return //be safe
            }
            
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
        else{
            let alert: UIAlertView = UIAlertView(title: "", message: "No social media channel yet for Instagram", delegate: nil, cancelButtonTitle: "OK");
            alert.show()
        }
        
    }
    
    @IBAction func twitterBtnClk(_ sender: UIButton) {
        if twitter != ""{
            self.linksView.isHidden = true
            let str = twitter.contains(find: "http://")
            if str == false{
                twitter = "http://" + twitter
            }
            guard let url = URL(string: twitter) else {
                return //be safe
            }
            
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
        else{
            let alert: UIAlertView = UIAlertView(title: "", message: "No social media channel yet for Twitter", delegate: nil, cancelButtonTitle: "OK");
            alert.show()
        }
        
    }
    
    @IBAction func cellBtnClk(_ sender: UIButton) {
        let position = self.vendorCollectionView.convert(CGPoint.zero, from: sender)
        let indexPath : IndexPath = vendorCollectionView.indexPathForItem(at: position)!
        let backItem = UIBarButtonItem()
        backItem.title = "Back"
        backItem.tintColor = UIColor.black
        navigationItem.backBarButtonItem = backItem
        let storyboard =  UIStoryboard(name: "Main", bundle: nil)
        let promo = storyboard.instantiateViewController(withIdentifier: "VendorDetailsViewController") as! VendorDetailsViewController
        promo.vendorID = self.idArr[indexPath.item]
        promo.vender_name = self.nameArr[indexPath.item]
        promo.poptoRoot = true
        self.navigationController?.pushViewController(promo, animated: true)
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        switch result.rawValue {
        case MFMailComposeResult.cancelled.rawValue:
            //NSLog("Mail Cancelled")
            self.dismiss(animated: false, completion: nil)
            break
        case MFMailComposeResult.saved.rawValue:
            //NSLog("Mail Saved")
            self.dismiss(animated: false, completion: nil)
            break
        case MFMailComposeResult.failed.rawValue:
            //NSLog("Mail Sent Fail", [error!.localizedDescription])
            self.dismiss(animated: false, completion: nil)
            break
        case MFMailComposeResult.sent.rawValue:
            //NSLog("Mail Sent")
            self.dismiss(animated: false, completion: nil)
            break
            
        default:
            break
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
