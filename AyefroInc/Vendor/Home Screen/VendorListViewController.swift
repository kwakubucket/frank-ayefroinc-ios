//
//  VendorListViewController.swift
//  AyefroInc
//
//  Created by Vijay Darkonde on 21/12/17.
//  Copyright © 2017 Inceptive Consulting Pvt. Ltd. All rights reserved.
//

import UIKit
import SDWebImage
import Alamofire

class VendorListViewController: UIViewController , UICollectionViewDataSource, UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {
    
    
    @IBOutlet weak var favCollectionView: UICollectionView!
    @IBOutlet weak var errorView: UIView!
    @IBOutlet weak var loaderView: UIImageView!
    @IBOutlet weak var noInternetView: UIView!
    @IBOutlet weak var retryBtn: UIButton!
    
    var imgArr : [String] = []
    var nameArr : [String] = []
    var ratingArr : [String] = []
    var idArr : [String] = []
    
    var searchtype: String! = ""
    var apiStr: String! = ""
    var serviceTD: String! = ""
    var location: String! = ""
    var header: String! = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = header
        self.tabBarController?.tabBar.isHidden = true
        
        let str: String? = Bundle.main.path(forResource: "loading_spinner", ofType: "gif")
        let fileData = NSData(contentsOfFile: str!)
        loaderView.image = UIImage.sd_animatedGIF(with: fileData! as Data)
        // Do any additional setup after loading the view.
        self.errorView.isHidden = true
        self.favCollectionView.isHidden =  true
        self.noInternetView.isHidden = true
        self.loaderView.isHidden = false
        self.retryBtn.layer.cornerRadius = 5
        
        if searchtype == "Service"{
            apiStr = "api/AccountAPI/ServiceDetailsAPI/?serviceId=" + serviceTD + "&UserId=" + userID
        }
        else{
            apiStr = "api/AccountAPI/GetProvidersOfSameLocation/?location=" + location + "&UserId=" + userID
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        getAllImages()
    }
    
    func getAllImages()  {
        imgArr  = []
        nameArr  = []
        ratingArr  = []
        idArr = []
        self.favCollectionView.reloadData()
        DispatchQueue.main.async {
            let manager = Alamofire.SessionManager.default
            manager.session.configuration.timeoutIntervalForRequest = 120
            
            manager.request( baseURL + self.apiStr, method: .get, parameters: ["":""], encoding: URLEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
                
                switch(response.result) {
                case .success(_):
                    if response.result.value != nil{
                        //print(response.result.value as Any)
                    }
                    
                    let jsonResults : NSDictionary
                    jsonResults = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! NSDictionary
                    if jsonResults.count == 0{
                        self.errorView.isHidden = false
                        self.favCollectionView.isHidden =  true
                        self.noInternetView.isHidden = true
                        self.loaderView.isHidden = true
                    }
                    else{
                        self.errorView.isHidden = true
                        self.noInternetView.isHidden = true
                        self.favCollectionView.isHidden =  false
                        self.loaderView.isHidden = true
                        let status = jsonResults["status"] as! String
                        if status == "Success"{
                            let serviceProvidersArray = jsonResults["serviceProvidersArray"] as! NSArray
                            for i in 0 ..< serviceProvidersArray.count{
                                let data = serviceProvidersArray[i] as! NSDictionary
                                let firstName = data["FirstName"] as! String
                                let lastName = data["LastName"] as! String
                                let ratings = data["averageRatings"] as! Double
                                let id = data["service_provider_id"] as! String
                                var imageName = ""
                                if (data["Image"] as? String) != nil{
                                    imageName = data["Image"] as! String
                                }
                                self.imgArr.append(imageName)
                                self.nameArr.append(firstName + " " + lastName)
                                self.ratingArr.append(String(ratings))
                                self.idArr.append(id)
                            }
                            self.favCollectionView.reloadData()
                        }
                        
                    }
                    
                    break
                    
                case .failure(_):
                    if response.error?.localizedDescription == "The Internet connection appears to be offline."
                    {
                        self.errorView.isHidden = true
                        self.favCollectionView.isHidden = true
                        self.noInternetView.isHidden = false
                        self.loaderView.isHidden = true
                    }
                    else{
                        self.loaderView.isHidden = true
                        let alert: UIAlertView = UIAlertView(title: "", message: response.error!.localizedDescription, delegate: nil, cancelButtonTitle: "OK");
                        alert.show()
                        print(response.result.error as Any)
                    }
                    break
                    
                }
            }
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FavVendorCell", for: indexPath) as! FavVendorCell
        let str: String? = Bundle.main.path(forResource: "loading_spinner", ofType: "gif")
        let fileData = NSData(contentsOfFile: str!)
        if imgArr[indexPath.item] != ""{
            cell.userImg.sd_setImage(with: URL(string: imgArr[indexPath.item]), placeholderImage: UIImage.sd_animatedGIF(with: fileData! as Data), options: SDWebImageOptions.allowInvalidSSLCertificates, progress: nil, completed: nil)
        }
        else{
            cell.userImg.image = UIImage(named: "user")
        }
        
        cell.layer.cornerRadius = 2
        cell.userNameLbl.text = nameArr[indexPath.item]
        cell.ratingLbl.text = ratingArr[indexPath.item]
        cell.userImg.layer.cornerRadius = cell.userImg.frame.height/2
        cell.userImg.layer.masksToBounds = true
        
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return nameArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let backItem = UIBarButtonItem()
        backItem.title = "Back"
        backItem.tintColor = UIColor.black
        navigationItem.backBarButtonItem = backItem
        let storyboard =  UIStoryboard(name: "Main", bundle: nil)
        let promo = storyboard.instantiateViewController(withIdentifier: "VendorDetailsViewController") as! VendorDetailsViewController
        promo.vendorID = self.idArr[indexPath.item]
        promo.poptoRoot = true
        self.navigationController?.pushViewController(promo, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if UIScreen.main.bounds.size.width == 414
        {
            let collectionViewWidthB: CGFloat? = collectionView.frame.size.width
            let width = collectionViewWidthB! / 3.5
            let cellSizeB = CGSize(width: width, height: 137.0)
            return cellSizeB
        }
        else if UIScreen.main.bounds.size.width == 375 && UIScreen.main.bounds.size.height == 667
        {
            let collectionViewWidthB: CGFloat? = collectionView.frame.size.width
            let collectionViewHeightB: CGFloat = collectionView.frame.size.height
            let width = collectionViewWidthB! / 3.5
            let heigth = collectionViewHeightB / 2.5
            let cellSizeB = CGSize(width: width, height: 137.0)
            return cellSizeB
            
            
        }
        else if UIScreen.main.bounds.size.width == 320 && UIScreen.main.bounds.size.height == 568
        {
            let collectionViewWidthB: CGFloat? = collectionView.frame.size.width
            let collectionViewHeightB: CGFloat = collectionView.frame.size.height
            let width = collectionViewWidthB! / 3.5
            let heigth = collectionViewHeightB / 2.5
            let cellSizeB = CGSize(width: width, height: 137.0)
            return cellSizeB
        }
        else if UIScreen.main.bounds.size.width == 320 && UIScreen.main.bounds.size.height == 480
        {
            let collectionViewWidthB: CGFloat? = collectionView.frame.size.width
            let width = collectionViewWidthB! / 3.5
            let cellSizeB = CGSize(width: width, height: 137.0)
            return cellSizeB
        }
        else{
            let collectionViewWidthB: CGFloat? = collectionView.frame.size.width
            let width = collectionViewWidthB! / 3.5
            let cellSizeB = CGSize(width: width, height: 137.0)
            return cellSizeB
        }
        
    }
    
    @IBAction func gotoGalleryBtnClk(_ sender: UIButton) {
        let backItem = UIBarButtonItem()
        backItem.title = "Back"
        backItem.tintColor = UIColor.black
        navigationItem.backBarButtonItem = backItem
        let storyboard =  UIStoryboard(name: "Main", bundle: nil)
        let promo = storyboard.instantiateViewController(withIdentifier: "BrowesGalleryViewController") as! BrowesGalleryViewController
        self.navigationController?.pushViewController(promo, animated: true)
    }
    
    @IBAction func retryBtnClk(_ sender: UIButton) {
        getAllImages()
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

