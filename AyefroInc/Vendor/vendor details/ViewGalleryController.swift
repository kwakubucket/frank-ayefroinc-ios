//
//  ViewGalleryController.swift
//  AyefroInc
//
//  Created by Vijay Darkonde on 20/12/17.
//  Copyright © 2017 Inceptive Consulting Pvt. Ltd. All rights reserved.
//

import UIKit
import SDWebImage
import Alamofire

class ViewGalleryController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {
    
    var imageName: [String] = []
    var imageId: [String] = []
    
    @IBOutlet var imageCollection: UICollectionView!
    @IBOutlet var loader: UIImageView!
    @IBOutlet var errorView: UIView!
    @IBOutlet var noInternetView: UIView!
    @IBOutlet var retryBtn: UIButton!
    
    @IBOutlet weak var rightBtn: UIBarButtonItem!
    var headerTitle: String! = ""
    var imageArr : NSArray!
    var vendorID : String! = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tabBarController?.tabBar.isHidden = true
        
        let str: String? = Bundle.main.path(forResource: "loading_spinner", ofType: "gif")
        let fileData = NSData(contentsOfFile: str!)
        loader.image = UIImage.sd_animatedGIF(with: fileData! as Data)
        // Do any additional setup after loading the view.
        self.errorView.isHidden = true
        self.imageCollection.isHidden =  true
        self.noInternetView.isHidden = true
        self.loader.isHidden = false
        self.retryBtn.layer.cornerRadius = 5
//        if imageArr.count != 0
//        {
//            for i in 0 ..< imageArr.count{
//                let dict = imageArr[i] as! NSDictionary
//                let imgName = dict["Image"] as! String
//                let Id = dict["Id"] as! Int
//                self.imageId.append(String(Id))
//                self.imageName.append(imgName)
//            }
//            self.imageCollection.reloadData()
//            imageCollection.isHidden = false
//            errorView.isHidden = true
//        }
//        else{
//            imageCollection.isHidden = true
//            errorView.isHidden = false
//        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
       getAllImages()
    }
    
    func getAllImages()  {
        DispatchQueue.main.async {
            self.imageName = []
            let manager = Alamofire.SessionManager.default
            manager.session.configuration.timeoutIntervalForRequest = 120
            
            manager.request( baseURL + "api/Albums/GetAlbumDetails/?providerId=" + self.vendorID + "&UserId=" + userID, method: .get, parameters: ["":""], encoding: URLEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
                
                switch(response.result) {
                case .success(_):
                    if response.result.value != nil{
                        //print(response.result.value as Any)
                    }
                    
                    let jsonResults : NSDictionary
                    jsonResults = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! NSDictionary
                    if jsonResults.count == 0{
                        
                    }
                    else{
                        
                        let album = jsonResults["album"] as! NSArray
                        if album.count != 0{
                            self.errorView.isHidden = true
                            self.noInternetView.isHidden = true
                            self.imageCollection.isHidden =  false
                            self.loader.isHidden = true
                            for i in 0 ..< album.count{
                                let data = album[i] as! NSDictionary
                                let CategoryTitle = data["Image"] as! String
                                let imageId = data["Id"] as! Int
                                self.imageId.append(String(imageId))
                                self.imageName.append(CategoryTitle)
                                let FirstName = data["FirstName"] as! String
                                let LastName = data["LastName"] as! String
                                self.title = FirstName + " " + LastName + "'s Gallery"
                            }
                            let favourite = jsonResults["favourite"] as! Bool
                            if favourite == true{
                                self.rightBtn.image = UIImage(named: "blackheart")
                            }
                            else{
                                self.rightBtn.image = UIImage(named: "favourite")
                            }
                            self.imageCollection.reloadData()
                        }
                        else{
                            self.errorView.isHidden = false
                            self.imageCollection.isHidden =  true
                            self.noInternetView.isHidden = true
                            self.loader.isHidden = true
                            self.rightBtn.image = nil
                        }
                        
                    }
                    
                    break
                    
                case .failure(_):
                    if response.error?.localizedDescription == "The Internet connection appears to be offline."
                    {
                        self.errorView.isHidden = true
                        self.imageCollection.isHidden = true
                        self.noInternetView.isHidden = false
                        self.loader.isHidden = true
                    }
                    else{
                        self.loader.isHidden = true
                        let alert: UIAlertView = UIAlertView(title: "", message: response.error!.localizedDescription, delegate: nil, cancelButtonTitle: "OK");
                        alert.show()
                        print(response.result.error as Any)
                    }
                    break
                    
                }
            }
        }
    }
    
    @IBAction func rightnBtnClk(_ sender: UIBarButtonItem) {
        DispatchQueue.main.async {
            
            let parameters : [String: Any] = [
                "UserId": userID,
                "ProviderId" : self.vendorID
            ]
            
            let manager = Alamofire.SessionManager.default
            manager.session.configuration.timeoutIntervalForRequest = 120
            
            manager.request(baseURL + "api/Favourites/PostFavourite", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
                
                switch(response.result) {
                case .success(_):
                    if response.result.value != nil{
                        //print(response.result.value as Any)
                    }
                    let jsonResults : NSDictionary
                    jsonResults = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! NSDictionary
                    let Message = jsonResults["Message"] as! String
                    if Message == "Provider unfavourite successfully"{
                        let alert: UIAlertView = UIAlertView(title: "", message: "Service Provider has been Removed from Your Favourites", delegate: nil, cancelButtonTitle: "OK");
                        alert.show()
                    }
                    else{
                        let alert: UIAlertView = UIAlertView(title: "", message: "Service Provider has been Added to Your Favourites List", delegate: nil, cancelButtonTitle: "OK");
                        alert.show()
                    }
                    self.getAllImages()
                    break
                    
                case .failure(_):
                    print(response.error!.localizedDescription)
                    let alert: UIAlertView = UIAlertView(title: "", message: response.error!.localizedDescription, delegate: nil, cancelButtonTitle: "OK");
                    alert.show()
                    self.view.hideActivityView()
                    print(response.result.error as Any)
                    break
                    
                }
            }
        }
    }
    
    
    @IBAction func retryBtnClk(_ sender: UIButton) {
        getAllImages()
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageGridCell", for: indexPath) as! ImageGridCell
        let str: String? = Bundle.main.path(forResource: "loading_spinner", ofType: "gif")
        let fileData = NSData(contentsOfFile: str!)
        
        cell.galleryImg.sd_setImage(with: URL(string: imageName[indexPath.item]), placeholderImage: UIImage.sd_animatedGIF(with: fileData! as Data), options: SDWebImageOptions.allowInvalidSSLCertificates, progress: nil, completed: nil)
        cell.layer.cornerRadius = 2
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imageName.count
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let backItem = UIBarButtonItem()
        backItem.title = "Back"
        backItem.tintColor = UIColor.black
        navigationItem.backBarButtonItem = backItem
        let storyboard =  UIStoryboard(name: "Main", bundle: nil)
        let promo = storyboard.instantiateViewController(withIdentifier: "ImageDetailViewController") as! ImageDetailViewController
        promo.vendorID = self.vendorID
        promo.QuestionId = self.imageId[indexPath.row]
        self.navigationController?.pushViewController(promo, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if UIScreen.main.bounds.size.width == 414
        {
            let collectionViewWidthB: CGFloat? = collectionView.frame.size.width
            let width = collectionViewWidthB! / 3.5
            let cellSizeB = CGSize(width: width, height: width)
            return cellSizeB
        }
        else if UIScreen.main.bounds.size.width == 375 && UIScreen.main.bounds.size.height == 667
        {
            let collectionViewWidthB: CGFloat? = collectionView.frame.size.width
            let width = collectionViewWidthB! / 3.5
            let cellSizeB = CGSize(width: width, height: width)
            return cellSizeB
        }
        else if UIScreen.main.bounds.size.width == 320 && UIScreen.main.bounds.size.height == 568
        {
            let collectionViewWidthB: CGFloat? = collectionView.frame.size.width
            let width = collectionViewWidthB! / 3.5
            let cellSizeB = CGSize(width: width, height: width)
            return cellSizeB
        }
        else if UIScreen.main.bounds.size.width == 320 && UIScreen.main.bounds.size.height == 480
        {
            let collectionViewWidthB: CGFloat? = collectionView.frame.size.width
            let width = collectionViewWidthB! / 3.5
            let cellSizeB = CGSize(width: width, height: width)
            return cellSizeB
        }
        else{
            let collectionViewWidthB: CGFloat? = collectionView.frame.size.width
            let width = collectionViewWidthB! / 3.5
            let cellSizeB = CGSize(width: width, height: width)
            return cellSizeB
        }
        
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let backItem = UIBarButtonItem()
        backItem.title = "Back"
        backItem.tintColor = UIColor.black
        navigationItem.backBarButtonItem = backItem
        let destinationVC = segue.destination as! UploadPhotoViewController
        // This will show in the next view controller being pushed
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

