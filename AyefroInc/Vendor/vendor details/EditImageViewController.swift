//
//  EditImageViewController.swift
//  AyefroInc
//
//  Created by Vijay Darkonde on 21/12/17.
//  Copyright © 2017 Inceptive Consulting Pvt. Ltd. All rights reserved.
//

import UIKit
import KMPlaceholderTextView
import Alamofire


class EditImageViewController: UIViewController, addServices, promoteArr{
    
    @IBOutlet var popUpView: UIView!
    @IBOutlet var commentTxtView: KMPlaceholderTextView!
    @IBOutlet var submitBtn: UIButton!
    @IBOutlet weak var serviceLbl: UILabel!
    var popViewController : CategoryPopupViewController!
    var popViewController1 : PromoteViewController!

    var answerID : String = ""
    var delegate: updateImage!
    var commentTxt : String! = ""
    var servicesArr: NSArray!
    var imgID : String! = ""
    
    var servicesIDArr : [String] = []
    var servicesNameArr : [String] = []
    var promotionAim: String! = ""
    var budget: String! = "0"
    var timespan: String! = ""
    var promotionStartDate: String! = ""
    var promotionEndDate: String! = ""
    
    var servicesAllIDArr : [String] = []
    var servicesAllNameArr : [String] = []
    
    var promotionText: String! = ""
    var startDate: String! = ""
    var endDate: String! = ""
    var budgetValue: String! = ""
    var timespanValue: String! = ""

    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    override init(nibName nibNameOrNil: String!, bundle nibBundleOrNil: Bundle!) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Edit or Promote"
//        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
//        self.popUpView.layer.cornerRadius = 5
//        self.popUpView.layer.shadowOpacity = 0.0
//        self.popUpView.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        self.commentTxtView.text = commentTxt
        for i in 0 ..< servicesArr.count{
            let dict = servicesArr[i] as! NSDictionary
            let status = dict["status"] as! Bool
            let id = dict["id"] as! Int
            let ServiceName = dict["ServiceName"] as! String
            if status == true{
                
                self.servicesIDArr.append(String(id))
                self.servicesNameArr.append(ServiceName)
            }
            self.servicesAllIDArr.append(String(id))
            self.servicesAllNameArr.append(ServiceName)
        }
        if self.servicesNameArr.count > 2{
            let count = self.servicesNameArr.count - 2
            self.serviceLbl.text = self.servicesNameArr[0] + "," + self.servicesNameArr[1] + "...+" + String(count) + " More"
        }
        else{
            let serviceString = self.servicesNameArr.joined(separator:",")
            self.serviceLbl.text = serviceString
        }
        
    }
    
    func showInView(_ aView: UIView!, animated: Bool)
    {
        aView.addSubview(self.view)
        if animated
        {
            self.showAnimate()
        }
    }
    
    func showAnimate()
    {
        self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view.alpha = 0.0;
        UIView.animate(withDuration: 0.25, animations: {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        });
    }
    
    func removeAnimate()
    {
        UIView.animate(withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view.alpha = 0.0;
        }, completion:{(finished : Bool)  in
            if (finished)
            {
                self.view.removeFromSuperview()
            }
        });
    }
    
    @IBAction func backBtnClk(_ sender: AnyObject) {
        commentTxtView.resignFirstResponder()
        removeAnimate()
    }
    
    @IBAction func serviceBtnClk(_ sender: UIButton) {
        let backItem = UIBarButtonItem()
        backItem.title = "Back"
        backItem.tintColor = UIColor.black
        navigationItem.backBarButtonItem = backItem
        self.popViewController = self.storyboard?.instantiateViewController(withIdentifier: "CategoryPopupViewController")as! CategoryPopupViewController
        self.popViewController.delagate = self
        self.popViewController.selectedCategory = self.servicesIDArr
        self.navigationController?.pushViewController(self.popViewController, animated: true)
    }
    
    @IBAction func promoteBtnClk(_ sender: UIButton) {
        let backItem = UIBarButtonItem()
        backItem.title = "Back"
        backItem.tintColor = UIColor.black
        navigationItem.backBarButtonItem = backItem
        self.popViewController1 = self.storyboard?.instantiateViewController(withIdentifier: "PromoteViewController")as! PromoteViewController
        self.popViewController1.delegate = self
        self.popViewController1.promotionText = self.promotionText
        self.popViewController1.startDate = self.startDate
        self.popViewController1.endDate = self.endDate
        self.popViewController1.budgetValue = self.budgetValue
        self.popViewController1.timespan = self.timespanValue
        self.navigationController?.pushViewController(self.popViewController1, animated: true)
    }
    
    func addService(services: String, idArr: [String]) {
        var servicesName : [String] = []
        self.servicesIDArr = idArr
        for i in 0 ..< idArr.count{
            if let indexof = servicesAllIDArr.index(of: idArr[i]) {
                servicesName.append(servicesAllNameArr[indexof])
            }
        }
        
        if servicesName.count > 2{
            let count = servicesName.count - 2
            self.serviceLbl.text = servicesName[0] + "," + servicesName[1] + "...+" + String(count) + " More"
        }
        else{
            self.serviceLbl.text = servicesName.joined(separator: ",")
        }
        
        
        
    }
    
    func getPromoteArr(aim: String, budget: String, timespan: String, startDate: String, endDate: String){
        self.promotionAim = aim
        self.budget = budget
        self.timespan = timespan
        self.promotionStartDate = startDate
        self.promotionEndDate = endDate
    }
    
    @IBAction func saveBtnClk(_ sender: AnyObject) {
        
        commentTxtView.resignFirstResponder()
        if commentTxtView.text.isEmpty
        {
            let alert: UIAlertView = UIAlertView(title: "", message: "Please Enter Description.", delegate: nil, cancelButtonTitle: "OK");
            alert.show()
        }
        else if servicesIDArr.count == 0{
            let alert: UIAlertView = UIAlertView(title: "", message: "Please Select at least One Service.", delegate: nil, cancelButtonTitle: "OK");
            alert.show()
        }
        else
        {
            self.submitBtn.isUserInteractionEnabled = false
            let services = self.servicesIDArr.joined(separator: ",")
            DispatchQueue.main.async {
                let parameters : [String: String] = [
                    "UserId": userID as String,
                    "Sevices": services as String,
                    "PromotionAim": self.promotionAim as String,
                    "Budget": self.budget as String,
                    "Timespan": self.timespan as String,
                    "ImageDesc":self.commentTxtView.text as String,
                    "ImgId": self.imgID,
                    "PStartDate": self.promotionStartDate,
                    "PEndDate": self.promotionEndDate,
                    ]
                
                let manager = Alamofire.SessionManager.default
                manager.session.configuration.timeoutIntervalForRequest = 120
                
                manager.request(baseURL + "api/ImageComments/UpdateImageDetail", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in

                    switch(response.result) {
                    case .success(_):
                        if response.result.value != nil{
                            //print(response.result.value as Any)
                        }
                        self.submitBtn.isUserInteractionEnabled = true
                        self.navigationController?.popViewController(animated: true)
                        break

                    case .failure(_):
                        print(response.error!.localizedDescription)
                        let alert: UIAlertView = UIAlertView(title: "", message: response.error!.localizedDescription, delegate: nil, cancelButtonTitle: "OK");
                        alert.show()
                        print(response.result.error as Any)
                        break

                    }
                }
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}



