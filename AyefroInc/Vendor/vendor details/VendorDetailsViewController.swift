//
//  VendorDetailsViewController.swift
//  AyefroInc
//
//  Created by Vijay Darkonde on 19/12/17.
//  Copyright © 2017 Inceptive Consulting Pvt. Ltd. All rights reserved.
//

import UIKit
import ImageSlideshow
import SDWebImage
import Alamofire
import MessageUI
import SystemConfiguration
import Branch

class VendorDetailsViewController: UIViewController, emailSend , MFMailComposeViewControllerDelegate{
    
    @IBOutlet weak var imgSlider: ImageSlideshow!
    @IBOutlet weak var imageCount: UILabel!
    @IBOutlet weak var userNameLbl: UILabel!
    @IBOutlet weak var companyNameLbl: UILabel!
    @IBOutlet weak var verfiedImg: UIImageView!
    @IBOutlet weak var scrollHeight: NSLayoutConstraint!
    @IBOutlet weak var serviceCountLbl: UILabel!
    @IBOutlet weak var reviewCount: UILabel!
    @IBOutlet weak var descrioptionLbl: UILabel!
    @IBOutlet weak var descptionViewHeight: NSLayoutConstraint!
    @IBOutlet weak var socialLinksViewHeight: NSLayoutConstraint!
    @IBOutlet weak var startingPriceLbl: UILabel!
    @IBOutlet weak var fbbtn: UIButton!
    @IBOutlet weak var instabtn: UIButton!
    @IBOutlet weak var twitterbtn: UIButton!
    @IBOutlet weak var websitebtn: UIButton!
    @IBOutlet weak var ratingLbl: UILabel!
    @IBOutlet weak var callbtn: UIButton!
    @IBOutlet weak var errorLbl: UILabel!
    @IBOutlet var rateAndReviewVendorButton: UIButton!
    @IBOutlet var rateReviewBtnHeightConstraint: NSLayoutConstraint!
    @IBOutlet var rateViewHeightConstraint: NSLayoutConstraint!
    
    
    var imgIdArr : [String] = []
    var sdWebImageSource = [SDWebImageSource(urlString: "https://images.unsplash.com/photo-1432679963831-2dab49187847?w=1080")!, SDWebImageSource(urlString: "https://images.unsplash.com/photo-1447746249824-4be4e1b76d66?w=1080")!, SDWebImageSource(urlString: "https://images.unsplash.com/photo-1463595373836-6e0b0a8ee322?w=1080")!]
    var vendorID: String! = ""
    var vender_name: String = ""
    var fbURL : String! = ""
    var instaURL: String! = ""
    var twitter: String! = ""
    var websiteURL: String! = ""
    var serviceArr : [NSDictionary]! = []
    var reviewArr : NSArray!
    var galleryImageArr : NSArray!
    var userName: String = ""
    var selectecImageID : String! = "0"
    var poptoRoot: Bool = false
    var popViewController : ContactPopUpViewController!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.socialLinksViewHeight.constant = 95
        self.callbtn.layer.borderColor = UIColor.gray.cgColor
        self.callbtn.layer.borderWidth = 0.5
        
        let button = UIButton(type: .system)
        button.setTitle(" Back      ", for: .normal)
        button.setImage(UIImage(named: "backbtn"), for: .normal)
        button.addTarget(self, action: #selector(reset), for: .touchUpInside)
        button.tintColor = UIColor.black
        button.contentHorizontalAlignment = .left
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: button)
        errorLbl.isHidden = true
        imageCount.text = ""
        userNameLbl.text = ""
        companyNameLbl.text = ""
        verfiedImg.image = nil
        serviceCountLbl.text = ""
        reviewCount.text = ""
        descrioptionLbl.text = ""
        startingPriceLbl.text = ""
        ratingLbl.text = ""
        
        if self.vendorID == userID {
//            rateViewHeightConstraint.constant = 151
//            rateReviewBtnHeightConstraint.constant = 0
            rateAndReviewVendorButton.setTitle("INVITE FRIENDS TO REVIEW", for: .normal)
        } else {
//            rateViewHeightConstraint.constant = 189
//            rateReviewBtnHeightConstraint.constant = 38
            rateAndReviewVendorButton.setTitle("RATE AND REVIEW THIS VENDOR", for: .normal)
        }
        
        Branch.getInstance().setIdentity(String(userID))
        Branch.getInstance().loadRewards { (changed, error) in
            if (error == nil) {
                let creditsForBucket = Branch.getInstance().getCredits()
                print("credit for bucket: \(creditsForBucket)")
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        _ = ModelManager.instance.getUserLoginStatus()
        imgSlider.backgroundColor = UIColor.white
        imgSlider.slideshowInterval = 30.0
//        imgSlider.pageControl.currentPageIndicatorTintColor = UIColor.clear
//        imgSlider.pageControl.pageIndicatorTintColor = UIColor.clear
        imgSlider.contentScaleMode = UIViewContentMode.scaleAspectFit
        self.tabBarController?.tabBar.isHidden = true
        imgSlider.activityIndicator = nil
        if isConnectedToNetwork() == true{
            imgSlider.currentPageChanged = { page in
                self.imageCount.text = " " + String(page + 1) + "/" + String(self.imgIdArr.count) + " "
                self.selectecImageID = self.imgIdArr[page]
            }
        }
        
        imgSlider.setCurrentPage(0, animated: true)
        
        let recognizer = UITapGestureRecognizer(target: self, action: #selector(VendorDetailsViewController.didTap))
        imgSlider.addGestureRecognizer(recognizer)
        
        self.sdWebImageSource = []
        getVendorDetails()
        imgIdArr = []
        serviceArr = []
        self.imgSlider.setImageInputs(self.sdWebImageSource)
        
    }
    
    func isConnectedToNetwork() -> Bool {
        
        var zeroAddress = sockaddr_in(sin_len: 0, sin_family: 0, sin_port: 0, sin_addr: in_addr(s_addr: 0), sin_zero: (0, 0, 0, 0, 0, 0, 0, 0))
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        
        return (isReachable && !needsConnection)
        
    }
    
    @objc func reset()  {
        if poptoRoot == true{
            self.navigationController?.popToRootViewController(animated: true)
        }
        else{
            self.navigationController?.popViewController(animated: true)
        }
        
    }
    
    @objc func didTap() {
        if isConnectedToNetwork() == true {
            if self.selectecImageID != nil{
                let backItem = UIBarButtonItem()
                backItem.title = "Back"
                backItem.tintColor = UIColor.black
                navigationItem.backBarButtonItem = backItem
                let storyboard =  UIStoryboard(name: "Main", bundle: nil)
                let promo = storyboard.instantiateViewController(withIdentifier: "ImageDetailViewController") as! ImageDetailViewController
                promo.vendorID = self.vendorID
                promo.QuestionId = self.selectecImageID
                self.navigationController?.pushViewController(promo, animated: true)
            }
        }
        
    }
    
    
    func getVendorDetails()  {
        self.view.showActivityView(withLabel: "Loading")
        DispatchQueue.main.async {
            let manager = Alamofire.SessionManager.default
            manager.session.configuration.timeoutIntervalForRequest = 120
            
            manager.request( baseURL + "api/ServiceProviderMappingsAPI/GetPerticularServiceProviderMapping/" + self.vendorID, method: .get, parameters: ["":""], encoding: URLEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
                
                switch(response.result) {
                case .success(_):
                    if response.result.value != nil{
                        //print(response.result.value as Any)
                    }
                    let jsonResults : NSDictionary
                    jsonResults = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! NSDictionary
                    let status = jsonResults["status"] as! NSString
                    if status == "Success"{
                        let GallaryImages = jsonResults["GallaryImages"] as! NSArray
                        self.galleryImageArr = GallaryImages
                        let str: String? = Bundle.main.path(forResource: "loading_spinner", ofType: "gif")
                        let fileData = NSData(contentsOfFile: str!)
                        self.reviewArr = jsonResults["getServiceProviderReview"] as! NSArray
                        
                        
                        let profileinfo = jsonResults["profileinfo"] as! NSArray
                        let profileDict = profileinfo[0] as! NSDictionary
                        let FirstName = profileDict["FirstName"] as! String
                        let LastName = profileDict["LastName"] as! String
                        self.userName = FirstName + " " + LastName
                        self.title = FirstName + " " + LastName
                        var AverageRatings : Double = 0.0
                        if (profileDict["AverageRatings"] as? Double) != nil{
                            AverageRatings = profileDict["AverageRatings"] as! Double
                        }
                        self.ratingLbl.text = String(AverageRatings)
                        var CompanyDescription = ""
                        if (profileDict["CompanyDescription"] as? String) != nil{
                            CompanyDescription = profileDict["CompanyDescription"] as! String
                        }
                        let Verified = profileDict["Verified"] as! Bool
                        if (profileDict["FacebookUrl"] as? String) != nil{
                            self.fbURL = profileDict["FacebookUrl"] as! String
                        }
                        if (profileDict["InstagramUrl"] as? String) != nil{
                            self.instaURL = profileDict["InstagramUrl"] as! String
                        }
                        if (profileDict["Twitter"] as? String) != nil{
                            self.twitter = profileDict["Twitter"] as! String
                        }
                        if (profileDict["Website"] as? String) != nil{
                            self.websiteURL = profileDict["Website"] as! String
                        }
                        var StartingPrice : Double = 0.0
                        if (profileDict["StartingPrice"] as? Double) != nil{
                            StartingPrice = profileDict["StartingPrice"] as! Double
                        }
                        let prefs: UserDefaults? = UserDefaults.standard
                        let currencySymbol = prefs?.string(forKey: "currencySymbol")
                        self.startingPriceLbl.text = "Starting Price: " + currencySymbol! + " " + String(StartingPrice)
                        if Verified == true{
                            self.verfiedImg.image = UIImage(named: "Verified")
                        }
                        else{
                            self.verfiedImg.image = nil
                        }
                        
                        var companyName = ""
                        if (jsonResults["companyName"] as? String) != nil{
                            companyName = jsonResults["companyName"] as! String
                        }
                        var companyLocation = ""
                        if (jsonResults["companyLocation"] as? String) != nil{
                            companyLocation = jsonResults["companyLocation"] as! String
                        }
                        self.userNameLbl.text = companyName
                        self.companyNameLbl.text = companyLocation
                        
                        if self.fbURL == ""{
                            self.fbbtn.setImage(UIImage(named:"fbgray"), for: UIControlState.normal)
                        }
                        else{
                            self.fbbtn.setImage(UIImage(named:"facebook"), for: UIControlState.normal)
                        }
                        if self.instaURL == ""{
                            self.instabtn.setImage(UIImage(named:"instagray"), for: UIControlState.normal)
                        }
                        else{
                            self.instabtn.setImage(UIImage(named:"instagram-1"), for: UIControlState.normal)
                        }
                        if self.twitter == ""{
                            self.twitterbtn.setImage(UIImage(named:"twittergray"), for: UIControlState.normal)
                        }
                        else{
                            self.twitterbtn.setImage(UIImage(named:"twitter"), for: UIControlState.normal)
                        }
                        if self.websiteURL == ""{
                            self.websitebtn.setImage(UIImage(named:"webgray"), for: UIControlState.normal)
                        }
                        else{
                            self.websitebtn.setImage(UIImage(named:"web"), for: UIControlState.normal)
                        }
                        
                        
                        let getServiceProviderReview = jsonResults["getServiceProviderReview"] as! NSArray
                        let ServiceProviderMapping = jsonResults["ServiceProviderMapping"] as! NSArray
                        for i in 0 ..< ServiceProviderMapping.count{
                            let dict = ServiceProviderMapping[i] as! NSDictionary
                            let selected = dict["selected"] as! Bool
                            if selected == true{
                                self.serviceArr.append(dict)
                            }
                        }
                        self.serviceCountLbl.text = "Service Provided: " + String(self.serviceArr.count) + " Service(s)"
                        self.reviewCount.text = "Rating and Reviews: " + String(getServiceProviderReview.count) + " Reviews(s)"
                        self.descrioptionLbl.text = CompanyDescription
                        
                        var viewHeight : CGFloat = 0
                        let size = CGSize(width: 359, height: 0)
                        viewHeight += self.descrioptionLbl.sizeThatFits(size).height
                        self.descptionViewHeight.constant = viewHeight + 70
                        
                        let h : CGFloat = 95
                        self.socialLinksViewHeight.constant = 95
                        
                        if UIScreen.main.bounds.size.width == 414
                        {
                            self.scrollHeight.constant = 50 + viewHeight  + h
                        }
                        else if UIScreen.main.bounds.size.width == 375 && UIScreen.main.bounds.size.height == 667
                        {
                            self.scrollHeight.constant = 50 + viewHeight + 70 + h
                        }
                        else if UIScreen.main.bounds.size.width == 320 && UIScreen.main.bounds.size.height == 568
                        {
                            self.scrollHeight.constant = 110  + viewHeight + 70 + h
                        }
                        else if UIScreen.main.bounds.size.width == 320 && UIScreen.main.bounds.size.height == 480
                        {
                            self.scrollHeight.constant = 200 + viewHeight + 70 + h
                        }
                        if GallaryImages.count > 10{
                            for i in 0 ..< 10{
                                let imgDict = GallaryImages[i] as! NSDictionary
                                let imgID = imgDict["Id"] as! Int
                                let Image = imgDict["Image"] as! String
                                self.imgIdArr.append(String(imgID))
                                let img = SDWebImageSource(urlString: Image, placeholder: UIImage.sd_animatedGIF(with: fileData! as Data))
                                self.sdWebImageSource.append(img!)
                            }
                        }
                        else{
                            for i in 0 ..< GallaryImages.count{
                                let imgDict = GallaryImages[i] as! NSDictionary
                                let imgID = imgDict["Id"] as! Int
                                let Image = imgDict["Image"] as! String
                                self.imgIdArr.append(String(imgID))
                                let img = SDWebImageSource(urlString: Image, placeholder: UIImage.sd_animatedGIF(with: fileData! as Data))
                                self.sdWebImageSource.append(img!)
                            }
                        }
                        
                        
                        if GallaryImages.count != 0 {
                            self.selectecImageID = self.imgIdArr[0]
                            self.imgSlider.setImageInputs(self.sdWebImageSource)
                            self.imageCount.text = " " + "1" + "/" + String(self.imgIdArr.count) + " "
                            self.errorLbl.isHidden = true
                            self.imgSlider.isUserInteractionEnabled = true
                        }
                        else{
                            self.imageCount.text = ""
                            self.imgSlider.isUserInteractionEnabled = false
                            self.errorLbl.isHidden = false
                        }
                        self.view.hideActivityView()
                    }
                    else{
                        self.view.hideActivityView()
                        if status == "Failed"{
                            var Message = ""
                            if (jsonResults["Message"] as? String) != nil
                            {
                                Message = jsonResults["Message"] as! String
                                if Message == "Unauthorized"{
                                    _ = ModelManager.instance.deleteUserTable()
                                    let storyboard =  UIStoryboard(name: "Main", bundle: nil)
                                    let promo = storyboard.instantiateViewController(withIdentifier: "LandingPageViewController") as! LandingPageViewController
                                    self.navigationController?.pushViewController(promo, animated: true)
                                }
                            }
                        }
                    }
                    
                    
                    break
                    
                case .failure(_):
                    print(response.error!.localizedDescription)
                    let alert: UIAlertView = UIAlertView(title: "", message: response.error!.localizedDescription, delegate: nil, cancelButtonTitle: "OK");
                    alert.show()
                    self.view.hideActivityView()
                    print(response.result.error as Any)
                    break
                    
                }
            }
        }
    }
    
    @IBAction func rateAndReviewVendorBtnClk(_ sender: Any) {
        if self.vendorID == userID {
//            let textToShare = "Hi, I would be grateful if you could Rate and Review my Vendor profile on the Ayefro inc App. Thanks."
//
//            if let myWebsite = NSURL(string: "https://play.google.com/store/apps/details?id=com.ayefroinc") {
//                let objectsToShare = [textToShare, myWebsite] as [Any] as [Any]
//                let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
////                activityVC.popoverPresentationController?.sourceView = treeView
//                self.present(activityVC, animated: true, completion: nil)
//            }
            
            let buo = BranchUniversalObject(canonicalIdentifier: "/ayefroinc")
            buo.canonicalUrl = "https://ayefroinc.com/"
            buo.title = "Ayefro Inc"
            buo.contentDescription = "Ayefro Inc - Event Planner"
            buo.imageUrl = "https://ayefroinc.files.wordpress.com/2017/11/cropped-logo3.png"
            
            
            let lp: BranchLinkProperties = BranchLinkProperties()
            lp.channel = "facebook"
            lp.feature = "sharing"
            lp.campaign = "Ayefro Inc"
            lp.stage = "new user"
            lp.tags = ["one", "two", "three"]
            
            lp.addControlParam("$desktop_url", withValue: "https://ayefroinc.com/")
            
            lp.addControlParam("$ios_url", withValue: "https://itunes.apple.com/us/app/ayefro-inc/id1339116784?ls=1&mt=8")
            lp.addControlParam("$android_url", withValue: "https://play.google.com/store/apps/details?id=com.ayefroinc")
            lp.addControlParam("$match_duration", withValue: "2000")
            
            lp.addControlParam("custom_data", withValue: "yes")
            lp.addControlParam("look_at", withValue: "this")
            lp.addControlParam("nav_to", withValue: "over here")
            lp.addControlParam("random", withValue: UUID.init().uuidString)
            
            
            buo.getShortUrl(with: lp) { (url, error) in
                print(url ?? "")
            }
            
            let message = "Hi, I would be grateful if you could Rate and Review my Vendor profile on the Ayefro inc App. Thanks."
            
            buo.showShareSheet(with: lp, andShareText: message, from: self) { (activityType, completed) in
                print(activityType ?? "")
            }

        } else {
            let storyboard =  UIStoryboard(name: "Main", bundle: nil)
            let promo = storyboard.instantiateViewController(withIdentifier: "AddReviewViewController") as! AddReviewViewController
            promo.eventID = String(0)   //eventID
            promo.ServiceProviderId = self.vendorID //self.prototypeEntitiesFromJSON[indexPath.row].value(forKey: "ServiecProviderId") as! String
            promo.vender_name = self.vender_name
            self.navigationController?.pushViewController(promo, animated: true)
        }
    }
    
    @IBAction func viewGalleryBtnClk(_ sender: UIButton) {
        let backItem = UIBarButtonItem()
        backItem.title = "Back"
        backItem.tintColor = UIColor.black
        navigationItem.backBarButtonItem = backItem
        let storyboard =  UIStoryboard(name: "Main", bundle: nil)
        let promo = storyboard.instantiateViewController(withIdentifier: "ViewGalleryController") as! ViewGalleryController
        promo.imageArr = self.galleryImageArr
        promo.headerTitle = self.userName
        promo.vendorID = self.vendorID
        self.navigationController?.pushViewController(promo, animated: true)
    }
    
    @IBAction func callBtnClk(_ sender: UIButton) {
        self.callbtn.isUserInteractionEnabled = false
        DispatchQueue.main.async {
            let manager = Alamofire.SessionManager.default
            manager.session.configuration.timeoutIntervalForRequest = 120
            
            manager.request( baseURL + "api/Albums/GetContactDetails/?providerId=" + self.vendorID, method: .get, parameters: ["":""], encoding: URLEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
                
                switch(response.result) {
                case .success(_):
                    if response.result.value != nil{
                        //print(response.result.value as Any)
                    }
                    
                    let jsonResults : NSArray
                    jsonResults = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! NSArray
                    let dict = jsonResults[0] as! NSDictionary
                    let phoneNumber = dict["phoneNumber"] as! String
                    let email = dict["email"] as! String
                    
                    self.popViewController = self.storyboard?.instantiateViewController(withIdentifier: "ContactPopUpViewController")as! ContactPopUpViewController
                    self.popViewController.phoneNum = phoneNumber
                    self.popViewController.emailID = email
                    self.popViewController.delegate = self
                    self.popViewController.showInView(self.view, animated: true)
                    self.callbtn.isUserInteractionEnabled = true
                    break
                    
                case .failure(_):
                    if response.error?.localizedDescription == "The Internet connection appears to be offline."
                    {
                        let alert: UIAlertView = UIAlertView(title: "", message: response.error!.localizedDescription, delegate: nil, cancelButtonTitle: "OK");
                        alert.show()
                    }
                    else{
                        let alert: UIAlertView = UIAlertView(title: "", message: response.error!.localizedDescription, delegate: nil, cancelButtonTitle: "OK");
                        alert.show()
                        print(response.result.error as Any)
                    }
                    break
                    
                }
            }
        }
    }
    
    func emailBox(email: String){
        if MFMailComposeViewController.canSendMail() {
            let toRecipients : [String] = [email]
            let mc: MFMailComposeViewController = MFMailComposeViewController()
            mc.mailComposeDelegate = self
            mc.setToRecipients(toRecipients)
            mc.isNavigationBarHidden = true
            self.present(mc, animated: false, completion: nil)
        }
        else
        {
            let alert: UIAlertView = UIAlertView(title: "", message: "Mail app not install on your device. Please send an email to info@ayefroinc.com.com", delegate: nil, cancelButtonTitle: "OK");
            alert.show()
        }
    }
    
    @IBAction func fbBtnClk(_ sender: UIButton) {
        if fbURL != ""{
            let str = fbURL.contains(find: "http://")
            if str == false{
                fbURL = "http://" + fbURL
            }
            guard let url = URL(string:  fbURL) else {
                return //be safe
            }
            
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
        else{
            let alert: UIAlertView = UIAlertView(title: "", message: "No social media channel yet for Facebook", delegate: nil, cancelButtonTitle: "OK");
            alert.show()
        }
    }
    
    @IBAction func instagramBtnClk(_ sender: UIButton) {
        if instaURL != ""{
            let str = instaURL.contains(find: "http://")
            if str == false{
                instaURL = "http://" + instaURL
            }
            
            guard let url = URL(string: instaURL) else {
                return //be safe
            }
            
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
        else{
            let alert: UIAlertView = UIAlertView(title: "", message: "No social media channel yet for Instagram", delegate: nil, cancelButtonTitle: "OK");
            alert.show()
        }
        
    }
    
    @IBAction func twitterBtnClk(_ sender: UIButton) {
        if twitter != ""{
            let str = twitter.contains(find: "http://")
            if str == false{
                twitter = "http://" + twitter
            }
            guard let url = URL(string: twitter) else {
                return //be safe
            }
            
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
        else{
            let alert: UIAlertView = UIAlertView(title: "", message: "No social media channel yet for Twitter", delegate: nil, cancelButtonTitle: "OK");
            alert.show()
        }
    }
    
    @IBAction func webSiteBtnClk(_ sender: UIButton) {
        if websiteURL != ""{
            let str = websiteURL.contains(find: "http://")
            if str == false{
                websiteURL = "http://" + websiteURL
            }
            guard let url = URL(string: websiteURL) else {
                return //be safe
            }
            
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
        else{
            let alert: UIAlertView = UIAlertView(title: "", message: "No social media channel yet for Website", delegate: nil, cancelButtonTitle: "OK");
            alert.show()
        }
        
    }
    
    @IBAction func serviceBtnClk(_ sender: UIButton) {
        let backItem = UIBarButtonItem()
        backItem.title = "Back"
        backItem.tintColor = UIColor.black
        navigationItem.backBarButtonItem = backItem
        let storyboard =  UIStoryboard(name: "Main", bundle: nil)
        let promo = storyboard.instantiateViewController(withIdentifier: "ServicesTableViewController") as! ServicesTableViewController
        promo.serviceArr = self.serviceArr
        self.navigationController?.pushViewController(promo, animated: true)
    }
    
    @IBAction func reviewBtnCLk(_ sender: Any) {
        let backItem = UIBarButtonItem()
        backItem.title = "Back"
        backItem.tintColor = UIColor.black
        navigationItem.backBarButtonItem = backItem
        let storyboard =  UIStoryboard(name: "Main", bundle: nil)
        let promo = storyboard.instantiateViewController(withIdentifier: "ReviewsViewController") as! ReviewsViewController
        promo.dataArr = self.reviewArr
        promo.redirectioValue = "SP"
        self.navigationController?.pushViewController(promo, animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        switch result.rawValue {
        case MFMailComposeResult.cancelled.rawValue:
            //NSLog("Mail Cancelled")
            self.dismiss(animated: false, completion: nil)
            break
        case MFMailComposeResult.saved.rawValue:
            //NSLog("Mail Saved")
            self.dismiss(animated: false, completion: nil)
            break
        case MFMailComposeResult.failed.rawValue:
            //NSLog("Mail Sent Fail", [error!.localizedDescription])
            self.dismiss(animated: false, completion: nil)
            break
        case MFMailComposeResult.sent.rawValue:
            //NSLog("Mail Sent")
            self.dismiss(animated: false, completion: nil)
            break
            
        default:
            break
        }
    }
 

}

extension String {
    func contains(find: String) -> Bool{
        return self.range(of: find) != nil
    }
    func containsIgnoringCase(find: String) -> Bool{
        return self.range(of: find, options: .caseInsensitive) != nil
    }
}

