//
//  ServicesTableViewController.swift
//  AyefroInc
//
//  Created by Vijay Darkonde on 20/12/17.
//  Copyright © 2017 Inceptive Consulting Pvt. Ltd. All rights reserved.
//

import UIKit

class ServicesTableViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var errorView: UIView!
    
    
    var menuArr: [String] = []
    var serviceArr : [NSDictionary]! = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if serviceArr.count != 0
        {
            for i in 0 ..< serviceArr.count{
                let dict = serviceArr[i] as! NSDictionary
                let ServiceName = dict["ServiceName"] as! String
                self.menuArr.append(ServiceName)
            }
            self.tableView.reloadData()
            tableView.isHidden = false
            errorView.isHidden = true
        }
        else{
            tableView.isHidden = true
            errorView.isHidden = false
        }
        
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "Cell")
        self.tableView.tableFooterView = UIView()
        
        self.title = "Selected Services"
        // Do any additional setup after loading the view.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as UITableViewCell
        cell.textLabel?.text = menuArr[indexPath.row]
        cell.textLabel?.font = UIFont(name:"HelveticaNeue", size:17)
        cell.textLabel?.textColor = UIColor.darkGray
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        cell.textLabel?.lineBreakMode = .byWordWrapping
        cell.textLabel?.numberOfLines = 0
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
 

}
