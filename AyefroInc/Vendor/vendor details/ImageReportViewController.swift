//
//  ImageReportViewController.swift
//  AyefroInc
//
//  Created by Vijay Darkonde on 20/12/17.
//  Copyright © 2017 Inceptive Consulting Pvt. Ltd. All rights reserved.
//

import UIKit
import KMPlaceholderTextView
import DropDown
import Alamofire

class ImageReportViewController: UIViewController {
    
    @IBOutlet var popUpView: UIView!
    @IBOutlet weak var chooseArticleButton: UIButton!
    @IBOutlet var commentTxtView: KMPlaceholderTextView!
    @IBOutlet var submitBtn: UIButton!
    
    let chooseArticleDropDown = DropDown()
    lazy var dropDowns: [DropDown] = {
        return [
            self.chooseArticleDropDown
        ]}()
    
    var category: [String] = ["Incorrect Information","Offensive","Advertisement","Fake Account","Pornography or Obscenity","Other"]
    var a_id : String = ""
    var action: String! = ""
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    override init(nibName nibNameOrNil: String!, bundle nibBundleOrNil: Bundle!) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        self.popUpView.layer.cornerRadius = 5
        self.popUpView.layer.shadowOpacity = 0.0
        self.popUpView.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        dropDowns.forEach { $0.dismissMode = .onTap }
        dropDowns.forEach { $0.direction = .any }
        setupChooseArticleDropDown()
        
    }
    
    func setupChooseArticleDropDown() {
        chooseArticleDropDown.anchorView = chooseArticleButton
        chooseArticleDropDown.bottomOffset = CGPoint(x: 0, y: chooseArticleButton.bounds.height)
        chooseArticleDropDown.dataSource = category
        chooseArticleDropDown.selectionAction = { [unowned self] (index, item) in
            self.chooseArticleButton.setTitle(item, for: .normal)
        }
    }
    
    @IBAction func chooseArticle(_ sender: AnyObject) {
        chooseArticleDropDown.show()
    }
    
    
    func showInView(_ aView: UIView!, animated: Bool)
    {
        aView.addSubview(self.view)
        if animated
        {
            self.showAnimate()
        }
    }
    
    func showAnimate()
    {
        self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view.alpha = 0.0;
        UIView.animate(withDuration: 0.25, animations: {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        });
    }
    
    func removeAnimate()
    {
        UIView.animate(withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view.alpha = 0.0;
        }, completion:{(finished : Bool)  in
            if (finished)
            {
                self.view.removeFromSuperview()
            }
        });
    }
    
    @IBAction func backBtnClk(_ sender: AnyObject) {
        commentTxtView.resignFirstResponder()
        removeAnimate()
    }
    @IBAction func saveBtnClk(_ sender: AnyObject) {
        
        commentTxtView.resignFirstResponder()
        if commentTxtView.text.isEmpty
        {
            commentTxtView.layer.borderColor = UIColor.red.cgColor
            commentTxtView.layer.borderWidth = 1
        }
        else if self.chooseArticleButton.titleLabel?.text == "Select reason" {
            let alert: UIAlertView = UIAlertView(title: "", message: "Please Select Reason", delegate: nil, cancelButtonTitle: "OK");
            alert.show()
        }
        else
        {
            self.submitBtn.isUserInteractionEnabled = false
            if action == "Comment"{
                DispatchQueue.main.async {
                    let parameters : [String: String] = [
                        "Reason": (self.chooseArticleButton.titleLabel?.text)!,
                        "ReasonDescription": self.commentTxtView.text as String,
                        "type": "ImgComment",
                        "User_Id": userID,
                        "Id": self.a_id
                    ]
                    
                    let manager = Alamofire.SessionManager.default
                    manager.session.configuration.timeoutIntervalForRequest = 120
                    
                    manager.request(baseURL + "api/ImageComments/ReportImage", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
                        
                        switch(response.result) {
                        case .success(_):
                            if response.result.value != nil{
                                //print(response.result.value as Any)
                            }
                            self.submitBtn.isUserInteractionEnabled = true
                            self.removeAnimate()
                            break
                            
                        case .failure(_):
                            print(response.error!.localizedDescription)
                            let alert: UIAlertView = UIAlertView(title: "", message: response.error!.localizedDescription, delegate: nil, cancelButtonTitle: "OK");
                            alert.show()
                            self.view.hideActivityView()
                            self.submitBtn.isUserInteractionEnabled = true
                            print(response.result.error as Any)
                            break
                            
                        }
                    }
                }
            }
            else{
                DispatchQueue.main.async {
                    let parameters : [String: String] = [
                        "Reason": (self.chooseArticleButton.titleLabel?.text)!,
                        "ReasonDescription": self.commentTxtView.text as String,
                        "type": "Image",
                        "User_Id": userID,
                        "Id": self.a_id
                    ]
                    
                    let manager = Alamofire.SessionManager.default
                    manager.session.configuration.timeoutIntervalForRequest = 120
                    
                    manager.request(baseURL + "api/Questions/PostTriggeredComment", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
                        
                        switch(response.result) {
                        case .success(_):
                            if response.result.value != nil{
                                //print(response.result.value as Any)
                            }
                            self.submitBtn.isUserInteractionEnabled = true
                            self.removeAnimate()
                            break
                            
                        case .failure(_):
                            print(response.error!.localizedDescription)
                            let alert: UIAlertView = UIAlertView(title: "", message: response.error!.localizedDescription, delegate: nil, cancelButtonTitle: "OK");
                            alert.show()
                            self.view.hideActivityView()
                            self.submitBtn.isUserInteractionEnabled = true
                            print(response.result.error as Any)
                            break
                            
                        }
                    }
                }
            }
            
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}


