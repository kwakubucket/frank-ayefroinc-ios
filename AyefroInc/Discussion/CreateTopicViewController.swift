//
//  CreateTopicViewController.swift
//  AyefroInc
//
//  Created by Vijay Darkonde on 16/11/17.
//  Copyright © 2017 Inceptive Consulting Pvt. Ltd. All rights reserved.
//

import UIKit
import KMPlaceholderTextView
import DropDown
import Alamofire


class CreateTopicViewController: UIViewController, UITextViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet var postBtn: UIButton!
    @IBOutlet var titleTF: NiceTextField!
    @IBOutlet var descriptionTV: KMPlaceholderTextView!
    @IBOutlet weak var chooseArticleButton: UIButton!
    @IBOutlet var imageView: UIImageView!
    
    var delegate : postQuestion!
    
    let chooseArticleDropDown = DropDown()
    lazy var dropDowns: [DropDown] = {
        return [
            self.chooseArticleDropDown
        ]}()
    
    var category: [String] = []
    var categoryID : [String] = []
    
    var selectedIndex : Int = 0
    var base64Str : String! = ""
    
    let imagePicker = UIImagePickerController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Create a New Topic"
        self.tabBarController?.tabBar.isHidden = true
        dropDowns.forEach { $0.dismissMode = .onTap }
        dropDowns.forEach { $0.direction = .any }
        descriptionTV.delegate = self
        category.remove(at: 0)
        categoryID.remove(at: 0)
        setupChooseArticleDropDown()
        imagePicker.delegate = self
        
        let button = UIButton(type: .system)
        button.setTitle(" Back", for: .normal)
        button.setImage(UIImage(named: "backbtn"), for: .normal)
        button.addTarget(self, action: #selector(reset), for: .touchUpInside)
        button.tintColor = UIColor.black
        button.contentHorizontalAlignment = .left
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: button)
        
        
        // Do any additional setup after loading the view.
    }
    
    @objc func reset()  {
        if titleTF.text == "" && descriptionTV.text == "" && base64Str == "" {
            self.navigationController?.popViewController(animated: true)
        }
        else{
            let refreshAlert = UIAlertController(title: "", message: "You have not finished posting. People want to hear what you have to say!", preferredStyle: UIAlertControllerStyle.alert)
            
            refreshAlert.addAction(UIAlertAction(title: "Discard", style: .cancel , handler: { (action: UIAlertAction!) in
                self.navigationController?.popViewController(animated: true)
            }))
            
            
            refreshAlert.addAction(UIAlertAction(title: "Continue", style: .default, handler: { (action: UIAlertAction!) in
            }))
            
            self.present(refreshAlert, animated: true, completion: nil)
        }
    }
    
    override func viewDidLayoutSubviews() {
        self.automaticallyAdjustsScrollViewInsets = false
    }
    
    func setupChooseArticleDropDown() {
        chooseArticleDropDown.anchorView = chooseArticleButton
        chooseArticleDropDown.bottomOffset = CGPoint(x: 0, y: chooseArticleButton.bounds.height)
        chooseArticleDropDown.dataSource = category
        chooseArticleDropDown.selectionAction = { [unowned self] (index, item) in
            self.chooseArticleButton.setTitle(item, for: .normal)
            self.selectedIndex = index
        }
    }
    
    @IBAction func chooseArticle(_ sender: AnyObject) {
        chooseArticleDropDown.show()
    }
    
    
    @IBAction func postBtnClk(_ sender: UIButton) {
        print(self.chooseArticleButton.titleLabel!.text)
        if titleTF.text == ""{
            let alert: UIAlertView = UIAlertView(title: "", message: "Please Enter Title of Topic", delegate: nil, cancelButtonTitle: "OK");
            alert.show()
        }
        else if descriptionTV.text == ""{
            let alert: UIAlertView = UIAlertView(title: "", message: "Please Enter Description of Topic", delegate: nil, cancelButtonTitle: "OK");
            alert.show()
        }
        else if self.chooseArticleButton.titleLabel!.text == "Select a Topic Category " as String{
            let alert: UIAlertView = UIAlertView(title: "", message: "Please Select Topic Category", delegate: nil, cancelButtonTitle: "OK");
            alert.show()
        }
        else if (descriptionTV.text.trimmingCharacters(in: .whitespaces).isEmpty) {
            // string contains non-whitespace characters
            let alert: UIAlertView = UIAlertView(title: "", message: "Please Enter Description of Topic", delegate: nil, cancelButtonTitle: "OK");
            alert.show()
        }
        else if (titleTF.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            // string contains non-whitespace characters
            let alert: UIAlertView = UIAlertView(title: "", message: "Please Enter Title of Topic", delegate: nil, cancelButtonTitle: "OK");
            alert.show()
        }
        else{
            postBtn.isUserInteractionEnabled = false
            DispatchQueue.main.async {
                let parameters : [String: String] = [
                    "Que_text": self.titleTF.text!,
                    "Que_Description": self.descriptionTV.text as String,
                    "topic_Id": self.categoryID[self.selectedIndex],
                    "User_Id": userID,
                    "image": self.base64Str
                ]
                
                let manager = Alamofire.SessionManager.default
                manager.session.configuration.timeoutIntervalForRequest = 120
                
                manager.request(baseURL + "api/Questions/PostQuestion", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
                    
                    switch(response.result) {
                    case .success(_):
                        if response.result.value != nil{
                            //print(response.result.value as Any)
                        }
                        let jsonResults : NSDictionary
                        jsonResults = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! NSDictionary
                        let status = jsonResults["status"] as! String
                        if status == "Success"{
                            let alert: UIAlertView = UIAlertView(title: "", message: "Successful", delegate: nil, cancelButtonTitle: "OK");
                            alert.show()
                            self.navigationController?.popViewController(animated: true)
                            self.delegate.updateList(topicID: "0")
                            
                        }
                        self.postBtn.isUserInteractionEnabled = true
                        break
                        
                    case .failure(_):
                        print(response.error!.localizedDescription)
                        let alert: UIAlertView = UIAlertView(title: "", message: response.error!.localizedDescription, delegate: nil, cancelButtonTitle: "OK");
                        alert.show()
                        self.view.hideActivityView()
                        self.postBtn.isUserInteractionEnabled = true
                        print(response.result.error as Any)
                        break
                        
                    }
                }
            }
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        let numberOfChars = newText.characters.count
        return numberOfChars < 1000
    }
    
    @IBAction func buttonOnClick(_ sender: UIButton)
    {
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallary()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera))
        {
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openGallary()
    {
        imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        imagePicker.allowsEditing = false
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    internal func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            imageView.image = pickedImage
            let imageData: NSData = UIImageJPEGRepresentation(pickedImage, 0.4)! as NSData
            let imageStr = imageData.base64EncodedString(options: .lineLength64Characters)
            base64Str = imageStr
        }
        
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }

    
    @IBAction func action(_ sender: UIBarButtonItem) {
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
}
