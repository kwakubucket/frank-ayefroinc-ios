//
//  RepliesViewController.swift
//  AyefroInc
//
//  Created by Vijay Darkonde on 17/12/17.
//  Copyright © 2017 Inceptive Consulting Pvt. Ltd. All rights reserved.
//

import UIKit
import Alamofire
import SystemConfiguration

class RepliesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var prototypeEntitiesFromJSON: [RepliesEntity] = []
    
    
    @IBOutlet var tableView: UITableView!
    var jsonResults: NSArray!
    
    var selectedIndex : IndexPath!
    var QuestionId : String = ""

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Replies"
        
        self.tableView.register(UINib.init(nibName: "RepliesCell", bundle: nil), forCellReuseIdentifier: "RepliesCell")
        self.tableView.fd_debugLogEnabled = true
        self.tableView.tableFooterView = UIView()
        
        let jsonArr = NSMutableArray()
        for i in 0 ..< jsonResults.count{
            let dicWithoutNulls = jsonResults[i] as! NSDictionary
            
            let outputDict = self.removeNSNull(from: dicWithoutNulls.mutableCopy() as! NSMutableDictionary)
            
            jsonArr.add(outputDict)
        }
        
        let arr = NSArray(array: jsonArr)
        for i in 0 ..< arr.count{
            self.prototypeEntitiesFromJSON.append(RepliesEntity(dictionary: arr[i] as! [AnyHashable : Any] ))
        }
        
        self.tableView.reloadData()
        
    }
    
    override func viewDidLayoutSubviews() {
        self.automaticallyAdjustsScrollViewInsets = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.setBackgroundImage(nil, for: .default)
        self.navigationController?.navigationBar.shadowImage = nil
    }
    
    func removeNSNull(from dict: NSMutableDictionary) -> NSDictionary {
        let mutableDict = dict
        let keysWithEmptString = dict.filter { $0.1 is NSNull }.map { $0.0 }
        for key in keysWithEmptString {
            mutableDict[key] = ""
        }
        
        return mutableDict
    }
    
    // MARK: - UITableViewDataSource
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let count = prototypeEntitiesFromJSON.count
        if count != 0 {
            return count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RepliesCell") as? RepliesCell
        if prototypeEntitiesFromJSON.count != 0{
            configureCell(cell!, at: indexPath)
        }
        return cell!
    }
    
    
    func configureCell(_ cell: RepliesCell, at indexPath: IndexPath) {
        if prototypeEntitiesFromJSON.count != 0{
            cell.fd_enforceFrameLayout = true
            cell.entity = prototypeEntitiesFromJSON[indexPath.row]
            cell.replyBtn.addTarget(self, action: #selector(self.replyBtnClk), for: UIControlEvents.touchUpInside)
            
            let tap2 = UITapGestureRecognizer(target: self, action: #selector(self.tapOnProfilePic))
            cell.profileImageView.isUserInteractionEnabled = true
            cell.profileImageView.addGestureRecognizer(tap2)
            
            let tap3 = UITapGestureRecognizer(target: self, action: #selector(self.tapOnProfilePic))
            cell.userNameLbl.isUserInteractionEnabled = true
            cell.userNameLbl.addGestureRecognizer(tap3)
            
            let tap4 = UITapGestureRecognizer(target: self, action: #selector(self.tapOnProfilePic))
            cell.activityLbl.isUserInteractionEnabled = true
            cell.activityLbl.addGestureRecognizer(tap4)
        }
        
    }
    
    @objc func tapOnProfilePic(_ sender: UITapGestureRecognizer) {
        let backItem = UIBarButtonItem()
        backItem.title = "Back"
        backItem.tintColor = UIColor.black
        navigationItem.backBarButtonItem = backItem
        var cell: RepliesCell?
        var view = sender.view
        while view != nil {
            if view is RepliesCell {
                cell = view as? RepliesCell
            }
            if view is UITableView {
                tableView = view as? UITableView
            }
            view = view?.superview
        }
        
        if let indexPath = (cell != nil) ? tableView?.indexPath(for: cell!) : nil {
            let role = prototypeEntitiesFromJSON[indexPath.row].replyUserRole
            if role == 3{
                let backItem = UIBarButtonItem()
                backItem.title = "Back"
                backItem.tintColor = UIColor.black
                navigationItem.backBarButtonItem = backItem
                let storyboard =  UIStoryboard(name: "Main", bundle: nil)
                let promo = storyboard.instantiateViewController(withIdentifier: "VendorDetailsViewController") as! VendorDetailsViewController
                promo.vendorID = self.prototypeEntitiesFromJSON[indexPath.row].value(forKey: "ReplySendUser") as! String
                promo.poptoRoot = true
                self.navigationController?.pushViewController(promo, animated: true)
            }
            
        }
    }
    
    // MARK: - UITableViewDelegate
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return tableView.fd_heightForCell(withIdentifier: "RepliesCell") { cell in
            self.configureCell(cell as! RepliesCell, at: indexPath)
        }
    }
    
    func isConnectedToNetwork() -> Bool {
        
        var zeroAddress = sockaddr_in(sin_len: 0, sin_family: 0, sin_port: 0, sin_addr: in_addr(s_addr: 0), sin_zero: (0, 0, 0, 0, 0, 0, 0, 0))
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        
        return (isReachable && !needsConnection)
        
    }
    
    @objc func replyBtnClk(_ sender: UIButton) {
        if isConnectedToNetwork() == true{
            let point = self.tableView.convert(CGPoint.zero, from: sender)
            var Reply_Id = ""
            var isHidden : Bool! = false
            if let indexPath = self.tableView.indexPathForRow(at: point) {
                Reply_Id = String(self.prototypeEntitiesFromJSON[indexPath.row].value(forKey: "ReplyId") as! Int)
                let likevalue = self.prototypeEntitiesFromJSON[indexPath.row].isliked
                if likevalue == true{
                    self.prototypeEntitiesFromJSON[indexPath.row].isliked = false
                    self.prototypeEntitiesFromJSON[indexPath.row].replyLikeCount = self.prototypeEntitiesFromJSON[indexPath.row].replyLikeCount - 1
                }
                else{
                    self.prototypeEntitiesFromJSON[indexPath.row].isliked = true
                    self.prototypeEntitiesFromJSON[indexPath.row].replyLikeCount = self.prototypeEntitiesFromJSON[indexPath.row].replyLikeCount + 1
                }
                
            }
            self.tableView.reloadData()
            DispatchQueue.main.async {
                
                let parameters : [String: String] = [
                    "Reply_Id": Reply_Id,
                    "User_Id": userID,
                    ]
                
                let manager = Alamofire.SessionManager.default
                manager.session.configuration.timeoutIntervalForRequest = 120
                
                manager.request(baseURL + "api/Likes/ReplyLike", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
                    
                    switch(response.result) {
                    case .success(_):
                        if response.result.value != nil{
                            //print(response.result.value as Any)
                        }
                        //                    self.buildTestData {
                        //
                        //                    }
                        break
                        
                    case .failure(_):
                        print(response.error!.localizedDescription)
                        let alert: UIAlertView = UIAlertView(title: "", message: response.error!.localizedDescription, delegate: nil, cancelButtonTitle: "OK");
                        alert.show()
                        self.view.hideActivityView()
                        print(response.result.error as Any)
                        break
                        
                    }
                }
            }
            
        }
        
    }
    
    func buildTestData(then: @escaping () -> Void) {
        self.prototypeEntitiesFromJSON = []
        self.tableView.reloadData()
        DispatchQueue.main.async {
            let manager = Alamofire.SessionManager.default
            manager.session.configuration.timeoutIntervalForRequest = 120
            
            manager.request( baseURL + "api/Answers/GetAllAnswersOfQuestion/?id=" + self.QuestionId + "&userId=" + userID, method: .get, parameters: ["":""], encoding: URLEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
                
                switch(response.result) {
                case .success(_):
                    if response.result.value != nil{
                        //print(response.result.value as Any)
                    }
                    let jsonResults : NSDictionary
                    jsonResults = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! NSDictionary
                    let jsonArr = NSMutableArray()
                    let AnsList = jsonResults["AnsList"] as! NSArray
                    let ansDict = AnsList[self.selectedIndex.row] as! NSDictionary
                    let replyList = ansDict["ReplyList"] as! NSArray
                    
                    for i in 0 ..< replyList.count{
                        let dicWithoutNulls = replyList[i] as! NSDictionary
                        
                        let outputDict = self.removeNSNull(from: dicWithoutNulls.mutableCopy() as! NSMutableDictionary)
                        
                        jsonArr.add(outputDict)
                    }
                    
                    let arr = NSArray(array: jsonArr)
                    for i in 0 ..< arr.count{
                        self.prototypeEntitiesFromJSON.append(RepliesEntity(dictionary: arr[i] as! [AnyHashable : Any] ))
                    }
                    
                    self.tableView.reloadData()
                    break
                    
                case .failure(_):
                    print(response.error!.localizedDescription)
                    let alert: UIAlertView = UIAlertView(title: "", message: response.error!.localizedDescription, delegate: nil, cancelButtonTitle: "OK");
                    alert.show()
                    self.view.hideActivityView()
                    print(response.result.error as Any)
                    break
                    
                }
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
}


