//
//  DiscussionViewController.swift
//  AyefroInc
//
//  Created by Vijay Darkonde on 15/11/17.
//  Copyright © 2017 Inceptive Consulting Pvt. Ltd. All rights reserved.
//

import UIKit
import Alamofire
import DropDown
import SDWebImage
import MessageUI

protocol postQuestion
{
    func updateList(topicID: String)
}

class DiscussionViewController: UIViewController, UITableViewDelegate,UITableViewDataSource, postQuestion, emailSend , MFMailComposeViewControllerDelegate{
    
    var feedEntitySections: [[FDFeedEntity]] = []
    
    var prototypeEntitiesFromJSON: [FDFeedEntity] = []
    
    @IBOutlet weak var twitterX: NSLayoutConstraint!
    @IBOutlet weak var fbX: NSLayoutConstraint!
    @IBOutlet weak var instaX: NSLayoutConstraint!
    @IBOutlet var headerView: UIView!
    @IBOutlet var loader: UIImageView!
    @IBOutlet var termsLbl: UILabel!
    @IBOutlet var tableView: UITableView!
    @IBOutlet var createBtn: UIButton!
    @IBOutlet var bgView: UIView!
    
    @IBOutlet weak var twitterBtn: UIButton!
    @IBOutlet weak var instaBtn: UIButton!
    @IBOutlet weak var fbBtn: UIButton!
    @IBOutlet weak var linksView: UIView!
    var category: [String] = ["All Topics"]
    var categoryID : [String] = ["0"]
    var activeState : [Bool] = []
    var QuestionId: String = ""
    var selectedIndex : Int = 0
    let chooseArticleDropDown = DropDown()
    
    let button = UIButton(type: .system)
    
    let statusHeightDefault : CGFloat = 20
    var statusHeight : CGFloat!
    let screenHeight : CGFloat = UIScreen.main.bounds.height
    var labelBottomY : CGFloat!
    lazy var dropDowns: [DropDown] = {
        return [
            self.chooseArticleDropDown
        ]}()
    
    var refreshControl: UIRefreshControl!
    var statusBarHidden = false
    var popViewController : ContactPopUpViewController!

    var fbURL : String! = ""
    var instaURL: String! = ""
    var twitter: String! = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        _ = ModelManager.instance.getUserLoginStatus()
        
        self.tableView.register(FDFeedCell.self, forCellReuseIdentifier: "cell")
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.allowsSelection = true
        self.tableView.fd_debugLogEnabled = true
        createBtn.layer.cornerRadius = 5
        bgView.layer.cornerRadius = 3
        
        self.tableView.tableFooterView = UIView()
        
        dropDowns.forEach { $0.dismissMode = .onTap }
        dropDowns.forEach { $0.direction = .any }
        
         self.linksView.isHidden = true
        self.tabBarController?.tabBar.isHidden = false
        
        refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: "Reloading")
        refreshControl.addTarget(self, action: #selector(refresh), for: UIControlEvents.valueChanged)
        tableView.addSubview(refreshControl)
        
        
        termsLbl.text = "Welcome to Ayefro Inc Forums!  Read the guidelines and Terms And Conditions here."
        let text = (termsLbl.text)!
        let underlineAttriString = NSMutableAttributedString(string: text)
        let range1 = (text as NSString).range(of: "Terms And Conditions")
        underlineAttriString.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor(red: 0.0/255.0, green: 122.0/255.0, blue: 255.0/255.0, alpha: 1.0), range: range1)
        
        termsLbl.attributedText = underlineAttriString
        termsLbl.isUserInteractionEnabled = true
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapLabel))
        termsLbl.addGestureRecognizer(tapGesture)
        
        let str: String? = Bundle.main.path(forResource: "loading_spinner", ofType: "gif")
        let fileData = NSData(contentsOfFile: str!)
        loader.image = UIImage.sd_animatedGIF(with: fileData! as Data)
        self.tableView.backgroundColor = UIColor.white
        
        _ = ModelManager.instance.getUserLoginStatus()
        let path = Bundle.main.path(forResource: "countryCode", ofType: "json")
        do{
            let data = try Data(contentsOf: URL(fileURLWithPath: path!), options: .mappedIfSafe)
            let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves) as! NSDictionary
            let countries = jsonResult.object(forKey: "countries") as! NSDictionary
            let country = countries.object(forKey: "country") as! NSArray
            for i in 0 ..< country.count
            {
                let countryObj = country[i] as! NSDictionary
                let countryName = countryObj.object(forKey: "countryName") as! String
                if Country == countryName{
                    let currencySymbol = countryObj.object(forKey: "currencySymbol") as! String
                    UserDefaults.standard.setValue(currencySymbol, forKey: "currencySymbol")
                    print("\(UserDefaults.standard.value(forKey: "currencySymbol")!)")
                    break
                }
            }
            
            
        }
        catch{
            
        }
        
        let leadingConstraint = NSLayoutConstraint(item: headerView, attribute: NSLayoutAttribute.leading, relatedBy: NSLayoutRelation.equal, toItem: headerView, attribute: NSLayoutAttribute.leading, multiplier: 1.0, constant: 8.0)
        
        let trailingConstraint = NSLayoutConstraint(item: headerView, attribute: NSLayoutAttribute.trailing, relatedBy: NSLayoutRelation.equal, toItem: headerView, attribute: NSLayoutAttribute.trailing, multiplier: 1.0, constant: -8.0)
        
        headerView.addConstraint(leadingConstraint)
        headerView.addConstraint(trailingConstraint)
        
        let topConstraint = NSLayoutConstraint(item: headerView, attribute: NSLayoutAttribute.top, relatedBy: NSLayoutRelation.equal, toItem: headerView, attribute: NSLayoutAttribute.top, multiplier: 1.0, constant: 0)
        
        let bottomConstraint = NSLayoutConstraint(item: headerView, attribute: NSLayoutAttribute.bottom, relatedBy: NSLayoutRelation.equal, toItem: headerView, attribute: NSLayoutAttribute.bottom, multiplier: 1.0, constant: 0)
        
        headerView.addConstraint(topConstraint)
        headerView.addConstraint(bottomConstraint)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            if forumNotification == true{
                let storyboard =  UIStoryboard(name: "Main", bundle: nil)
                let promo = storyboard.instantiateViewController(withIdentifier: "ForumDetailViewController") as! ForumDetailViewController
                promo.QuestionId = q_id
                promo.viewtitle = ""
                reloadValue = true
                forumNotification = false
                self.navigationController?.pushViewController(promo, animated: true)
            }
        }
        
        buildTestData {
        }
        
        let tapGesture1: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UserHomeViewController.removeView))
        self.view.addGestureRecognizer(tapGesture1)
        
    }
    
    @objc func removeView()  {
        linksView.isHidden = true
    }
    
    @objc func backButtonAction() {
        
    }
    
    @objc func tapLabel(gesture: UITapGestureRecognizer) {
        let backItem = UIBarButtonItem()
        backItem.title = "Back"
        backItem.tintColor = UIColor.black
        navigationItem.backBarButtonItem = backItem
        let storyboard =  UIStoryboard(name: "Main", bundle: nil)
        let promo = storyboard.instantiateViewController(withIdentifier: "ForumTermsViewController") as! ForumTermsViewController
        self.navigationController?.pushViewController(promo, animated: true)
    }
    
    
    @objc func showCategories() {
        chooseArticleDropDown.show()
    }
    
    func setupChooseArticleDropDown() {
        chooseArticleDropDown.anchorView = button
        chooseArticleDropDown.bottomOffset = CGPoint(x: 0, y: button.bounds.height)
        chooseArticleDropDown.dataSource = category
        chooseArticleDropDown.selectionAction = { [unowned self] (index, item) in
            self.button.setTitle(item, for: .normal)
            self.button.semanticContentAttribute = .forceRightToLeft
            
            self.selectedIndex = index
            self.updateList(topicID: self.categoryID[index])
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        getAllTopic()
        self.tabBarController?.tabBar.isHidden = false
        self.navigationItem.rightBarButtonItem = nil
       
        if reloadValue == true{
            reloadValue = false
            buildTestData {
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        button.semanticContentAttribute = .forceRightToLeft
        button.setTitle("             All Topics", for: .normal)
        button.setImage(UIImage(named: "drop"), for: .normal)
        button.addTarget(self, action: #selector(showCategories), for: .touchUpInside)
        button.tintColor = UIColor.black
        button.titleLabel?.textAlignment = NSTextAlignment.right
        button.titleLabel?.lineBreakMode = NSLineBreakMode.byTruncatingTail
        button.sizeToFit()
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: button)
        
    }
    
    override func viewDidLayoutSubviews() {
        self.automaticallyAdjustsScrollViewInsets = false
        if UIScreen.main.bounds.size.width == 414
        {
            self.shyNavBarManager.scrollView = self.tableView;
        }
        else if UIScreen.main.bounds.size.width == 375 && UIScreen.main.bounds.size.height == 667
        {
            self.shyNavBarManager.scrollView = self.tableView;
        }
        else if UIScreen.main.bounds.size.width == 320 && UIScreen.main.bounds.size.height == 568
        {
            self.shyNavBarManager.scrollView = self.tableView;
        }
        else if UIScreen.main.bounds.size.width == 320 && UIScreen.main.bounds.size.height == 480
        {
            
        }
    }
    
    
    @objc func refresh() {
        // Code to refresh table view
        self.refreshControl.endRefreshing()
        self.prototypeEntitiesFromJSON = []
        self.tableView.reloadData()
        self.loader.isHidden = true
        DispatchQueue.global().async {
            // Data from `data.json`
            let manager = Alamofire.SessionManager.default
            manager.session.configuration.timeoutIntervalForRequest = 120
            
            manager.request(baseURL + "api/Questions/GetAllQuestions/?id=" + userID + "&topicid=0", method: .get, parameters: ["":""], encoding: URLEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
                
                switch(response.result) {
                case .success(_):
                    if response.result.value != nil{
                        let jsonResults : NSDictionary
                        jsonResults = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! NSDictionary
                        let ForumList = jsonResults["ForumList"] as! NSArray
                        let jsonArr = NSMutableArray()
                        for i in 0 ..< ForumList.count{
                            let dicWithoutNulls = ForumList[i] as! NSDictionary
                            
                            let outputDict = self.removeNSNull(from: dicWithoutNulls.mutableCopy() as! NSMutableDictionary)
                            
                            jsonArr.add(outputDict)
                        }
                        
                        let arr = NSArray(array: jsonArr)
                        for i in 0 ..< arr.count{
                            self.prototypeEntitiesFromJSON.append(FDFeedEntity(dictionary: arr[i] as! [AnyHashable : Any] ))
                        }
                        
                    }
                    self.tableView.reloadData()
                    
                    break
                    
                case .failure(_):
                    print(response.error!.localizedDescription)
                    let alert: UIAlertView = UIAlertView(title: "", message: response.error!.localizedDescription, delegate: nil, cancelButtonTitle: "OK");
                    alert.show()
                    self.view.hideActivityView()
                    print(response.result.error as Any)
                    break
                    
                }
            }
        }
    }
    
    
    func buildTestData(then: @escaping () -> Void) {
        // Simulate an async request
        self.loader.isHidden = false
        self.tableView.backgroundColor = UIColor.white
        DispatchQueue.main.async {
            
            // Data from `data.json`
            self.prototypeEntitiesFromJSON = []
            self.tableView.reloadData()
            let manager = Alamofire.SessionManager.default
            manager.session.configuration.timeoutIntervalForRequest = 120
            
            manager.request(baseURL + "api/Questions/GetAllQuestions/?id=" + userID + "&topicid=0", method: .get, parameters: ["":""], encoding: URLEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
                
                switch(response.result) {
                case .success(_):
                    if response.result.value != nil{
                        self.tableView.backgroundColor = UIColor(red: 230.0/255.0, green: 230.0/255.0, blue: 230.0/255.0, alpha: 1.0)
                        self.loader.isHidden = true
                        let jsonResults : NSDictionary
                        jsonResults = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! NSDictionary
                        let ForumList = jsonResults["ForumList"] as! NSArray
                        let jsonArr = NSMutableArray()
                        for i in 0 ..< ForumList.count{
                            let dicWithoutNulls = ForumList[i] as! NSDictionary
                            
                            let outputDict = self.removeNSNull(from: dicWithoutNulls.mutableCopy() as! NSMutableDictionary)
                            
                            jsonArr.add(outputDict)
                        }
                        
                        let arr = NSArray(array: jsonArr)
                        for i in 0 ..< arr.count{
                            self.prototypeEntitiesFromJSON.append(FDFeedEntity(dictionary: arr[i] as! [AnyHashable : Any] ))
                        }
                    }
                    self.tableView.reloadData()
                    break
                    
                case .failure(_):
                    print(response.error!.localizedDescription)
                    let alert: UIAlertView = UIAlertView(title: "", message: response.error!.localizedDescription, delegate: nil, cancelButtonTitle: "OK");
                    alert.show()
                    self.view.hideActivityView()
                    print(response.result.error as Any)
                    break
                    
                }
            }
        }
    }
    
    func updateList(topicID: String)  {
        DispatchQueue.main.async {
            self.prototypeEntitiesFromJSON = []
            let manager = Alamofire.SessionManager.default
            manager.session.configuration.timeoutIntervalForRequest = 120
            manager.request(baseURL + "api/Questions/GetAllQuestions/?id=" + userID + "&topicid=" + topicID, method: .get, parameters: ["":""], encoding: URLEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
                
                switch(response.result) {
                case .success(_):
                    if response.result.value != nil{
                        let jsonResults : NSDictionary
                        jsonResults = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! NSDictionary
                        let ForumList = jsonResults["ForumList"] as! NSArray
                        
                        let jsonArr = NSMutableArray()
                        for i in 0 ..< ForumList.count{
                            let dicWithoutNulls = ForumList[i] as! NSDictionary
                            
                            let outputDict = self.removeNSNull(from: dicWithoutNulls.mutableCopy() as! NSMutableDictionary)
                            
                            jsonArr.add(outputDict)
                        }
                        
                        let arr = NSArray(array: jsonArr)
                        for i in 0 ..< arr.count{
                            self.prototypeEntitiesFromJSON.append(FDFeedEntity(dictionary: arr[i] as! [AnyHashable : Any] ))
                        }
                        
                        
                    }
                    self.tableView.reloadData()
                    self.view.hideActivityView()
                    break
                    
                case .failure(_):
                    print(response.error!.localizedDescription)
                    let alert: UIAlertView = UIAlertView(title: "", message: response.error!.localizedDescription, delegate: nil, cancelButtonTitle: "OK");
                    alert.show()
                    self.view.hideActivityView()
                    print(response.result.error as Any)
                    break
                    
                }
            }
        }
    }
    
    func removeNSNull(from dict: NSMutableDictionary) -> NSDictionary {
        let mutableDict = dict
        let keysWithEmptString = dict.filter { $0.1 is NSNull }.map { $0.0 }
        for key in keysWithEmptString {
            mutableDict[key] = ""
        }
        
        return mutableDict
    }
    
    // MARK: - UITableViewDataSource
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return prototypeEntitiesFromJSON.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FDFeedCell") as? FDFeedCell
        if prototypeEntitiesFromJSON.count != 0{
            configureCell(cell!, at: indexPath)
        }
        
        return cell!
    }
    
    
    func configureCell(_ cell: FDFeedCell, at indexPath: IndexPath) {
        if prototypeEntitiesFromJSON.count != 0{
            cell.entity = prototypeEntitiesFromJSON[indexPath.row]
            cell.contentImageView.tag = indexPath.row
            let tap = UITapGestureRecognizer(target: self, action: #selector(self.onTapImage))
            cell.contentImageView.addGestureRecognizer(tap)
            cell.contentImageView.isUserInteractionEnabled = true
            
            let tap1 = UITapGestureRecognizer(target: self, action: #selector(self.tapCellTap))
            cell.addGestureRecognizer(tap1)
            cell.isUserInteractionEnabled = true

//            let tap2 = UITapGestureRecognizer(target: self, action: #selector(self.tapOnProfilePic))
//            cell.profileImage.isUserInteractionEnabled = true
//            cell.profileImage.addGestureRecognizer(tap2)
        }
    }
    
    @objc func tapCellTap(_ sender: UITapGestureRecognizer) {
        let backItem = UIBarButtonItem()
        backItem.title = "Back"
        backItem.tintColor = UIColor.black
        navigationItem.backBarButtonItem = backItem
        var cell: FDFeedCell?
        var view = sender.view
        while view != nil {
            if view is FDFeedCell {
                cell = view as? FDFeedCell
            }
            if view is UITableView {
                tableView = view as? UITableView
            }
            view = view?.superview
        }
        
        if let indexPath = (cell != nil) ? tableView?.indexPath(for: cell!) : nil {
            let backItem = UIBarButtonItem()
            backItem.title = "Back"
            backItem.tintColor = UIColor.black
            navigationItem.backBarButtonItem = backItem
            if prototypeEntitiesFromJSON.count != 0{
                let type = prototypeEntitiesFromJSON[indexPath.row].type as String
                if type == "Forum"{
                    self.QuestionId = String(prototypeEntitiesFromJSON[indexPath.row].qId)
                    let viewTitle = prototypeEntitiesFromJSON[indexPath.row].qtext
                    let destinationVC = storyboard?.instantiateViewController(withIdentifier: "ForumDetailViewController") as! ForumDetailViewController
                    destinationVC.QuestionId = self.QuestionId
                    destinationVC.viewtitle = viewTitle!
                    self.navigationController?.pushViewController(destinationVC, animated: true)
                }
                else{
                    self.handleAdvertise(index: indexPath.row)
                }
            }
        }
        
        
    }
    
    @objc func tapOnProfilePic(_ sender: UITapGestureRecognizer) {
        let backItem = UIBarButtonItem()
        backItem.title = "Back"
        backItem.tintColor = UIColor.black
        navigationItem.backBarButtonItem = backItem
        var cell: FDFeedCell?
        var view = sender.view
        while view != nil {
            if view is FDFeedCell {
                cell = view as? FDFeedCell
            }
            if view is UITableView {
                tableView = view as? UITableView
            }
            view = view?.superview
        }
        
        if let indexPath = (cell != nil) ? tableView?.indexPath(for: cell!) : nil {
            let backItem = UIBarButtonItem()
            backItem.title = "Back"
            backItem.tintColor = UIColor.black
            navigationItem.backBarButtonItem = backItem
            let storyboard =  UIStoryboard(name: "Main", bundle: nil)
            let promo = storyboard.instantiateViewController(withIdentifier: "VendorDetailsViewController") as! VendorDetailsViewController
            promo.vendorID = self.prototypeEntitiesFromJSON[indexPath.row].value(forKey: "AnsSendUser") as! String
            promo.poptoRoot = true
            self.navigationController?.pushViewController(promo, animated: true)
        }
    }
    
    // MARK: - UITableViewDelegate
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return tableView.fd_heightForCell(withIdentifier: "FDFeedCell") { cell in
            self.configureCell(cell as! FDFeedCell, at: indexPath)
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        
        
    }
    
    
    @objc func onTapImage(_ sender: UITapGestureRecognizer) {
        let backItem = UIBarButtonItem()
        backItem.title = "Back"
        backItem.tintColor = UIColor.black
        navigationItem.backBarButtonItem = backItem
        var cell: FDFeedCell?
        var view = sender.view
        while view != nil {
            if view is FDFeedCell {
                cell = view as? FDFeedCell
            }
            if view is UITableView {
                tableView = view as? UITableView
            }
            view = view?.superview
        }
        
        if let indexPath = (cell != nil) ? tableView?.indexPath(for: cell!) : nil {
            let type = prototypeEntitiesFromJSON[indexPath.row].type as String
            if type == "Forum"{
                
                let imageURL = prototypeEntitiesFromJSON[indexPath.row].image as String
                if imageURL != ""{
                    let storyboard =  UIStoryboard(name: "Main", bundle: nil)
                    let promo = storyboard.instantiateViewController(withIdentifier: "ImageDisplayViewController") as! ImageDisplayViewController
                    print(imageURL)
                    promo.imageURL = imageURL
                    promo.deleteShow = false
                    self.navigationController?.pushViewController(promo, animated: true)
                }
                else{
                    self.QuestionId = String(prototypeEntitiesFromJSON[indexPath.row].qId)
                    let viewTitle = prototypeEntitiesFromJSON[indexPath.row].qtext
                    let destinationVC = storyboard?.instantiateViewController(withIdentifier: "ForumDetailViewController") as! ForumDetailViewController
                    destinationVC.QuestionId = self.QuestionId
                    destinationVC.viewtitle = viewTitle!
                    self.navigationController?.pushViewController(destinationVC, animated: true)
                }
                
            }
            else{
                self.handleAdvertise(index: indexPath.row)
            }
        }
    }
    
    func handleAdvertise(index: Int)  {
        let promotionAim = prototypeEntitiesFromJSON[index].promotionAim as String
        if promotionAim == "Increase brand Awareness"{
            let backItem = UIBarButtonItem()
            backItem.title = "Back"
            backItem.tintColor = UIColor.black
            navigationItem.backBarButtonItem = backItem
            let storyboard =  UIStoryboard(name: "Main", bundle: nil)
            let promo = storyboard.instantiateViewController(withIdentifier: "VendorDetailsViewController") as! VendorDetailsViewController
            promo.vendorID = prototypeEntitiesFromJSON[index].providerId as String
            promo.poptoRoot = true
            self.navigationController?.pushViewController(promo, animated: true)
        }
        if promotionAim == "Get more website visits"{
            var websiteList = prototypeEntitiesFromJSON[index].websiteLink as String
            if websiteList != ""{
                let str = websiteList.contains(find: "http://")
                if str == false{
                    websiteList = "http://" + websiteList
                }
                guard let url = URL(string: websiteList) else {
                    return //be safe
                }
                
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
            else{
                let alert: UIAlertView = UIAlertView(title: "", message: "Website link not updated by service provider", delegate: nil, cancelButtonTitle: "OK");
                alert.show()
            }
        }
        if promotionAim == "Get more calls for your business"{
            self.tableView.isUserInteractionEnabled = false
            DispatchQueue.main.async {
                let manager = Alamofire.SessionManager.default
                manager.session.configuration.timeoutIntervalForRequest = 120

                let id = self.prototypeEntitiesFromJSON[index].providerId as String
                manager.request( baseURL + "api/Albums/GetContactDetails/?providerId=" + id, method: .get, parameters: ["":""], encoding: URLEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
                    
                    switch(response.result) {
                    case .success(_):
                        if response.result.value != nil{
                            //print(response.result.value as Any)
                        }
                        
                        let jsonResults : NSArray
                        jsonResults = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! NSArray
                        let dict = jsonResults[0] as! NSDictionary
                        let phoneNumber = dict["phoneNumber"] as! String
                        let email = dict["email"] as! String
                        
                        self.popViewController = self.storyboard?.instantiateViewController(withIdentifier: "ContactPopUpViewController")as! ContactPopUpViewController
                        self.popViewController.phoneNum = phoneNumber
                        self.popViewController.emailID = email
                        self.popViewController.delegate = self
                        self.popViewController.showInView(self.view, animated: true)
                        self.tableView.isUserInteractionEnabled = true
                        break
                        
                    case .failure(_):
                        if response.error?.localizedDescription == "The Internet connection appears to be offline."
                        {
                            let alert: UIAlertView = UIAlertView(title: "", message: response.error!.localizedDescription, delegate: nil, cancelButtonTitle: "OK");
                            alert.show()
                        }
                        else{
                            let alert: UIAlertView = UIAlertView(title: "", message: response.error!.localizedDescription, delegate: nil, cancelButtonTitle: "OK");
                            alert.show()
                            print(response.result.error as Any)
                        }
                        break
                        
                    }
                }
            }
        }
        if promotionAim == "Promote On Social Media"{
            self.linksView.isHidden = false
            self.fbURL = self.prototypeEntitiesFromJSON[index].socialMedia_facebook as String
            self.instaURL = self.prototypeEntitiesFromJSON[index].socialMedia_Instagram as String
            self.twitter = self.prototypeEntitiesFromJSON[index].socialMedia_twitter as String
            if self.fbURL == ""{
                self.fbBtn.setImage(UIImage(named:"fbgray"), for: UIControlState.normal)
            }
            else{
                self.fbBtn.setImage(UIImage(named:"facebook"), for: UIControlState.normal)
            }
            if self.instaURL == ""{
                self.instaBtn.setImage(UIImage(named:"instagray"), for: UIControlState.normal)
            }
            else{
                self.instaBtn.setImage(UIImage(named:"instagram-1"), for: UIControlState.normal)
            }
            if self.twitter == ""{
                self.twitterBtn.setImage(UIImage(named:"twittergray"), for: UIControlState.normal)
            }
            else{
                self.twitterBtn.setImage(UIImage(named:"twitter"), for: UIControlState.normal)
            }
            
            if self.fbURL == "" && self.instaURL == "" && self.twitter != ""{
                self.twitterX.constant = 0.4
            }
            else if self.fbURL == "" && self.instaURL != "" && self.twitter != ""{
                self.instaX.constant = 0.4
                self.twitterX.constant = 0
            }
            else if self.instaURL == "" && self.twitter != ""{
                self.twitterX.constant = 0
            }
        }
    }
    
    func emailBox(email: String){
        if MFMailComposeViewController.canSendMail() {
            let toRecipients : [String] = [email]
            let mc: MFMailComposeViewController = MFMailComposeViewController()
            mc.mailComposeDelegate = self
            mc.setToRecipients(toRecipients)
            mc.isNavigationBarHidden = true
            self.present(mc, animated: false, completion: nil)
        }
        else
        {
            let alert: UIAlertView = UIAlertView(title: "", message: "Mail app not install on your device. Please send an email to info@ayefroinc.com.com", delegate: nil, cancelButtonTitle: "OK");
            alert.show()
        }
    }
    
    @IBAction func createNewTopicBtnClk(_ sender: UIButton) {
        
    }
    
    func getAllTopic()  {
        self.category = ["All Topics"]
        self.categoryID = ["0"]
        DispatchQueue.main.async {
            let manager = Alamofire.SessionManager.default
            manager.session.configuration.timeoutIntervalForRequest = 120

            manager.request( baseURL + "api/TopicCategories/GetAllTopic", method: .get, parameters: ["":""], encoding: URLEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
                
                switch(response.result) {
                case .success(_):
                    if response.result.value != nil{
                        //print(response.result.value as Any)
                    }
                    let jsonResults : NSArray
                    jsonResults = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! NSArray
                    for i in 0 ..< jsonResults.count{
                        let data = jsonResults[i] as! NSDictionary
                        let categoryID = data["Id"] as! Int
                        let CategoryTitle = data["CategoryTitle"] as! String
                        let Active = data["Active"] as! Bool
                        if Active == true{
                            self.category.append(CategoryTitle)
                            self.categoryID.append(String(categoryID))
                        }
                    }
                    
                    self.setupChooseArticleDropDown()
                    break
                    
                case .failure(_):
                    print(response.error!.localizedDescription)
                    let alert: UIAlertView = UIAlertView(title: "", message: response.error!.localizedDescription, delegate: nil, cancelButtonTitle: "OK");
                    alert.show()
                    self.view.hideActivityView()
                    print(response.result.error as Any)
                    break
                    
                }
            }
        }
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let backItem = UIBarButtonItem()
        backItem.title = "Back"
        backItem.tintColor = UIColor.black
        navigationItem.backBarButtonItem = backItem
        if (segue.identifier == "CreateTopic") {
            let destinationVC = segue.destination as! CreateTopicViewController
            print(self.categoryID)
            print(self.category)
            destinationVC.categoryID = self.categoryID
            destinationVC.category = self.category
            destinationVC.delegate = self
        }
        if (segue.identifier == "ForumDetail") {
            if let indexPath = tableView.indexPathForSelectedRow{
                let selectedRow = indexPath.row
                self.QuestionId = String(prototypeEntitiesFromJSON[selectedRow].qId)
                let viewTitle = prototypeEntitiesFromJSON[selectedRow].qtext
                let destinationVC = segue.destination as! ForumDetailViewController
                destinationVC.QuestionId = self.QuestionId
                destinationVC.viewtitle = viewTitle!
            }
            
        }
        
        // This will show in the next view controller being pushed
    }
    
    @IBAction func fbBtnClk(_ sender: UIButton) {
        self.linksView.isHidden = true
        if fbURL != ""{
            let str = fbURL.contains(find: "http://")
            if str == false{
                fbURL = "http://" + fbURL
            }
            guard let url = URL(string:  fbURL) else {
                return //be safe
            }
            
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
        else{
            let alert: UIAlertView = UIAlertView(title: "", message: "No social media channel yet for Facebook", delegate: nil, cancelButtonTitle: "OK");
            alert.show()
        }
        
    }
    
    @IBAction func instaBtnClk(_ sender: UIButton) {
        self.linksView.isHidden = true
        if instaURL != ""{
            let str = instaURL.contains(find: "http://")
            if str == false{
                instaURL = "http://" + instaURL
            }
            
            guard let url = URL(string: instaURL) else {
                return //be safe
            }
            
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
        else{
            let alert: UIAlertView = UIAlertView(title: "", message: "No social media channel yet for Instagram", delegate: nil, cancelButtonTitle: "OK");
            alert.show()
        }
        
    }
    
    @IBAction func twitterBtnClk(_ sender: UIButton) {
        self.linksView.isHidden = true
        if twitter != ""{
            let str = twitter.contains(find: "http://")
            if str == false{
                twitter = "http://" + twitter
            }
            guard let url = URL(string: twitter) else {
                return //be safe
            }
            
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
        else{
            let alert: UIAlertView = UIAlertView(title: "", message: "No social media channel yet for Twitter", delegate: nil, cancelButtonTitle: "OK");
            alert.show()
        }
        
       
    }
    
    @IBAction func cellBtnClk(_ sender: UIButton) {
        let position = self.tableView.convert(CGPoint.zero, from: sender)
        let indexPath : IndexPath = tableView.indexPathForRow(at: position)!
        if prototypeEntitiesFromJSON.count != 0{
            let type = prototypeEntitiesFromJSON[indexPath.row].type as String
            if type == "Forum"{
                self.QuestionId = String(prototypeEntitiesFromJSON[indexPath.row].qId)
                let viewTitle = prototypeEntitiesFromJSON[indexPath.row].qtext
                let destinationVC = storyboard?.instantiateViewController(withIdentifier: "ForumDetailViewController") as! ForumDetailViewController
                destinationVC.QuestionId = self.QuestionId
                destinationVC.viewtitle = viewTitle!
                self.navigationController?.pushViewController(destinationVC, animated: true)
            }
            else{
                self.handleAdvertise(index: indexPath.row)
            }
        }
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        switch result.rawValue {
        case MFMailComposeResult.cancelled.rawValue:
            //NSLog("Mail Cancelled")
            self.dismiss(animated: false, completion: nil)
            break
        case MFMailComposeResult.saved.rawValue:
            //NSLog("Mail Saved")
            self.dismiss(animated: false, completion: nil)
            break
        case MFMailComposeResult.failed.rawValue:
            //NSLog("Mail Sent Fail", [error!.localizedDescription])
            self.dismiss(animated: false, completion: nil)
            break
        case MFMailComposeResult.sent.rawValue:
            //NSLog("Mail Sent")
            self.dismiss(animated: false, completion: nil)
            break
            
        default:
            break
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}



