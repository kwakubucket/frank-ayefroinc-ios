//
//  ImageDisplayViewController.swift
//  AyefroInc
//
//  Created by Vijay Darkonde on 23/11/17.
//  Copyright © 2017 Inceptive Consulting Pvt. Ltd. All rights reserved.
//

import UIKit
import SDWebImage
import ZoomImageView
import Alamofire

class ImageDisplayViewController: UIViewController,UIScrollViewDelegate {
    
    
   
    @IBOutlet var imageView: ZoomImageView!
    var imageURL : String! = ""
    var hideShow : Bool! = true
    var deleteShow : Bool! = false
    var imageID : String! = ""
    
        
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let url = URL(string: imageURL)
        let str: String? = Bundle.main.path(forResource: "loading_spinner", ofType: "gif")
        let fileData = NSData(contentsOfFile: str!)
        self.imageView.sd_internalSetImage(with: url, placeholderImage: UIImage.sd_animatedGIF(with: fileData! as Data), options: SDWebImageOptions.allowInvalidSSLCertificates, operationKey: nil, setImageBlock: { (image, imageData) in
            self.imageView.image = image
        }, progress: nil, completed: nil)
       
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.tintColor = UIColor.black
        self.tabBarController?.tabBar.isHidden = true
        
        self.imageView.zoomMode = .fit
        
        let tapGesture1: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(ImageDisplayViewController.showBar))
        tapGesture1.numberOfTapsRequired = 1
        self.view.addGestureRecognizer(tapGesture1)
        
        let doubleTap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(ImageDisplayViewController.doubletap))
        doubleTap.numberOfTapsRequired = 2
        view.addGestureRecognizer(doubleTap)
        
        let threeTap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(ImageDisplayViewController.threetap))
        threeTap.numberOfTapsRequired = 3
        view.addGestureRecognizer(threeTap)
        
       tapGesture1.require(toFail: threeTap)
        
        if deleteShow == true{
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Delete", style: .done, target: self, action: #selector(deleteTap))
        }
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(hideShow, animated: false)
    }
    
    @objc func deleteTap(){
        let refreshAlert = UIAlertController(title: "", message: "Are you sure you want to DELETE the picture? This cannot be UNDONE.", preferredStyle: UIAlertControllerStyle.alert)
        
        refreshAlert.addAction(UIAlertAction(title: "No, I don’t", style: .cancel , handler: { (action: UIAlertAction!) in
            
        }))
        
        
        refreshAlert.addAction(UIAlertAction(title: "Yes, I want to delete", style: .default, handler: { (action: UIAlertAction!) in
            DispatchQueue.main.async {
                var imgarr : [Int] = []
                imgarr.append(Int(self.imageID)!)
                
                let parameters : [String: Any] = [
                    "ImagesId": imgarr,
                    "ServiceProviderId" : userID
                ]
                
                let manager = Alamofire.SessionManager.default
                manager.session.configuration.timeoutIntervalForRequest = 120
                
                manager.request(baseURL + "api/Albums/DeletePhotoGallery", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
                    
                    switch(response.result) {
                    case .success(_):
                        if response.result.value != nil{
                            //print(response.result.value as Any)
                        }
                        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                            // your code here
                             self.navigationController?.popViewController(animated: true)
                        }
                       
                        break
                        
                    case .failure(_):
                        print(response.error!.localizedDescription)
                        let alert: UIAlertView = UIAlertView(title: "", message: response.error!.localizedDescription, delegate: nil, cancelButtonTitle: "OK");
                        alert.show()
                        self.view.hideActivityView()
                        print(response.result.error as Any)
                        break
                        
                    }
                }
            }
        }))
        
        self.present(refreshAlert, animated: true, completion: nil)
    }
    
    @objc func doubletap() {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    @objc func threetap() {
    }
    
    @objc func showBar()  {
        
        if hideShow == true{
            hideShow = false
            self.navigationController?.setNavigationBarHidden(hideShow, animated: true)
            
        }
        else{
            hideShow = true
            self.navigationController?.setNavigationBarHidden(hideShow, animated: true)
            
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}
