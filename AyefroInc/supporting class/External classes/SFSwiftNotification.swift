//
//  SFSwiftNotification.swift
//  SFSwiftNotification
//
//  Created by Simone Ferrini on 13/07/14.
//  Copyright (c) 2014 sferrini. All rights reserved.
//

import UIKit

enum AnimationType {
    case AnimationTypeCollision
    case AnimationTypeBounce
}

struct AnimationSettings {
    var duration:TimeInterval = 1
    var delay :TimeInterval = 1
    var damping:CGFloat = 0.6
    var velocity:CGFloat = 0.9
    var elasticity:CGFloat = 0.3
}

enum Direction {
    case TopToBottom
    case LeftToRight
    case RightToLeft
}

protocol SFSwiftNotificationProtocol {
    func didNotifyFinishedAnimation(results: Bool)
    func didTapNotification()
}

class SFSwiftNotification: UIView, UICollisionBehaviorDelegate, UIDynamicAnimatorDelegate {
    
    var label = UILabel()
    var description_Label = UILabel()
    var image = UIImageView()
    var animationType:AnimationType?
    var animationSettings = AnimationSettings()
    var direction:Direction?
    var dynamicAnimator = UIDynamicAnimator()
    var delegate: SFSwiftNotificationProtocol?
    var canNotify = true
    var offScreenFrame = CGRect()
    var toFrame = CGRect()
    var delay = TimeInterval()
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    init(frame: CGRect, title: NSString?, animationType:AnimationType, direction:Direction, delegate: SFSwiftNotificationProtocol?) {
        super.init(frame: frame)
        
        self.animationType = animationType
        self.direction = direction
        self.delegate = delegate
        
        label = UILabel(frame: CGRect(x:60, y:25, width:self.frame.width-60, height:20))
        label.text = title as? String
        label.textAlignment = NSTextAlignment.left
        label.font = UIFont.boldSystemFont(ofSize: 15)
        self.addSubview(label)
        
        description_Label = UILabel(frame: CGRect(x:60, y:53, width:self.frame.width-60, height:20))
        description_Label.textAlignment = NSTextAlignment.left
        description_Label.font = UIFont.boldSystemFont(ofSize: 13)
        self.addSubview(description_Label)
        
        image = UIImageView(frame: CGRect(x:20, y:40, width:30, height:30))
        self.addSubview(image)
        
        // Create gesture recognizer to detect notification touches
        let tapReconizer = UITapGestureRecognizer()
        tapReconizer.addTarget(self, action: #selector(SFSwiftNotification.invokeTapAction));
        
        // Add Touch recognizer to notification view
        self.addGestureRecognizer(tapReconizer)
        
        offScreen()
    }
    
    @objc func invokeTapAction() {
        self.removeFromSuperview()
        self.delegate!.didTapNotification()
        self.canNotify = true
    }
    
    func offScreen() {
        
        self.offScreenFrame = self.frame
        
        switch direction! {
        case .TopToBottom:
            self.offScreenFrame.origin.y = -self.frame.size.height
        case .LeftToRight:
            self.offScreenFrame.origin.x = -self.frame.size.width
        case .RightToLeft:
            self.offScreenFrame.origin.x = +self.frame.size.width
        }
        
        self.frame = offScreenFrame
    }
    
    func animate(toFrame:CGRect, delay:TimeInterval) {
        
        self.toFrame = toFrame
        self.delay = delay
        
        if canNotify {
            self.canNotify = false
            
            switch self.animationType! {
            case .AnimationTypeCollision:
                setupCollisionAnimation(toFrame: toFrame)
                
            case .AnimationTypeBounce:
                setupBounceAnimation(toFrame: toFrame, delay: delay)
            }
        }
    }
    
    func setupCollisionAnimation(toFrame:CGRect) {
        
        self.dynamicAnimator = UIDynamicAnimator(referenceView: self.superview!)
        self.dynamicAnimator.delegate = self
        
        let elasticityBehavior = UIDynamicItemBehavior(items: [self])
        elasticityBehavior.elasticity = animationSettings.elasticity;
        self.dynamicAnimator.addBehavior(elasticityBehavior)
        
        let gravityBehavior = UIGravityBehavior(items: [self])
        self.dynamicAnimator.addBehavior(gravityBehavior)
        
        let collisionBehavior = UICollisionBehavior(items: [self])
        collisionBehavior.collisionDelegate = self
        self.dynamicAnimator.addBehavior(collisionBehavior)
        
        collisionBehavior.addBoundary(withIdentifier: "BoundaryIdentifierBottom" as NSCopying, from: CGPoint(x:-self.frame.width, y:self.frame.height+0.5), to: CGPoint(x:self.frame.width*2, y:self.frame.height+0.5))
        
        switch self.direction! {
        case .TopToBottom:
            break
        case .LeftToRight:
            collisionBehavior.addBoundary(withIdentifier: "BoundaryIdentifierRight" as NSCopying, from: CGPoint(x:self.toFrame.width-0.5, y:0), to: CGPoint(x:self.toFrame.width-0.5, y:self.toFrame.height))
            gravityBehavior.gravityDirection = CGVector(dx:10, dy:1)
        case .RightToLeft:
            collisionBehavior.addBoundary(withIdentifier: "BoundaryIdentifierLeft" as NSCopying, from: CGPoint(x:+0.5, y:0), to: CGPoint(x:+0.5, y:self.toFrame.height))
            gravityBehavior.gravityDirection = CGVector(dx:-10, dy:1)
        }
    }
    
    func setupBounceAnimation(toFrame:CGRect , delay:TimeInterval) {
        
        UIView.animate(
            withDuration: animationSettings.duration,
            delay: animationSettings.delay,
            usingSpringWithDamping: animationSettings.damping,
            initialSpringVelocity: animationSettings.velocity,
            options: [UIViewAnimationOptions.beginFromCurrentState,UIViewAnimationOptions.allowUserInteraction],
            animations: {
                self.frame = toFrame
            },
            completion: { (value: Bool) in
                self.hide(toFrame: toFrame, delay: delay)
        })
        
    }
    
    func dynamicAnimatorDidPause(_ animator: UIDynamicAnimator) {
        
        hide(toFrame: self.toFrame, delay: self.delay)
    }
    
    func hide(toFrame:CGRect, delay:TimeInterval) {
        
        UIView.animate(
            withDuration: animationSettings.duration,
            delay: animationSettings.delay,
            usingSpringWithDamping: animationSettings.damping,
            initialSpringVelocity: animationSettings.velocity,
            options: [UIViewAnimationOptions.beginFromCurrentState,UIViewAnimationOptions.allowUserInteraction],
            animations: {
                self.frame = toFrame
        }, completion: {
                (value: Bool) in
                self.delegate!.didNotifyFinishedAnimation(results: true)
            DispatchQueue.main.asyncAfter(deadline: .now() + 2) { // in half a second...
                print("Are we there yet?")
                self.removeFromSuperview()
            }
            
                self.canNotify = true
        })
    }
}





