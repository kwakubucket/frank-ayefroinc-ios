//
//  ModelManager.swift
//  DataBaseDemo
//
//  Created by Krupa-iMac on 05/08/14.
//  Copyright (c) 2014 TheAppGuruz. All rights reserved.
//

import UIKit

let sharedInstance = ModelManager()

class ModelManager: NSObject {
    
    var database: FMDatabase? = nil
    
    class var instance: ModelManager {
        sharedInstance.database = FMDatabase(path: Util.getPath(fileName: "Ayefro.sqlite"))
        let path = Util.getPath(fileName: "Ayefro.sqlite")
        print("path : \(path)")
        return sharedInstance
    }
    
    func addUserDetails(_ productInfo: ProductInfo) -> Bool {
        sharedInstance.database!.open()
        
        let isInserted = sharedInstance.database!.executeUpdate("INSERT INTO USER_TABLE (Id,FirstName,LastName,Email,PhoneNumber,Country,Role,companyAddress,companyName,image) VALUES (?,?,?,?,?,?,?,?,?,?)", withArgumentsIn: [productInfo.userID, productInfo.userFirstName, productInfo.userLastName, productInfo.userEmail,productInfo.PhoneNumber, productInfo.Country, productInfo.Role, productInfo.companyAddress, productInfo.companyName, productInfo.image])
        
        sharedInstance.database!.close()
        return isInserted
        
    }
    
    func getAlertValue() -> Bool{
        sharedInstance.database!.open()
        let resultSet: FMResultSet! = sharedInstance.database!.executeQuery("SELECT * From ONBOARDINGSCREEN", withArgumentsIn:[])
        var isData : Bool! = false
        if (resultSet != nil) {
            
            while resultSet.next() {
                alertValue = resultSet.bool(forColumn: "show") as Bool
                isData = alertValue
            }
        }
        sharedInstance.database!.close()
        return isData
    }
    
    func getUserLoginStatus() -> Bool{
        sharedInstance.database!.open()
        let resultSet: FMResultSet! = sharedInstance.database!.executeQuery("SELECT * From USER_TABLE", withArgumentsIn:[])
        
        var isData : Bool! = false
        if (resultSet != nil) {
            
            while resultSet.next() {
                userFirstName = resultSet.string(forColumn: "FirstName")as String
                userLastName = resultSet.string(forColumn: "LastName")as String
                userEmail = resultSet.string(forColumn: "Email")as String
                PhoneNumber = resultSet.string(forColumn: "PhoneNumber")as String
                Country = resultSet.string(forColumn: "Country")as String
                let role = resultSet.string(forColumn: "Role")as String
                Role = Int(role)
                companyAddress = resultSet.string(forColumn: "companyAddress")as String
                companyName = resultSet.string(forColumn: "companyName")as String
                image = resultSet.string(forColumn: "image")as String
                userID = resultSet.string(forColumn: "Id")as String
                isData = true
            }
        }
        sharedInstance.database!.close()
        return isData
    }
    
    func updateAlerTable(_ productInfo: ProductInfo) -> Bool
    {
        sharedInstance.database!.open()
        
        let isUpdated = sharedInstance.database!.executeUpdate("Update ONBOARDINGSCREEN SET show = ?", withArgumentsIn:[productInfo.alertValue])
        sharedInstance.database!.close()
        
        return isUpdated
    }
    
    func updateUserInfo(_ productInfo: ProductInfo) -> Bool
    {
        sharedInstance.database!.open()
        
        let isUpdated = sharedInstance.database!.executeUpdate("Update USER_TABLE SET FirstName = ?, LastName = ?, companyAddress = ?, companyName = ?, image = ? WHERE Id = ?", withArgumentsIn:[ productInfo.userFirstName, productInfo.userLastName, productInfo.companyAddress, productInfo.companyName, productInfo.image, productInfo.userID])
        sharedInstance.database!.close()
        
        return isUpdated
    }
    
    func deleteUserTable() -> Bool {
        sharedInstance.database!.open()
        let isDeletedOrderTable = sharedInstance.database!.executeUpdate("DELETE From USER_TABLE", withArgumentsIn: [])
        sharedInstance.database!.close()
        return isDeletedOrderTable
    }
}



