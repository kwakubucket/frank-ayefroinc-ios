//
//  BidEventsCell.h
//  AyefroInc
//
//  Created by Vijay Darkonde on 15/12/17.
//  Copyright © 2017 Inceptive Consulting Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BidEventEntity.h"
@interface BidEventsCell : UITableViewCell

@property (nonatomic, strong) BidEventEntity *entity;

@property (strong, nonatomic) IBOutlet UILabel *eventNameLbl;
@property (strong, nonatomic) IBOutlet UILabel *dateLbl;
@property (strong, nonatomic) IBOutlet UILabel *locationLbl;
@property (strong, nonatomic) IBOutlet UILabel *servicesLbl;
@property (strong, nonatomic) IBOutlet UILabel *bidCountLbl;


@end
