//
//  DairyEntity.h
//  AyefroInc
//
//  Created by Vijay Darkonde on 09/01/18.
//  Copyright © 2018 Inceptive Consulting Pvt. Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DairyEntity : NSObject
- (instancetype)initWithDictionary:(NSDictionary *)dictionary;

@property (nonatomic, copy ) NSString *Type;
@property (nonatomic, copy ) NSString *DImageDesc;
@property (nonatomic, copy ) NSString *DImageUser;
@property (nonatomic, copy ) NSString *DImgUserName;
@property (nonatomic, copy ) NSString *DImgUserImage;
@property (nonatomic, copy ) NSArray *DTopics;
//@property (nonatomic, copy ) NSString *DTopicsCount;
@property (nonatomic ) int TimeDiffhr;
@property (nonatomic ) int TimeDiffmin;
@property (nonatomic ) int TimeDiffsec;
@property (nonatomic ) int DImglikeCount;
@property (nonatomic ) int DImgCmtCount;
@property (nonatomic ) int ImageId;
@property (nonatomic, copy ) NSString *image;
@property (nonatomic ) Boolean DImgliked;


@property (nonatomic, copy ) NSString *PromotionAim;
@property (nonatomic, copy ) NSString *ProviderId;
@property (nonatomic, copy ) NSString *WebsiteLink;
@property (nonatomic, copy ) NSString *contactDetails;
@property (nonatomic, copy ) NSString *SocialMedia_facebook;
@property (nonatomic, copy ) NSString *SocialMedia_Instagram;
@property (nonatomic, copy ) NSString *SocialMedia_twitter;

@property (nonatomic, copy ) NSString *DImgUserCountry;
@property (nonatomic, copy ) NSArray *comments;



@property (nonatomic, copy ) NSString *identifier;



@end
