//
//  DairyCell.m
//  AyefroInc
//
//  Created by Vijay Darkonde on 09/01/18.
//  Copyright © 2018 Inceptive Consulting Pvt. Ltd. All rights reserved.
//

#import "DairyCell.h"

@implementation DairyCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    // Fix the bug in iOS7 - initial constraints warning
    self.contentView.bounds = [UIScreen mainScreen].bounds;
    _profileImage.layer.cornerRadius = _profileImage.frame.size.height/2;
    _profileImage.layer.masksToBounds = YES;
    _commenterImg.layer.cornerRadius = _commenterImg.frame.size.height/2;
    _commenterImg.layer.masksToBounds = YES;
    self.contentImageView.contentMode = UIViewContentModeScaleAspectFit;
    self.commentLikeView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.commentLikeView.layer.borderWidth = 0.5;
    self.userInteractionEnabled = YES;
    
}

- (void)setEntity:(DairyEntity *)entity
{
    
    _entity = entity;
    
    if ([entity.Type isEqual: @"Advertise"]){
        [self.bottemView setHidden:YES];
        [self.commentLikeView setHidden:YES];
        self.profileImage.image = nil;
        self.titleLabel.text = @"";
        self.descriptionLabel.text = @"";
        self.contentLabel.text = @"";
        self.tagsLbl.text = @"";
        self.commentTxt.text = @"";
        self.commenterImg.image = nil;
        self.commenterName.text = @"";
        self.commentTimeLbl.text = @"";
        self.countryLbl.text = @"";
        
        self.secondSpacing.constant = 0.0;
        self.thirdSpacing.constant = 0.0;
        self.fourthSpacing.constant = 0.0;
        self.fifthSpacing.constant = 0.0;
        self.nameSpace.constant = 0.0;
        self.commentSpace.constant = 0.0;
        self.bottomSpace.constant = 0.0;
        self.timeSpace.constant = 0.0;
        
        NSString *str=[[NSBundle mainBundle] pathForResource:@"loading_spinner" ofType:@"gif"];
        self.userInteractionEnabled = YES;
        NSData *fileData = [NSData dataWithContentsOfFile:str];
        [self.contentImageView sd_setImageWithURL:[NSURL URLWithString:entity.image] placeholderImage:[UIImage sd_animatedGIFWithData:fileData]];
        self.sponsoredImg.image = [UIImage imageNamed:@"sponsored.png"];
        if ([entity.PromotionAim isEqual: @"Get more calls for your business"]) {
            self.actionLbl.image = [UIImage imageNamed:@"callnow.png"];
        }
        else{
            self.actionLbl.image = [UIImage imageNamed:@"learnmore.png"];
        }
    }
    else{
        [self.commentLikeView setHidden:NO];
        [self.bottemView setHidden:NO];
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        formatter.dateFormat = @"yyyy-MM-dd'T'HH:mm:ss.SSS";
        int CDate =  entity.TimeDiffsec;
        int Cmin =  entity.TimeDiffmin;
        int Chours =  entity.TimeDiffhr;
        NSString *time = @"";

        if (Chours > 0){
            if ((Chours / 24) <= 0){
                time = [NSString stringWithFormat:@"%d hour(s)", entity.TimeDiffhr];
            }
            else if ((Chours) / (24 * 30) <= 0){
                time = [NSString stringWithFormat:@"%d day(s)", (Chours / 24)];
            }
            else{
                time = [NSString stringWithFormat:@"%d month(s)", (Chours) / (24 * 30)];
            }
        }
        else if (Cmin > 0){
            time = [NSString stringWithFormat:@"%d minute(s)", entity.TimeDiffmin];
        }
        else{
            time = [NSString stringWithFormat:@"%d second(s)", entity.TimeDiffsec];
        }
        
        self.titleLabel.text = entity.DImgUserName;
        NSString *greeting = [entity.DTopics componentsJoinedByString:@", "];
        if ([entity.DTopics count] > 2){
            NSString *firstTopic = [entity.DTopics objectAtIndex:0];
            NSString *secondTopic = [entity.DTopics objectAtIndex:1];
            long countremaining = [entity.DTopics count] - 2;
            NSString *countStr = [NSString stringWithFormat:@" ...%ld more", countremaining];
            
            NSString *topics = [firstTopic stringByAppendingString:[@", " stringByAppendingString:[secondTopic stringByAppendingString:countStr]]];
             self.tagsLbl.text = [@"Tags: " stringByAppendingString:topics];
        }
        else{
            self.tagsLbl.text = [@"Tags: " stringByAppendingString:greeting];
        }
//        if ([entity.DTopicsCount isEqual:@""]) {
//            self.tagsLbl.text = [@"Tags: " stringByAppendingString:entity.DTopics];
//        }
//        else{
//            self.tagsLbl.text = [@"Tags: " stringByAppendingString:entity.DTopicsCount];
//        }
        
        self.contentLabel.text = [time stringByAppendingString:@" ago."];
        if(entity.image.length > 0){
            NSString *str=[[NSBundle mainBundle] pathForResource:@"loading_spinner" ofType:@"gif"];
            
            NSData *fileData = [NSData dataWithContentsOfFile:str];
            [self.contentImageView sd_setImageWithURL:[NSURL URLWithString:entity.image] placeholderImage:[UIImage sd_animatedGIFWithData:fileData]];
        }
        else{
            self.contentImageView.image = nil;
        }
        
        long len = [entity.DImageDesc length];
        if ([[UIScreen mainScreen]bounds].size.width == 375 && [[UIScreen mainScreen]bounds].size.height == 667) {
            if(len > 60){
                NSString *trimStr = [entity.DImageDesc substringToIndex:55];
                self.descriptionLabel.text = [trimStr stringByAppendingString:@"...more"];
            }
            else
            {
                self.descriptionLabel.text = entity.DImageDesc;
            }
        }
        else if ([[UIScreen mainScreen]bounds].size.width == 414) {
            if(len > 80){
                NSString *trimStr = [entity.DImageDesc substringToIndex:80];
                self.descriptionLabel.text = [trimStr stringByAppendingString:@"...more"];
            }
            else
            {
                self.descriptionLabel.text = entity.DImageDesc;
            }
        }
        else if ([[UIScreen mainScreen]bounds].size.width == 320 && [[UIScreen mainScreen]bounds].size.height == 568) {
            if(len > 50){
                NSString *trimStr = [entity.DImageDesc substringToIndex:45];
                self.descriptionLabel.text = [trimStr stringByAppendingString:@"...more"];
            }
            else
            {
                self.descriptionLabel.text = entity.DImageDesc;
            }
        }
        else if ([[UIScreen mainScreen]bounds].size.width == 320 && [[UIScreen mainScreen]bounds].size.height == 480) {
            if(len > 50){
                NSString *trimStr = [entity.DImageDesc substringToIndex:45];
                self.descriptionLabel.text = [trimStr stringByAppendingString:@"...more"];
            }
            else
            {
                self.descriptionLabel.text = entity.DImageDesc;
            }
        }
        else{
            if(len > 60){
                NSString *trimStr = [entity.DImageDesc substringToIndex:55];
                self.descriptionLabel.text = [trimStr stringByAppendingString:@"...more"];
            }
            else
            {
                self.descriptionLabel.text = entity.DImageDesc;
            }
        }
        
        if (entity.DImgliked == YES) {
            [self.likeBtn setImage:[UIImage imageNamed:@"likesmall"] forState:UIControlStateNormal];
        }
        else{
            [self.likeBtn setImage:[UIImage imageNamed:@"dlikesmall"] forState:UIControlStateNormal];
        }
        
        
        if(entity.DImgUserImage.length > 0){
            NSString *str=[[NSBundle mainBundle] pathForResource:@"loading_spinner" ofType:@"gif"];
            NSData *fileData = [NSData dataWithContentsOfFile:str];
            [self.profileImage sd_setImageWithURL:[NSURL URLWithString:entity.DImgUserImage] placeholderImage:[UIImage sd_animatedGIFWithData:fileData]];
        }
        else{
            self.profileImage.image = [UIImage imageNamed:@"user.ing"];
        }
        
        if (entity.DImgCmtCount == 0 || entity.DImgCmtCount == 1){
            self.usernameLabel.text = [[NSString stringWithFormat:@"%d", entity.DImgCmtCount] stringByAppendingString:@" Comment"];
        }
        else{
            self.usernameLabel.text = [[NSString stringWithFormat:@"%d", entity.DImgCmtCount] stringByAppendingString:@" Comments"];
        }
        
        if (entity.DImglikeCount == 0 || entity.DImglikeCount == 1){
            self.timeLabel.text = [[NSString stringWithFormat:@"%d", entity.DImglikeCount] stringByAppendingString:@" Like"];
        }
        else{
            self.timeLabel.text = [[NSString stringWithFormat:@"%d", entity.DImglikeCount] stringByAppendingString:@" Likes"];
        }
        
        
        self.countryLbl.text = entity.DImgUserCountry;
        self.userInteractionEnabled = YES;
        self.sponsoredImg.image = nil;
        self.actionLbl.image = nil;
        self.secondSpacing.constant = 20.0;
        self.thirdSpacing.constant = 15.0;
        self.fourthSpacing.constant = 15.0;
        self.fifthSpacing.constant = 8.0;
        
        if ([entity.comments count] > 0) {
            NSDictionary *dict = [entity.comments objectAtIndex:0];
            NSString *CommentText = [dict valueForKey:@"CommentText"];
            NSString *CommentUsername = [dict valueForKey:@"CommentUsername"];
            NSString *CommentUserImage = [dict valueForKey:@"CommentUserImage"];
            
            long len = [CommentText length];
            if ([[UIScreen mainScreen]bounds].size.width == 375 && [[UIScreen mainScreen]bounds].size.height == 667) {
                if(len > 60){
                    NSString *trimStr = [CommentText substringToIndex:55];
                    self.commentTxt.text = [trimStr stringByAppendingString:@"...more"];
                }
                else
                {
                    self.commentTxt.text = CommentText;
                }
            }
            else if ([[UIScreen mainScreen]bounds].size.width == 414) {
                if(len > 60){
                    NSString *trimStr = [CommentText substringToIndex:55];
                    self.commentTxt.text = [trimStr stringByAppendingString:@"...more"];
                }
                else
                {
                    self.commentTxt.text = CommentText;
                }
            }
            else if ([[UIScreen mainScreen]bounds].size.width == 320 && [[UIScreen mainScreen]bounds].size.height == 568) {
                if(len > 50){
                    NSString *trimStr = [CommentText substringToIndex:45];
                    self.commentTxt.text = [trimStr stringByAppendingString:@"...more"];
                }
                else
                {
                    self.commentTxt.text = CommentText;
                }
            }
            else if ([[UIScreen mainScreen]bounds].size.width == 320 && [[UIScreen mainScreen]bounds].size.height == 480) {
                if(len > 50){
                    NSString *trimStr = [CommentText substringToIndex:45];
                    self.commentTxt.text = [trimStr stringByAppendingString:@"...more"];
                }
                else
                {
                    self.commentTxt.text = CommentText;
                }
            }
            else{
                if(len > 60){
                    NSString *trimStr = [CommentText substringToIndex:55];
                    self.commentTxt.text = [trimStr stringByAppendingString:@"...more"];
                }
                else
                {
                    self.commentTxt.text = CommentText;
                }
            }
            
            self.commenterName.text = CommentUsername;
            if([CommentUserImage isEqual:[NSNull null]]){
                self.commenterImg.image = [UIImage imageNamed:@"user.ing"];
                
            }
            else{
                NSString *str=[[NSBundle mainBundle] pathForResource:@"loading_spinner" ofType:@"gif"];
                NSData *fileData = [NSData dataWithContentsOfFile:str];
                [self.commenterImg sd_setImageWithURL:[NSURL URLWithString:CommentUserImage] placeholderImage:[UIImage sd_animatedGIFWithData:fileData]];
            }
            
//            int TimeDiffsec = [dict valueForKey:@"TimeDiffsec"];
//            int TimeDiffmin = [dict valueForKey:@"TimeDiffmin"];
//            int TimeDiffhr = [dict valueForKey:@"TimeDiffhr"];
            
            int CDate =  [[dict valueForKey:@"TimeDiffsec"] intValue];
            int Cmin =  [[dict valueForKey:@"TimeDiffmin"] intValue];
            int Chours =  [[dict valueForKey:@"TimeDiffhr"] intValue];
            NSString *time = @"";
            
            if (Chours > 0){
                if ((Chours / 24) <= 0){
                    time = [NSString stringWithFormat:@"%d hour(s)", Chours];
                }
                else if ((Chours) / (24 * 30) <= 0){
                    time = [NSString stringWithFormat:@"%d day(s)", (Chours / 24)];
                }
                else{
                    time = [NSString stringWithFormat:@"%d month(s)", (Chours) / (24 * 30)];
                }
            }
            else if (Cmin > 0){
                time = [NSString stringWithFormat:@"%d minute(s)", Cmin];
            }
            else{
                time = [NSString stringWithFormat:@"%d second(s)", CDate];
            }
            self.commentTimeLbl.text = [time stringByAppendingString:@" ago."];
            
            
            self.nameSpace.constant = 8.0;
            self.commentSpace.constant = 8.0;
            self.bottomSpace.constant = 20.0;
            self.timeSpace.constant = 4.0;
        }
        else{
            self.commentTxt.text = @"";
            self.commenterImg.image = nil;
            self.commenterName.text = @"";
            self.commentTimeLbl.text = @"";
            self.nameSpace.constant = 0.0;
            self.commentSpace.constant = 0.0;
            self.bottomSpace.constant = 0.0;
            self.timeSpace.constant = 0.0;
        }
    }
    
}

- (CGSize)sizeThatFits:(CGSize)size {
    CGFloat totalHeight = 0;
    if ([_entity.Type isEqual: @"Diary"]){
        totalHeight += [self.titleLabel sizeThatFits:size].height;
        totalHeight += [self.contentLabel sizeThatFits:size].height;
        if ([_entity.image  isEqual: @""]){
            totalHeight += 0;
            totalHeight += [self.usernameLabel sizeThatFits:size].height;
            totalHeight += [self.descriptionLabel sizeThatFits:size].height;
            totalHeight += [self.contentLabel sizeThatFits:size].height;
            totalHeight += 40;
            if ([_entity.comments count] > 0) {
                totalHeight += 120;
            }
        }
        else{
            totalHeight += 260;
            totalHeight += [self.usernameLabel sizeThatFits:size].height;
            totalHeight += [self.descriptionLabel sizeThatFits:size].height;
            totalHeight += [self.contentLabel sizeThatFits:size].height;
            totalHeight += [self.tagsLbl sizeThatFits:size].height;
            if ([_entity.comments count] > 0) {
                totalHeight += 120;
            }
        }
        totalHeight += 50;
    }
    else{
        totalHeight += 240;
        totalHeight += 10;
    }
    
    // margins
    
    return CGSizeMake(size.width, totalHeight);
}

@end

