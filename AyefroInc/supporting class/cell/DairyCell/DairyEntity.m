//
//  DairyEntity.m
//  AyefroInc
//
//  Created by Vijay Darkonde on 09/01/18.
//  Copyright © 2018 Inceptive Consulting Pvt. Ltd. All rights reserved.
//

#import "DairyEntity.h"

@implementation DairyEntity
- (instancetype)initWithDictionary:(NSDictionary *)dictionary
{
    self = super.init;
    if (self) {
        
        _identifier = [self uniqueIdentifier];
        _DTopics = dictionary[@"DTopics"];
        _DImageDesc = dictionary[@"DImageDesc"];
        _DImageUser = dictionary[@"DImageUser"];
        _image = dictionary[@"image"];
        _DImgUserName = dictionary[@"DImgUserName"];
        _DImgCmtCount = [[dictionary objectForKey:@"DImgCmtCount"] intValue];
        _DImglikeCount = [[dictionary objectForKey:@"DImglikeCount"] intValue];
        _DImgUserImage = dictionary[@"DImgUserImage"];
//        _DTopicsCount = dictionary[@"DTopicsCount"];
        _DImgliked = [[dictionary objectForKey:@"DImgliked"] boolValue];
        _TimeDiffhr = [[dictionary objectForKey:@"TimeDiffhr"] intValue];
        _TimeDiffmin = [[dictionary objectForKey:@"TimeDiffmin"] intValue];
        _TimeDiffsec = [[dictionary objectForKey:@"TimeDiffsec"] intValue];
        _Type = dictionary[@"Type"];
        _PromotionAim= dictionary[@"PromotionAim"];
        _ProviderId= dictionary[@"ProviderId"];
        _WebsiteLink= dictionary[@"WebsiteLink"];
        _contactDetails= dictionary[@"contactDetails"];
        _SocialMedia_facebook= dictionary[@"SocialMedia_facebook"];
        _SocialMedia_Instagram= dictionary[@"SocialMedia_Instagram"];
        _SocialMedia_twitter= dictionary[@"SocialMedia_twitter"];
        _ImageId = [[dictionary objectForKey:@"ImageId"] intValue];
        
        _DImgUserCountry = dictionary[@"DImgUserCountry"];
        _comments = dictionary[@"comments"];
    }
    return self;
}

- (NSString *)uniqueIdentifier
{
    static NSInteger counter = 0;
    return [NSString stringWithFormat:@"unique-id-%@", @(counter++)];
}

@end
