//
//  DairyCell.h
//  AyefroInc
//
//  Created by Vijay Darkonde on 09/01/18.
//  Copyright © 2018 Inceptive Consulting Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import <SDWebImage/UIImage+GIF.h>
#import "DairyEntity.h"

@interface DairyCell : UITableViewCell
{
    BOOL isFullScreen;
    CGRect prevFrame;
    UITapGestureRecognizer *tap;
}
@property (nonatomic, strong) DairyEntity *entity;
@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
@property (nonatomic, weak) IBOutlet UILabel *contentLabel;
@property (nonatomic, weak) IBOutlet UIImageView *contentImageView;
@property (nonatomic, weak) IBOutlet UILabel *usernameLabel;
@property (nonatomic, weak) IBOutlet UILabel *timeLabel;
@property (strong, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (strong, nonatomic) IBOutlet UIImageView *profileImage;
@property (weak, nonatomic) IBOutlet UIView *bottemView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *thirdSpacing;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *secondSpacing;
@property (weak, nonatomic) IBOutlet UIImageView *sponsoredImg;
@property (weak, nonatomic) IBOutlet UIImageView *actionLbl;
@property (weak, nonatomic) IBOutlet UILabel *tagsLbl;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *fourthSpacing;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *fifthSpacing;


@property (weak, nonatomic) IBOutlet UILabel *commentTxt;
@property (weak, nonatomic) IBOutlet UILabel *commentTimeLbl;
@property (weak, nonatomic) IBOutlet UILabel *commenterName;
@property (weak, nonatomic) IBOutlet UIImageView *commenterImg;
@property (weak, nonatomic) IBOutlet UIView *commentLikeView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *nameSpace;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *commentSpace;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomSpace;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *timeSpace;
@property (weak, nonatomic) IBOutlet UILabel *countryLbl;
@property (weak, nonatomic) IBOutlet UIButton *likeBtn;
@property (weak, nonatomic) IBOutlet UIButton *commentbtn;


@end
