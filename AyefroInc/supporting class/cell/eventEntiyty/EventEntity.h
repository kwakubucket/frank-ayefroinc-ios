//
//  EventEntity.h
//  AyefroInc
//
//  Created by Vijay Darkonde on 23/12/17.
//  Copyright © 2017 Inceptive Consulting Pvt. Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EventEntity : NSObject
- (instancetype)initWithDictionary:(NSDictionary *)dictionary;

@property (nonatomic, copy) NSString *identifier;
@property (nonatomic, copy) NSString *FirstName;
@property (nonatomic, copy) NSString *LastName;
@property (nonatomic, copy) NSString *image;
@property (nonatomic) NSString *ServiecProviderId;
@property (nonatomic) NSArray *OrderServicesArray;

@end
