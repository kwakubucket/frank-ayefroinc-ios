//
//  EventEntity.m
//  AyefroInc
//
//  Created by Vijay Darkonde on 23/12/17.
//  Copyright © 2017 Inceptive Consulting Pvt. Ltd. All rights reserved.
//

#import "EventEntity.h"

@implementation EventEntity
- (instancetype)initWithDictionary:(NSDictionary *)dictionary
{
    self = super.init;
    if (self) {
        
        _identifier = [self uniqueIdentifier];
        _FirstName = dictionary[@"FirstName"];
        _LastName = dictionary[@"LastName"];
        _image = dictionary[@"image"];
        _ServiecProviderId = dictionary[@"ServiecProviderId"];
        _OrderServicesArray = dictionary[@"OrderServicesArray"];
    }
    return self;
}

- (NSString *)uniqueIdentifier
{
    static NSInteger counter = 0;
    return [NSString stringWithFormat:@"unique-id-%@", @(counter++)];
}

@end
