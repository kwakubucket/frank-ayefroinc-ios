//
//  EventCell.m
//  AyefroInc
//
//  Created by Vijay Darkonde on 23/12/17.
//  Copyright © 2017 Inceptive Consulting Pvt. Ltd. All rights reserved.
//

#import "EventCell.h"
#import "UITableView+FDTemplateLayoutCell.h"

@implementation EventCell
@synthesize userNameLbl,profileImageView,activityLbl,addlabl;

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.contentView.bounds = [UIScreen mainScreen].bounds;
    profileImageView.layer.cornerRadius = profileImageView.frame.size.height/2;
    profileImageView.layer.masksToBounds = YES;
    
}

- (void)setEntity:(EventEntity *)entity
{
    
    _entity = entity;
    self.userNameLbl.text = [entity.FirstName stringByAppendingString:[@" " stringByAppendingString:entity.LastName]];
    
    if(entity.image.length > 0){
        NSString *str=[[NSBundle mainBundle] pathForResource:@"loading_spinner" ofType:@"gif"];
        NSData *fileData = [NSData dataWithContentsOfFile:str];
        [self.profileImageView sd_setImageWithURL:[NSURL URLWithString:entity.image] placeholderImage:[UIImage sd_animatedGIFWithData:fileData]];
    }
    else{
        self.profileImageView.image = [UIImage imageNamed:@"user.ing"];
    }
    
    NSMutableArray *arr = [[NSMutableArray alloc]init];
    
    for(int i = 0; i < [entity.OrderServicesArray count]; i++){
        NSDictionary *dict = [[NSDictionary alloc] init];
        dict = [entity.OrderServicesArray objectAtIndex:i];
        NSString *serviceName = [dict objectForKey:@"serviceName"];
        [arr addObject:serviceName];
    }
    NSString *joinedComponents = [arr componentsJoinedByString:@","];
    self.activityLbl.text = joinedComponents;
                                 
}

- (CGSize)sizeThatFits:(CGSize)size {
    CGFloat totalHeight = 0;
    totalHeight += [self.userNameLbl sizeThatFits:size].height;
    totalHeight += [self.profileImageView sizeThatFits:size].height;
    totalHeight += [self.activityLbl sizeThatFits:size].height;
    totalHeight += [self.addlabl sizeThatFits:size].height;
    totalHeight += 40; // margins
    
    return CGSizeMake(size.width, totalHeight);
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end

