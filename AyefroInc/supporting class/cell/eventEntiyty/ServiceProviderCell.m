//
//  ServiceProviderCell.m
//  AyefroInc
//
//  Created by Vijay Darkonde on 23/12/17.
//  Copyright © 2017 Inceptive Consulting Pvt. Ltd. All rights reserved.
//

#import "ServiceProviderCell.h"

@implementation ServiceProviderCell
@synthesize userNameLbl,profileImageView,activityLbl,costLbl,addlabl;

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.contentView.bounds = [UIScreen mainScreen].bounds;
    profileImageView.layer.cornerRadius = profileImageView.frame.size.height/2;
    profileImageView.layer.masksToBounds = YES;
    
}

- (void)setEntity:(EventEntity *)entity
{
    
    _entity = entity;
    self.userNameLbl.text = [entity.FirstName stringByAppendingString:[@" " stringByAppendingString:entity.LastName]];
    
    if(entity.image.length > 0){
        NSString *str=[[NSBundle mainBundle] pathForResource:@"loading_spinner" ofType:@"gif"];
        NSData *fileData = [NSData dataWithContentsOfFile:str];
        [self.profileImageView sd_setImageWithURL:[NSURL URLWithString:entity.image] placeholderImage:[UIImage sd_animatedGIFWithData:fileData]];
    }
    else{
        self.profileImageView.image = [UIImage imageNamed:@"user.ing"];
    }
    
    NSMutableArray *arr = [[NSMutableArray alloc]init];
    NSMutableArray *arr1 = [[NSMutableArray alloc]init];
    
    for(int i = 0; i < [entity.OrderServicesArray count]; i++){
        NSDictionary *dict = [[NSDictionary alloc] init];
        dict = [entity.OrderServicesArray objectAtIndex:i];
        NSString *serviceName = [dict objectForKey:@"serviceName"];
        [arr addObject:serviceName];
        NSString *cost = [dict objectForKey:@"Cost"];
        [arr1 addObject:cost];
    }
    NSString *joinedComponents = [arr componentsJoinedByString:@","];
    self.activityLbl.text = joinedComponents;
    NSString *joinedComponentsCost = [arr1 componentsJoinedByString:@","];
    self.currencySymbolObjC = [[NSUserDefaults standardUserDefaults] valueForKey:@"currencySymbol"];
    self.costLbl.text = [joinedComponentsCost  stringByAppendingString:self.currencySymbolObjC];
//    for (int i = 0; i < arr.count; i++) {
//        self.activityLbl.text = arr[i];
//        self.costLbl.text = [[NSString stringWithFormat:@"%@ ", arr1[i]] stringByAppendingString:self.currencySymbolObjC];
//    }
}
  
- (CGSize)sizeThatFits:(CGSize)size {
    CGFloat totalHeight = 0;
    totalHeight += [self.userNameLbl sizeThatFits:size].height;
    totalHeight += [self.activityLbl sizeThatFits:size].height;
    totalHeight += [self.addlabl sizeThatFits:size].height;
    totalHeight += 50; // margins
    
    return CGSizeMake(size.width, totalHeight);
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end


