//
//  ServiceProviderCell.h
//  AyefroInc
//
//  Created by Vijay Darkonde on 23/12/17.
//  Copyright © 2017 Inceptive Consulting Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EventEntity.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import <SDWebImage/UIImage+GIF.h>

@interface ServiceProviderCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *profileImageView;
@property (strong, nonatomic) IBOutlet UILabel *userNameLbl;
@property (strong, nonatomic) IBOutlet UILabel *activityLbl;
    @property (strong, nonatomic) IBOutlet UILabel *costLbl;
    @property (nonatomic, strong) EventEntity *entity;
@property (weak, nonatomic) IBOutlet UILabel *addlabl;
    @property (weak, nonatomic) NSString *currencySymbolObjC;
    
@end
