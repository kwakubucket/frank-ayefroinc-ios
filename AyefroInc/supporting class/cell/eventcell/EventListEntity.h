//
//  EventListEntity.h
//  AyefroInc
//
//  Created by Vijay Darkonde on 24/11/17.
//  Copyright © 2017 Inceptive Consulting Pvt. Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EventListEntity : NSObject
- (instancetype)initWithDictionary:(NSDictionary *)dictionary;

@property (nonatomic, copy ) NSString *identifier;
@property (nonatomic, copy ) NSString *created_At;
@property (nonatomic ) int eventBudget;
@property (nonatomic ) int eventId;
@property (nonatomic, copy ) NSString *eventName;
@property (nonatomic ) int eventServices;
@property (nonatomic, copy ) NSArray *eventServicesArray;
@property (nonatomic, copy ) NSString *userFirstName;
@property (nonatomic, copy ) NSString *userId;
@property (nonatomic, copy ) NSString *userPhoto;
@property (nonatomic, copy ) NSString *eventUserLocation;
@property (nonatomic ) int guest;
@property (nonatomic ) int serviceId;
@end
