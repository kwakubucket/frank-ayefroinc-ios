//
//  FDFeedEntity.h
//  Demo
//
//  Created by sunnyxx on 15/4/16.
//  Copyright (c) 2015年 forkingdog. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FDFeedEntity : NSObject
    {
        
    }

- (instancetype)initWithDictionary:(NSDictionary *)dictionary;

@property (nonatomic, copy ) NSString *identifier;
@property (nonatomic, copy ) NSString *Sendate;
@property (nonatomic, copy ) NSString *Qtext;
@property (nonatomic, copy ) NSString *QDesc;
@property (nonatomic, copy ) NSString *image;
@property (nonatomic, copy ) NSString *QUsername;
@property (nonatomic, copy ) NSString *QSendUser;
@property (nonatomic, copy ) NSString *QUserImage;
@property (nonatomic ) int TimeDiffhr;
@property (nonatomic ) int TimeDiffmin;
@property (nonatomic ) int TimeDiffsec;
@property (nonatomic ) int AnswerCount;
@property (nonatomic ) int LikeCount;
@property (nonatomic ) Boolean Isliked;
@property (nonatomic ) int QId;

@property (nonatomic, copy ) NSString *Type;
@property (nonatomic ) int ImageId;
@property (nonatomic, copy ) NSString *PromotionAim;
@property (nonatomic, copy ) NSString *ProviderId;
@property (nonatomic, copy ) NSString *WebsiteLink;
@property (nonatomic, copy ) NSString *contactDetails;
@property (nonatomic, copy ) NSString *SocialMedia_facebook;
@property (nonatomic, copy ) NSString *SocialMedia_Instagram;
@property (nonatomic, copy ) NSString *SocialMedia_twitter;


@end
