//
//  FDFeedCell.m
//  Demo
//
//  Created by sunnyxx on 15/4/17.
//  Copyright (c) 2015年 forkingdog. All rights reserved.
//

#import "FDFeedCell.h"


@interface FDFeedCell ()



@end

@implementation FDFeedCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    // Fix the bug in iOS7 - initial constraints warning
    self.contentView.bounds = [UIScreen mainScreen].bounds;
    _profileImage.layer.cornerRadius = _profileImage.frame.size.height/2;
    _profileImage.layer.masksToBounds = YES;
    self.contentImageView.contentMode = UIViewContentModeScaleAspectFit;
    self.userInteractionEnabled = YES;
    

}

- (void)setEntity:(FDFeedEntity *)entity
{
    
    _entity = entity;
    
    if ([entity.Type isEqual: @"Advertise"]){
        [self.bottemView setHidden:YES];
        self.profileImage.image = nil;
        self.titleLabel.text = @"";
        self.descriptionLabel.text = @"";
        self.contentLabel.text = @"";
        self.secondSpacing.constant = 0.0;
        self.thirdSpacing.constant = 0.0;
        NSString *str=[[NSBundle mainBundle] pathForResource:@"loading_spinner" ofType:@"gif"];
        self.userInteractionEnabled = YES;
        NSData *fileData = [NSData dataWithContentsOfFile:str];
        [self.contentImageView sd_setImageWithURL:[NSURL URLWithString:entity.image] placeholderImage:[UIImage sd_animatedGIFWithData:fileData]];
        self.sponsoredImg.image = [UIImage imageNamed:@"sponsored.png"];
        if ([entity.PromotionAim isEqual: @"Get more calls for your business"]) {
            self.actionLbl.image = [UIImage imageNamed:@"callnow.png"];
        }
        else{
            self.actionLbl.image = [UIImage imageNamed:@"learnmore.png"];
        }
    }
    else{
        [self.bottemView setHidden:NO];
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        formatter.dateFormat = @"yyyy-MM-dd'T'HH:mm:ss.SSS";
        int CDate =  entity.TimeDiffsec;
        int Cmin =  entity.TimeDiffmin;
        int Chours =  entity.TimeDiffhr;
        NSString *time = @"";
        if (Chours > 0){
            if ((Chours / 24) <= 0){
                time = [NSString stringWithFormat:@"%d hour(s)", entity.TimeDiffhr];
            }
            else if ((Chours) / (24 * 30) <= 0){
                time = [NSString stringWithFormat:@"%d day(s)", (Chours / 24)];
            }
            else{
                time = [NSString stringWithFormat:@"%d month(s)", (Chours) / (24 * 30)];
            }
        }
        else if (Cmin > 0){
            time = [NSString stringWithFormat:@"%d minute(s)", entity.TimeDiffmin];
        }
        else{
            time = [NSString stringWithFormat:@"%d second(s)", entity.TimeDiffsec];
        }
        
        
//        if (CDate >= 60 ) {
//            int minutes = (CDate / 60);
//            if (minutes >= 60) {
//                int hours = CDate / 3600;
//                if (hours >= 24) {
//                    int days = hours / 24;
//                    time = [NSString stringWithFormat:@"%d day(s)", days];
//                }
//                else{
//                    time = [NSString stringWithFormat:@"%d hour(s)", hours];
//                }
//
//            }
//            else{
//                time = [NSString stringWithFormat:@"%d minute(s)", minutes];
//            }
//        }
//        else{
//            time = [NSString stringWithFormat:@"%d second(s)", CDate];
//        }
        self.titleLabel.text = entity.Qtext;
        
        self.descriptionLabel.text = entity.QDesc;
        self.contentLabel.text = [entity.QUsername stringByAppendingString: [@"- Last activity " stringByAppendingString:[time stringByAppendingString:@" ago."]]];
        if(entity.image.length > 0){
            NSString *str=[[NSBundle mainBundle] pathForResource:@"loading_spinner" ofType:@"gif"];
            
            NSData *fileData = [NSData dataWithContentsOfFile:str];
            [self.contentImageView sd_setImageWithURL:[NSURL URLWithString:entity.image] placeholderImage:[UIImage sd_animatedGIFWithData:fileData]];
        }
        else{
            self.contentImageView.image = nil;
        }
        
        
        if(entity.QUserImage.length > 0){
            NSString *str=[[NSBundle mainBundle] pathForResource:@"loading_spinner" ofType:@"gif"];
            
            NSData *fileData = [NSData dataWithContentsOfFile:str];
            [self.profileImage sd_setImageWithURL:[NSURL URLWithString:entity.QUserImage] placeholderImage:[UIImage sd_animatedGIFWithData:fileData]];
        }
        else{
            self.profileImage.image = [UIImage imageNamed:@"user.ing"];
        }
        self.usernameLabel.text = [NSString stringWithFormat:@"%d", entity.AnswerCount];
        self.timeLabel.text = [NSString stringWithFormat:@"%d", entity.LikeCount];
        self.userInteractionEnabled = YES;
        self.sponsoredImg.image = nil;
        self.actionLbl.image = nil;
        self.secondSpacing.constant = 20.0;
        self.thirdSpacing.constant = 15.0;
        
        
    }
    
}

- (CGSize)sizeThatFits:(CGSize)size {
    CGFloat totalHeight = 0;
    if ([_entity.Type isEqual: @"Forum"]){
        totalHeight += [self.titleLabel sizeThatFits:size].height;
        totalHeight += [self.contentLabel sizeThatFits:size].height;
        if ([_entity.image  isEqual: @""]){
            totalHeight += 0;
            totalHeight += [self.usernameLabel sizeThatFits:size].height;
            totalHeight += [self.descriptionLabel sizeThatFits:size].height;
            totalHeight += [self.contentLabel sizeThatFits:size].height;
            totalHeight += 40;
        }
        else{
            totalHeight += 260;
            totalHeight += [self.usernameLabel sizeThatFits:size].height;
            totalHeight += [self.descriptionLabel sizeThatFits:size].height;
            totalHeight += [self.contentLabel sizeThatFits:size].height;
        }
        totalHeight += 50;
    }
    else{
        totalHeight += 240;
        totalHeight += 10;
    }
    
   // margins
    
    return CGSizeMake(size.width, totalHeight);
}

@end
