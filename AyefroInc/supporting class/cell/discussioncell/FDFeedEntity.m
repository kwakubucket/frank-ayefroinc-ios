//
//  FDFeedEntity.m
//  Demo
//
//  Created by sunnyxx on 15/4/16.
//  Copyright (c) 2015年 forkingdog. All rights reserved.
//

#import "FDFeedEntity.h"

@implementation FDFeedEntity

- (instancetype)initWithDictionary:(NSDictionary *)dictionary
{
    self = super.init;
    if (self) {

        _identifier = [self uniqueIdentifier];
        _Qtext= dictionary[@"Qtext"];
        _QDesc = dictionary[@"QDesc"];
        _image = dictionary[@"image"];
        _QUsername = dictionary[@"QUsername"];
        _Sendate = dictionary[@"Sendate"];
        _QUserImage= dictionary[@"QUserImage"];
        _QUserImage= dictionary[@"QUserImage"];
        _TimeDiffhr = [[dictionary objectForKey:@"TimeDiffhr"] intValue];
        _TimeDiffmin = [[dictionary objectForKey:@"TimeDiffmin"] intValue];
        _TimeDiffsec = [[dictionary objectForKey:@"TimeDiffsec"] intValue];
        _AnswerCount = [[dictionary objectForKey:@"AnswerCount"] intValue];
        _LikeCount = [[dictionary objectForKey:@"LikeCount"] intValue];
        _QId = [[dictionary objectForKey:@"QId"] intValue];
        _Isliked = [[dictionary objectForKey:@"Isliked"] boolValue];
        
        _Type = dictionary[@"Type"];
        _PromotionAim= dictionary[@"PromotionAim"];
        _ProviderId= dictionary[@"ProviderId"];
        _WebsiteLink= dictionary[@"WebsiteLink"];
        _contactDetails= dictionary[@"contactDetails"];
        _SocialMedia_facebook= dictionary[@"SocialMedia_facebook"];
        _SocialMedia_Instagram= dictionary[@"SocialMedia_Instagram"];
        _SocialMedia_twitter= dictionary[@"SocialMedia_twitter"];
        _ImageId = [[dictionary objectForKey:@"ImageId"] intValue];

    }
    return self;
}

- (NSString *)uniqueIdentifier
{
    static NSInteger counter = 0;
    return [NSString stringWithFormat:@"unique-id-%@", @(counter++)];
}

@end
