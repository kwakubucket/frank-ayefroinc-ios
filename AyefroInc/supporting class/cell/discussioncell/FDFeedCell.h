//
//  FDFeedCell.h
//  Demo
//
//  Created by sunnyxx on 15/4/17.
//  Copyright (c) 2015年 forkingdog. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FDFeedEntity.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import <SDWebImage/UIImage+GIF.h>
@interface FDFeedCell : UITableViewCell<UIGestureRecognizerDelegate>
{
    BOOL isFullScreen;
    CGRect prevFrame;
    UITapGestureRecognizer *tap;
}
@property (nonatomic, strong) FDFeedEntity *entity;
@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
@property (nonatomic, weak) IBOutlet UILabel *contentLabel;
@property (nonatomic, weak) IBOutlet UIImageView *contentImageView;
@property (nonatomic, weak) IBOutlet UILabel *usernameLabel;
@property (nonatomic, weak) IBOutlet UILabel *timeLabel;
@property (strong, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (strong, nonatomic) IBOutlet UIImageView *profileImage;
@property (weak, nonatomic) IBOutlet UIView *bottemView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *thirdSpacing;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *secondSpacing;
@property (weak, nonatomic) IBOutlet UIImageView *sponsoredImg;
@property (weak, nonatomic) IBOutlet UIImageView *actionLbl;

@end
