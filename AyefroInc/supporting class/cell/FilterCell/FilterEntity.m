//
//  FilterEntity.m
//  AyefroInc
//
//  Created by Vijay Darkonde on 16/12/17.
//  Copyright © 2017 Inceptive Consulting Pvt. Ltd. All rights reserved.
//

#import "FilterEntity.h"

@implementation FilterEntity
- (instancetype)initWithDictionary:(NSDictionary *)dictionary
{
    self = super.init;
    if (self) {
        
        _identifier = [self uniqueIdentifier];
        _created_At = dictionary[@"created_At"];
        _eventBudget = [[dictionary objectForKey:@"eventBudget"] intValue];
        _eventId = [[dictionary objectForKey:@"eventId"] intValue];
        _isEventHide = [[dictionary objectForKey:@"isEventHide"] boolValue];
        _eventName = dictionary[@"eventName"];
        _eventServices = [[dictionary objectForKey:@"eventServices"] intValue];
        _eventServicesArray = [dictionary objectForKey:@"eventServicesArray"];
        _userFirstName = dictionary[@"userFirstName"];
        _userId = dictionary[@"userId"];
        _eventUserLocation = dictionary[@"eventUserLocation"];
        _userPhoto = dictionary[@"userPhoto"];
        _guest = [[dictionary objectForKey:@"guest"] intValue];
        _serviceId = [[dictionary objectForKey:@"serviceId"] intValue];
        
        
    }
    return self;
}

- (NSString *)uniqueIdentifier
{
    static NSInteger counter = 0;
    return [NSString stringWithFormat:@"unique-id-%@", @(counter++)];
}
@end
