//
//  FilterEventCell.m
//  AyefroInc
//
//  Created by Vijay Darkonde on 16/12/17.
//  Copyright © 2017 Inceptive Consulting Pvt. Ltd. All rights reserved.
//

#import "FilterEventCell.h"

@implementation FilterEventCell
@synthesize profilePicImg,eventNameLb,budgetLbl,userNameLbl,dateLbl,serviceLbl,backView;

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.contentView.bounds = [UIScreen mainScreen].bounds;
    profilePicImg.layer.cornerRadius = profilePicImg.frame.size.height/2;
    profilePicImg.layer.masksToBounds = YES;
    backView.layer.cornerRadius = 3;
    
}

- (IBAction)hideBtnClk:(id)sender {
    
}


- (void)setEntity:(FilterEntity *)entity
{
    
    _entity = entity;
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *currencySymbol = [prefs stringForKey:@"currencySymbol"];
    
    if(entity.userPhoto.length > 0){
        NSString *str=[[NSBundle mainBundle] pathForResource:@"loading_spinner" ofType:@"gif"];
        
        NSData *fileData = [NSData dataWithContentsOfFile:str];
        [self.profilePicImg sd_setImageWithURL:[NSURL URLWithString:entity.userPhoto] placeholderImage:[UIImage sd_animatedGIFWithData:fileData]];
    }
    else{
        self.profilePicImg.image = [UIImage imageNamed:@"user.ing"];
    }
    
    self.eventNameLb.text = entity.eventName;
    self.userNameLbl.text = entity.userFirstName;
    self.budgetLbl.text = [[NSString stringWithFormat:@"%d", entity.eventBudget] stringByAppendingString:[@" " stringByAppendingString:currencySymbol]];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"yyyy-MM-dd'T'HH:mm:ss";
    NSDate *date = [formatter dateFromString:entity.created_At];
    formatter.dateFormat = @"dd/MM/yyyy";
    NSString *strDate = [formatter stringFromDate:date];
    self.dateLbl.text = [@"" stringByAppendingString: strDate];
    NSMutableArray *servicesArr = [[NSMutableArray alloc]init];
    for (int i = 0; i < [entity.eventServicesArray count]; i++) {
        NSDictionary *dict = [entity.eventServicesArray objectAtIndex:i];
        NSString *serviceName = [dict objectForKey:@"serviceName"];
        [servicesArr addObject:serviceName];
    }
    if ([servicesArr count] > 2) {
        NSString *firstStr = [servicesArr objectAtIndex:0];
        NSString *secondStr = [servicesArr objectAtIndex:1];
        NSString *combineStr = [@"" stringByAppendingString:[firstStr stringByAppendingString:[@"," stringByAppendingString:[ secondStr stringByAppendingString:@"..More"]]]];
        self.serviceLbl.text = combineStr;
    }
    else{
        NSString *string = [servicesArr componentsJoinedByString:@","];
        self.serviceLbl.text =  [@"" stringByAppendingString:string];
    }
    
    self.locationLbl.text = entity.eventUserLocation;
    
    if (entity.isEventHide == YES){
        [self.hideBtn setImage:[UIImage imageNamed:@"unhide"] forState:UIControlStateNormal];
    }
    else{
        [self.hideBtn setImage:[UIImage imageNamed:@"hidebtn"] forState:UIControlStateNormal];
    }
    
}

- (CGSize)sizeThatFits:(CGSize)size {
    CGFloat totalHeight = 0;
    totalHeight += [self.eventNameLb sizeThatFits:size].height;
    totalHeight += [self.eventNameLb sizeThatFits:size].height;
    totalHeight += [self.eventNameLb sizeThatFits:size].height;
    totalHeight += [self.eventNameLb sizeThatFits:size].height;
    totalHeight += [self.eventNameLb sizeThatFits:size].height;
    totalHeight += [self.eventNameLb sizeThatFits:size].height;
    totalHeight += [self.eventNameLb sizeThatFits:size].height;
    totalHeight += [self.eventNameLb sizeThatFits:size].height;
    totalHeight += 40; // margins
    
    return CGSizeMake(size.width, totalHeight);
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end


