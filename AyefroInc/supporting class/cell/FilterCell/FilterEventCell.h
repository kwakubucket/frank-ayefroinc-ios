//
//  FilterEventCell.h
//  AyefroInc
//
//  Created by Vijay Darkonde on 16/12/17.
//  Copyright © 2017 Inceptive Consulting Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FilterEntity.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import <SDWebImage/UIImage+GIF.h>

@interface FilterEventCell : UITableViewCell

@property (nonatomic, strong) FilterEntity *entity;
@property (strong, nonatomic) IBOutlet UIImageView *profilePicImg;
@property (strong, nonatomic) IBOutlet UILabel *eventNameLb;
@property (strong, nonatomic) IBOutlet UILabel *userNameLbl;
@property (strong, nonatomic) IBOutlet UILabel *budgetLbl;
@property (strong, nonatomic) IBOutlet UILabel *dateLbl;
@property (strong, nonatomic) IBOutlet UILabel *serviceLbl;
@property (strong, nonatomic) IBOutlet UIButton *hideBtn;
@property (strong, nonatomic) IBOutlet UIView *backView;
@property (strong, nonatomic) IBOutlet UIButton *detailBtn;
@property (strong, nonatomic) IBOutlet UILabel *locationLbl;

@end
