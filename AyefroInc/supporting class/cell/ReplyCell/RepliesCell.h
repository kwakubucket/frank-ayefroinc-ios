//
//  RepliesCell.h
//  AyefroInc
//
//  Created by Vijay Darkonde on 17/12/17.
//  Copyright © 2017 Inceptive Consulting Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RepliesEntity.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import <SDWebImage/UIImage+GIF.h>

@interface RepliesCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIImageView *profileImageView;
@property (strong, nonatomic) IBOutlet UILabel *userNameLbl;
@property (strong, nonatomic) IBOutlet UILabel *activityLbl;
@property (strong, nonatomic) IBOutlet UILabel *commentTxtLbl;
@property (strong, nonatomic) IBOutlet UIImageView *likeImgView;
@property (strong, nonatomic) IBOutlet UILabel *likeLbl;
@property (nonatomic, strong) RepliesEntity *entity;
@property (weak, nonatomic) IBOutlet UIButton *replyBtn;


@end
