//
//  RepliesEntity.h
//  AyefroInc
//
//  Created by Vijay Darkonde on 17/12/17.
//  Copyright © 2017 Inceptive Consulting Pvt. Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RepliesEntity : NSObject
- (instancetype)initWithDictionary:(NSDictionary *)dictionary;

@property (nonatomic, copy ) NSString *identifier;
@property (nonatomic ) Boolean Isliked;
@property (nonatomic ) int ReplyId;
@property (nonatomic ) int ReplyLikeCount;
@property (nonatomic, copy ) NSString *ReplySendUser;
@property (nonatomic, copy ) NSString *ReplySenddate;
@property (nonatomic, copy ) NSString *ReplyUserImage;
@property (nonatomic, copy ) NSString *ReplyUserName;
@property (nonatomic ) int ReplyUserRole;
@property (nonatomic, copy ) NSString *Replytext;
@property (nonatomic ) int TimeDiffhr;
@property (nonatomic ) int TimeDiffmin;
@property (nonatomic ) int TimeDiffsec;



@end
