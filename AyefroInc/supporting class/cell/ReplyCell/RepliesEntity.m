//
//  RepliesEntity.m
//  AyefroInc
//
//  Created by Vijay Darkonde on 17/12/17.
//  Copyright © 2017 Inceptive Consulting Pvt. Ltd. All rights reserved.
//

#import "RepliesEntity.h"

@implementation RepliesEntity
- (instancetype)initWithDictionary:(NSDictionary *)dictionary
{
    self = super.init;
    if (self) {
        
        _identifier = [self uniqueIdentifier];
        _Replytext = dictionary[@"Replytext"];
        _ReplyUserName = dictionary[@"ReplyUserName"];
        _ReplySenddate = dictionary[@"ReplySenddate"];
        _ReplyUserImage= dictionary[@"ReplyUserImage"];
        _TimeDiffhr = [[dictionary objectForKey:@"TimeDiffhr"] intValue];
        _ReplyUserRole = [[dictionary objectForKey:@"ReplyUserRole"] intValue];
        _TimeDiffmin = [[dictionary objectForKey:@"TimeDiffmin"] intValue];
        _TimeDiffsec = [[dictionary objectForKey:@"TimeDiffsec"] intValue];
        _ReplySendUser = [dictionary objectForKey:@"ReplySendUser"];
        _ReplyLikeCount = [[dictionary objectForKey:@"ReplyLikeCount"] intValue];
        _ReplyId = [[dictionary objectForKey:@"ReplyId"] intValue];
        _ReplyUserRole = [[dictionary objectForKey:@"ReplyUserRole"] intValue];
        _Isliked = [[dictionary objectForKey:@"Isliked"] boolValue];
    }
    return self;
}

- (NSString *)uniqueIdentifier
{
    static NSInteger counter = 0;
    return [NSString stringWithFormat:@"unique-id-%@", @(counter++)];
}
@end
