//
//  BidEventEntity.h
//  AyefroInc
//
//  Created by Vijay Darkonde on 15/12/17.
//  Copyright © 2017 Inceptive Consulting Pvt. Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BidEventEntity : NSObject
- (instancetype)initWithDictionary:(NSDictionary *)dictionary;

@property (nonatomic, copy ) NSString *identifier;
@property (nonatomic ) int EventId;
@property (nonatomic, copy ) NSString *EventName;
@property (nonatomic ) int EventBudget;
@property (nonatomic ) int EventGuest;
@property (nonatomic, copy ) NSString *EventUpdatedDate;
@property (nonatomic, copy ) NSString *EventUser;
@property (nonatomic, copy ) NSString *EventUserName;
@property (nonatomic, copy ) NSString *EventUserLocation;
@property (nonatomic ) int BidCount;
@property (nonatomic, copy ) NSArray *EventBidList;
@property (nonatomic, copy ) NSArray *eventServicesArray;

@end
