//
//  BidEventEntity.m
//  AyefroInc
//
//  Created by Vijay Darkonde on 15/12/17.
//  Copyright © 2017 Inceptive Consulting Pvt. Ltd. All rights reserved.
//

#import "BidEventEntity.h"

@implementation BidEventEntity
- (instancetype)initWithDictionary:(NSDictionary *)dictionary
{
    self = super.init;
    if (self) {
        
        _identifier = [self uniqueIdentifier];
        _EventUpdatedDate = dictionary[@"EventUpdatedDate"];
        _EventBudget = [[dictionary objectForKey:@"EventBudget"] intValue];
        _BidCount = [[dictionary objectForKey:@"BidCount"] intValue];
        _EventId = [[dictionary objectForKey:@"EventId"] intValue];
        _EventName = dictionary[@"EventName"];
        _EventBidList = [dictionary objectForKey:@"EventBidList"];
        _EventUserName = dictionary[@"EventUserName"];
        _EventUser = dictionary[@"EventUser"];
        _EventUserLocation = dictionary[@"EventUserLocation"];
        _EventGuest = [[dictionary objectForKey:@"EventGuest"] intValue];
        _eventServicesArray = [dictionary objectForKey:@"EventServices"];

    }
    return self;
}

- (NSString *)uniqueIdentifier
{
    static NSInteger counter = 0;
    return [NSString stringWithFormat:@"unique-id-%@", @(counter++)];
}
@end
