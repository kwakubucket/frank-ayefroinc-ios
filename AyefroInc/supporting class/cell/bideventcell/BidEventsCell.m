//
//  BidEventsCell.m
//  AyefroInc
//
//  Created by Vijay Darkonde on 15/12/17.
//  Copyright © 2017 Inceptive Consulting Pvt. Ltd. All rights reserved.
//

#import "BidEventsCell.h"

@implementation BidEventsCell
@synthesize eventNameLbl,dateLbl,locationLbl,servicesLbl,bidCountLbl, backView;

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.backView.layer.cornerRadius = 5;
}

- (void)setEntity:(BidEventEntity *)entity
{
    
    _entity = entity;
   
    self.eventNameLbl.text = entity.EventName;
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"yyyy-MM-dd'T'HH:mm:ss";
    NSDate *date = [formatter dateFromString:entity.EventUpdatedDate];
    if (date == nil){
        formatter.dateFormat = @"yyyy-MM-dd'T'HH:mm:ss.SSS";
        date = [formatter dateFromString:entity.EventUpdatedDate];
    }
    formatter.dateFormat = @"dd/MM/yyyy";
    
    NSString *strDate = [formatter stringFromDate:date];
    self.dateLbl.text = [@"" stringByAppendingString: strDate];
    
    self.locationLbl.text = entity.EventUserLocation;
    
    NSMutableArray *servicesArr = [[NSMutableArray alloc]init];
    for (int i = 0; i < [entity.eventServicesArray count]; i++) {
        NSDictionary *dict = [entity.eventServicesArray objectAtIndex:i];
        NSString *serviceName = [dict objectForKey:@"serviceName"];
        [servicesArr addObject:serviceName];
    }
    if ([servicesArr count] != 0){
        if ([servicesArr count] > 2) {
            NSString *firstStr = [servicesArr objectAtIndex:0];
            NSString *secondStr = [servicesArr objectAtIndex:1];
            int count = [servicesArr count] - 2;
            NSString *countStr = [NSString stringWithFormat:@"%d", count];
            NSString *combineStr = [@"" stringByAppendingString:[firstStr stringByAppendingString:[@"," stringByAppendingString:[ secondStr stringByAppendingString:[@"..+" stringByAppendingString:[countStr stringByAppendingString:@" More"]]]]]];
            self.servicesLbl.text = combineStr;
        }
        else{
            NSString *string = [servicesArr componentsJoinedByString:@","];
            self.servicesLbl.text =  [@"" stringByAppendingString:string];
        }
    }
    else{
        self.servicesLbl.text = @"No Services";
    }
    
    
    
    self.bidCountLbl.text = [[NSString stringWithFormat:@"%d", entity.BidCount] stringByAppendingString:@" Bid(s)"];
    
    
    
}

- (CGSize)sizeThatFits:(CGSize)size {
    CGFloat totalHeight = 0;
    totalHeight += [self.eventNameLbl sizeThatFits:size].height;
    totalHeight += [self.dateLbl sizeThatFits:size].height;
    totalHeight += [self.servicesLbl sizeThatFits:size].height;
    totalHeight += [self.servicesLbl sizeThatFits:size].height;
    totalHeight += [self.locationLbl sizeThatFits:size].height;
    totalHeight += 80; // margins
    
    return CGSizeMake(size.width, totalHeight);
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
