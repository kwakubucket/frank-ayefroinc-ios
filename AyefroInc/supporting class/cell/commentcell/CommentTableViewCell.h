//
//  CommentTableViewCell.h
//  AyefroInc
//
//  Created by Vijay Darkonde on 21/11/17.
//  Copyright © 2017 Inceptive Consulting Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CommentEntity.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import <SDWebImage/UIImage+GIF.h>

@interface CommentTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIView *backView;
@property (strong, nonatomic) IBOutlet UIImageView *profileImageView;
@property (strong, nonatomic) IBOutlet UILabel *userNameLbl;
@property (strong, nonatomic) IBOutlet UILabel *activityLbl;
@property (strong, nonatomic) IBOutlet UILabel *commentTxtLbl;
@property (strong, nonatomic) IBOutlet UIImageView *commentImgView;
@property (strong, nonatomic) IBOutlet UIImageView *likeImgView;
@property (strong, nonatomic) IBOutlet UILabel *likeLbl;
@property (nonatomic, strong) CommentEntity *entity;
@property (strong, nonatomic) IBOutlet UIButton *chooseActionBtn;
@property (strong, nonatomic) IBOutlet UILabel *replyLbl;


@end
