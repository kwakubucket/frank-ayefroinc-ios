//
//  CommentEntity.h
//  AyefroInc
//
//  Created by Vijay Darkonde on 20/11/17.
//  Copyright © 2017 Inceptive Consulting Pvt. Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CommentEntity : NSObject
- (instancetype)initWithDictionary:(NSDictionary *)dictionary;

@property (nonatomic, copy ) NSString *identifier;
@property (nonatomic, copy ) NSString *AnsSendate;
@property (nonatomic, copy ) NSString *Qtext;
@property (nonatomic, copy ) NSString *Anstext;
@property (nonatomic, copy ) NSString *image;
@property (nonatomic, copy ) NSString *AnsUsername;
@property (nonatomic, copy ) NSString *AnsSendUser;
@property (nonatomic, copy ) NSString *AnsUserImage;
@property (nonatomic ) int TimeDiffhr;
@property (nonatomic ) int Role;
@property (nonatomic ) int TimeDiffmin;
@property (nonatomic ) int TimeDiffsec;
@property (nonatomic ) int AnswerCount;
@property (nonatomic ) int AnsLikeCount;
@property (nonatomic ) Boolean Isliked;
@property (nonatomic ) int AnsId;
@property (nonatomic ) NSArray *ReplyList;



@end
