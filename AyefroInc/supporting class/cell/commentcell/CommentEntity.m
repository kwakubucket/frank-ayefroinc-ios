//
//  CommentEntity.m
//  AyefroInc
//
//  Created by Vijay Darkonde on 20/11/17.
//  Copyright © 2017 Inceptive Consulting Pvt. Ltd. All rights reserved.
//

#import "CommentEntity.h"

@implementation CommentEntity
- (instancetype)initWithDictionary:(NSDictionary *)dictionary
{
    self = super.init;
    if (self) {
        
        _identifier = [self uniqueIdentifier];
        _AnsUsername= dictionary[@"AnsUsername"];
        _Anstext = dictionary[@"Anstext"];
        _image = dictionary[@"image"];
        _AnsUsername = dictionary[@"AnsUsername"];
        _AnsSendate = dictionary[@"AnsSendate"];
        _AnsUserImage= dictionary[@"AnsUserImage"];
        _TimeDiffhr = [[dictionary objectForKey:@"TimeDiffhr"] intValue];
        _Role = [[dictionary objectForKey:@"Role"] intValue];
        _TimeDiffmin = [[dictionary objectForKey:@"TimeDiffmin"] intValue];
        _TimeDiffsec = [[dictionary objectForKey:@"TimeDiffsec"] intValue];
        _AnsSendUser = [dictionary objectForKey:@"AnsSendUser"];
        _AnsLikeCount = [[dictionary objectForKey:@"AnsLikeCount"] intValue];
        _AnsId = [[dictionary objectForKey:@"AnsId"] intValue];
        _Isliked = [[dictionary objectForKey:@"Isliked"] boolValue];
        _ReplyList = dictionary[@"ReplyList"];
    }
    return self;
}

- (NSString *)uniqueIdentifier
{
    static NSInteger counter = 0;
    return [NSString stringWithFormat:@"unique-id-%@", @(counter++)];
}
@end
