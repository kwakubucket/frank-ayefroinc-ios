//
//  ImageCommentEntity.h
//  AyefroInc
//
//  Created by Vijay Darkonde on 20/12/17.
//  Copyright © 2017 Inceptive Consulting Pvt. Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ImageCommentEntity : NSObject
- (instancetype)initWithDictionary:(NSDictionary *)dictionary;

@property (nonatomic, copy ) NSString *identifier;
@property (nonatomic ) int CommentId;
@property (nonatomic, copy ) NSString *CommentText;
@property (nonatomic, copy ) NSString *CommentSendUser;
@property (nonatomic, copy ) NSString *CommentUsername;
@property (nonatomic, copy ) NSString *CommentUserImage;
@property (nonatomic, copy ) NSString *CommentSendate;
@property (nonatomic ) int TimeDiffmin;
@property (nonatomic ) int TimeDiffsec;
@property (nonatomic ) int TimeDiffhr;
@property (nonatomic ) int CommentLikeCount;
@property (nonatomic ) Boolean Isliked;
@property (nonatomic ) int Role;
@property (nonatomic ) NSArray *ReplyList;

@end
