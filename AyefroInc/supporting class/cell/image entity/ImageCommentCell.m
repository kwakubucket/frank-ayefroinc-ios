//
//  ImageCommentCell.m
//  AyefroInc
//
//  Created by Vijay Darkonde on 20/12/17.
//  Copyright © 2017 Inceptive Consulting Pvt. Ltd. All rights reserved.
//

#import "ImageCommentCell.h"
#import "UITableView+FDTemplateLayoutCell.h"

@implementation ImageCommentCell
@synthesize userNameLbl,backView,profileImageView,activityLbl,commentTxtLbl,likeLbl,likeImgView,chooseActionBtn;

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.contentView.bounds = [UIScreen mainScreen].bounds;
    profileImageView.layer.cornerRadius = profileImageView.frame.size.height/2;
    profileImageView.layer.masksToBounds = YES;
    backView.layer.cornerRadius = 3;
    
}

- (void)setEntity:(ImageCommentEntity *)entity
{
    
    _entity = entity;
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"yyyy-MM-dd'T'HH:mm:ss.SSS";
    int CDate =  entity.TimeDiffsec;
    int Cmin =  entity.TimeDiffmin;
    int Chours =  entity.TimeDiffhr;
    NSString *time = @"";
//    if (CDate >= 60 ) {
//        int minutes = (CDate / 60);
//        if (minutes >= 60) {
//            int hours = CDate / 3600;
//            if (hours >= 24) {
//                int days = hours / 24;
//                time = [NSString stringWithFormat:@"%d day(s)", days];
//            }
//            else{
//                time = [NSString stringWithFormat:@"%d hour(s)", hours];
//            }
//
//        }
//        else{
//            time = [NSString stringWithFormat:@"%d minute(s)", minutes];
//        }
//    }
//    else{
//        time = [NSString stringWithFormat:@"%d second(s)", CDate];
//    }
    if (Chours > 0){
        if ((Chours / 24) <= 0){
            time = [NSString stringWithFormat:@"%d hour(s)", entity.TimeDiffhr];
        }
        else if ((Chours) / (24 * 30) <= 0){
            time = [NSString stringWithFormat:@"%d day(s)", (Chours / 24)];
        }
        else{
            time = [NSString stringWithFormat:@"%d month(s)", (Chours) / (24 * 30)];
        }
    }
    else if (Cmin > 0){
        time = [NSString stringWithFormat:@"%d minute(s)", entity.TimeDiffmin];
    }
    else{
        time = [NSString stringWithFormat:@"%d second(s)", entity.TimeDiffsec];
    }
    
    NSString *newstr = [entity.CommentUsername stringByAppendingString:@"  "];
    NSTextAttachment *attachment = [[NSTextAttachment alloc] init];
    attachment.image = [UIImage imageNamed:@"Vendor"];
    attachment.bounds = CGRectMake(0, -5, 50,20);
    NSAttributedString *attachmentString = [NSAttributedString attributedStringWithAttachment:attachment];
    NSMutableAttributedString *myString= [[NSMutableAttributedString alloc] initWithString:newstr];
    [myString appendAttributedString:attachmentString];
    
    
    if (entity.Role == 3) {
        
        [self.userNameLbl setAttributedText: myString];
    }
    else{
        self.userNameLbl.text = entity.CommentUsername;
    }
    
    self.commentTxtLbl.text = entity.CommentText;
    self.activityLbl.text = [entity.CommentUsername stringByAppendingString: [@"- Last activity " stringByAppendingString:[time stringByAppendingString:@" ago."]]];
    
    
    if(entity.CommentUserImage.length > 0){
        NSString *str=[[NSBundle mainBundle] pathForResource:@"loading_spinner" ofType:@"gif"];
        
        NSData *fileData = [NSData dataWithContentsOfFile:str];
        [self.profileImageView sd_setImageWithURL:[NSURL URLWithString:entity.CommentUserImage] placeholderImage:[UIImage sd_animatedGIFWithData:fileData]];
    }
    else{
        self.profileImageView.image = [UIImage imageNamed:@"user.ing"];
    }
    if (entity.Isliked == YES) {
        self.likeImgView.image = [UIImage imageNamed:@"like"];
        self.likeLbl.textColor = [UIColor blackColor];
    }
    else{
        self.likeImgView.image = [UIImage imageNamed:@"dlike"];
        self.likeLbl.textColor = [UIColor colorWithRed:70.0/255.0 green:70.0/255.0 blue:70.0/255.0 alpha:1.0];
    }
    
    self.likeLbl.text = [NSString stringWithFormat:@"%d", entity.CommentLikeCount];
    
    if ([entity.ReplyList count] != 0) {
        if ([entity.ReplyList count] == 1) {
            self.replyLbl.text = @"1 Reply";
        }
        else{
            int a = [entity.ReplyList count];
            NSString *replycount = [NSString stringWithFormat:@"%d", a];
            self.replyLbl.text = [replycount stringByAppendingString:@" Replies"];
        }
    }
    else {
        self.replyLbl.text = @"";
    }
    
}

- (CGSize)sizeThatFits:(CGSize)size {
    CGFloat totalHeight = 0;
    totalHeight += [self.userNameLbl sizeThatFits:size].height;
    totalHeight += [self.activityLbl sizeThatFits:size].height;
    totalHeight += [self.commentTxtLbl sizeThatFits:size].height;
    totalHeight += [self.activityLbl sizeThatFits:size].height;
    totalHeight += [self.activityLbl sizeThatFits:size].height;
    totalHeight += [self.activityLbl sizeThatFits:size].height;
    totalHeight += [self.activityLbl sizeThatFits:size].height;
    totalHeight += [self.replyLbl sizeThatFits:size].height;
    totalHeight += 40; // margins
    
    return CGSizeMake(size.width, totalHeight);
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end
