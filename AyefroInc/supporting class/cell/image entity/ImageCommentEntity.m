//
//  ImageCommentEntity.m
//  AyefroInc
//
//  Created by Vijay Darkonde on 20/12/17.
//  Copyright © 2017 Inceptive Consulting Pvt. Ltd. All rights reserved.
//

#import "ImageCommentEntity.h"

@implementation ImageCommentEntity
- (instancetype)initWithDictionary:(NSDictionary *)dictionary
{
    self = super.init;
    if (self) {
        
        _identifier = [self uniqueIdentifier];
        _CommentUsername= dictionary[@"CommentUsername"];
        _CommentText = dictionary[@"CommentText"];
        _CommentSendate = dictionary[@"CommentSendate"];
        _CommentUserImage = dictionary[@"CommentUserImage"];
        _TimeDiffhr = [[dictionary objectForKey:@"TimeDiffhr"] intValue];
        _Role = [[dictionary objectForKey:@"Role"] intValue];
        _TimeDiffmin = [[dictionary objectForKey:@"TimeDiffmin"] intValue];
        _TimeDiffsec = [[dictionary objectForKey:@"TimeDiffsec"] intValue];
        _CommentSendUser = [dictionary objectForKey:@"CommentSendUser"];
        _CommentLikeCount = [[dictionary objectForKey:@"CommentLikeCount"] intValue];
        _CommentId = [[dictionary objectForKey:@"CommentId"] intValue];
        _Isliked = [[dictionary objectForKey:@"Isliked"] boolValue];
        _ReplyList = dictionary[@"ReplyList"];
    }
    return self;
}

- (NSString *)uniqueIdentifier
{
    static NSInteger counter = 0;
    return [NSString stringWithFormat:@"unique-id-%@", @(counter++)];
}
@end
